<?php

namespace App\Services;

use Illuminate\Support\Facades\File;
use App\Models\Slider;

class SliderService{
    private function fetch(array $request): object{
        return (object)[
            'self' => [
                'title' => $request['title'],
                'desc' => $request['desc'],
                'url' => $request['url']
            ],
            'image' => $request['image'][0] ?? null,
        ];
    }
    public function store(array $request): Slider{
        $request = $this->fetch($request);
        $slider = Slider::create($request->self);
        $this->saveImage($request->image,$slider);

        return $slider;
    }

    public function update(array $request, Slider $slider): Slider{
        $request = $this->fetch($request);
        $slider->update($request->self);
        $this->updateImage($request->image,$slider);

        return $slider;
    }

    public function delete(Slider $slider): bool{
        if ($this->deleteImage($slider)) {
            $slider->update(['image' => null]);

            return $slider->delete();
        }
    }

    private function moveImage($image = null,Slider $slider){
        if ($image) {
            $new_name = time().'_'.str_replace(' ', '_', $image);

            $path = public_path('images/slider/');
            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (File::move(storage_path('tmp/').$image, $path.$new_name)) {
                return $slider->update([
                    'image' => $new_name
                ]);
            }
        }
    }
    private function saveImage($image = null,Slider $slider){
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                return $this->moveImage($image,$slider);
            }
        }
    }
    private function updateImage($image = null,Slider $slider){
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                if ($this->deleteImage($slider)) {
                    $this->saveImage($image,$slider);
                }
            }
        } else {
            if ($this->deleteImage($slider)) {
                $slider->update(['image' => null]);
            }
        }
    }
    private function deleteImage(Slider $slider){
        if ($slider->image) {
            if (File::exists(public_path('images/slider/').$slider->image)) {
                File::delete(public_path('images/slider/').$slider->image);
            }
        }

        return true;
    }
}
