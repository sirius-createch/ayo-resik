<?php

namespace App\Console\Commands;

use App\Models\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class NotificationBroadcast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:broadcast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start broadcast unbroadcasted notification.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = Notification::with('broadcasted_to_user')->whereNull('broadcasted_at')->whereHas('broadcasted_to_user', function ($query) {
            $query->whereNotNull('device_token');
        })->get();

        foreach ($notifications as $notification) {
            $SERVER_API_KEY = config('app.firebase.server_key');

            $data = [
                'registration_ids' => [$notification->broadcasted_to_user->device_token],
                'data' => [
                    'title' => $notification->title,
                    'options' => [
                        'icon' => asset('images/assets/favicons/icon-128x128.png'),
                        'body' => $notification->detail,
                        'data' => [
                            'url' => route('app.notification.goto', ['notification' => $notification->id]),
                        ],
                    ],
                ],
            ];

            $headers = [
                'Authorization' => 'key='.$SERVER_API_KEY,
                'Accept' => 'application/json',
            ];

            $response = Http::withHeaders($headers)->post('https://fcm.googleapis.com/fcm/send', $data);

            if (json_decode($response->body(), true)['success']) {
                $notification->update(['broadcasted_at' => now()]);
            }
        }
    }
}
