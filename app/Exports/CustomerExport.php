<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CustomerExport implements FromCollection, ShouldAutoSize, WithStyles
{
    private int $totalRows;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $customers = DB::table('users')
            ->select(['users.id', 'users.name', 'users.email', 'users.phone', DB::raw('partner.name as partner'), 'users.address'])
            ->join(DB::raw('users as partner'), 'users.partner_id', 'partner.id')
            ->where('users.role', 'customer')
            ->whereNull('users.deleted_at')
            ->whereNull('partner.deleted_at')
            ->get()
            ->map(function($customer, $index) {
                return [
                    'no' => $index + 1,
                    'name' => $customer->name,
                    'email' => $customer->email,
                    'phone' => "62" . $customer->phone,
                    'partner' => $customer->partner,
                    'address' => $customer->address,
                ];
            });

        $this->totalRows = count($customers) + 3;

        return collect(
            [
                ['DAFTAR CUSTOMER'],
                [''],
                ['No', 'Nama', 'Alamat Email', 'Nomor HP', 'Partner', 'Alamat']
            ]
        )->merge($customers->toArray());
    }

    public function styles(Worksheet $sheet)
    {
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];

        $tableHeaderStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => Color::COLOR_WHITE],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FF343A40',
                ],
            ],
        ];

        $sheet->mergeCells('A1:F1');
        $sheet->getStyle('A1')->applyFromArray($headerStyle);
        $sheet->getRowDimension(1)->setRowHeight(21, 'pt');

        $sheet->getStyle('A3:F3')->applyFromArray($tableHeaderStyle);
        $sheet->getRowDimension(3)->setRowHeight(21, 'pt');

        foreach (range(4, $this->totalRows) as $row) {
            $sheet->getRowDimension($row)->setRowHeight(21, 'pt');
        }

        $tableBorder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ]
            ]
        ];

        $sheet->getStyle("A4:F{$this->totalRows}")->applyFromArray($tableBorder);
    }
}
