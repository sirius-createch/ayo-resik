<?php

namespace App\Rules;

use App\Helpers\Sirius;
use Illuminate\Contracts\Validation\Rule;

class MultipliesOf implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(private int $value)
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value % $this->value === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':Attribute harus kelipatan dari ' . Sirius::toRupiah($this->value) . '.';
    }
}
