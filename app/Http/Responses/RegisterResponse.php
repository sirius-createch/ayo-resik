<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\RegisterResponse as RegisterResponseContract;

class RegisterResponse implements RegisterResponseContract
{
    /**
     * Create an HTTP response after successfully logged in.
     *
     * @param  Request  $request
     * @return Response
     */
    public function toResponse($request)
    {
        return redirect()->route('app.customer.address.confirm');
    }
}
