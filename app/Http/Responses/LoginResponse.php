<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * Create an HTTP response after successfully logged in.
     *
     * @param  Request  $request
     * @return Response
     */
    public function toResponse($request)
    {
        $role = auth()->user()->role;
        if ($role == 'admin') {
            return redirect()->route('admin.home');
        } elseif ($role == 'partner') {
            return redirect()->route('app.partner.home');
        }

        return redirect()->route('app.customer.home');
    }
}
