<?php

namespace App\Http\Responses;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Fortify\Contracts\LogoutResponse as LogoutResponseContract;
use Symfony\Component\HttpFoundation\Response;

class LogoutResponse implements LogoutResponseContract
{
    /**
     * Create an HTTP response after successfully logged out.
     *
     * @param  Request  $request
     * @return Response
     */
    public function toResponse($request)
    {
        if (Str::contains(url()->previous(), 'admin')) {
            return redirect()->route('admin.login');
        }

        return redirect()->route('app.login');
    }
}
