<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsPartnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->role == 'admin') {
            return redirect(route('admin.home'));
        }

        abort_if((auth()->user()->role !== 'partner' && auth()->user()->role !== 'admin'), 404);

        return $next($request);
    }
}
