<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MustFillAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->address == '' && auth()->user()->latitude == 0 && auth()->user()->longitude == 0) {
            return redirect(route('app.customer.address.confirm'));
        }

        return $next($request);
    }
}
