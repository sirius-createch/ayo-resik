<?php

namespace App\Http\Requests;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AppUpdateGeneralProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'avatar' => ['nullable', 'array'],
            'name' => ['required'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore(auth()->user())->where(fn ($query) => $query->whereNull('deleted_at'))],
            'phone' => ['required', Rule::unique('users', 'phone')->ignore(auth()->user())->where(fn ($query) => $query->whereNull('deleted_at'))],
            'address' => ['required'],
            'longitude' => ['required', 'numeric'],
            'latitude' => ['required', 'numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nama',
            'email' => 'Alamat email',
            'phone' => 'Nomor HP',
            'address' => 'Alamat',
            'latitude' => 'Garis lintang',
            'lotitude' => 'Garis bujur',
        ];
    }

    public function messages()
    {
        return [
            'latitude.numeric' => 'Garis lintang tidak sesuai.',
            'longitude.numeric' => 'Garis bujur tidak sesuai.',
        ];
    }

    public function prepareForValidation()
    {
        $phone = Str::replace('-', '', Str::replace('.', '', Str::replace('+62', '', $this->phone)));
        $phone = $phone[0] == 0 ? Str::replaceFirst('0', '', $phone) : $phone;

        $this->merge([
            'phone' => $phone,
        ]);
    }
}
