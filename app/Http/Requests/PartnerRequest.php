<?php

namespace App\Http\Requests;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'avatar' => ['nullable', 'array'],
            'name' => ['required'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->when(!empty($this->partner), fn ($item) => $item->ignore($this->partner))->where(fn ($query) => $query->whereNull('deleted_at'))],
            'phone' => ['required', Rule::unique('users', 'phone')->when(!empty($this->partner), fn ($item) => $item->ignore($this->partner))->where(fn ($query) => $query->whereNull('deleted_at'))],
            'address' => ['required'],
            'schedule' => ['required', 'array'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nama',
            'email' => 'Alamat email',
            'phone' => 'Nomor HP',
            'address' => 'Alamat',
            'schedule' => 'Jadwal pengambilan',
        ];
    }

    public function prepareForValidation()
    {
        $phone = Str::replace('-', '', Str::replace('.', '', Str::replace('+62', '', $this->phone)));
        $phone = $phone[0] == 0 ? Str::replaceFirst('0', '', $phone) : $phone;

        $this->merge([
            'phone' => $phone,
        ]);
    }
}
