<?php

namespace App\Http\Requests;

use App\Rules\MultipliesOf;
use Illuminate\Foundation\Http\FormRequest;

class AppStoreWithdrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nominal' => ['required', new MultipliesOf(50000)],
            'selected_nominal' => ['nullable'],
        ];
    }

    public function attributes()
    {
        return [
            'selected_nominal' => 'Nominal',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'nominal' => str($this->nominal)->remove('.')->toInteger(),
        ]);
    }
}
