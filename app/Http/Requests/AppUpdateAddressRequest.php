<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppUpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'address' => ['required'],
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'address' => 'Alamat',
            'latitude' => 'Garis lintang',
            'lotitude' => 'Garis bujur',
        ];
    }

    public function messages()
    {
        return [
            'latitude.numeric' => 'Garis lintang tidak sesuai.',
            'longitude.numeric' => 'Garis bujur tidak sesuai.',
        ];
    }
}
