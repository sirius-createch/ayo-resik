<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Http\Requests\LoginRequest as LaravelLoginRequest;

class LoginRequest extends LaravelLoginRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules[Fortify::username()] = ['required', 'string'];

        if (Route::is('admin.login')) {
            $rules['password'] = ['required', 'string'];
        }

        return $rules;
    }
}
