<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'title' => 'nullable|max:255',
            'url' => 'nullable|url',
            'desc' => 'nullable',
        ];
    }

    public function attributes(){
        return [
            'title' => 'Judul',
            'desc' => 'Keterangan',
            'image' => 'Gambar',
            'url' => 'URL',
        ];
    }
}
