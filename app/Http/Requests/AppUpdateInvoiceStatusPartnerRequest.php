<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppUpdateInvoiceStatusPartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules['status'] = ['required'];

        if ($this->status == 'confirmed') {
            $rules['day_taken'] = ['required'];
        } elseif ($this->status == 'verified') {
            $rules['products'] = ['required', 'array'];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'day_taken' => 'Jadwal ambil',
            'products' => 'Daftar produk',
        ];
    }
}
