<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => ['required'],
            'value' => ['required'],
        ];

        if ($this->name == 'email') {
            $rules['value'][1] = 'email';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'value' => 'Isian',
        ];
    }
}
