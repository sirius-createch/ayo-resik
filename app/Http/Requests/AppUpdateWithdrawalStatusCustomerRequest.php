<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppUpdateWithdrawalStatusCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'status' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'status.required' => 'Status yang diinginkan tidak ditemukan.',
        ];
    }
}
