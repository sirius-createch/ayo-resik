<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppStoreInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'products' => ['required', 'json'],
            'address' => ['required'],
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
            'payment' => ['required', 'numeric'],
            'note' => ['nullable'],
        ];
    }

    public function attributes()
    {
        return [
            'products' => 'Produk',
            'address' => 'Alamat',
            'latitude' => 'Garis lintang',
            'lotitude' => 'Garis bujur',
            'payment' => 'Metode pembayaran',
            'note' => 'Catatan',
        ];
    }

    public function messages()
    {
        return [
            'latitude.numeric' => 'Garis lintang tidak sesuai.',
            'longitude.numeric' => 'Garis bujur tidak sesuai.',
            'payment.numeric' => 'Harap pilih metode pembayaran yang tersedia.',
        ];
    }
}
