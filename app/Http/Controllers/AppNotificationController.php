<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\View\View;

class AppNotificationController extends Controller
{
    /**
     * Retrieve all unreaded notifications, and pagination for all readed notifications
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $notifications = auth()->user()->notifikasi()
                                                ->when(request()->search, function ($query) {
                                                    $query->where(function ($query) {
                                                        $query->where('title', 'like', '%'.request()->search.'%')
                                                            ->orWhere('detail', 'like', '%'.request()->search.'%');
                                                    });
                                                })
                                                ->when(request()->order, function ($query) {
                                                    $query->orderBy(request()->order, request()->sort);
                                                });

                $notifications = $notifications->simplePaginate(request()->limit ?? 10)->withQueryString();

                if (! empty($notifications)) {
                    $pagination = $notifications->toArray();
                    $notifications = $notifications->map(function ($notification) {
                        return [
                            'icon' => $notification->icon,
                            'title' => Str::limit($notification->title, 37),
                            'detail' => Str::limit($notification->detail, 47),
                            'link' => $notification->readed_at ? $notification->link : route('app.notification.goto', ['notification' => $notification->id]),
                            'color' => $notification->readed_at ? 'gradient-gray' : 'gradient-red',
                        ];
                    });
                    $pagination['data'] = $notifications;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                $data = $this->apiResponse(false, $e->getMessage());
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                $data = $this->apiResponse(false, $e->getMessage());
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.notification');
    }

    /**
     * Set notification as read and redirect user to the correct page
     *
     * @param  Notification  $notification
     * @return RedirectResponse
     */
    public function goto(Notification $notification): RedirectResponse
    {
        $notification->update(['readed_at' => now()]);

        return redirect($notification->link);
    }
}
