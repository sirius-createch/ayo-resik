<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class InvoiceController extends Controller
{
    /**
     * Handling route for list of invoice and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $invoices = DB::table('invoices')
                            ->select(['invoices.id', 'invoices.code','invoices.service_charge', DB::raw('partner.name as partner'), DB::raw('customer.name as customer'), 'invoices.total_bill', 'invoices.total_weight', 'invoices.current_status'])
                            ->join(DB::raw('users as partner'), 'invoices.partner_id', 'partner.id')
                            ->join(DB::raw('users as customer'), 'invoices.customer_id', 'customer.id')
                            ->whereNull('invoices.deleted_at')
                            ->whereNull('partner.deleted_at')
                            ->whereNull('customer.deleted_at');

            return datatables($invoices)
                ->filter(function ($query) {
                    $query->when(request()->has('search'), function ($query) {
                        $query->where(function ($query) {
                            $status = Invoice::statusSearchFilter(request('search.value'));
                            $query->where('invoices.code', 'like', '%'.request('search.value').'%')
                                  ->orWhere('partner.name', 'like', '%'.request('search.value').'%')
                                  ->orWhere('customer.name', 'like', '%'.request('search.value').'%')
                                  ->orWhere('invoices.total_bill', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', request('search.value')).'%')
                                  ->orWhere(DB::raw('ROUND(invoices.total_weight, 1)'), 'like', Str::replace([' ', 'k', 'g'], '', Str::replace(',', '.', '%'.request('search.value').'%')))
                                  ->when(count($status) > 0, function ($query) use ($status) {
                                      $query->orWhereIn('invoices.current_status', $status);
                                  });
                        });
                    });
                })
                ->addIndexColumn()
                ->editColumn('total_bill', fn ($invoice) => Sirius::toRupiah($invoice->total_bill))
                ->editColumn('service_charge', fn ($invoice) => Sirius::toRupiah($invoice->service_charge))
                ->editColumn('total_weight', fn ($invoice) => Invoice::attrTotalWeightConverted($invoice))
                ->editColumn('current_status', fn ($invoice) => Invoice::attrCurrentStatusBadgeConverted($invoice))
                ->editColumn('action', 'datatables._action_transaction_show')
                ->rawColumns(['current_status', 'action'])
                ->toJson();
        }

        return view('admin.services.transactions');
    }

    /**
     * Show invoice's detail
     *
     * @param  Invoice  $transaksi
     * @return JsonResponse
     */
    public function show(Invoice $transaksi)
    {
        try {
            $invoice = $transaksi;
            $products = [];
            foreach ($invoice->products as $product) {
                $products[] = [
                    'name' => $product->name,
                    'weight' => Str::replace(',0', '', number_format($product->pivot->weight_real ?? $product->pivot->weight, 1, ',', '.')).' kg',
                    'total' => Sirius::toRupiah($product->pivot->price * ($product->pivot->weight_real ?? $product->pivot->weight)),
                    'price' => Sirius::toRupiah($product->pivot->price),
                ];
            }

            $statuses = [];
            foreach (($invoice->statuses->sortByDesc('id'))->values()->all() as $status) {
                $statuses[] = [
                    'name' => $status->name_converted,
                    'icon' => InvoiceStatus::mappingNameIcon($status->name),
                    'color' => InvoiceStatus::mappingNameColor($status->name),
                    'time' => Sirius::toLongDateTime($status->created_at, separator: ' - '),
                ];
            }

            return response()->json([
                'code' => $invoice->code,
                'datetime' => Sirius::toLongDateDayTime($invoice->created_at, separator: ' - '),
                'partner' => $invoice->partner->name,
                'customer' => $invoice->customer->name,
                'total_bill' => Sirius::toRupiah($invoice->total_bill),
                'service_charge' => Sirius::toRupiah($invoice->service_charge),
                'total_weight' => Str::replace(',0', '', number_format($invoice->total_weight, 1, ',', '.')).' kg',
                'day_taken' => $invoice->day_taken_converted,
                'current_status' => $invoice->current_status_badge_converted,
                'address' => $invoice->address,
                'latitude' => $invoice->latitude,
                'longitude' => $invoice->longitude,
                'products' => $products,
                'statuses' => $statuses,
                'note' => $invoice->note,
            ]);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        } catch (Error $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        }
    }
}
