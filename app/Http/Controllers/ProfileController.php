<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdatePasswordRequest;
use App\Http\Requests\UpdateGeneralRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class ProfileController extends Controller
{
    public function index(): View
    {
        return view('admin.profile');
    }

    public function updateGeneral(UpdateGeneralRequest $request): JsonResponse
    {
        try {
            abort_if(! in_array($request->name, ['name', 'email']), 404, 'Pengaturan tidak ditemukan');

            auth()->user()->update([
                $request->name => $request->value,
            ]);
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    public function updatePassword(ProfileUpdatePasswordRequest $request): JsonResponse
    {
        try {
            auth()->user()->update([
                'password' => Hash::make($request->password),
            ]);
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    public function updateAvatar(UpdateGeneralRequest $request): JsonResponse
    {
        try {
            auth()->user()->simpanAvatar($request->value[0]);
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }
}
