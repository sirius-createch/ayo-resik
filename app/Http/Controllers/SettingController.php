<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingUpdateTotalRequest;
use App\Http\Requests\UpdateGeneralRequest;
use App\Models\Setting;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Illuminate\View\View;

class SettingController extends Controller
{
    public function index(): View
    {
        $settings = Setting::pluck('value', 'name');

        return view('admin.compro.settings', compact('settings'));
    }

    public function updateGeneral(UpdateGeneralRequest $request): JsonResponse
    {
        try {
            abort_if(! in_array($request->name, ['address', 'email', 'phone']), 404, 'Pengaturan tidak ditemukan');

            if ($request->name == 'phone') {
                $phone = Str::replace('-', '', Str::replace('.', '', Str::replace('+62', '', Str::replaceFirst('0', '', $request->value))));
                $phone = $phone[0] == 0 ? Str::replaceFirst('0', '', $phone) : $phone;
                $request->value = $phone;
            }
            Setting::findOrFail($request->name)->update(['value' => $request->value]);
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    public function updateTotal(SettingUpdateTotalRequest $request): JsonResponse
    {
        try {
            $request->value = floatval(Str::replace(',', '.', Str::remove('.', $request->value)));
            Setting::findOrFail($request->name.'-value')->update(['value' => $request->value]);
            Setting::findOrFail($request->name.'-toggle')->update(['value' => $request->toggle]);
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }
}
