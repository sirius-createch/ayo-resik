<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ReportController extends Controller
{
    /**
     * Handle route for expense report and datatables
     *
     * @param  Request  $request
     * @return View|JsonResponse
     */
    public function expense(Request $request): View|JsonResponse
    {
        if (request()->ajax()) {
            if ($request->has('forDT')) {
                $data = Invoice::report($request->all())->union(Withdrawal::report($request->all()));

                return datatables($data)
                    ->filter(fn ($query) => null)
                    ->addIndexColumn()
                    ->editColumn('amount', fn ($d) => Sirius::toRupiah($d->amount))
                    ->editColumn('time', fn ($d) => date('d/m/y H:i', strtotime($d->time)))
                    ->toJson();
            } else {
                $transactions = Invoice::whereCurrentStatus('done')->wherePaymentMethodId(1)->whereDate('created_at', '>=', $request['startDate'])->whereDate('created_at', '<=', $request['endDate'])->sum('total_bill');
                $withdrawals = Withdrawal::whereCurrentStatus('done')->whereDate('created_at', '>=', $request['startDate'])->whereDate('created_at', '<=', $request['endDate'])->sum('amount');
                $result['total_transaction'] = $transactions;
                $result['total_withdrawal'] = $withdrawals;
                $result['total_amount'] = $transactions + $withdrawals;

                return response()->json($result, 200);
            }
        }
        $customers = User::whereRole('customer')->pluck('name', 'id')->toArray();
        $partners = User::whereRole('partner')->pluck('name', 'id')->toArray();

        return view('admin.reports.expenses', compact('customers', 'partners'));
    }

    /**
     * Handle route for product report and datatables
     *
     * @param  Request  $request
     * @return View|JsonResponse
     */
    public function product(Request $request): View|JsonResponse
    {
        if (request()->ajax()) {
            if ($request->has('forDT')) {
                $data = Product::report($request->all());

                return datatables($data)
                    ->filter(fn ($query) => null)
                    ->editColumn('price', fn ($d) => Sirius::toRupiah($d->price))
                    ->editColumn('weight', fn ($d) => Str::remove(',0', number_format($d->weight, 1, ',', '.')).' kg')
                    ->addIndexColumn()
                    ->toJson();
            } else {
                $product = DB::table('invoice_product')->select([DB::raw('SUM(price * weight_real) as price'), DB::raw('SUM(weight_real) as weight')])->whereDate('created_at', '>=', $request['startDate'])->whereDate('created_at', '<=', $request['endDate'])->first();
                $result['total_weight'] = Str::remove(',0', number_format($product->weight ?? 0, 1, ',', '.')).' kg';
                $result['total_price'] = Sirius::toRupiah($product->price ?? 0);

                return response()->json($result, 200);
            }
        }

        return view('admin.reports.products');
    }
}
