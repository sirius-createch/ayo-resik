<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Models\Withdrawal;
use App\Models\WithdrawalStatus;
use Error;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WithdrawalController extends Controller
{
    /**
     * Handling route for list of withdrawal and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $withdrawals = DB::table('withdrawals')
                             ->select(['withdrawals.id', 'withdrawals.code', DB::raw('partner.name as partner'), DB::raw('customer.name as customer'), 'withdrawals.amount', 'withdrawals.current_status', 'withdrawals.note'])
                             ->join(DB::raw('users as partner'), 'withdrawals.partner_id', 'partner.id')
                             ->join(DB::raw('users as customer'), 'withdrawals.customer_id', 'customer.id')
                             ->whereNull('withdrawals.deleted_at')
                             ->whereNull('partner.deleted_at')
                             ->whereNull('customer.deleted_at');

            return datatables($withdrawals)
                ->filter(function ($query) {
                    $status = Withdrawal::statusSearchFilter(request('search.value'));
                    $query->where('withdrawals.code', 'like', '%'.request('search.value').'%')
                          ->orWhere('partner.name', 'like', '%'.request('search.value').'%')
                          ->orWhere('customer.name', 'like', '%'.request('search.value').'%')
                          ->orWhere('withdrawals.amount', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', request('search.value')).'%')
                          ->when(count($status) > 0, function ($query) use ($status) {
                              $query->orWhereIn('withdrawals.current_status', $status);
                          });
                })
                ->addIndexColumn()
                ->editColumn('amount', fn ($withdrawal) => Sirius::toRupiah($withdrawal->amount))
                ->editColumn('current_status', fn ($withdrawal) => Withdrawal::attrCurrentStatusBadgeConverted($withdrawal))
                ->editColumn('action', 'datatables._action_transaction_show')
                ->rawColumns(['current_status', 'action'])
                ->toJson();
        }

        return view('admin.services.withdrawals');
    }

    /**
     * Show withdrawal's detail
     *
     * @param  Withdrawal  $penarikan_saldo
     * @return JsonResponse
     */
    public function show(Withdrawal $penarikan_saldo): JsonResponse
    {
        try {
            $withdrawal = $penarikan_saldo;

            $statuses = [];
            foreach (($withdrawal->statuses->sortByDesc('id'))->values()->all() as $status) {
                $statuses[] = [
                    'name' => $status->name_converted,
                    'icon' => WithdrawalStatus::mappingNameIcon($status->name),
                    'color' => WithdrawalStatus::mappingNameColor($status->name),
                    'time' => Sirius::toLongDateTime($status->created_at, separator: ' - '),
                ];
            }

            return response()->json([
                'code' => $withdrawal->code,
                'datetime' => Sirius::toLongDateDayTime($withdrawal->created_at, separator: ' - '),
                'partner' => $withdrawal->partner->name,
                'customer' => $withdrawal->customer->name,
                'amount' => $withdrawal->amount,
                'current_status' => $withdrawal->current_status_badge_converted,
                'statuses' => $statuses,
                'note' => $withdrawal->note,
            ]);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        } catch (Error $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        }
    }
}
