<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Handle the incoming request
     * Search products
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $selected = json_decode($request->selected);
            $products = Product::where('name', 'like', "%$request->search%")
                                ->whereNotIn('id', $selected)
                                ->get()
                                ->map(function ($product) {
                                    return [
                                        'id' => $product->id,
                                        'name' => $product->name,
                                        'image' => $product->image_link,
                                        'price' => number_format($product->base_price, thousands_separator: '.'),
                                    ];
                                })
                                ->filter()
                                ->keyBy('id');
            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', $products->toArray());
        } catch (Exception $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
