<?php

namespace App\Http\Controllers\Partner;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppConfirmLinkingPartnerCustomerRequest;
use App\Models\Notification;
use App\Models\User;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * Handle route to partner's list of customer page and json for listing
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $profile = User::where('role', 'customer')
                                ->where('partner_id', auth()->id())
                                ->when(request()->search, function ($query) {
                                    $query->where(function ($query) {
                                        $query->where('name', 'like', '%'.request()->search.'%')
                                            ->orWhere('address', 'like', '%'.request()->search.'%');
                                    });
                                })
                                ->when(request()->order, function ($query) {
                                    $query->orderBy(request()->order, request()->sort);
                                });

                $profile = $profile->simplePaginate(request()->limit ?? 10)->withQueryString();

                if (! empty($profile)) {
                    $pagination = $profile->toArray();
                    $profile = $profile->map(function ($customer) {
                        return [
                            'id' => $customer->id,
                            'name' => $customer->name,
                            'avatar' => $customer->avatar_link,
                            'address' => $customer->address,
                            'isConfirmed' => ($customer->partner_confirm_at) ? '' : "<div class='chip chip-s bg-yellow-dark mt-2 mb-0 mx-0'><i class='fa-solid fa-triangle-exclamation color-white bg-yellow-light'></i><span class='color-white'>Perlu Konfirmasi</span></div>",
                        ];
                    });
                    $pagination['data'] = $profile;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.partner.customers.index');
    }

    /**
     * Show customer's information detail
     *
     * @param  User  $customer
     * @return View
     */
    public function show(User $customer): View
    {
        $isConfirmed = $customer->partner_confirm_at ? true : false;

        return view('app.partner.customers.show', compact('customer', 'isConfirmed'));
    }

    /**
     * Accept / reject customer's linking request
     *
     * @param  AppConfirmLinkingPartnerCustomerRequest  $request
     * @param  User  $customer
     * @return JsonResponse
     */
    public function update(AppConfirmLinkingPartnerCustomerRequest $request, User $customer): JsonResponse
    {
        DB::beginTransaction();
        try {
            if ($request->confirm) {
                $customer->update(['partner_confirm_at' => now()]);

                Notification::create([
                    'title' => 'Hubungan Partner Diterima!',
                    'detail' => auth()->user()->name.' telah menerima permintaan hubungan Anda! Mulai setor sampah Anda sekarang!',
                    'link' => route('app.customer.orders.create'),
                    'broadcasted_to' => $customer->id,
                ]);
            } else {
                $customer->update(['partner_id' => null]);

                Notification::create([
                    'title' => 'Hubungan Partner Ditolak!',
                    'detail' => auth()->user()->name.' menolak permintaan hubungan Anda! Cari partner lain untuk mulai menyetor sampah Anda!',
                    'link' => route('app.customer.partner.index'),
                    'broadcasted_to' => $customer->id,
                ]);
            }

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', ['now' => Sirius::toLongDateDayTime(date('Y-m-d H:i:s'), separator: ' - ')]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
