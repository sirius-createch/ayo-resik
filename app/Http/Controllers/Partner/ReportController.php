<?php

namespace App\Http\Controllers\Partner;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Product;
use Carbon\Carbon;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ReportController extends Controller
{
    /**
     * Show partner's report page
     *
     * @return View
     */
    public function index(): View
    {
        $startDate = Invoice::first()->created_at ?? now();
        $endDate = now();

        $years = $months = $days = [];

        for ($year = $startDate->year; $year <= $endDate->year; $year++) {
            $years[] = $year;
            if ($startDate->year == $endDate->year) {
                for ($month = $startDate->month; $month <= $endDate->month; $month++) {
                    $months[$year.Str::padLeft($month, 2, '0')] = Sirius::longMonth($month).' '.$year;
                }
            } elseif ($year == $startDate->year) {
                for ($month = $startDate->month; $month <= 12; $month++) {
                    $months[$year.'-'.Str::padLeft($month, 2, '0')] = Sirius::longMonth($month).' '.$year;
                }
            } elseif ($year == $endDate->year) {
                for ($month = 1; $month <= $endDate->month; $month++) {
                    $months[$year.'-'.Str::padLeft($month, 2, '0')] = Sirius::longMonth($month).' '.$year;
                }
            } else {
                for ($month = 1; $month <= 12; $month++) {
                    $months[$year.'-'.Str::padLeft($month, 2, '0')] = Sirius::longMonth($month).' '.$year;
                }
            }
        }

        $diff = ((int)$startDate->diff($endDate->addDay())->format('%a'));
        for ($i = ($diff <= 7 ? $diff : 7); $i > 0; $i--) {
            $day = now()->subDays($i - 1);
            $days[$day->format('Ymd')] = $day->format('d') . ' ' . Sirius::longMonth($day->format('m')) . ' - ' . Sirius::longDay($day->format('N'));
        }

        $days = collect($days)->sortDesc();
        $months = collect($months)->sortKeysDesc();
        $years = collect($years)->sortDesc();

        return view('app.partner.reports', compact('years', 'months', 'days'));
    }

    /**
     * Calculte nominal trash dump / transaction (rupiah)
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function nominal(Request $request): JsonResponse
    {
        try {
            if ($request->period == 'yearly') {
                $startDate = date('Y-m-d', strtotime($request->value.'-01-01'));
                $endDate = date('Y-m-d', strtotime($request->value.'-12-31'));
            } else if($request->period == 'monthly'){
                $startDate = date('Y-m-01', strtotime($request->value));
                $endDate = date('Y-m-t', strtotime($request->value));
            } else {
                $startDate = date('Y-m-d 00:00:00', strtotime($request->value));
                $endDate = date('Y-m-d 23:59:59', strtotime($request->value));
            }

            $params = [
                'period' => $request->period,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ];

            $report = Product::expenseReport($params);
            $data = $this->apiResponse(true, 'OK', $report);
            $http_code = 200;
        } catch (Exception $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    /**
     * Calculate weight total of trash dump / transaction (kg)
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function weights(Request $request): JsonResponse
    {
        try {
            if ($request->period == 'yearly') {
                $startDate = date('Y-m-d', strtotime($request->value.'-01-01'));
                $endDate = date('Y-m-d', strtotime($request->value.'-12-31'));
            } else if($request->period == 'monthly'){
                $startDate = date('Y-m-01', strtotime($request->value));
                $endDate = date('Y-m-t', strtotime($request->value));
            } else {
                $startDate = date('Y-m-d 00:00:00', strtotime($request->value));
                $endDate = date('Y-m-d 23:59:59', strtotime($request->value));
            }

            $params = [
                'period' => $request->period,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ];

            $report = Product::weightReport($params);
            $data = $this->apiResponse(true, 'OK', $report);
            $http_code = 200;
        } catch (Exception $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
