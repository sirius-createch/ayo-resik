<?php

namespace App\Http\Controllers\Partner;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppUpdateWithdrawalStatusPartnerRequest;
use App\Models\Notification;
use App\Models\Withdrawal;
use App\Models\WithdrawalStatus;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class WithdrawalController extends Controller
{
    /**
     * Handle route to partner's list of withdrawal page and json for listing
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $withdrawals = Withdrawal::index(request())
                                    ->with(['customer:id,name,address'])
                                    ->simplePaginate(request()->limit ?? 10)
                                    ->withQueryString();

                if (! empty($withdrawals)) {
                    $pagination = $withdrawals->toArray();
                    $withdrawals = $withdrawals->map(function ($withdrawal) {
                        return [
                            'code' => $withdrawal->code,
                            'date' => Sirius::toLongDate($withdrawal->created_at),
                            'statusColor' => Withdrawal::mappingCurrentStatusColor($withdrawal->current_status),
                            'status' => $withdrawal->current_status_converted,
                            'customer' => $withdrawal->customer->name,
                            'address' => $withdrawal->customer->address,
                            'amount' => Sirius::toRupiah($withdrawal->amount ?? 0),
                        ];
                    });
                    $pagination['data'] = $withdrawals;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.partner.withdrawals.index');
    }

    /**
     * Show withdrawal request's detail
     *
     * @param  Withdrawal  $withdrawal
     * @return View
     */
    public function show(Withdrawal $withdrawal): View
    {
        abort_if($withdrawal->partner_id != auth()->id(), 404);

        if ($withdrawal->current_status == 'requested') {
            $withdrawal->update(['current_status' => 'seen']);
            WithdrawalStatus::create([
                'withdrawal_id' => $withdrawal->id,
                'name' => 'seen',
            ]);
        }

        $withdrawal = $withdrawal->load(['customer', 'statuses']);
        $statuses = $withdrawal->statuses->map(function ($status) {
            return [
                'id' => $status->id,
                'icon' => Withdrawal::mappingCurrentStatusIcon($status->name, 'fontawesome'),
                'color' => Withdrawal::mappingCurrentStatusColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];
        })->sortByDesc('id');

        return view('app.partner.withdrawals.show', compact('withdrawal', 'statuses'));
    }

    /**
     * Update withdrawal current status by partner.
     *
     * @param  AppUpdateWithdrawalStatusPartnerRequest  $request
     * @param  Withdrawal  $withdrawal
     * @return View|JsonResponse
     */
    public function update(AppUpdateWithdrawalStatusPartnerRequest $request, Withdrawal $withdrawal): View|JsonResponse
    {
        DB::beginTransaction();
        try {
            $status = $withdrawal->ubahStatus($request->all());

            if ($request->status == 'denied') {
                $notification = 'Permintaan penarikan saldo ditolak!';
            } elseif ($request->status == 'confirmed') {
                $notification = 'Permintaan penarikan saldo telah dikonfirmasi!';
            } else {
                $notification = 'Uang Anda telah dikirim!';
            }

            Notification::create([
                'title' => $notification,
                'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                'link' => route('app.customer.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                'broadcasted_to' => $withdrawal->customer_id,
            ]);

            $status = [
                'icon' => WithdrawalStatus::mappingNameIcon($status->name, 'fontawesome'),
                'color' => WithdrawalStatus::mappingNameColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', ['status' => $status]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
