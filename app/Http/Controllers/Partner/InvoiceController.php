<?php

namespace App\Http\Controllers\Partner;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppUpdateInvoiceStatusPartnerRequest;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Notification;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class InvoiceController extends Controller
{
    /**
     * Handle route to partner's list of invoice page and json for listing
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $invoices = Invoice::index(request())
                                    ->with(['products', 'customer:id,name'])
                                    ->simplePaginate(request()->limit ?? 10)
                                    ->withQueryString();

                if (! empty($invoices)) {
                    $pagination = $invoices->toArray();
                    $invoices = $invoices->map(function ($invoice) {
                        return [
                            'code' => $invoice->code,
                            'date' => Sirius::toLongDate($invoice->created_at),
                            'statusColor' => Invoice::mappingCurrentStatusColor($invoice->current_status),
                            'status' => $invoice->current_status_converted,
                            'products' => $invoice->products->count(),
                            'customer' => $invoice->customer->name,
                            'address' => $invoice->address,
                            'day' => $invoice->day_taken ? Sirius::longDay(date('w', strtotime($invoice->day_taken))) : '-',
                        ];
                    });
                    $pagination['data'] = $invoices;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.partner.orders.index');
    }

    /**
     * Show order / trash dump request's detail
     *
     * @param  Invoice  $invoice
     * @return View
     */
    public function show(Invoice $invoice): View
    {
        abort_if($invoice->partner_id != auth()->id(), 404);

        if ($invoice->current_status == 'requested') {
            $invoice->update(['current_status' => 'seen']);
            InvoiceStatus::create([
                'invoice_id' => $invoice->id,
                'name' => 'seen',
            ]);
        }

        $invoice = $invoice->load(['customer', 'statuses']);
        $statuses = $invoice->statuses->map(function ($status) {
            return [
                'id' => $status->id,
                'icon' => Invoice::mappingCurrentStatusIcon($status->name, 'fontawesome'),
                'color' => Invoice::mappingCurrentStatusColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];
        })->sortByDesc('id');

        return view('app.partner.orders.show', compact('invoice', 'statuses'));
    }

    /**
     * Update order / trash dump request current status by partner.
     *
     * @param  AppUpdateInvoiceStatusPartnerRequest  $request
     * @param  Invoice  $invoice
     * @return View|JsonResponse
     */
    public function update(AppUpdateInvoiceStatusPartnerRequest $request, Invoice $invoice): View|JsonResponse
    {
        DB::beginTransaction();
        try {
            $status = $invoice->ubahStatus($request->all());
            $additional = [];

            if ($request->status == 'denied') {
                $notification = 'Permintaan pengambilan sampah ditolak';
            } elseif ($request->status == 'confirmed') {
                $notification = 'Permintaan pengambilan sampah telah dikonfirmasi';
                $invoice->update(['day_taken' => $request->day_taken]);
                $additional = [
                    'dayTaken' => $invoice->day_taken_converted,
                ];
            } elseif ($request->status == 'verified') {
                $notification = 'Sampah Anda telah diverifikasi!';
                $invoice->konfirmasi($request->all());

                $newPrice = $newWeight = 0;
                $additional['price'] = $additional['weight'] = [];
                foreach ($invoice->products as $product) {
                    $newPrice += ($product->pivot->weight_real * $product->pivot->price);
                    $newWeight += $product->pivot->weight_real;
                    $additional['price'][$product->id] = Sirius::toRupiah(($product->pivot->weight_real * $product->pivot->price));
                    $additional['weight'][$product->id] = Str::replace(',0', '', number_format($product->pivot->weight_real, 1, ',', '.')).' kg';
                }
                $additional['total_price'] = Sirius::toRupiah($newPrice);
                $serviceCharge = $newPrice * config('app.service_rate');
                $newPrice -= $serviceCharge;
                $additional['service_charge'] = Sirius::toRupiah($serviceCharge);
                $additional['total_bill'] = Sirius::toRupiah($newPrice);
                $invoice->update([
                    'total_bill' => $newPrice,
                    'total_weight' => $newWeight,
                    'service_charge' => $serviceCharge
                ]);
            } else {
                $notification = 'Pengambilan sampah telah selesai';
                if ($invoice->payment_method_id == 2) { // Masuk Saldo
                    $invoice->customer->update([
                        'balance' => $invoice->customer->balance + $invoice->total_bill,
                    ]);
                }
            }

            Notification::create([
                'title' => $notification,
                'detail' => 'Nomor Transaksi: '.$invoice->code,
                'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                'link' => route('app.customer.orders.show', ['invoice' => $invoice->code]),
                'broadcasted_to' => $invoice->customer_id,
            ]);

            $status = [
                'icon' => InvoiceStatus::mappingNameIcon($status->name, 'fontawesome'),
                'color' => InvoiceStatus::mappingNameColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', ['status' => $status, 'additional' => $additional]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
