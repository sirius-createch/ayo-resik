<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Handling route for list of product and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $products = Product::query();

            return datatables($products)
                ->addIndexColumn()
                ->editColumn('image', 'datatables._image')
                ->editColumn('base_price', function ($product) {
                    return Sirius::toRupiah($product->base_price);
                })
                ->editColumn('action', 'datatables._actions')
                ->rawColumns(['image', 'action'])
                ->toJson();
        }

        return view('admin.master.products');
    }

    /**
     * Insert new product
     *
     * @param  ProductRequest  $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            Product::buat($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Show product's detail
     *
     * @param  Product  $produk
     * @return JsonResponse
     */
    public function show(Product $produk): JsonResponse
    {
        try {
            return response()->json([
                'image' => $produk->image,
                'image_link' => $produk->image_link,
                'image_size' => $produk->image_size,
                'name' => $produk->name,
                'base_price' => $produk->base_price,
            ]);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), $e->getCode());
        } catch (Error $e) {
            return  response()->json($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update product's detail
     *
     * @param  ProductRequest  $request
     * @param  Product  $produk
     * @return JsonResponse
     */
    public function update(ProductRequest $request, Product $produk): JsonResponse
    {
        try {
            DB::beginTransaction();
            $produk->ubah($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Remove product from the list
     *
     * @param  Product  $produk
     * @return JsonResponse
     */
    public function destroy(Product $produk): JsonResponse
    {
        try {
            DB::beginTransaction();
            $produk->hapus();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }
}
