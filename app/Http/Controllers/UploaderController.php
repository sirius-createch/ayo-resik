<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploaderRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;

class UploaderController extends Controller
{
    /**
     * Handle Dropzone request
     *
     * @param  UploaderRequest  $request
     * @return JsonResponse|Response
     */
    public function dropzone(UploaderRequest $request): JsonResponse|Response
    {
        $path = storage_path('tmp');
        if (! file_exists($path)) {
            mkdir($path, 0777, true);
        }
        if ($request->has('is_removing')) {
            if (File::exists(storage_path('tmp/').$request->file_name)) {
                File::delete(storage_path('tmp/').$request->file_name);
            }

            return response()->noContent();
        } else {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $file->move($path, $name);

            return response()->json([
                'name' => $name,
                'original_name' => $file->getClientOriginalName(),
            ]);
        }
    }

    /**
     * Handle TinyMCE request
     *
     * @param  UploaderRequest  $request
     * @return JsonResponse
     */
    public function tinymce(UploaderRequest $request): JsonResponse
    {
        $path = public_path('images/content/');
        if (! file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');
        $name = time().'.'.$file->getClientOriginalExtension();
        $file->move($path, $name);

        return response()->json([
            'location' => asset('images/content').'/'.$name,
        ]);
    }
}
