<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Http\Requests\AppUpdateAddressRequest;
use App\Http\Requests\AppUpdateGeneralProfileRequest;
use App\Http\Requests\AppUpdateSettingsProfileRequest;
use App\Http\Requests\AppUpdatePasswordRequest;
use App\Models\User;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Hash;

class AppProfileController extends Controller
{
    /**
     * Handle route to user's profile page
     *
     * @return View
     */
    public function index(): View
    {
        return view('app.profil.index');
    }

    /**
     * Show the form for editing user's profile.
     *
     * @return View
     */
    public function edit(): View
    {
        return view('app.profil.edit');
    }

    /**
     * Update user's profile.
     *
     * @param  AppUpdateGeneralProfileRequest  $request
     * @return JsonResponse
     */
    public function update(AppUpdateGeneralProfileRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            auth()->user()->ubah($request->all());
            $data = $this->apiResponse(true, 'Ubah data berhasil');
            $http_code = 200;
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollback();
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    /**
     * Show the form for editing user's settings.
     *
     * @return View
     */
    public function setting(): View
    {
        return view('app.partner.setting');
    }

    /**
     * Update user's setting.
     *
     * @param  AppUpdateSettingsProfileRequest  $request
     * @return JsonResponse
     */
    public function set(AppUpdateSettingsProfileRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            auth()->user()->schedule->update([
                'sunday' => $request->schedule['sunday'],
                'monday' => $request->schedule['monday'],
                'tuesday' => $request->schedule['tuesday'],
                'wednesday' => $request->schedule['wednesday'],
                'thursday' => $request->schedule['thursday'],
                'friday' => $request->schedule['friday'],
                'saturday' => $request->schedule['saturday'],
            ]);

            $products = [];

            foreach ($request->products as $id => $price) {
                $products[$id] = ['price' => Str::replace('.', '', $price)];
            }

            auth()->user()->products()->sync($products);

            $data = $this->apiResponse(true, 'OK');
            $http_code = 200;
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollback();
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    public function confirmAddress(): View
    {
        return view('app.customer.address');
    }

    public function updateAddress(AppUpdateAddressRequest $request): RedirectResponse
    {
        auth()->user()->update([
            'address' => $request->address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
        ]);

        return redirect()->route('app.customer.partner.index')->with('success', 'Pendaftaran berhasil!');
    }

    public function editPassword(): View
    {
        return view('app.profil.edit-password');
    }

    public function updatePassword(AppUpdatePasswordRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            auth()->user()->update([
                'password' => Hash::make($request->password)
            ]);
            $data = $this->apiResponse(true, 'Ubah data berhasil');
            $http_code = 200;
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }
        return  response()->json($data, $http_code);
    }
}
