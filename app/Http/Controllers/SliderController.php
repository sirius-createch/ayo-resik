<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Services\SliderService;
use App\Http\Requests\SliderRequest;
use Illuminate\Support\Facades\DB;
use Exception;
use Error;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return datatables(Slider::query())
                ->addIndexColumn()
                ->editColumn('image', 'datatables._image')
                ->editColumn('title', fn($slider) => $slider->title ?? '-')
                ->editColumn('url', fn($slider) => $slider->url ?? '#')
                ->editColumn('desc', fn($slider) => $slider->desc ?? '-')
                ->editColumn('action', 'datatables._actions')
                ->rawColumns(['image', 'action'])
                ->toJson();
        }

        return view('admin.compro.sliders');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SliderRequest  $request
     * @param  \App\Services\SliderService  $service
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request, SliderService $service)
    {
        try {
            DB::beginTransaction();
            $service->store($request->validated());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        try {
            return response()->json($slider->append('image_link','image_size')->toArray());
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), $e->getCode());
        } catch (Error $e) {
            return  response()->json($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SliderRequest  $request
     * @param  \App\Models\Slider  $slider
     * @param  \App\Services\SliderService  $service
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, Slider $slider, SliderService $service)
    {
        try {
            DB::beginTransaction();
            $service->update($request->validated(),$slider);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider, SliderService $service)
    {
        try {
            DB::beginTransaction();
            $service->delete($slider);
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }
}
