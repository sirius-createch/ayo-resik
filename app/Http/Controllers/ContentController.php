<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContentRequest;
use App\Models\Content;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $contents = DB::table('contents')
                            ->select(['contents.id', 'contents.slug', 'contents.title', 'contents.banner', DB::raw("IFNULL(contents.published_at, '-') as published_at"), 'author.name as author'])
                            ->join(DB::raw('users as author'), 'contents.author_id', 'author.id')
                            ->whereNull('contents.deleted_at')
                            ->whereNull('author.deleted_at');

            return datatables($contents)
                ->filter(function ($query) {
                    if (request()->has('search')) {
                        $query->where(function ($query) {
                            $query->Where('contents.title', 'like', '%'.request('search.value').'%')
                                  ->orWhere('author.name', 'like', '%'.request('search.value').'%')
                                  ->when(request('search.value') == '-', function ($query) {
                                      $query->orWhereNull('published_at');
                                  })
                                  ->when(request('search.value') != '-', function ($query) {
                                      $query->orWhere(DB::raw("DATE_FORMAT(published_at, '%d/%m/%Y')"), 'like', '%'.request('search.value').'%');
                                  });
                        });
                    }
                })
                ->addIndexColumn()
                ->editColumn('published_at', function ($content) {
                    return $content->published_at != '-' ? date('d/m/Y', strtotime($content->published_at)) : $content->published_at;
                })
                ->editColumn('banner', function ($content) {
                    $link = Content::attrBannerLink($content);

                    return "
                        <a href='$link' data-fancybox style='cursor: zoom-in'>
                            <img src='$link' alt='Banner $content->title' class='rounded pull-up' style='object-fit: cover; width:25px; height: 25px;'>
                        </a>
                    ";
                })
                ->editColumn('action', 'datatables._actions_content')
                ->rawColumns(['banner', 'action'])
                ->toJson();
        }

        return view('admin.compro.contents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.compro.contents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContentRequest $request)
    {
        Content::buat($request->all());

        return redirect()->route('admin.compro.contents.index')->with('success', 'Simpan konten berhasil');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Content  $konten
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $konten)
    {
        $content = $konten;

        return view('admin.compro.contents.edit', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Content  $konten
     * @return \Illuminate\Http\Response
     */
    public function update(ContentRequest $request, Content $konten)
    {
        $konten->ubah($request->all());

        return redirect()->route('admin.compro.contents.index')->with('success', 'Ubah konten berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Content  $konten
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $konten)
    {
        $konten->hapus();

        return redirect()->route('admin.compro.contents.index')->with('success', 'Hapus konten berhasil');
    }
}
