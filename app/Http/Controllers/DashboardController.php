<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Content;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Slider;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Handle route to company profile welcome page
     *
     * @return View
     */
    public function compro(): View
    {
        $contents = Content::whereNotNull('published_at')->where('published_at', '<=', now())->orderByDesc('published_at')->limit(4)->get();
        $articles = Article::whereNotNull('published_at')->where('published_at', '<=', now())->orderByDesc('published_at')->limit(4)->get();
        $sliders = Slider::all();

        return view('welcome', compact('contents', 'articles', 'sliders'));
    }

    /**
     * Handle route to admin dashboard page
     *
     * @return View
     */
    public function admin(): View
    {
        $partners = User::whereRole('partner')->count();
        $customers = User::whereRole('customer')->count();
        $products = Product::count();
        $nominal = Invoice::whereCurrentStatus('done')->sum('total_bill');
        $weight = Invoice::whereCurrentStatus('done')->sum('total_weight');

        return view('admin.index', compact('partners', 'customers', 'products', 'nominal', 'weight'));
    }

    /**
     * Handle route to either partner or customer page
     *
     * @return RedirectResponse
     */
    public function app(): RedirectResponse
    {
        if (auth()->guest()) {
            return redirect(route('app.login'));
        }

        if (auth()->user()->role == 'partner') {
            return redirect(route('app.partner.home'));
        }

        return redirect(route('app.customer.home'));
    }

    /**
     * Handle route to partner page
     *
     * @return View
     */
    public function partner(): View
    {
        $invoice = Invoice::where('partner_id', auth()->id())
                            ->where('current_status', 'done')
                            ->select([DB::raw('SUM(total_bill) nominal'), DB::raw('SUM(total_weight) weight')])
                            ->first();

        $nominal = $invoice->nominal ?? 0;
        $weight = $invoice->weight ?? 0;

        $invoices = Invoice::with(['customer:id,name', 'products:id'])
                            ->select(['id', 'code', 'created_at', 'current_status', 'address', 'customer_id', 'address'])
                            ->where('partner_id', auth()->id())
                            ->whereNotIn('current_status', ['denied', 'canceled', 'done'])
                            ->orderByDesc('code')
                            ->limit(2)
                            ->get();

        $withdrawals = Withdrawal::with(['customer:id,name'])
                                    ->select(['id', 'code', 'created_at', 'current_status', 'amount', 'customer_id'])
                                    ->where('partner_id', auth()->id())
                                    ->whereNotIn('current_status', ['denied', 'canceled', 'done'])
                                    ->orderByDesc('code')
                                    ->limit(2)
                                    ->get();

        return view('app.partner.index', compact('nominal', 'weight', 'invoices', 'withdrawals'));
    }

    /**
     * Handle route to customer page
     *
     * @return View
     */
    public function customer(): View
    {
        $contents = Content::where('published_at', '<=', now())->orderByDesc('published_at')->limit(5)->get();
        $articles = Article::where('published_at', '<=', now())->orderByDesc('published_at')->limit(5)->get();

        return view('app.customer.index', compact('contents', 'articles'));
    }
}
