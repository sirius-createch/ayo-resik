<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartnerRequest;
use App\Models\User;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class PartnerController extends Controller
{
    /**
     * Handling route for list of partner and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $partners = User::withCount(['customers'])->where('role', 'partner');

            return datatables($partners)
                ->filter(function ($query) {
                    if (request()->has('search')) {
                        $query->where(function ($query) {
                            $query->where('phone', 'like', '%'.Str::replaceFirst('0', '', Str::replace('-', '', request('search.value'))).'%')
                                  ->orWhere('name', 'like', '%'.request('search.value').'%')
                                  ->orWhere('email', 'like', '%'.request('search.value').'%');
                        });
                    }
                })
                ->addIndexColumn()
                ->editColumn('avatar', 'datatables._avatar')
                ->editColumn('phone', function ($partner) {
                    return $partner->phone_converted;
                })
                ->editColumn('action', 'datatables._actions')
                ->rawColumns(['avatar', 'action'])
                ->toJson();
        }

        return view('admin.master.partners');
    }

    /**
     * Insert new partner
     *
     * @param  PartnerRequest  $request
     * @return JsonResponse
     */
    public function store(PartnerRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            User::buat($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Show partner's detail
     *
     * @param  User  $partner
     * @return JsonResponse
     */
    public function show(User $partner): JsonResponse
    {
        try {
            if ($partner->role == 'partner') {
                $text_schedule = "<span class='badge bg-label-danger'>Belum Diatur</span>";

                if ($partner->schedule->count() > 0) {
                    $text_schedule = [];
                    if ($partner->schedule->monday) {
                        $text_schedule[] = 'Senin';
                    }
                    if ($partner->schedule->tuesday) {
                        $text_schedule[] = 'Selasa';
                    }
                    if ($partner->schedule->wednesday) {
                        $text_schedule[] = 'Rabu';
                    }
                    if ($partner->schedule->thursday) {
                        $text_schedule[] = 'Kamis';
                    }
                    if ($partner->schedule->friday) {
                        $text_schedule[] = 'Jumat';
                    }
                    if ($partner->schedule->saturday) {
                        $text_schedule[] = 'Sabtu';
                    }
                    if ($partner->schedule->sunday) {
                        $text_schedule[] = 'Minggu';
                    }
                    $text_schedule = implode(', ', $text_schedule);
                }

                return response()->json([
                    'avatar' => $partner->avatar,
                    'avatar_link' => $partner->avatar_link,
                    'name' => $partner->name,
                    'email' => $partner->email,
                    'phone' => $partner->phone ?? '',
                    'phone_converted' => $partner->phone_converted ?? '',
                    'address' => $partner->address,
                    'latitude' => $partner->latitude,
                    'longitude' => $partner->longitude,
                    'customers' => ($partner->customers->map(fn ($customer) => ['name' => $customer->name, 'address' => $customer->address])->sortBy('name'))->values()->all(),
                    'total_customer' => $partner->customers->count(),
                    'schedules' => $partner->schedule,
                    'text_schedule' => $text_schedule,
                ]);
            }

            throw new Exception('partner not found', 404);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        } catch (Error $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        }
    }

    /**
     * Update partner's detail
     *
     * @param  PartnerRequest  $request
     * @param  User  $partner
     * @return JsonResponse
     */
    public function update(PartnerRequest $request, User $partner): JsonResponse
    {
        try {
            DB::beginTransaction();
            $partner->ubah($request->all(), true);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Remove partner from the list
     *
     * @param  User  $partner
     * @return JsonResponse
     */
    public function destroy(User $partner): JsonResponse
    {
        try {
            DB::beginTransaction();
            $partner->hapus();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Search partner for select2
     *
     * @param  Request  $request
     * @return array
     */
    public function search(Request $request): array
    {
        $partners = User::select(['name', 'id'])
                        ->whereRole('partner')
                        ->where('name', 'like', '%'.$request->search.'%')
                        ->get()
                        ->map(function ($partner) {
                            return [
                                'id' => $partner->id,
                                'text' => $partner->name,
                            ];
                        })
                        ->toArray();

        return $partners;
    }
}
