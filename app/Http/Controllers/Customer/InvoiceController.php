<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppStoreInvoiceRequest;
use App\Http\Requests\AppUpdateInvoiceStatusCustomerRequest;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Notification;
use App\Models\PaymentMethod;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class InvoiceController extends Controller
{
    /**
     * Handle route to customer's list of invoice page and json for listing
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $invoices = Invoice::index(request())
                                    ->with(['products'])
                                    ->simplePaginate(request()->limit ?? 10)
                                    ->withQueryString();

                if (! empty($invoices)) {
                    $pagination = $invoices->toArray();
                    $invoices = $invoices->map(function ($invoice) {
                        return [
                            'code' => $invoice->code,
                            'date' => Sirius::toLongDate($invoice->created_at),
                            'statusColor' => Invoice::mappingCurrentStatusColor($invoice->current_status),
                            'status' => $invoice->current_status_converted,
                            'bill' => Sirius::toRupiah($invoice->total_bill),
                            'weight' => number_format($invoice->total_weight, 1, ',', '.').' kg',
                            'billText' => in_array($invoice->current_status, ['verified', 'done']) ? 'Pendapatan' : 'Perkiraan Pendapatan',
                            'products' => $invoice->products->count(),
                            'day' => $invoice->day_taken ? Sirius::longDay(date('w', strtotime($invoice->day_taken))) : '-',
                        ];
                    });
                    $pagination['data'] = $invoices;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.customer.orders.index');
    }

    /**
     * Show the form for send a new order / trash dump request
     *
     * @return View
     */
    public function create(): View
    {
        $payments = PaymentMethod::latest('id')->pluck('name', 'id');

        return view('app.customer.orders.create', compact('payments'));
    }

    /**
     * Create new order / trash dump request
     *
     * @param  AppStoreInvoiceRequest
     * @return JsonResponse
     */
    public function store(AppStoreInvoiceRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $invoice = Invoice::buat($request->all());

            InvoiceStatus::create([
                'invoice_id' => $invoice->id,
                'name' => 'requested',
            ]);

            $invoice = Invoice::findOrFail($invoice->id);

            Notification::create([
                'title' => 'Ada permintaan pengambilan sampah!',
                'detail' => 'Estimasi Tagihan: '.Sirius::toRupiah($invoice->total_bill),
                'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                'link' => route('app.partner.orders.show', ['invoice' => $invoice->code]),
                'broadcasted_to' => auth()->user()->partner_id,
            ]);

            $data = $this->apiResponse(true, 'OK', $invoice->code);
            $http_code = 200;
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollback();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    /**
     * Show order / trash dump request's detail
     *
     * @param  Invoice  $invoice
     * @return View
     */
    public function show(Invoice $invoice): View
    {
        abort_if($invoice->customer_id != auth()->id(), 404);
        $invoice = $invoice->load(['partner', 'statuses']);
        $statuses = $invoice->statuses->map(function ($status) {
            return [
                'id' => $status->id,
                'icon' => Invoice::mappingCurrentStatusIcon($status->name, 'fontawesome'),
                'color' => Invoice::mappingCurrentStatusColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];
        })->sortByDesc('id');

        return view('app.customer.orders.show', compact('invoice', 'statuses'));
    }

    /**
     * Update order / trash dump current status by customer.
     *
     * @param  AppUpdateInvoiceStatusCustomerRequest  $request
     * @param  Invoice  $invoice
     * @return JsonResponse
     */
    public function update(AppUpdateInvoiceStatusCustomerRequest $request, Invoice $invoice): JsonResponse
    {
        DB::beginTransaction();
        try {
            $invoice->update(['current_status' => $request->status]);

            $status = InvoiceStatus::create([
                'invoice_id' => $invoice->id,
                'name' => $request->status,
            ]);

            Notification::create([
                'title' => 'Permintaan pengambilan sampah dibatalkan',
                'detail' => 'Nomor Transaksi: '.$invoice->code,
                'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                'link' => route('app.partner.orders.show', ['invoice' => $invoice->code]),
                'broadcasted_to' => auth()->user()->partner_id,
            ]);

            $status = [
                'icon' => InvoiceStatus::mappingNameIcon($status->name, 'fontawesome'),
                'color' => InvoiceStatus::mappingNameColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', $status);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
