<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Handle route to customer partner's list of products
     *
     * @return View
     */
    public function index(): View
    {
        return view('app.customer.products');
    }

    /**
     * Searching products to dump
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $partnerProducts = auth()->user()->partner->products;
            $selected = collect(json_decode($request->selected))->pluck('id')->toArray();
            $products = Product::where('name', 'like', "%$request->search%")
                                ->whereNotIn('id', $selected)
                                ->get()
                                ->map(function ($product) use ($partnerProducts) {
                                    $accepted = $partnerProducts->pluck('id')->toArray();
                                    if (in_array($product->id, $accepted)) {
                                        return [
                                            'id' => $product->id,
                                            'name' => $product->name,
                                            'price' => $partnerProducts->where('id', $product->id)->first()->pivot->price,
                                            'image' => $product->image_link,
                                            'weight' => 0,
                                        ];
                                    }
                                })
                                ->filter()
                                ->keyBy('id');
            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', $products->toArray());
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
