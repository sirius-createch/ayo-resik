<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\Sirius;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppStoreWithdrawalRequest;
use App\Http\Requests\AppUpdateWithdrawalStatusCustomerRequest;
use App\Models\Notification;
use App\Models\Withdrawal;
use App\Models\WithdrawalStatus;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class WithdrawalController extends Controller
{
    /**
     * Handle route to customer's list of withdrawal page and json for listing
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            try {
                $withdrawals = Withdrawal::index(request())
                                            ->simplePaginate(request()->limit ?? 10)
                                            ->withQueryString();

                if (! empty($withdrawals)) {
                    $pagination = $withdrawals->toArray();
                    $withdrawals = $withdrawals->map(function ($withdrawal) {
                        return [
                            'code' => $withdrawal->code,
                            'date' => Sirius::toLongDate($withdrawal->created_at),
                            'statusColor' => Withdrawal::mappingCurrentStatusColor($withdrawal->current_status),
                            'status' => $withdrawal->current_status_converted,
                            'amount' => Sirius::toRupiah($withdrawal->amount),
                        ];
                    });
                    $pagination['data'] = $withdrawals;

                    $http_code = 200;
                    $data = $this->apiResponse(true, 'OK', $pagination);
                } else {
                    $http_code = 404;
                    $data = $this->apiResponse(false);
                }
            } catch (Exception $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            } catch (Error $e) {
                // $data = $this->apiResponse(false, $e->getMessage());
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
                $http_code = $e->getCode() == 422 ? 422 : 421;
            }

            return  response()->json($data, $http_code);
        }

        return view('app.customer.withdrawals.index');
    }

    /**
     * Show the form for send a new withdrawal request.
     *
     * @return View
     */
    public function create(): View
    {
        return view('app.customer.withdrawals.create');
    }

    /**
     * Send request for withdrawal to the partner
     *
     * @param  AppStoreWithdrawalRequest  $request
     * @return JsonResponse
     */
    public function store(AppStoreWithdrawalRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $withdrawal = Withdrawal::buat($request->all());
            auth()->user()->update(['balance' => DB::raw('balance - '.$withdrawal->amount)]);

            WithdrawalStatus::create([
                'withdrawal_id' => $withdrawal->id,
                'name' => 'requested',
            ]);

            $withdrawal = Withdrawal::findOrFail($withdrawal->id);

            Notification::create([
                'title' => 'Ada permintaan penarikan saldo!',
                'detail' => 'Nominal: '.Sirius::toRupiah($withdrawal->amount),
                'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                'broadcasted_to' => auth()->user()->partner_id,
            ]);

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', $withdrawal->code);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            if ($e->getCode() == 422) {
                $data = $this->apiResponse(false, $e->getMessage());
            } else {
                $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            }
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    /**
     * Show withdrawal request's detail
     *
     * @param  Withdrawal  $withdrawal
     * @return View
     */
    public function show(Withdrawal $withdrawal): View
    {
        abort_if($withdrawal->customer_id != auth()->id(), 404);
        $withdrawal = $withdrawal->load(['partner', 'statuses']);
        $statuses = $withdrawal->statuses->map(function ($status) {
            return [
                'id' => $status->id,
                'icon' => Withdrawal::mappingCurrentStatusIcon($status->name, 'fontawesome'),
                'color' => Withdrawal::mappingCurrentStatusColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];
        })->sortByDesc('id');

        return view('app.customer.withdrawals.show', compact('withdrawal', 'statuses'));
    }

    /**
     * Update withdrawal current status by customer.
     *
     * @param  AppUpdateWithdrawalStatusCustomerRequest  $request
     * @param  Withdrawal  $withdrawal
     * @return JsonResponse
     */
    public function update(AppUpdateWithdrawalStatusCustomerRequest $request, Withdrawal $withdrawal): JsonResponse
    {
        DB::beginTransaction();
        try {
            $withdrawal->update(['current_status' => $request->status]);

            $status = WithdrawalStatus::create([
                'withdrawal_id' => $withdrawal->id,
                'name' => $request->status,
            ]);

            if ($request->status == 'canceled') {
                $notification = 'Permintaan penarikan saldo dibatalkan!';
                auth()->user()->update(['balance' => (auth()->user()->balance + $withdrawal->amount)]);
            } elseif ($request->status == 'done') {
                $notification = 'Uang telah diterima oleh customer Anda!';
            } else {
                $notification = 'Customer belum menerima uangnya!';
            }

            Notification::create([
                'title' => $notification,
                'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                'broadcasted_to' => auth()->user()->partner_id,
            ]);

            $status = [
                'icon' => WithdrawalStatus::mappingNameIcon($status->name, 'fontawesome'),
                'color' => WithdrawalStatus::mappingNameColor($status->name),
                'name' => $status->name_converted,
                'datetime' => Sirius::toLongDateDayTime($status->created_at, separator: ' - '),
            ];

            $http_code = 200;
            $data = $this->apiResponse(true, 'OK', $status);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
