<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppChangePartnerRequest;
use App\Models\Invoice;
use App\Models\Notification;
use App\Models\User;
use App\Models\Withdrawal;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class PartnerController extends Controller
{
    /**
     * Handle route to customer's partner
     *
     * @return View
     */
    public function index(): View
    {
        $isConnected = auth()->user()->partner_confirm_at ? true : false;
        $partner = auth()->user()->partner;

        $schedules = $partner->schedules_converted ?? [];
        $schedules = implode(', ', $schedules);

        return view('app.customer.partner', compact('partner', 'isConnected', 'schedules'));
    }

    /**
     * Searching nearby partner
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function nearby(Request $request): JsonResponse
    {
        try {
            $result = auth()->user()
                            ->partner_terdekat($request)
                            ->when($request->search, function ($query) use ($request) {
                                $query->where(function ($query) use ($request) {
                                    $query->where('p.name', 'like', "%$request->search%")
                                        ->orWhere('p.address', 'like', "%$request->search%")
                                        ->orWhere('p.phone', 'like', '%'.Str::replaceFirst('0', '', Str::replace('-', '', $request->search)).'%');
                                });
                            })
                            ->simplePaginate($request->limit ?? 10)
                            ->withQueryString();

            $distances = $result->pluck('distance', 'id')->toArray();
            $result = $result->toArray();

            $order = explode('.', $request->order);
            $order = $order[count($order) - 1];
            $partners = User::with(['schedule', 'products'])
                            ->whereRole('partner')
                            ->find(array_keys($distances))
                            ->map(function ($partner) use ($distances) {
                                return [
                                    'id' => $partner->id,
                                    'name' => $partner->name,
                                    'address' => $partner->address,
                                    'distance' => round($distances[$partner->id]),
                                    'phone' => $partner->phone_converted,
                                    'schedules' => implode(', ', $partner->schedules_converted),
                                    'products' => implode(', ', $partner->products->pluck('name')->toArray()),
                                ];
                            })
                            ->when(($request->sort == 'asc'), function ($query) use ($order) {
                                return $query->sortBy($order);
                            })
                            ->when(($request->sort == 'desc'), function ($query) use ($order) {
                                return $query->sortByDesc($order);
                            })
                            ->values()->all();

            $result['data'] = $partners;

            $data = $this->apiResponse(true, 'OK', $result);
            $http_code = 200;
            if (empty($partners)) {
                $data = $this->apiResponse(false);
            }
        } catch (Exception $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    /**
     * Changing customer's partner
     *
     * @param  AppChangePartnerRequest  $request
     * @return JsonResponse
     */
    public function change(AppChangePartnerRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $invoices = Invoice::where('customer_id', auth()->id())->whereNotIn('current_status', ['denied', 'canceled', 'done'])->get();
            if (! $invoices) {
                DB::rollBack();
                $data = $this->apiResponse(false, 'Sedang ada transaksi sampah yang belum selesai!');

                return  response()->json($data, 403);
            }

            $withdrawals = Withdrawal::where('customer_id', auth()->id())->whereNotIn('current_status', ['denied', 'canceled', 'done'])->get();
            if (! $withdrawals) {
                DB::rollBack();
                $data = $this->apiResponse(false, 'Masih ada permintaan penarikan saldo Anda yang belum selesai!');

                return  response()->json($data, 403);
            }

            if (auth()->user()->ubahPartner($request->partner_id)) {
                Notification::create([
                    'title' => 'Customer Baru!',
                    'detail' => auth()->user()->name.' ingin Anda menjadi partnernya! Terima?',
                    'link' => route('app.partner.profile.customers.show', ['customer' => auth()->id()]),
                    'broadcasted_to' => $request->partner_id,
                ]);

                $partner = User::findOrFail($request->partner_id);

                $partner = [
                    'name' => $partner->name,
                    'address' => $partner->address,
                    'schedules' => implode(', ', $partner->schedules_converted),
                ];

                DB::commit();
                $data = $this->apiResponse(true, 'OK', $partner);
                $http_code = 200;
            } else {
                DB::rollBack();
                $data = $this->apiResponse(false);
                $http_code = 404;
            }
        } catch (Exception $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            DB::rollBack();
            // $data = $this->apiResponse(false, $e->getMessage());
            $data = $this->apiResponse(false, 'Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!');
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }
}
