<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class PaymentMethodController extends Controller
{
    /**
     * Handling route for list of payment method and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $payments = PaymentMethod::query();

            return datatables($payments)
                ->addIndexColumn()
                ->editColumn('action', '-')
                ->toJson();
        }

        return view('admin.master.payments');
    }
}
