<?php

namespace App\Http\Controllers\Compro;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::whereNotNull('published_at')
                            ->where('published_at', '<=', now())
                            ->orderByDesc('published_at')
                            ->when($request->search, function ($query) use ($request) {
                                $query->where('title', 'like', "%$request->search%")
                                      ->where('content', 'like', "%$request->search%");
                            })
                            ->paginate(9)
                            ->withQueryString();

        $data = [];
        foreach ($articles as $article) {
            $data[] = [
                'slug' => $article->slug,
                'title' => $article->title,
                'banner' => $article->banner_link,
                'content' => $article->content,
                'published_at' => $article->published_at,
            ];
        }

        $articles = $articles->toArray();
        $articles['data'] = $data;

        $recents = Article::whereNotNull('published_at')
                          ->where('published_at', '<=', now())
                          ->orderByDesc('published_at')
                          ->limit(5)
                          ->get();

        return view('compro.articles.index', compact('articles', 'recents'));
    }

    public function show(Article $artikel)
    {
        $article = $artikel;
        $recents = Article::whereNotNull('published_at')
                          ->where('published_at', '<=', now())
                          ->orderByDesc('published_at')
                          ->limit(5)
                          ->get();

        return view('compro.articles.show', compact('article', 'recents'));
    }
}
