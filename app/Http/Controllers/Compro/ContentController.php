<?php

namespace App\Http\Controllers\Compro;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        $contents = Content::whereNotNull('published_at')
                            ->where('published_at', '<=', now())
                            ->orderByDesc('published_at')
                            ->when($request->search, function ($query) use ($request) {
                                $query->where(function ($query) use ($request) {
                                    $query->where('title', 'like', "%$request->search%")
                                        ->orWhere('content', 'like', "%$request->search%");
                                });
                            })
                            ->paginate(6)
                            ->withQueryString();

        $data = [];
        foreach ($contents as $content) {
            $data[] = [
                'slug' => $content->slug,
                'title' => $content->title,
                'banner' => $content->banner_link,
                'published_at' => $content->published_at,
            ];
        }

        $contents = $contents->toArray();
        $contents['data'] = $data;

        $recents = Content::whereNotNull('published_at')
                          ->where('published_at', '<=', now())
                          ->orderByDesc('published_at')
                          ->limit(5)
                          ->get();

        return view('compro.contents.index', compact('contents', 'recents'));
    }

    public function show(Content $konten)
    {
        $content = $konten;
        $recents = Content::whereNotNull('published_at')
                          ->where('published_at', '<=', now())
                          ->orderByDesc('published_at')
                          ->limit(5)
                          ->get();

        return view('compro.contents.show', compact('content', 'recents'));
    }
}
