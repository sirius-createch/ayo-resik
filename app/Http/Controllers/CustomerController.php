<?php

namespace App\Http\Controllers;

use App\Exports\CustomerExport;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\CustomerUpdatePasswordRequest;
use App\Models\User;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerController extends Controller
{
    /**
     * Handling route for list of customer and datatables
     *
     * @return View|JsonResponse
     */
    public function index(): View|JsonResponse
    {
        if (request()->ajax()) {
            $customers = DB::table('users')
                            ->select(['users.id', 'users.avatar', 'users.name', 'users.email', 'users.phone', DB::raw('partner.name as partner')])
                            ->join(DB::raw('users as partner'), 'users.partner_id', 'partner.id')
                            ->where('users.role', 'customer')
                            ->whereNull('users.deleted_at')
                            ->whereNull('partner.deleted_at');

            return datatables($customers)
                ->filter(function ($query) {
                    if (request()->has('search')) {
                        $query->where(function ($query) {
                            $query->where('users.phone', 'like', '%'.Str::replaceFirst('0', '', Str::replace('-', '', request('search.value'))).'%')
                                  ->orWhere('users.name', 'like', '%'.request('search.value').'%')
                                  ->orWhere('users.email', 'like', '%'.request('search.value').'%')
                                  ->orWhere('partner.name', 'like', '%'.request('search.value').'%');
                        });
                    }
                })
                ->addIndexColumn()
                ->editColumn('avatar', function ($customer) {
                    $link = User::attrAvatarLink($customer);

                    return "
                        <a href='$link' data-fancybox style='cursor: zoom-in'>
                            <img src='$link' alt='Avatar $customer->name' class='rounded pull-up' style='object-fit: cover; width:25px; height: 25px;'>
                        </a>
                    ";
                })
                ->editColumn('phone', fn ($customer) => User::attrPhoneConverted($customer))
                ->editColumn('action', 'datatables._actions')
                ->rawColumns(['avatar', 'action'])
                ->toJson();
        }

        return view('admin.master.customers');
    }

    /**
     * Show customer's detail
     *
     * @param  User  $customer
     * @return JsonResponse
     */
    public function show(User $customer): JsonResponse
    {
        try {
            if ($customer->role == 'customer') {
                return response()->json([
                    'avatar' => $customer->avatar,
                    'avatar_link' => $customer->avatar_link,
                    'name' => $customer->name,
                    'email' => $customer->email,
                    'phone' => $customer->phone ?? '',
                    'phone_converted' => $customer->phone_converted ?? '',
                    'partner' => $customer->partner->name,
                    'address' => $customer->address,
                    'latitude' => $customer->latitude,
                    'longitude' => $customer->longitude,
                ]);
            }

            throw new Exception('customer not found', 404);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        } catch (Error $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        }
    }

    /**
     * Update customer's detail
     *
     * @param  CustomerRequest  $request
     * @param  User  $customer
     * @return JsonResponse
     */
    public function update(CustomerRequest $request, User $customer): JsonResponse
    {
        try {
            DB::beginTransaction();
            $customer->ubah($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Remove customer from the list
     *
     * @param  User  $customer
     * @return JsonResponse
     */
    public function destroy(User $customer): JsonResponse
    {
        try {
            DB::beginTransaction();
            $customer->hapus();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }

    /**
     * Search customer for select2
     *
     * @param  Request  $request
     * @return array
     */
    public function search(Request $request): array
    {
        $customers = User::select(['name', 'id'])
                        ->whereRole('customer')
                        ->where('name', 'like', '%'.$request->search.'%')
                        ->get()
                        ->map(function ($customer) {
                            return [
                                'id' => $customer->id,
                                'text' => $customer->name,
                            ];
                        })
                        ->toArray();

        return $customers;
    }

    public function download(): BinaryFileResponse
    {
        return Excel::download(new CustomerExport(), "Daftar Customer.xlsx");
    }

    public function generatePassword(): JsonResponse
    {
        try {
            return response()->json([
                'password' => str()->random(),
            ]);
        } catch (Exception $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        } catch (Error $e) {
            return  response()->json($e->getMessage(), ($e->getCode() != 0 ? $e->getCode() : 500));
        }
    }

    public function updatePassword(CustomerUpdatePasswordRequest $request, User $customer): JsonResponse
    {
        try {
            DB::beginTransaction();
            $customer->ubahPassword($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode() == 422 ? 422 : 421;
            $response = $this->error_msg($e);
        }

        return response()->json($response, $http_code);
    }
}
