<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Response for showing detail data
     *
     * @param  array  $data
     * @return array
     */
    public function show_detail_response(array $data): array
    {
        $msg = '';
        $success = false;
        if (isset($data['id'])) {
            $msg = 'OK';
            $success = true;
        } else {
            $msg = 'Data tidak ditemukan';
            $success = false;
        }

        return [
            'success' => $success,
            'message' => $msg,
            'data' => $data,
        ];
    }

    /**
     * Response for saving data
     *
     * @return array
     */
    public function save_response(): array
    {
        return $this->general_success_msg('c');
    }

    /**
     * Response for updating data
     *
     * @return array
     */
    public function update_response(): array
    {
        return $this->general_success_msg('u');
    }

    /**
     * Response for deleting request
     *
     * @return array
     */
    public function delete_response(): array
    {
        return $this->general_success_msg('d');
    }

    /**
     * Handling general success message
     *
     * @param  string  $action
     * @return array
     */
    public function general_success_msg(string $action): array
    {
        $msg = '';
        switch ($action) {
            case 'c': $msg = 'Tambah'; break;
            case 'u': $msg = 'Ubah'; break;
            case 'd': $msg = 'Hapus'; break;
            default: $msg = 'Tambah'; break;
        }

        return [
            'success' => true,
            'message' => "$msg data berhasil",
        ];
    }

    /**
     * Handling general error message
     *
     * @param  Exception  $e
     * @return array
     */
    public function error_msg(Exception $e): array
    {
        return [
            'success' => false,
            'message' => 'Terjadi kesalahan pada server. Segera hubungi pihak penyedia layanan anda!',
            'error' => $e->getCode().'-'.$e->getMessage(),
        ];
    }

    /**
     * Handling API response
     *
     * @param  bool  $success
     * @param  string  $msg
     * @param  mixed  $data
     * @return void
     */
    public function apiResponse(bool $success, string $msg = 'Data not found', mixed $data = null)
    {
        return [
            'success' => $success,
            'message' => $msg,
            'data' => $data,
        ];
    }
}
