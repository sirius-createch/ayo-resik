<?php

namespace App\Http\Controllers;

use App\Models\User;
use Error;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;

/**
 * Depedency that required to handle Forget Password by User
 */
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use Illuminate\Support\Facades\Password;
use Laravel\Fortify\Fortify;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;

class AuthController extends Controller
{
    /**
     * Handle route for login as an admin
     *
     * @return View|RedirectResponse
     */
    public function adminLoginView(): View|RedirectResponse
    {
        if (! auth()->guest()) {
            if (auth()->user()->role == 'admin') {
                return redirect(route('admin.home'));
            }

            return abort(403, 'Anda sedang masuk sebagai admin! Harap keluar dari akun Anda terlebih dahulu!');
        }

        return view('admin.auth.login');
    }

    /**
     * Handle route for app login
     *
     * @return View|RedirectResponse
     */
    public function appLoginView(): View|RedirectResponse
    {
        if (! auth()->guest()) {
            if (auth()->user()->role == 'partner') {
                return redirect(route('app.partner.home'));
            } elseif (auth()->user()->role == 'customer') {
                return redirect(route('app.customer.home'));
            }

            return abort(404);
        }

        return view('app.auth.login');
    }

    /**
     * Handle route for app registration
     *
     * @return View|RedirectResponse
     */
    public function appRegisterView(): View|RedirectResponse
    {
        if (! auth()->guest()) {
            if (auth()->user()->role == 'partner') {
                return redirect(route('app.partner.home'));
            } elseif (auth()->user()->role == 'customer') {
                return redirect(route('app.customer.home'));
            }

            return abort(403, 'Anda tidak berhak mengakses halaman ini!');
        }

        return view('app.auth.register');
    }

    /**
     * Handle route for OTP verification
     *
     * @return View|RedirectResponse
     */
    public function appVerifyView(): View|RedirectResponse
    {
        return view('app.auth.verify');

        if (! auth()->guest()) {
            if (auth()->user()->role != 'admin') {
                return redirect(route('app.verify'));
            }

            return abort(403, 'Anda tidak berhak mengakses halaman ini!');
        }

        return abort(403, 'Anda tidak berhak mengakses halaman ini!');
    }

    public function storeDeviceToken(Request $request)
    {
        try {
            auth()->user()->update(['device_token' => $request->device_token]);

            $data = $this->apiResponse(true, 'OK');
            $http_code = 200;
        } catch (Exception $e) {
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        } catch (Error $e) {
            $data = $this->apiResponse(false, $e->getMessage());
            $http_code = $e->getCode() == 422 ? 422 : 421;
        }

        return  response()->json($data, $http_code);
    }

    public function sessionLogin(Request $request)
    {
        try {
            $user = User::find(Crypt::decrypt($request->loggedInAs));
            if (! $user) {
                throw new Exception('user not found', 404);
            }
            Auth::login($user);

            return redirect()->route('app.home');
        } catch (Exception $e) {
            return redirect()->route('app.login', ['loginFailed' => 1]);
        }
    }

    public function forgotPasswordView()
    {
        if (! auth()->guest()) {
            if (auth()->user()->role == 'partner') {
                return redirect(route('app.partner.home'));
            } elseif (auth()->user()->role == 'customer') {
                return redirect(route('app.customer.home'));
            }

            return abort(403, 'Anda tidak berhak mengakses halaman ini!');
        }

        return view('app.auth.forgot-password');
    }

    public function sendRequestPasswordReset(Request $request)
    {
        $request->validate([Fortify::email() => 'required|email']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = $this->broker()->sendResetLink(
            $request->only(Fortify::email())
        );

        // dd($status,Password::RESET_LINK_SENT);
        \Log::info($status.' - '.Password::RESET_LINK_SENT);
        return $status == Password::RESET_LINK_SENT
                    ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                    : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(): PasswordBroker
    {
        return Password::broker(config('fortify.passwords'));
    }

    public function resetPasswordView(string $token,Request $request)
    {
        if (! auth()->guest()) {
            if (auth()->user()->role == 'partner') {
                return redirect(route('app.partner.home'));
            } elseif (auth()->user()->role == 'customer') {
                return redirect(route('app.customer.home'));
            }

            return abort(403, 'Anda tidak berhak mengakses halaman ini!');
        }

        abort_if(! $request->token, 403, 'Token tidak ditemukan!');

        return view('app.auth.reset-password',['token' => $token,'email' => $request['email']]);
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );
        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('app.login')->with('success', __($status))
                    : back()->withErrors(['email' => [__($status)]]);
    }
}
