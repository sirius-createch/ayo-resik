<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $articles = DB::table('articles')
                            ->select(['articles.id', 'articles.slug', 'articles.title', 'articles.banner', DB::raw("IFNULL(articles.published_at, '-') as published_at"), 'author.name as author'])
                            ->join(DB::raw('users as author'), 'articles.author_id', 'author.id')
                            ->whereNull('articles.deleted_at')
                            ->whereNull('author.deleted_at');

            return datatables($articles)
                ->filter(function ($query) {
                    if (request()->has('search')) {
                        $query->where(function ($query) {
                            $query->Where('articles.title', 'like', '%'.request('search.value').'%')
                                  ->orWhere('author.name', 'like', '%'.request('search.value').'%')
                                  ->when(request('search.value') == '-', function ($query) {
                                      $query->orWhereNull('published_at');
                                  })
                                  ->when(request('search.value') != '-', function ($query) {
                                      $query->orWhere(DB::raw("DATE_FORMAT(published_at, '%d/%m/%Y')"), 'like', '%'.request('search.value').'%');
                                  });
                        });
                    }
                })
                ->addIndexColumn()
                ->editColumn('published_at', function ($article) {
                    return $article->published_at != '-' ? date('d/m/Y', strtotime($article->published_at)) : $article->published_at;
                })
                ->editColumn('banner', function ($article) {
                    $link = Article::attrBannerLink($article);

                    return "
                        <a href='$link' data-fancybox style='cursor: zoom-in'>
                            <img src='$link' alt='Banner $article->title' class='rounded pull-up' style='object-fit: cover; width:25px; height: 25px;'>
                        </a>
                    ";
                })
                ->editColumn('action', 'datatables._actions_article')
                ->rawColumns(['banner', 'action'])
                ->toJson();
        }

        return view('admin.compro.articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.compro.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        Article::buat($request->all());

        return redirect()->route('admin.compro.articles.index')->with('success', 'Simpan artikel berhasil');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $artikel)
    {
        $article = $artikel;

        return view('admin.compro.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $artikel)
    {
        $artikel->ubah($request->all());

        return redirect()->route('admin.compro.articles.index')->with('success', 'Ubah artikel berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $artikel)
    {
        $artikel->hapus();

        return redirect()->route('admin.compro.articles.index')->with('success', 'Hapus artikel berhasil');
    }
}
