<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminFormTextarea extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private ?string $labelBadge = null,
        private ?string $subLabel = null,
        private ?string $label = null,
        private ?string $placeholder = null,
        private ?string $required = null,
        private ?string $readonly = null,
        private ?string $disabled = null,
        private ?string $autofocus = null,
        private ?string $tinymce = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-form-textarea', [
            'id' => $this->id,
            'name' => $this->name,
            'labelBadge' => $this->labelBadge,
            'subLabel' => $this->subLabel,
            'label' => $this->label,
            'placeholder' => $this->placeholder,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'autofocus' => $this->autofocus,
            'tinymce' => $this->tinymce,
        ]);
    }
}
