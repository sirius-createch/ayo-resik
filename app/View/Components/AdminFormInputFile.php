<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminFormInputFile extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private string $formId,
        private int $totalFileMax = 1,
        private ?string $accepted = null,
        private ?string $label = null,
        private ?string $labelBadge = null,
        private ?string $value = null,
        private ?string $valueSize = null,
        private ?bool $required = null,
        private ?bool $multiple = null
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-form-input-file', [
            'id' => $this->id,
            'name' => $this->name,
            'formId' => $this->formId,
            'totalFileMax' => $this->totalFileMax,
            'accepted' => $this->accepted,
            'label' => $this->label,
            'labelBadge' => $this->labelBadge,
            'value' => $this->value,
            'valueSize' => $this->valueSize,
            'required' => $this->required,
            'multiple' => $this->multiple,
        ]);
    }
}
