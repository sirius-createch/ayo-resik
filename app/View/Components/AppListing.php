<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppListing extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $route,
        private array $data,
        private string $name = 'Data',
        private array $orders = ['name' => 'Nama'],
        private array $limits = [5, 10, 25, 50],
        private string $sort = 'asc',
        private ?string $skeleton = null,
        private ?string $listGroup = null,
        private string $listGroupSize = 'large',
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-listing', [
            'id' => $this->id,
            'route' => $this->route,
            'data' => $this->data,
            'name' => $this->name,
            'orders' => $this->orders,
            'limits' => $this->limits,
            'sort' => $this->sort,
            'skeleton' => $this->skeleton,
            'listGroup' => $this->listGroup,
            'listGroupSize' => $this->listGroupSize,
        ]);
    }
}
