<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppListGroupItem extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $title,
        private string $link = '#',
        private ?string $subtitle = null,
        private ?string $icon = null,
        private ?string $iconColor = null,
        private ?string $action = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-list-group-item', [
            'title' => $this->title,
            'link' => $this->link,
            'subtitle' => $this->subtitle,
            'icon' => $this->icon,
            'iconColor' => $this->iconColor,
            'action' => $this->action,
        ]);
    }
}
