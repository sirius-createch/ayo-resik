<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminFormCheckbox extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private string $value = '',
        private ?bool $checked = null,
        private ?bool $required = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-form-checkbox', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'checked' => $this->checked,
            'required' => $this->required,
        ]);
    }
}
