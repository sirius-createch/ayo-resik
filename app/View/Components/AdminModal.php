<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $title,
        private ?string $size = null,
        private ?string $footer = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-modal', [
            'id' => $this->id,
            'title' => $this->title,
            'size' => $this->size,
            'footer' => $this->footer,
        ]);
    }
}
