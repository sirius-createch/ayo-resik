<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $action = '',
        private string $method = 'get',
        private ?bool $sendFile = null,
        private ?string $submit = null,
        private ?string $submitColor = 'bg-highlight',
        private ?string $submitSize = 'm',
        private ?string $submitIcon = 'fa-solid fa-paper-plane',
        private ?bool $submitWide = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-form', [
            'action' => $this->action,
            'method' => $this->method,
            'sendFile' => $this->sendFile,
            'submit' => $this->submit,
            'submitColor' => $this->submitColor,
            'submitSize' => $this->submitSize,
            'submitIcon' => $this->submitIcon,
            'submitWide' => $this->submitWide,
        ]);
    }
}
