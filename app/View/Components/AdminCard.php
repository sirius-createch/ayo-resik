<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminCard extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public ?string $header = null,
        public ?string $headerAction = null
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-card', [
            'header' => $this->header,
            'headerAction' => $this->headerAction,
        ]);
    }
}
