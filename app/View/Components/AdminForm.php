<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $action = '',
        private string $method = 'get',
        private ?bool $sendFile = null,
        private ?string $submit = null,
        private ?string $submitFace = 'primary',
        private ?string $submitIcon = 'bx bxs-paper-plane',
        private ?bool $submitWide = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-form', [
            'action' => $this->action,
            'method' => $this->method,
            'sendFile' => $this->sendFile,
            'submit' => $this->submit,
            'submitFace' => $this->submitFace,
            'submitIcon' => $this->submitIcon,
            'submitWide' => $this->submitWide,
        ]);
    }
}
