<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppTabGroupControls extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private array $tabs
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-tab-group-controls', [
            'tabs' => $this->tabs,
        ]);
    }
}
