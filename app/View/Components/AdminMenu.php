<?php

namespace App\View\Components;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class AdminMenu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $route,
        private string $icon,
        private ?string $submenus = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $activeRoute = $this->route;

        if (Str::contains($activeRoute, '.index')) {
            $activeRoute = Str::remove('.index', $activeRoute).'.*';
        }

        return view('components.admin-menu', [
            'activeRoute' => $activeRoute,
            'route' => $this->route,
            'icon' => $this->icon,
            'submenus' => $this->submenus,
        ]);
    }
}
