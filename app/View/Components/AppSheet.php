<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppSheet extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $title,
        private int $height = 365,
        private ?string $subtitle = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-sheet', [
            'id' => $this->id,
            'title' => $this->title,
            'height' => $this->height,
            'subtitle' => $this->subtitle,
        ]);
    }
}
