<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminButton extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $type = 'button',
        private string $face = 'primary',
        private string $size = 'md',
        private ?string $icon = null,
        private ?string $link = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-button', [
            'type' => $this->type,
            'face' => $this->face,
            'size' => $this->size,
            'icon' => $this->icon,
            'link' => $this->link,
        ]);
    }
}
