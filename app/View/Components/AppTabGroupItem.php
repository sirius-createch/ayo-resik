<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppTabGroupItem extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $parent,
        private string $id,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-tab-group-item', [
            'parent' => $this->parent,
            'id' => $this->id,
        ]);
    }
}
