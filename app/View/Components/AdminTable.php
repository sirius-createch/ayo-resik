<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminTable extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id = 'table',
        private string $tbody = '',
        private ?string $thead = null,
        private ?string $tfoot = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-table', [
            'id' => $this->id,
            'thead' => $this->thead,
            'tbody' => $this->tbody,
            'tfoot' => $this->tfoot,
        ]);
    }
}
