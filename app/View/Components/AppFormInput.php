<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppFormInput extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private ?string $icon = null,
        private ?string $type = null,
        private ?string $subLabel = null,
        private ?string $label = null,
        private ?string $value = null,
        private ?string $placeholder = null,
        private ?string $required = null,
        private ?string $readonly = null,
        private ?string $disabled = null,
        private ?string $autofocus = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-form-input', [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->icon,
            'type' => $this->type,
            'label' => $this->label,
            'subLabel' => $this->subLabel,
            'value' => $this->value,
            'placeholder' => $this->placeholder,
            'required' => $this->required,
            'readonly' => $this->readonly,
            'disabled' => $this->disabled,
            'autofocus' => $this->autofocus,
        ]);
    }
}
