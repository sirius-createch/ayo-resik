<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppFormInputFile extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private string $formId,
        private int $totalFileMax = 1,
        private ?string $label = null,
        private ?string $accepted = null,
        private ?string $value = null,
        private ?string $valueSize = null,
        private ?bool $required = null,
        private ?bool $multiple = null
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-form-input-file', [
            'id' => $this->id,
            'name' => $this->name,
            'formId' => $this->formId,
            'totalFileMax' => $this->totalFileMax,
            'label' => $this->label,
            'accepted' => $this->accepted,
            'value' => $this->value,
            'valueSize' => $this->valueSize,
            'required' => $this->required,
            'multiple' => $this->multiple,
        ]);
    }
}
