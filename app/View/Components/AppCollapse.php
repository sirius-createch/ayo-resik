<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppCollapse extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $title,
        private string $color = 'gradient-highlight',
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-collapse', [
            'id' => $this->id,
            'title' => $this->title,
            'color' => $this->color,
        ]);
    }
}
