<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppButton extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $type = 'button',
        private string $color = 'bg-highlight',
        private string $size = 'm',
        private ?string $icon = null,
        private ?string $link = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app-button', [
            'type' => $this->type,
            'color' => $this->color,
            'size' => $this->size,
            'icon' => $this->icon,
            'link' => $this->link,
        ]);
    }
}
