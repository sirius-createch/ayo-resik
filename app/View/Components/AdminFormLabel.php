<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminFormLabel extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private ?string $for = null,
        private ?bool $required = null,
        private ?string $badge = null,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin-form-label', [
            'for' => $this->for,
            'required' => $this->required,
            'badge' => $this->badge,
        ]);
    }
}
