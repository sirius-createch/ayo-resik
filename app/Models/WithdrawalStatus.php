<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawalStatus extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'withdrawal_id',
        'name',
    ];

    /* -- accessor & mutator -- */
    protected function nameConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrNameConverted($this)
        );
    }

    protected function nameBadgeConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrNameBadgeConverted($this)
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator -- */
    public static function attrNameConverted($withdrawal): string
    {
        return match ($withdrawal->name) {
            'requested' => 'Permintaan Diajukan',
            'seen' => 'Permintaan Dilihat',
            'denied' => 'Permintaan Ditolak',
            'confirmed' => 'Permintaan Dikonfirmasi',
            'sent' => 'Uang Telah Diberikan',
            'not_received' => 'Uang Belum Diterima',
            'done' => 'Penarikan Selesai',
            'canceled' => 'Penarikan Dibatalkan',
        };
    }

    public static function attrNameBadgeConverted($withdrawal): string
    {
        return "<span class='badge rounded-pill bg-label-".self::mappingNameColor($withdrawal->name)."'><i class='".self::mappingNameIcon($withdrawal->name)." align-middle me-1'></i>".self::attrNameConverted($withdrawal).'</span>';
    }

    public static function mappingNameColor($status): string
    {
        return match ($status) {
            'requested' => 'secondary',
            'seen' => 'info',
            'denied', 'canceled' => 'danger',
            'confirmed' => 'primary',
            'not_received' => 'warning',
            'sent' => 'dark',
            'done' => 'success',
        };
    }

    public static function mappingNameIcon($status, $font = 'boxicon'): string
    {
        return match ($status) {
            'requested' => $font == 'boxicon' ? 'bx bxs-send' : 'fa-solid fa-paper-plane',
            'seen' => $font == 'boxicon' ? 'bx bx-show-alt' : 'fa-solid fa-eye',
            'denied' => $font == 'boxicon' ? 'bx bx-x-circle' : 'fa-regular fa-circle-xmark',
            'confirmed' => $font == 'boxicon' ? 'bx bx-check-circle' : 'fa-regular fa-circle-check',
            'sent' => $font == 'boxicon' ? 'bx bx-money' : 'fa-solid fa-money-bill-wave',
            'not_received' => $font == 'boxicon' ? 'bx bx-error' : 'fa-solid fa-triangle-exclamation',
            'done' => $font == 'boxicon' ? 'bx bx-check-double' : 'fa-solid fa-check-double',
            'canceled' => $font == 'boxicon' ? 'bx bx-x' : 'fa-solid fa-xmark',
        };
    }
    /* -- pendamping accessor & mutator -- */

    /* -- relation -- */
    public function withdrawal()
    {
        return $this->belongsTo(Withdrawal::class, 'withdrawal_id');
    }
    /* -- relation -- */
}
