<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /* -- function -- */
    private static function fetch($args = [])
    {
        return [
            'name' => $args->add_nama ?? $args->edit_nama,
        ];
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);

        return self::create($request);
    }

    public function ubah($params = [])
    {
        $request = self::fetch((object) $params);

        return $this->update($request);
    }

    public function hapus()
    {
        return $this->delete();
    }
    /* -- function -- */
}
