<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'detail',
        'link',
        'icon',
        'broadcasted_to',
        'broadcasted_at',
        'readed_at',
    ];

    public function broadcasted_to_user()
    {
        return $this->belongsTo(User::class, 'broadcasted_to');
    }
}
