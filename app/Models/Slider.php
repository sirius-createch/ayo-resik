<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Slider extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['title','desc','image','url'];

    /* -- accessor & mutator -- */
    public function imageLink(): Attribute{
        return Attribute::make(
            get: function () {
                if ($this->image) {
                    if (Str::startsWith($this->image, 'http')) {
                        return $this->image;
                    } else {
                        return asset('images/slider/'.$this->image);
                    }
                }

                return 'https://ui-avatars.com/api/?name='.$this->name;
            }
        );
    }
    public function imageSize(): Attribute{
        return Attribute::make(
            get: function () {
                if ($this->image) {
                    return File::size(public_path('images/slider/'.$this->image));
                }

                return 0;
            }
        );
    }
}
