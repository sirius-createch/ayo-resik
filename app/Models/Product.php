<?php

namespace App\Models;

use App\Helpers\Sirius;
use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'image',
        'image_size',
        'base_price',
    ];

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class)->withPivot('weight', 'weight_real', 'price', 'photo')->withTimestamps();
    }

    public function partners()
    {
        return $this->belongsToMany(Product::class, 'product_user', 'product_id', 'partner_id')->withPivot('price')->withTimestamps();
    }

    /* -- accessor & mutator -- */
    public function imageLink(): Attribute
    {
        return Attribute::make(
            get: function () {
                if ($this->image) {
                    if (Str::startsWith($this->image, 'http')) {
                        return $this->image;
                    } else {
                        return asset('images/product/'.$this->image);
                    }
                }

                return 'https://ui-avatars.com/api/?name='.$this->name;
            }
        );
    }
    /* -- accessor & mutator -- */

    /* -- function -- */
    private static function fetch($args = [])
    {
        return (object) [
            'detail' => [
                'name' => $args->name,
                'base_price' => Str::remove('.', $args->base_price),
            ],
            'image' => $args->image[0] ?? $args->image_edit[0] ?? null,
        ];
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);
        $product = self::create($request->detail);
        $product->simpanGambar($request->image);

        return $product;
    }

    public function ubah($params = [])
    {
        $request = self::fetch((object) $params);
        $this->update($request->detail);
        $this->ubahGambar($request->image);

        return $this;
    }

    public function hapus()
    {
        if ($this->hapusGambar()) {
            $this->update(['image' => null, 'image_size' => null]);

            return $this->delete();
        }
    }

    public function pindahGambar($image = null)
    {
        if ($image) {
            $new_name = time().'_'.str_replace(' ', '_', $image);

            $path = public_path('images/product/');
            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (File::move(storage_path('tmp/').$image, $path.$new_name)) {
                return $this->update([
                    'image' => $new_name,
                    'image_size' => File::size($path.$new_name),
                ]);
            }
        }
    }

    public function simpanGambar($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                return $this->pindahGambar($image);
            }
        }
    }

    public function ubahGambar($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                if ($this->hapusGambar()) {
                    $this->simpanGambar($image);
                }
            }
        } else {
            if ($this->hapusGambar()) {
                $this->update(['image' => null, 'image_size' => null]);
            }
        }
    }

    public function hapusGambar()
    {
        if ($this->image) {
            if (File::exists(public_path('images/product/').$this->image)) {
                File::delete(public_path('images/product/').$this->image);
            }
        }

        return true;
    }

    private static function kueri($params)
    {
        return DB::table('products as p')->join('invoice_product as ip', 'ip.product_id', 'p.id')
        ->when(isset($params['startDate']), function ($query) use ($params) {
            $query->whereDate('ip.created_at', '>=', $params['startDate']);
        })
        ->when(isset($params['endDate']), function ($query) use ($params) {
            $query->whereDate('ip.created_at', '<=', $params['endDate']);
        })->whereNotNull('ip.weight_real');
    }

    public static function report($params = [])
    {
        return self::kueri($params)
        ->select(
            'p.name',
            DB::raw('SUM(ip.weight_real) as weight'),
            DB::raw('SUM(ip.price * ip.weight_real) as price'),
        )
        ->when(isset($params['search']['value']), function ($query) use ($params) {
            $query->where(function ($query) use ($params) {
                $query->where(function ($query) use ($params) {
                    $query->where('p.name', 'like', '%'.$params['search']['value'].'%');
                });
            });
        })
        ->groupBy('p.id')->groupBy('p.name');
    }

    public static function expenseReport($params)
    {
        $date = DB::raw('p.name AS date');
        $group = DB::raw('p.id, p.name');

        $reports = self::kueri($params)
        ->select(DB::raw('SUM(ip.price * ip.weight_real) as total_price'), $date)
        ->groupBy($group)->get();
        $service_charges = Invoice::where('partner_id', auth()->id())
                                    ->when(isset($params['startDate']), function ($query) use ($params) {
                                        $query->whereDate('created_at', '>=', $params['startDate']);
                                    })
                                    ->when(isset($params['endDate']), function ($query) use ($params) {
                                        $query->whereDate('created_at', '<=', $params['endDate']);
                                    })
                                    ->sum('service_charge');

        foreach ($reports as $report) {
            $temp[$report->date] = $report->total_price;
        }

        $data = [];
        $data['service_charge'] = $service_charges;
        $data['overview'] = 0;
        $data['labels'] = [];
        $data['datasets'] = [];

        foreach ($reports as $report) {
            $nominal = round($temp[$report->date]);
            $data['overview'] = ($data['overview'] ?? 0) + ($nominal);
            $data['labels'][] = $report->date;
            $data['datasets'][] = $nominal;
        }

        return $data;
    }

    public static function weightReport($params = [])
    {
        $date = DB::raw('p.name AS date');
        $group = DB::raw('p.id, p.name');

        $reports = self::kueri($params)
        ->select(DB::raw('sum(ip.weight_real) as total_weight'), $date)
        ->groupBy($group)->get();

        foreach ($reports as $report) {
            $temp[$report->date] = $report->total_weight;
        }

        $data = [];
        $data['overview'] = 0;
        $data['labels'] = [];
        $data['datasets'] = [];

        foreach ($reports as $report) {
            $nominal = round($temp[$report->date]);
            $data['overview'] = ($data['overview'] ?? 0) + ($nominal);
            $data['labels'][] = $report->date;
            $data['datasets'][] = $nominal;
        }

        return $data;
    }
    /* -- function -- */
}
