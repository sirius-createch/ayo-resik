<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Invoice extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'partner_id',
        'customer_id',
        'total_bill',
        'total_weight',
        'day_taken',
        'address',
        'latitude',
        'longitude',
        'payment_method_id',
        'current_status',
        'note',
        'service_charge'
    ];

    public function getRouteKeyName()
    {
        return 'code';
    }

    public $preventAccessor = false;

    /* -- accessor & mutator -- */
    protected function code(): Attribute
    {
        return Attribute::make(
            set: function ($value) {
                if ($this->preventAccessor) {
                    return $value;
                } else {
                    $lastId = self::whereDate('created_at', $value)->count() + 1;

                    return 'INV'.date('Ymd', strtotime($value)).Str::padLeft($lastId, 3, '0');
                }
            }
        );
    }

    protected function totalWeightConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrTotalWeightConverted($this)
        );
    }

    protected function currentStatusConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrCurrentStatusConverted($this)
        );
    }

    protected function currentStatusBadgeConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrCurrentStatusBadgeConverted($this)
        );
    }

    protected function dayTakenConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => match ($this->day_taken) {
                'monday' => 'Senin',
                'tuesday' => 'Selasa',
                'wednesday' => 'Rabu',
                'thursday' => 'Kamis',
                'friday' => 'Jumat',
                'saturday' => 'Sabtu',
                'sunday' => 'Minggu',
                default => '-'
            }
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator -- */
    public static function attrTotalWeightConverted($invoice): string
    {
        return Str::replace(',0', '', number_format($invoice->total_weight, 1, ',', '.')).' kg';
    }

    public static function attrCurrentStatusConverted($invoice): string
    {
        return match ($invoice->current_status) {
            'requested' => 'Permintaan Diajukan',
            'seen' => 'Permintaan Dilihat',
            'denied' => 'Permintaan Ditolak',
            'confirmed' => 'Permintaan Dikonfirmasi',
            'verified' => 'Sampah Diverifikasi',
            'done' => 'Transaksi Selesai',
            'canceled' => 'Transaksi Dibatalkan',
        };
    }

    public static function attrCurrentStatusBadgeConverted($invoice): string
    {
        return "<span class='badge rounded-pill bg-label-".self::mappingCurrentStatusColor($invoice->current_status)."'><i class='".self::mappingCurrentStatusIcon($invoice->current_status)." align-middle me-1'></i>".self::attrCurrentStatusConverted($invoice).'</span>';
    }

    public static function mappingCurrentStatusColor($status): string
    {
        return match ($status) {
            'requested' => 'secondary',
            'seen' => 'info',
            'denied', 'canceled' => 'danger',
            'confirmed' => 'primary',
            'verified' => 'dark',
            'done' => 'success',
        };
    }

    public static function mappingCurrentStatusIcon($status, $font = 'boxicon'): string
    {
        return match ($status) {
            'requested' => $font == 'boxicon' ? 'bx bxs-send' : 'fa-solid fa-paper-plane',
            'seen' => $font == 'boxicon' ? 'bx bx-show-alt' : 'fa-solid fa-eye',
            'denied' => $font == 'boxicon' ? 'bx bx-x-circle' : 'fa-regular fa-circle-xmark',
            'confirmed' => $font == 'boxicon' ? 'bx bx-check-circle' : 'fa-regular fa-circle-check',
            'verified' => $font == 'boxicon' ? 'bx bx-task' : 'fa-solid fa-clipboard-check',
            'done' => $font == 'boxicon' ? 'bx bx-check-double' : 'fa-solid fa-check-double',
            'canceled' => $font == 'boxicon' ? 'bx bx-x' : 'fa-solid fa-x',
        };
    }
    /* -- pendamping accessor & mutator -- */

    /* -- relation -- */
    public function statuses()
    {
        return $this->hasMany(InvoiceStatus::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('weight', 'weight_real', 'price', 'photo')->withTimestamps()->withTrashed();
    }

    public function partner()
    {
        return $this->belongsTo(User::class, 'partner_id')->withTrashed();
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id')->withTrashed();
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
    /* -- relation -- */

    /* -- function -- */
    public static function available_status()
    {
        return [
            1 => 'requested',
            2 => 'seen',
            3 => 'denied',
            4 => 'confirmed',
            5 => 'verified',
            6 => 'done',
            7 => 'canceled',
        ];
    }

    public static function statusSearchFilter($search)
    {
        $status = [];
        if (Str::contains('Permintaan Diajukan', $search, true)) {
            $status = ['requested'];
        }
        if (Str::contains('Permintaan Dilihat', $search, true)) {
            $status = ['seen'];
        }
        if (Str::contains('Permintaan Ditolak', $search, true)) {
            $status = ['denied'];
        }
        if (Str::contains('Permintaan Dikonfirmasi', $search, true)) {
            $status = ['confirmed'];
        }
        if (Str::contains('Sampah Diverifikasi', $search, true)) {
            $status = ['verified'];
        }
        if (Str::contains('Transaksi Selesai', $search, true)) {
            $status = ['done'];
        }
        if (Str::contains('Transaksi Dibatalkan', $search, true)) {
            $status = ['canceled'];
        }
        if (Str::contains('Permintaan', $search, true)) {
            $status = ['requested', 'seen', 'denied', 'confirmed'];
        }
        if (Str::contains('Transaksi', $search, true)) {
            $status = ['done', 'canceled'];
        }

        return $status;
    }

    public static function dayTakenSearchFilter($search)
    {
        $day = [];
        if (Str::contains('Minggu', $search, true)) {
            $day = ['sunday'];
        }
        if (Str::contains('Senin', $search, true)) {
            $day = ['monday'];
        }
        if (Str::contains('Selasa', $search, true)) {
            $day = ['tuesday'];
        }
        if (Str::contains('Rabu', $search, true)) {
            $day = ['wednesday'];
        }
        if (Str::contains('Kamis', $search, true)) {
            $day = ['thursday'];
        }
        if (Str::contains('Jumat', $search, true)) {
            $day = ['friday'];
        }
        if (Str::contains('Sabtu', $search, true)) {
            $day = ['saturday'];
        }

        return $day;
    }

    private static function fetch($args = [])
    {
        $products = collect(json_decode($args->products))->toArray();

        $bill = $weight = 0;
        $details = [];
        foreach ($products as $detail) {
            $details[$detail->id] = [
                'weight' => $detail->weight,
                'price' => $detail->price,
            ];
            $bill += ($detail->price * $detail->weight);
            $weight += $detail->weight;
        }
        $bill = intval(round($bill), 0);
        $service_charge = config('app.service_rate') * $bill;
        $bill -= $service_charge;

        return (object) [
            'invoice' => [
                'partner_id' => auth()->user()->partner_id ?? 1,
                'customer_id' => auth()->id() ?? 1,
                'total_bill' => $bill ?? 0,
                'service_charge' => $service_charge ?? 0,
                'total_weight' => $weight ?? 0,
                'day_taken' => $args->day_taken ?? null,
                'address' => $args->address ?? '',
                'latitude' => $args->latitude ?? auth()->user()->latitude,
                'longitude' => $args->longitude ?? auth()->user()->longitude,
                'payment_method_id' => $args->payment,
                'current_status' => $args->current_status ?? 'requested',
                'note' => $args->note ?? '',
            ],
            'details' => $details,
        ];
    }

    private static function fetchStatus($args, $id)
    {
        if (in_array($args->status, self::available_status())) {
            return (object) [
                'detail' => ['current_status' => $args->status],
                'invoice_statuses' => [
                    'name' => $args->status,
                    'invoice_id' => $id,
                ],
            ];
        } else {
            throw new Exception('Status baru tidak dikenali!', 422);
        }
    }

    private static function fetchConfirm($args = [])
    {
        $details = [];
        foreach ($args->products as $key => $detail) {
            $details[$key]['weight_real'] = Str::replace(',', '.', $detail);
        }

        return (object) [
            'details' => $details,
        ];
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);
        $request->invoice['code'] = now();
        $invoice = self::create($request->invoice);
        $invoice->products()->sync($request->details);

        return $invoice;
    }

    public function ubah($params = [])
    {
        //
    }

    public function ubahStatus($params = [])
    {
        $request = self::fetchStatus((object) $params, $this->id);
        $this->update($request->detail);

        return InvoiceStatus::create($request->invoice_statuses);
    }

    public function konfirmasi($params = [])
    {
        try {
            $request = self::fetchConfirm((object) $params);

            foreach ($request->details as $key => $value) {
                $this->products()->updateExistingPivot($key, $value);
            }

            return true;
        } catch (Exception $e) {
            throw new Exception('Terjadi masalah saat mengimplementasikan berat yang baru', 422);
        }
    }

    public static function kueri($params = [])
    {
        $request = collect($params);

        return DB::table('invoices')->join('users as customer', 'invoices.customer_id', 'customer.id')
        ->join('users as partner', 'invoices.partner_id', 'partner.id')
        ->when(isset($request['search']['value']), function ($query) use ($request) {
            $query->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('code', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('partner.name', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('customer.name', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('invoices.total_bill', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', $request['search']['value']).'%')
                        ->orWhere(DB::raw("DATE_FORMAT(invoices.created_at, '%d/%m/%y %H:%i')"), 'like', '%'.$request['search']['value'].'%');
                });
            });
        })
        ->when(isset($request['customerId']), function ($query) use ($request) {
            $query->where('invoices.customer_id', $request['customerId']);
        })
        ->when(isset($request['partnerId']), function ($query) use ($request) {
            $query->where('invoices.partner_id', $request['partnerId']);
        })
        ->when(isset($request['startDate']), function ($query) use ($request) {
            $query->whereDate('invoices.created_at', '>=', $request['startDate']);
        })
        ->when(isset($request['endDate']), function ($query) use ($request) {
            $query->whereDate('invoices.created_at', '<=', $request['endDate']);
        })
        ->where(['invoices.current_status' => 'done', 'invoices.payment_method_id' => 1])->whereNull('invoices.deleted_at');
    }

    public static function report($params = [])
    {
        return self::kueri($params)
        ->select(
            'invoices.id',
            'invoices.code',
            'invoices.total_bill as amount',
            'customer.name as customer_name',
            'partner.name as partner_name',
            'invoices.created_at as time',
            DB::Raw('1 as is_invoice'),
        );
    }

    public static function chart($params = [])
    {
        return self::kueri($params)
        ->select(
            'invoices.id',
            'invoices.code',
            'invoices.total_bill as amount',
            'customer.name as customer_name',
            'partner.name as partner_name',
            'invoices.created_at as time'
        );
    }

    public static function index(mixed $params)
    {
        return self::when(auth()->user()->role == 'partner', function ($query) {
            $query->where('partner_id', auth()->id());
        })
        ->when(auth()->user()->role == 'customer', function ($query) {
            $query->where('customer_id', auth()->id());
        })
        ->when($params->search, function ($query) use ($params) {
            $query->where(function ($query) use ($params) {
                $status = Invoice::statusSearchFilter($params->search);
                $dayTaken = Invoice::dayTakenSearchFilter($params->search);
                $query->where('code', 'like', '%'.$params->search.'%')
                    ->orWhere(DB::raw('ROUND(total_weight, 1)'), 'like', Str::replace([' ', 'k', 'g'], '', Str::replace(',', '.', '%'.$params->search.'%')))
                    ->orWhere('total_bill', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', $params->search.'%'))
                    ->when(auth()->user()->role == 'partner', function ($query) use ($params) {
                        $query->orWhereHas('customer', function ($query) use ($params) {
                            $query->where(function ($query) use ($params) {
                                $query->where('name', 'like', '%'.$params->search.'%')
                                      ->orWhere('address', 'like', '%'.$params->search.'%');
                            });
                        });
                    })
                    ->when(auth()->user()->role == 'customer', function ($query) use ($params) {
                        $query->orWhereHas('partner', function ($query) use ($params) {
                            $query->where(function ($query) use ($params) {
                                $query->where('name', 'like', '%'.$params->search.'%')
                                      ->orWhere('address', 'like', '%'.$params->search.'%');
                            });
                        });
                    })
                    ->when(count($status) > 0, function ($query) use ($status) {
                        $query->orWhereIn('current_status', $status);
                    })
                    ->when(count($dayTaken) > 0, function ($query) use ($dayTaken) {
                        $query->orWhereIn('day_taken', $dayTaken);
                    });
            });
        })
        ->when($params->order, function ($query) use ($params) {
            $query->orderBy($params->order, $params->sort);
        });
    }

    public static function cari($id)
    {
        $invoice = self::whereId($id)->when(auth()->user()->role == 'customer', function ($query) {
            $query->where('customer_id', auth()->id());
        })->when(auth()->user()->role == 'partner', function ($query) {
            $query->where('partner_id', auth()->id());
        })->first();
        if (empty($invoice)) {
            throw new Exception('Data tidak ditemukan!', 422);
        }
        $produk = [];
        foreach ($invoice->products as $data) {
            $produk[] = [
                'invoice_id' => $data->pivot->invoice_id,
                'product_id' => $data->pivot->product_id,
                'weight' => $data->pivot->weight,
                'weight_real' => $data->pivot->weight_real,
                'price' => $data->pivot->price,
                'photo' => $data->pivot->photo,
                'created_at' => $data->pivot->created_at,
            ];
        }

        return [
            'invoice' => $invoice->without('products')->first(),
            'product' => $produk,
            'history_status' => $invoice->statuses,
        ];
    }
    /* -- function -- */
}
