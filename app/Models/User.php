<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\ResetPasswordNotificationQueue;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'phone',
        'name',
        'email',
        'password',
        'avatar',
        'role',
        'partner_id',
        'balance',
        'address',
        'latitude',
        'longitude',
        'device_token',
        'partner_confirm_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];

    /* -- accessor & mutator -- */
    public function avatarLink(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrAvatarLink($this)
        );
    }

    public function phoneConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrPhoneConverted($this),
        );
    }

    public function schedulesConverted(): Attribute
    {
        return Attribute::make(
            get: function () {
                $schedules = [];
                if ($this->schedule->monday) {
                    $schedules[] = 'Senin';
                }
                if ($this->schedule->tuesday) {
                    $schedules[] = 'Selasa';
                }
                if ($this->schedule->wednesday) {
                    $schedules[] = 'Rabu';
                }
                if ($this->schedule->thursday) {
                    $schedules[] = 'Kamis';
                }
                if ($this->schedule->friday) {
                    $schedules[] = 'Jumat';
                }
                if ($this->schedule->saturday) {
                    $schedules[] = 'Sabtu';
                }
                if ($this->schedule->sunday) {
                    $schedules[] = 'Minggu';
                }

                return $schedules;
            }
        );
    }

    public function schedules(): Attribute
    {
        return Attribute::make(
            get: function () {
                $schedules = [];
                if ($this->schedule->monday) {
                    $schedules[] = 'monday';
                }
                if ($this->schedule->tuesday) {
                    $schedules[] = 'tuesday';
                }
                if ($this->schedule->wednesday) {
                    $schedules[] = 'wednesday';
                }
                if ($this->schedule->thursday) {
                    $schedules[] = 'thursday';
                }
                if ($this->schedule->friday) {
                    $schedules[] = 'friday';
                }
                if ($this->schedule->saturday) {
                    $schedules[] = 'saturday';
                }
                if ($this->schedule->sunday) {
                    $schedules[] = 'sunday';
                }

                return $schedules;
            }
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator */
    public static function attrAvatarLink($user)
    {
        if ($user->avatar) {
            if (Str::startsWith($user->avatar, 'http')) {
                return $user->avatar;
            } else {
                return asset('images/avatar/'.$user->avatar);
            }
        }

        return 'https://ui-avatars.com/api/?size=250&name='.Str::replace(' ', '%20', $user->name);
    }

    public static function attrPhoneConverted($user)
    {
        return implode('-', str_split(0 .$user->phone, 4));
    }
    /* -- pendamping accessor & mutator */

    /* -- relation --*/
    public function customers()
    {
        return $this->hasMany(User::class, 'partner_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_user', 'partner_id', 'product_id')->withPivot('price')->withTimestamps();
    }

    public function partner_invoices()
    {
        return $this->hasMany(Invoice::class, 'partner_id');
    }

    public function customer_invoices()
    {
        return $this->hasMany(Invoice::class, 'customer_id');
    }

    public function partner_withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'partner_id');
    }

    public function customer_withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'customer_id');
    }

    public function contents()
    {
        return $this->hasMany(Content::class, 'author_id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'author_id');
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class, 'partner_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'broadcasted_to');
    }

    public function otps()
    {
        return $this->hasMany(Otp::class);
    }

    public function partner()
    {
        return $this->belongsTo(User::class, 'partner_id');
    }
    /* -- relation --*/

    /* -- function -- */
    public function available_role()
    {
        return [
            1 => 'admin',
            2 => 'partner',
            3 => 'customer',
        ];
    }

    private static function fetchPrice($args = [])
    {
        $params = [];
        foreach ($args->list_harga as $list) {
            $params[$list->product_id] = ['price' => $list->price];
        }

        return $params;
    }

    public function set_products_price($params = [])
    {
        $request = self::fetchPrice($params);

        return $this->products()->sync($request);
    }

    private static function fetch($args = [])
    {
        $schedule = [];
        if (isset($args->schedule)) {
            foreach ($args->schedule as $key => $value) {
                $args->schedule[] = $key;
            }
            foreach (Schedule::days() as $day) {
                $schedule[$day] = in_array($day, $args->schedule) ? 1 : 0;
            }
        } else {
            foreach (Schedule::days() as $day) {
                $schedule[$day] = 0;
            }
        }

        if ($args->phone) {
            $phone = Str::replace('-', '', Str::replace('.', '', Str::replace('+62', '', $args->phone)));
            $phone = $phone[0] == 0 ? Str::replaceFirst('0', '', $phone) : $phone;
        }

        $data = [
            'detail' => [
                'name' => $args->name ?? null,
                'email' => $args->email ?? null,
                'phone' => $args->phone ? $phone : null,
                'address' => $args->address ?? null,
            ],
            'schedule' => $schedule,
            'avatar' => $args->avatar[0] ?? null,
        ];

        if (isset($args->latitude)) {
            $data['detail']['latitude'] = $args->latitude;
        }

        if (isset($args->longitude)) {
            $data['detail']['longitude'] = $args->longitude;
        }

        return (object) $data;
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);
        $request->detail['password'] = Hash::make('password');
        $request->detail['role'] = 'partner';
        $request->detail['latitude'] = 0;
        $request->detail['longitude'] = 0;
        $partner = self::create($request->detail);
        $partner->simpanAvatar($request->avatar);
        if ($partner->role == 'partner') {
            $partner->schedule()->create($request->schedule);
        }

        return $partner;
    }

    public function ubah($params = [], $isAdmin = false)
    {
        $request = self::fetch((object) $params);
        $this->update($request->detail);
        $this->ubahAvatar($request->avatar);
        if ($this->role == 'partner' && $isAdmin) {
            $this->schedule()->update($request->schedule);
        }

        return $this;
    }

    public function hapus()
    {
        if ($this->hapusAvatar()) {
            $this->update(['avatar' => null, 'avatar_size' => null]);

            return $this->delete();
        }
    }

    public function pindahAvatar($image = null)
    {
        if ($image) {
            $new_name = time().'_'.str_replace(' ', '_', $image);

            $path = public_path('images/avatar/');
            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (File::move(storage_path('tmp/').$image, $path.$new_name)) {
                return $this->update([
                    'avatar' => $new_name,
                    'avatar_size' => File::size($path.$new_name),
                ]);
            }
        }
    }

    public function simpanAvatar($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                return $this->pindahAvatar($image);
            }
        }
    }

    public function ubahAvatar($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                if ($this->hapusAvatar()) {
                    $this->simpanAvatar($image);
                }
            }
        } else {
            if ($this->hapusAvatar()) {
                $this->update(['avatar' => null, 'avatar_size' => null]);
            }
        }
    }

    public function hapusAvatar()
    {
        if ($this->avatar) {
            if (File::exists(public_path('images/avatar/').$this->avatar)) {
                File::delete(public_path('images/avatar/').$this->avatar);
            }
        }

        return true;
    }

    public function filter($keyword = '')
    {
        return self::where('name', 'like', $keyword.'%')->orWhere('address', 'like', '%'.$keyword.'%')->where('role', 'partner')->get();
    }

    public function partner_terdekat($request = null)
    {
        if ($this->partner) { // Saat sudah terhubung dengan partner
            $distanceQuery = '111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(u.latitude))
                                      * COS(RADIANS(p.latitude))
                                      * COS(RADIANS(u.longitude - p.longitude))
                                      + SIN(RADIANS(u.latitude))
                                      * SIN(RADIANS(p.latitude))))) AS distance';

            $query = DB::table('users as u');
        } else { // Saat tidak terhubung dengan partner manapun
            $distanceQuery = '111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS('.auth()->user()->latitude.'))
                                      * COS(RADIANS(p.latitude))
                                      * COS(RADIANS('.auth()->user()->longitude.' - p.longitude))
                                      + SIN(RADIANS('.auth()->user()->latitude.'))
                                      * SIN(RADIANS(p.latitude))))) AS distance';

            $query = DB::table('users as p');
        }

        return $query->select('p.id', DB::raw($distanceQuery))
                ->when($this->partner, function ($query) {
                    $query->join('users as p', 'u.partner_id', '<>', 'p.id')
                        ->where('u.id', $this->id);
                })
                ->where('p.role', 'partner')
                ->when($request, function ($query) use ($request) {
                    $query->orderBy($request->order, $request->sort);
                });
    }

    public function ubahPartner($new_partner_id)
    {
        if (!empty($this->partner)) {
            $partner = $this->partner;
            $partner->balance -= $this->balance;
            $partner->save();
        }

        return $this->update([
            'balance' => 0,
            'partner_id' => $new_partner_id,
            'partner_confirm_at' => null,
        ]);
    }

    public function confirmCustomer($params, $customer_id)
    {
        if (isset($params['confirm'])) {
            if ($params['confirm']) {
                return self::whereId($customer_id)->update(['partner_confirm_at' => now()]);
            } else {
                return self::whereId($customer_id)->update(['partner_id' => null]);
            }
        }
    }

    public function daftarProduk()
    {
        return DB::table('product_user as pu')->select('p.name', 'p.base_price')
        ->join('products as p', 'p.id', 'pu.product_id')->wherePartnerId($this->partner_id)->get()->toArray() ?? [];
    }

    public function daftarUbahPartner()
    {
        return User::wherePartnerId($this->id)->whereNull('partner_confirm_at')->select('id', 'name')->get()->toArray() ?? [];
    }

    public static function cari($id)
    {
        $profile = self::whereId($id)->first();
        $produk = [];
        foreach ($profile->products as $product) {
            $produk[] = [
                'partner_id' => $product->pivot->partner_id,
                'product_id' => $product->pivot->product_id,
                'name' => $product->name,
                'base_price' => $product->base_price,
                'price' => $product->pivot->price,
                'image' => $product->pivot->image,
            ];
        }

        return [
            'profile' => $profile->without('schedule', 'schedule')->first(),
            'schedule' => $profile->schedule,
            'products' => $produk,
        ];
    }

    public function notifikasi()
    {
        return Notification::where('broadcasted_to', $this->id);
    }
    /*
    * Ubah password user by Admih
    */
    public function ubahPassword(array $request){
        return $this->update([
            'password' => Hash::make($request['password'])
        ]);
    }
    /* -- function -- */

    /**
     * Send a password reset notification to the user.
     *
     * @param  string  $token
     */
    public function sendPasswordResetNotification($token): void
    {
        // $this->notify(new ResetPasswordNotification($token)); Original without queue
        $this->notify(new ResetPasswordNotificationQueue($token));
    }
}
