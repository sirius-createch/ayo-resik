<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Withdrawal extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'partner_id',
        'customer_id',
        'amount',
        'current_status',
        'note',
    ];

    public function getRouteKeyName()
    {
        return 'code';
    }

    public $preventAccessor = false;

    /* -- accessor & mutator -- */
    protected function code(): Attribute
    {
        return Attribute::make(
            set: function ($value) {
                if ($this->preventAccessor) {
                    return $value;
                } else {
                    $lastId = self::whereDate('created_at', $value)->count() + 1;

                    return 'WIT'.date('Ymd', strtotime($value)).Str::padLeft($lastId, 3, '0');
                }
            },
        );
    }

    protected function currentStatusConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrCurrentStatusConverted($this)
        );
    }

    protected function currentStatusBadgeConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrCurrentStatusBadgeConverted($this)
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator -- */
    public static function attrCurrentStatusConverted($withdrawal): string
    {
        return match ($withdrawal->current_status) {
            'requested' => 'Permintaan Diajukan',
            'seen' => 'Permintaan Dilihat',
            'denied' => 'Permintaan Ditolak',
            'confirmed' => 'Permintaan Dikonfirmasi',
            'sent' => 'Uang Telah Diberikan',
            'not_received' => 'Uang Belum Diterima',
            'done' => 'Penarikan Selesai',
            'canceled' => 'Penarikan Dibatalkan',
        };
    }

    public static function attrCurrentStatusBadgeConverted($withdrawal): string
    {
        return "<span class='badge rounded-pill bg-label-".self::mappingCurrentStatusColor($withdrawal->current_status)."'><i class='".self::mappingCurrentStatusIcon($withdrawal->current_status)." align-middle me-1'></i>".self::attrCurrentStatusConverted($withdrawal).'</span>';
    }

    public static function mappingCurrentStatusColor($status): string
    {
        return match ($status) {
            'requested' => 'secondary',
            'seen' => 'info',
            'denied', 'canceled' => 'danger',
            'confirmed' => 'primary',
            'not_received' => 'warning',
            'sent' => 'dark',
            'done' => 'success',
        };
    }

    public static function mappingCurrentStatusIcon($status, $font = 'boxicon'): string
    {
        return match ($status) {
            'requested' => $font == 'boxicon' ? 'bx bxs-send' : 'fa-solid fa-paper-plane',
            'seen' => $font == 'boxicon' ? 'bx bx-show-alt' : 'fa-solid fa-eye',
            'denied' => $font == 'boxicon' ? 'bx bx-x-circle' : 'fa-regular fa-circle-xmark',
            'confirmed' => $font == 'boxicon' ? 'bx bx-check-circle' : 'fa-regular fa-circle-check',
            'sent' => $font == 'boxicon' ? 'bx bx-money' : 'fa-solid fa-money-bill-wave',
            'not_received' => $font == 'boxicon' ? 'bx bx-error' : 'fa-solid fa-triangle-exclamation',
            'done' => $font == 'boxicon' ? 'bx bx-check-double' : 'fa-solid fa-check-double',
            'canceled' => $font == 'boxicon' ? 'bx bx-x' : 'fa-solid fa-x',
        };
    }

    /* -- pendamping accessor & mutator -- */

    /* -- relation -- */
    public function statuses()
    {
        return $this->hasMany(WithdrawalStatus::class);
    }

    public function partner()
    {
        return $this->belongsTo(User::class, 'partner_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }
    /* -- relation -- */

    /* -- function -- */
    public static function available_status()
    {
        return [
            1 => 'requested',
            2 => 'seen',
            3 => 'denied',
            4 => 'confirmed',
            5 => 'sent',
            6 => 'not_received',
            7 => 'done',
            8 => 'canceled',
        ];
    }

    public static function statusSearchFilter($search)
    {
        $status = [];
        if (Str::contains('Permintaan Diajukan', $search, true)) {
            $status = ['requested'];
        }
        if (Str::contains('Permintaan Dilihat', $search, true)) {
            $status = ['seen'];
        }
        if (Str::contains('Permintaan Ditolak', $search, true)) {
            $status = ['denied'];
        }
        if (Str::contains('Permintaan Dikonfirmasi', $search, true)) {
            $status = ['confirmed'];
        }
        if (Str::contains('Uang Telah Diberikan', $search, true)) {
            $status = ['sent'];
        }
        if (Str::contains('Uang Belum Diterima', $search, true)) {
            $status = ['not_received'];
        }
        if (Str::contains('Transaksi Selesai', $search, true)) {
            $status = ['done'];
        }
        if (Str::contains('Transaksi Dibatalkan', $search, true)) {
            $status = ['canceled'];
        }
        if (Str::contains('Permintaan', $search, true)) {
            $status = ['requested', 'seen', 'denied', 'confirmed'];
        }
        if (Str::contains('Uang', $search, true)) {
            $status = ['sent', 'not_received'];
        }
        if (Str::contains('Penarikan', $search, true)) {
            $status = ['done', 'canceled'];
        }

        return $status;
    }

    private static function fetch($args = [])
    {
        $amount = $args->selected_nominal ?? 0;
        if ($amount == 0 || $amount == '') {
            $amount = Str::replace('.', '', $args->nominal);
        }

        return [
            'amount' => (int) $amount,
            'partner_id' => auth()->user()->partner_id,
            'customer_id' => auth()->id(),
            'current_status' => $args->status ?? 'requested',
            'note' => $args->note ?? null,
        ];
    }

    private static function fetchStatus($args, $id)
    {
        if (in_array($args->status, self::available_status())) {
            return (object) [
                'detail' => ['current_status' => $args->status],
                'withdrawal_statuses' => [
                    'name' => $args->status,
                    'withdrawal_id' => $id,
                ],
            ];
        } else {
            throw new Exception('Status baru tidak dikenali!', 422);
        }
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);

        if ($request['amount'] > auth()->user()->balance) {
            throw new Exception('Saldo Anda kurang!', 422);
        }

        $request['code'] = now();

        return self::create($request);
    }

    public function ubah($params = [])
    {
        $request = self::fetch((object) $params);

        return $this->update($request);
    }

    public function ubahStatus($params = [])
    {
        $request = self::fetchStatus((object) $params, $this->id);
        $this->update($request->detail);

        return WithdrawalStatus::create($request->withdrawal_statuses);
    }

    public function hapus()
    {
        return $this->delete();
    }

    public static function report($params = [])
    {
        $request = collect($params);

        return DB::table('withdrawals')->join('users as customer', 'withdrawals.customer_id', 'customer.id')
        ->join('users as partner', 'withdrawals.partner_id', 'partner.id')
        ->when(isset($request['search']['value']), function ($query) use ($request) {
            $query->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('code', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('partner.name', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('customer.name', 'like', '%'.$request['search']['value'].'%')
                        ->orWhere('withdrawals.amount', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', $request['search']['value']).'%')
                        ->orWhere(DB::raw("DATE_FORMAT(withdrawals.created_at, '%d/%m/%y %H:%i')"), 'like', '%'.$request['search']['value'].'%');
                });
            });
        })
        ->when(isset($request['customerId']), function ($query) use ($request) {
            $query->where('withdrawals.customer_id', $request['customerId']);
        })
        ->when(isset($request['partnerId']), function ($query) use ($request) {
            $query->where('withdrawals.partner_id', $request['partnerId']);
        })
        ->when(isset($request['startDate']), function ($query) use ($request) {
            $query->whereDate('withdrawals.created_at', '>=', $request['startDate']);
        })
        ->when(isset($request['endDate']), function ($query) use ($request) {
            $query->whereDate('withdrawals.created_at', '<=', $request['endDate']);
        })
        ->where(['withdrawals.current_status' => 'done'])->whereNull('withdrawals.deleted_at')
        ->select(
            'withdrawals.id',
            'withdrawals.code',
            'withdrawals.amount',
            'customer.name as customer_name',
            'partner.name as partner_name',
            'withdrawals.created_at as time',
            DB::Raw('0 as is_invoice'),
        );
    }

    public static function index(mixed $params)
    {
        return self::when(auth()->user()->role == 'partner', function ($query) {
            $query->where('partner_id', auth()->id());
        })
        ->when(auth()->user()->role == 'customer', function ($query) {
            $query->where('customer_id', auth()->id());
        })
        ->when($params->search, function ($query) use ($params) {
            $query->where(function ($query) use ($params) {
                $status = Withdrawal::statusSearchFilter($params->search);
                $query->where('code', 'like', '%'.$params->search.'%')
                    ->when(auth()->user()->role == 'partner', function ($query) use ($params) {
                        $query->orWhereHas('customer', function ($query) use ($params) {
                            $query->where(function ($query) use ($params) {
                                $query->where('name', 'like', '%'.$params->search.'%')
                                      ->orWhere('address', 'like', '%'.$params->search.'%');
                            });
                        });
                    })
                    ->when(auth()->user()->role == 'customer', function ($query) use ($params) {
                        $query->orWhereHas('partner', function ($query) use ($params) {
                            $query->where(function ($query) use ($params) {
                                $query->where('name', 'like', '%'.$params->search.'%')
                                      ->orWhere('address', 'like', '%'.$params->search.'%');
                            });
                        });
                    })
                    ->orWhere('amount', 'like', '%'.Str::replace(['R', 'p', '.', ',', '-'], '', $params->search.'%'))
                    ->when(count($status) > 0, function ($query) use ($status) {
                        $query->orWhereIn('current_status', $status);
                    });
            });
        })
        ->when($params->order, function ($query) use ($params) {
            $query->orderBy($params->order, $params->sort);
        });
    }

    public static function cari($id)
    {
        $withdrawal = self::whereId($id)
        ->when(auth()->user()->role == 'customer', function ($query) {
            $query->where('customer_id', auth()->id());
        })->when(auth()->user()->role == 'partner', function ($query) {
            $query->where('partner_id', auth()->id());
        })
        ->first();
        if (empty($withdrawal)) {
            throw new Exception('Data tidak ditemukan!', 422);
        }

        return [
            'withdrawal' => $withdrawal->without('statuses')->first(),
            'history_status' => $withdrawal->statuses,
        ];
    }
    /* -- function -- */
}
