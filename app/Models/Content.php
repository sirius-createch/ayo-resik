<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Content extends Model
{
    use HasFactory, SoftDeletes, HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'content',
        'banner',
        'banner_size',
        'author_id',
        'published_at',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
           ->generateSlugsFrom('title')
           ->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /* -- accessor & mutator -- */
    public function bannerLink(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrBannerLink($this)
        );
    }

    public function status(): Attribute
    {
        return Attribute::make(
            get: function () {
                if ($this->published_at == null) {
                    return 'draft';
                } elseif ($this->published_at > now()) {
                    return 'scheduled';
                } else {
                    return 'published';
                }
            }
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator */
    public static function attrBannerLink($content)
    {
        if ($content->banner) {
            if (Str::startsWith($content->banner, 'http')) {
                return $content->banner;
            } else {
                return asset('images/banner_content/'.$content->banner);
            }
        }

        return 'https://ui-avatars.com/api/?size=250&name='.Str::replace(' ', '%20', $content->title);
    }
    /* -- pendamping accessor & mutator */

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    /* -- function -- */
    private static function fetch($args = [])
    {
        $published_at = null;
        if ($args->status == 'schedule') {
            $published_at = $args->published_at;
        } elseif ($args->status == 'publish') {
            $published_at = now();
        }

        return (object) [
            'content' => [
                'title' => $args->title ?? '',
                'content' => $args->content ?? '',
                'author_id' => auth()->id(),
                'published_at' => $published_at,
            ],
            'banner' => $args->banner[0] ?? null,
        ];
    }

    public static function buat($params = [])
    {
        $request = self::fetch((object) $params);
        $content = self::create($request->content);

        return $content->simpanBanner($request->banner);
    }

    public function ubah($params = [])
    {
        $request = self::fetch((object) $params);
        $this->update($request->content);

        return $this->ubahBanner($request->banner);
    }

    public function hapus()
    {
        $this->hapusBanner();

        return $this->delete();
    }

    public function pindahBanner($image = null)
    {
        if ($image) {
            $new_name = time().'_'.str_replace(' ', '_', $image);

            $path = public_path('images/banner_content/');
            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (File::move(storage_path('tmp/').$image, $path.$new_name)) {
                return $this->update([
                    'banner' => $new_name,
                    'banner_size' => File::size($path.$new_name),
                ]);
            }
        }
    }

    public function simpanBanner($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                return $this->pindahBanner($image);
            }
        }
    }

    public function ubahBanner($image = null)
    {
        if ($image) {
            if (File::exists(storage_path('tmp/').$image)) {
                if ($this->hapusBanner()) {
                    $this->simpanBanner($image);
                }
            }
        } else {
            if ($this->hapusBanner()) {
                $this->update(['banner' => null, 'banner_size' => null]);
            }
        }
    }

    public function hapusBanner()
    {
        if ($this->banner) {
            if (File::exists(public_path('images/banner_content/').$this->banner)) {
                File::delete(public_path('images/banner_content/').$this->banner);
            }
        }

        return true;
    }
    /* -- function -- */
}
