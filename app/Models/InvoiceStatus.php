<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'invoice_id',
        'name',
    ];

    /* -- accessor & mutator -- */
    protected function nameConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrNameConverted($this)
        );
    }

    protected function nameBadgeConverted(): Attribute
    {
        return Attribute::make(
            get: fn () => self::attrNameBadgeConverted($this)
        );
    }
    /* -- accessor & mutator -- */

    /* -- pendamping accessor & mutator -- */
    public static function attrNameConverted($invoice): string
    {
        return match ($invoice->name) {
            'requested' => 'Permintaan Diajukan',
            'seen' => 'Permintaan Dilihat',
            'denied' => 'Permintaan Ditolak',
            'confirmed' => 'Permintaan Dikonfirmasi',
            'verified' => 'Sampah Diverifikasi',
            'done' => 'Transaksi Selesai',
            'canceled' => 'Transaksi Dibatalkan',
        };
    }

    public static function attrNameBadgeConverted($invoice): string
    {
        return "<span class='badge rounded-pill bg-label-".self::mappingNameColor($invoice->current_status)."'><i class='".self::mappingNameIcon($invoice->current_status)." align-middle me-1'></i>".self::attrNameConverted($invoice).'</span>';
    }

    public static function mappingNameColor($status): string
    {
        return match ($status) {
            'requested' => 'secondary',
            'seen' => 'info',
            'denied', 'canceled' => 'danger',
            'confirmed' => 'primary',
            'verified' => 'dark',
            'done' => 'success',
        };
    }

    public static function mappingNameIcon($status, $font = 'boxicon'): string
    {
        return match ($status) {
            'requested' => $font == 'boxicon' ? 'bx bxs-send' : 'fa-solid fa-paper-plane',
            'seen' => $font == 'boxicon' ? 'bx bx-show-alt' : 'fa-solid fa-eye',
            'denied' => $font == 'boxicon' ? 'bx bx-x-circle' : 'fa-regular fa-circle-xmark',
            'confirmed' => $font == 'boxicon' ? 'bx bx-check-circle' : 'fa-regular fa-circle-check',
            'verified' => $font == 'boxicon' ? 'bx bx-task' : 'fa-solid fa-clipboard-check',
            'done' => $font == 'boxicon' ? 'bx bx-check-double' : 'fa-solid fa-check-double',
            'canceled' => $font == 'boxicon' ? 'bx bx-x' : 'fa-solid fa-xmark',
        };
    }
    /* -- pendamping accessor & mutator -- */

    public function invoice()
    {
        return $this->belongsTo(invoice::class);
    }
}
