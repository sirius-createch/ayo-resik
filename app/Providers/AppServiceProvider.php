<?php

namespace App\Providers;

use App\Models\Notification;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layout.app.main', 'app.notification'], function ($view) {
            $unread = Notification::where('broadcasted_to', auth()->id())->whereNull('readed_at')->count();
            $view->with('unread', ($unread > 99 ? '99+' : $unread));
        });

        view()->composer(['layout.compro.main', 'compro.contact'], function ($view) {
            $settings = Setting::pluck('value', 'name');
            $view->with('settings', $settings);
        });
    }
}
