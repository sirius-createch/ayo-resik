<?php

namespace App\Actions\Fortify;

use App\Helpers\Sirius;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        if ($input['phone']) {
            $input['phone'] = Sirius::sanitizePhoneNumber($input['phone']);
        }

        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->where(fn ($query) => $query->whereNull('deleted_at'))],
            'phone' => ['required', 'string', 'max:255', Rule::unique('users', 'phone')->where(fn ($query) => $query->whereNull('deleted_at'))],
            'password' => $this->passwordRules(),
        ], customAttributes: [
            'name' => 'Nama',
            'phone' => 'Nomor HP',
            'email' => 'Alamat Email',
            'password' => 'Kata Sandi',
        ])->validate();

        return User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'password' => Hash::make($input['password']),
            'address' => '',
            'latitude' => 0,
            'longitude' => 0,
            'role' => 'customer',
        ]);
    }
}
