@extends('layout.app.main', ['title' => "Offline", 'back' => true, 'notification' => false])

@section('contents')
    <div class="mx-3 mb-3"><img src="{{ asset('images/assets/offline.png') }}" class="w-100" alt="Offline"></div>

    <div class="mx-3">
        <div class="row">
            <div class="col-6">
                <x-app-button data-back-button class="w-100 mb-2"><i class="fa-solid fa-arrow-left me-2"></i>Kembali</x-app-button>
            </div>
            <div class="col-6">
                <x-app-button onclick="location.reload(true)" class="w-100"><i class="fa-solid fa-arrows-rotate me-2"></i>Refresh</x-app-button>
            </div>
        </div>
    </div>
@endsection
