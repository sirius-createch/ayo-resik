<!DOCTYPE HTML>
<html lang="id">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>{{ $title }} | App - Ayoresik</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/fonts/css/all.min.css') }}">
    {{-- <script src="https://kit.fontawesome.com/740dcf2f7e.js" crossorigin="anonymous"></script> --}}

    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/highlights/highlight_green.css') }}" class="page-highlight" >
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/custom.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
    @stack('css')

    <!-- PWA Configuration -->
    @laravelPWA
</head>
<body class="theme-light" data-highlight="highlight-green">
    <script> function documentReady() { } </script>

    <div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

    <div id="page">
        <div class="header header-fixed header-logo-center header-auto-show">
            <span href="index.html" class="header-title fw-bold font-18 color-phone">{!! $header ?? $headerSticky ?? $title !!}</span>
            @isset ($back) <a href="@if (is_bool($back)) {{ '#' }} @else {{ $back }} @endif" @if (is_bool($back)) data-back-button @endif class="header-icon header-icon-1 color-phone"><i class="fa-solid fa-chevron-left"></i></a> @endisset
            <div>
                @isset ($notification) @else <a href="{{ route('app.notification.index') }}" class="header-icon header-icon-3 color-phone"><i class="fa-solid fa-bell"></i>@if($unread != 0)<span class="position-absolute badge rounded-pill bg-danger">{{ $unread }}</span>@endif</a> @endisset
                <button class="header-icon header-icon-4 color-phone" data-menu="sheet-logout-confirmation"><i class="fa-solid fa-right-from-bracket"></i></button>
            </div>
        </div>

        @if(Route::is('app.partner.*') && (!isset($footer) || (isset($footer) && $footer)))
            <div id="footer-bar" class="footer-bar-6">
                <a href="{{ route('app.partner.orders.index') }}" class="@if(Route::is('app.partner.orders.*')) active-nav color-green-dark @endif"><i class="fa-solid fa-cart-flatbed"></i><span>Pesanan</span><em></em></a>
                <a href="{{ route('app.partner.withdrawals.index') }}" class="@if(Route::is('app.partner.withdrawals.*')) active-nav color-green-dark @endif"><i class="fa-solid fa-money-bill-wave"></i><span>Tarik Tunai</span><em></em></a>
                <a href="{{ route('app.partner.home') }}" class="circle-nav @if(Route::is('app.partner.home')) active-nav color-green-dark @endif"><i class="fa-solid fa-home"></i><span>Beranda</span><strong><u></u></strong></a>
                <a href="{{ route('app.partner.reports.index') }}" class="@if(Route::is('app.partner.reports.index')) active-nav color-green-dark @endif"><i class="fa-solid fa-clipboard-list"></i><span>Laporan</span><em></em></a>
                <a href="{{ route('app.partner.profile.index') }}" class="@if(Route::is('app.partner.profile.*')) active-nav color-green-dark @endif"><i class="fa-solid fa-user"></i><span>Profil</span><em></em></a>
            </div>
        @elseif(Route::is('app.customer.*') && (!isset($footer) || (isset($footer) && $footer)))
            <div id="footer-bar" class="footer-bar-1 shadow-sm" style="border-radius: 25px 25px 0 0;">
                <a href="{{ route('app.customer.home') }}" class="@if(Route::is('app.customer.home')) active-nav color-green-dark @endif"><i class="fa-solid fa-home"></i><span>Beranda</span><em></em></a>
                <a href="{{ route('app.customer.orders.index') }}" class="@if(Route::is('app.customer.orders.*') && !Route::is('app.customer.orders.create')) active-nav color-green-dark @endif"><i class="fa-solid fa-clipboard-list"></i><span>Pesanan</span><em></em></a>
                <a href="{{ route('app.customer.orders.create') }}" class="@if(Route::is('app.customer.orders.create')) active-nav color-green-dark @endif"><img src="{{ asset('images/assets/LogoApps.jpg') }}" alt="Icon Setor" style="margin-top: -45px; border-radius: 100%; width: 80%"><span>Setor</span><strong><u></u></strong></a>
                <a href="{{ route('app.notification.index') }}" class="@if(Route::is('app.notification.index')) active-nav color-green-dark @endif"><i class="fa-solid fa-bell"></i><span>Notifikasi</span><em></em></a>
                <a href="{{ route('app.customer.profile.index') }}" class="@if(Route::is('app.customer.profile.*')) active-nav color-green-dark @endif"><i class="fa-solid fa-user"></i><span>Profil</span><em></em></a>
            </div>
        @endif

        @yield('footer')

        <div class="page-title page-title-fixed d-flex justify-content-between align-items-center">
            <div class="d-flex align-items-center">
                @isset ($back) <a href="@if (is_bool($back)) {{ '#' }} @else {{ $back }} @endif" @if (is_bool($back)) data-back-button @endif class="ms-3 page-title-icon shadow-xl bg-theme color-phone"><i class="fa-solid fa-chevron-left"></i></a> @endisset
                <div class="@isset ($back) m-0 @else ms-4 mb-0 @endisset h4 fw-bold color-phone">{!! $header ?? $headerTop ?? $title !!}</div>
            </div>
            <span class="d-flex">
                @isset ($notification) <a style="width: 38px"></a> @else <a href="{{ route('app.notification.index') }}" class="page-title-icon shadow-xl bg-theme color-phone"><i class="fa-solid fa-bell"></i>@if($unread != 0)<span class="position-absolute badge rounded-pill bg-danger">{{ $unread }}</span>@endif</a> @endisset
                <button class="page-title-icon shadow-xl bg-theme color-phone" data-menu="sheet-logout-confirmation"><i class="fa-solid fa-right-from-bracket"></i></button>
            </span>
        </div>

        <div class="page-title-clear"></div>

        <div class="page-content">
            @yield('contents')
            @stack('js')
        </div>

        @yield('sheets')

        <x-app-sheet id="sheet-logout-confirmation" title="Konfirmasi">
            <div class="mx-3 text-center">
                <div class="m-5 fw-bold">Ingin keluar dari akun Anda?</div>
                <div>
                    <x-app-button class="me-2 close-menu"><i class="fa-solid fa-xmark me-2"></i>Tidak</x-app-button>
                    <x-app-button color="bg-red-dark" class="me-2" id="logout"><i class="fa-solid fa-check me-2"></i>Ya</x-app-button>
                </div>
            </div>
        </x-app-sheet>

        <form action="{{ route('logout') }}" method="post" id="form-logout">
            @csrf
        </form>

        <!-- Install Prompt for Android -->
        <div id="menu-install-pwa-android" class="menu menu-box-bottom rounded-m"
            data-menu-height="380"
            data-menu-effect="menu-parallax">
            <img class="mx-auto mt-4 rounded-m" src="{{ asset('images/assets/favicons/icon-128x128.png') }}" alt="img" width="90">
            <h4 class="text-center mt-4 mb-2">Install Ayoresik!</h4>
            <p class="text-center boxed-text-xl">
                Install Ayoresik ke ponsel Anda! Anda dapat dengan mudah membuka aplikasi kami dari homescreen!
            </p>
            <div class="boxed-text-l">
                <a href="#" class="pwa-install mx-auto btn btn-m font-600 bg-highlight">Install Aplikasi</a>
                <a href="#" class="pwa-dismiss close-menu btn-full mt-3 pt-2 text-center text-uppercase font-600 color-red-light font-12">Nanti saja</a>
            </div>
        </div>

        <!-- Install instructions for iOS -->
        <div id="menu-install-pwa-ios"
            class="menu menu-box-bottom rounded-m"
            data-menu-height="350"
            data-menu-effect="menu-parallax">
            <div class="boxed-text-xl top-25">
                <img class="mx-auto mt-4 rounded-m" src="{{ asset('images/assets/favicons/icon-128x128.png') }}" alt="img" width="90">
                <h4 class="text-center mt-4 mb-2">Install Ayoresik!</h4>
                <p class="text-center ms-3 me-3">
                    Install Ayoresik ke ponsel Anda! Anda dapat dengan mudah membuka aplikasi kami dari homescreen! Buka menu di browser Safari Anda, dan pilih "Add to Home Screen".
                </p>
                <a href="#" class="pwa-dismiss close-menu btn-full mt-3 text-center text-uppercase font-900 color-red-light opacity-90 font-110">Nanti saja</a>
            </div>
        </div>
    </div>

    <!-- Main Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('themes/app/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/app/js/custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-element-bundle.min.js"></script>

    <!-- Custom JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/my.js') }}"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;

        @if (!Route::is('app.offline') && auth()->user())
            if (window.localStorage.getItem('loggedInUser') == null) {
                window.localStorage.setItem('loggedInUser', "{{ Crypt::encrypt(auth()->user()->id) }}")
            }
            console.log("Logged in as:", window.localStorage.getItem('loggedInUser'));
        @elseif (!auth()->user())
            window.location.href = "{{ route('app.login') }}"
        @endif

        $('a').click(function () {
            if ($(this).attr('href') != "#") {
                var preloader = document.getElementById('preloader')
                if(preloader){preloader.classList.remove('preloader-hide');}
            }
        })

        $("#logout").click(function () {
            window.localStorage.removeItem('loggedInUser');
            document.getElementById('form-logout').submit()
        })
    </script>

    @if (!Route::is('app.offline'))
    <script type="module">
        // Import the functions you need from the SDKs you need
        import { initializeApp } from "https://www.gstatic.com/firebasejs/9.9.2/firebase-app.js";
        import { getMessaging, getToken } from "https://www.gstatic.com/firebasejs/9.9.2/firebase-messaging.js";
        // TODO: Add SDKs for Firebase products that you want to use
        // https://firebase.google.com/docs/web/setup#available-libraries

        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        const firebaseConfig = {
            apiKey: "{{ config('app.firebase.api_key') }}",
            authDomain: "{{ config('app.firebase.auth_domain') }}",
            projectId: "{{ config('app.firebase.project_id') }}",
            storageBucket: "{{ config('app.firebase.storage_bucket') }}",
            messagingSenderId: "{{ config('app.firebase.messaging_sender_id') }}",
            appId: "{{ config('app.firebase.app_id') }}",
            measurementId: "{{ config('app.firebase.measurement_id') }}"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const messaging = getMessaging(app);

        navigator.serviceWorker.register('/serviceworker.js').then(registration => {
            getToken(messaging, { vapidKey: "{{ config('app.firebase.key_pair') }}", serviceWorkerRegistration: registration }).then((deviceToken) => {
                if (deviceToken) {
                    var latestToken = "{{ auth()->user()->deviceToken ?? '' }}";
                    if (deviceToken != latestToken) {
                        const data = new FormData()
                        data.append('_method', 'post')
                        data.append('_token', "{{ csrf_token() }}")
                        data.append('device_token', deviceToken)
                        ajaxPost("{{ route('app.store-device-token') }}", data, null, null, function (errorParams) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Notifikasi tidak dapat dikirim!',
                                text: "Terjadi masalah saat menyimpan informasi perangkat. Masalah: " + errorParams.message
                            })
                        }, false);
                    }
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Izin notifikasi tidak diberikan!',
                        text: "Harap berikan izin notifikasi pada aplikasi kami agar Anda dapat menerima notifikasi terkait jalannya transaksi."
                    })
                }
            }).catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
            });
        })
    </script>
    @endif
</body>
</html>
