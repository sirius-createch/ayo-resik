<!DOCTYPE HTML>
<html lang="id">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>{{ $title }} | App - Ayoresik</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/fonts/css/all.min.css') }}">
    {{-- <script src="https://kit.fontawesome.com/740dcf2f7e.js" crossorigin="anonymous"></script> --}}

    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/app/css/highlights/highlight_green.css') }}" class="page-highlight" >

    <!-- Custom CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
    @stack('css')

    <!-- PWA Configuration -->
    @laravelPWA
</head>
<body class="theme-light">
    <div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

    <div id="page">
        <div class="page-content pb-0">
            <div class="card rounded-0 mb-0" data-card-height="cover-full">
                <div class="card-center">
                    {{-- Title --}}
                    <div class="text-center">
                        <span class="color-white scale-box d-inline-block rounded-s border-0" style="width: 70%; max-width: 300px;">
                            <img src="{{ asset('images/assets/logo.png') }}" class="w-100" alt="Logo Ayoresik">
                        </span>
                    </div>

                    {{-- Contents --}}
                    @yield('contents')

                    <x-app-form action="{{ route('app.auto-login') }}" id="form-auto-login" method="post">
                        <input type="hidden" name="loggedInAs" id="logged-in-as">
                    </x-app-form>
                </div>
            </div>
        </div>
    </div>

    <!-- Main Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('themes/app/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/app/js/custom.js') }}"></script>

    <!-- Custom JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript" src="{{ asset('js/my.js') }}"></script>
    @stack('js')

    <script>
        if (window.localStorage.getItem('loggedInUser') != null && {{ request()->has('loginFailed') ? 'true' : 'false' }}) {
            $("#logged-in-as").val(window.localStorage.getItem('loggedInUser'))
            $("#form-auto-login").submit()
        }

        $('a').click(function () {
            if ($(this).attr('href') != "#") {
                var preloader = document.getElementById('preloader')
                if(preloader){preloader.classList.remove('preloader-hide');}
            }
        })
    </script>
</body>
</html>
