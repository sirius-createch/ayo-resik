<!DOCTYPE html>
<html lang="id" class="light-style customizer-hide" dir="ltr">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>{{ $title }} | Admin - Ayoresik</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('images/assets/favicons/icon-512x512.png') }}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('themes/admin/fonts/boxicons/boxicons.css') }}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('themes/admin/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('themes/admin/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('themes/admin/css/theme-default.css') }}" class="template-customizer-theme-css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('themes/admin/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" />

    <!-- Page CSS -->
    @stack('css')
    <link rel="stylesheet" href="../assets/vendor/css/pages/page-auth.css" />

    <!-- Helpers -->
    <script src="{{ asset('themes/admin/js/helpers.js') }}"></script>

    <!-- Config  -->
    <script src="{{ asset('themes/admin/js/config.js') }}"></script>
</head>
<body>
    <div class="container-xxl">
      <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner">
            <x-admin-card>
                <div class="app-brand justify-content-center">
                    <a href="index.html" class="app-brand-link p-3">
                        <span class="app-brand-logo w-100 p-5">
                            <img src="{{ asset('images/assets/logo.png') }}" alt="Logo Ayoresik" class="w-100">
                        </span>
                    </a>
                </div>
                @yield('content')
            </x-admin-card>
        </div>
      </div>
    </div>

    <!-- Core JS -->
    <script src="{{ asset('themes/admin/plugins/jquery/jquery.js') }}"></script>
    <script src="{{ asset('themes/admin/plugins/popper/popper.js') }}"></script>
    <script src="{{ asset('themes/admin/js/bootstrap.js') }}"></script>
    <script src="{{ asset('themes/admin/plugins/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('themes/admin/js/menu.js') }}"></script>

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="{{ asset('themes/admin/js/main.js') }}"></script>

    <!-- Page JS -->
    @stack('js')

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>
