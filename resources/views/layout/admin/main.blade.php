<!DOCTYPE html>
<!-- beautify ignore:start -->
<html lang="id" class="light-style layout-menu-fixed" dir="ltr">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>{{ $title }} | Admin - Ayoresik</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('images/assets/favicons/icon-512x512.png') }}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('themes/admin/fonts/boxicons/boxicons.css') }}" />
    <script src="https://kit.fontawesome.com/740dcf2f7e.js" crossorigin="anonymous"></script>

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('themes/admin/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('themes/admin/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('themes/admin/css/theme-default.css') }}" class="template-customizer-theme-css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('themes/admin/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <!-- Page CSS -->
    @stack('css')

    <!-- Helpers -->
    <script src="{{ asset('themes/admin/js/helpers.js') }}"></script>

    <!-- Config  -->
    <script src="{{ asset('themes/admin/js/config.js') }}"></script>
</head>
<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <!-- Menu -->
            <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
                <div class="app-brand mb-4">
                    <a href="{{ route('admin.home') }}" class="app-brand-link">
                        <span class="app-brand-logo w-100">
                            <img src="{{ asset('images/assets/logo.png') }}" alt="Logo Ayoresik" class="w-100">
                        </span>
                    </a>

                    <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                        <i class="bx bx-chevron-left bx-sm align-middle" style="font-size:1.55em!important"></i>
                    </a>
                </div>

                <div class="menu-inner-shadow mt-4"></div>

                <ul class="menu-inner py-1 pb-5">

                    <x-admin-menu route="admin.home" icon="bx bx-home-circle">Dasbor</x-admin-menu>

                    {{-- Master --}}
                    <li class="menu-header small text-uppercase"><span class="menu-header-text">Master</span></li>
                    <x-admin-menu route="admin.master.customers.index" icon="bx bxs-user-account">Customer</x-admin-menu>
                    <x-admin-menu route="admin.master.partners.index" icon="bx bxs-user-badge">Partner</x-admin-menu>
                    <x-admin-menu route="admin.master.products.index" icon="bx bxs-package">Produk</x-admin-menu>
                    {{-- <x-admin-menu route="admin.master.payments.index" icon="bx bxs-wallet">Metode Pembayaran</x-admin-menu> --}}

                    {{-- Layanan --}}
                    <li class="menu-header small text-uppercase"><span class="menu-header-text">Layanan</span></li>
                    <x-admin-menu route="admin.services.transactions.index" icon="bx bx-recycle">Transaksi</x-admin-menu>
                    <x-admin-menu route="admin.services.withdrawals.index" icon="bx bx-money">Penarikan Saldo</x-admin-menu>

                    {{-- Laporan --}}
                    <li class="menu-header small text-uppercase"><span class="menu-header-text">Laporan</span></li>
                    <x-admin-menu route="admin.reports.expenses" icon="bx bxs-report">Pengeluaran</x-admin-menu>
                    <x-admin-menu route="admin.reports.products" icon="bx bxs-archive-out">Produk</x-admin-menu>

                    {{-- Company Profile --}}
                    <li class="menu-header small text-uppercase"><span class="menu-header-text">Company Profile</span></li>
                    <x-admin-menu route="admin.compro.settings.index" icon="bx bxs-wrench">Pengaturan</x-admin-menu>
                    <x-admin-menu route="admin.compro.sliders.index" icon="bx bxs-image">Slider</x-admin-menu>
                    <x-admin-menu route="admin.compro.contents.index" icon="bx bxs-file-blank">Konten</x-admin-menu>
                    <x-admin-menu route="admin.compro.articles.index" icon="bx bxs-file">Artikel</x-admin-menu>
                    <x-admin-menu route="admin.compro.pages.*" icon="bx bxs-folder">
                        Halaman
                        @slot('submenus')
                            @foreach (\App\Models\Page::pluck('title', 'slug') as $slug => $page)
                                <x-admin-submenu route="admin.compro.pages.edit" :routeParams="['page' => $slug]">{{ $page }}</x-admin-submenu>
                            @endforeach
                        @endslot
                    </x-admin-menu>

                </ul>
            </aside>

            <!-- Layout container -->
            <div class="layout-page">

                <!-- Navbar -->
                <nav class="layout-navbar container-fluid navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                            <i class="bx bx-menu bx-sm"></i>
                        </a>
                    </div>

                    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                        {{-- Breadcrumb --}}
                        @php
                            $url = Request::url();
                            $titles = explode('/', $url);
                            $real_titles = [];
                            $main_title = 'Dasbor';
                            $after_admin = false;
                            foreach ($titles as $key => $title) {
                                if ($after_admin) {
                                    $title = Str::title(str_replace('-', ' ', $title));
                                    if ($key != count($titles) - 1) {
                                        $real_titles[] = $title;
                                    }
                                    else {
                                        $main_title = $title;
                                    }
                                }
                                if ($title == 'admin') $after_admin = true;
                            }
                        @endphp
                        <div class="my-3">
                            <h5 class="fw-bold my-3">
                                <span class="text-muted fw-light">
                                    @foreach ($real_titles as $title)
                                        {{ $title }} /
                                    @endforeach
                                </span> {{ $main_title }}
                            </h5>
                        </div>

                        <ul class="navbar-nav flex-row align-items-center ms-auto">
                            <!-- User -->
                            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                                    <div class="avatar">
                                        <img src="{{ auth()->user()->avatarLink }}" alt="Foto {{ auth()->user()->name }}" class="w-px-40 rounded-circle" />
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li>
                                        <a class="dropdown-item" href="{{ route('admin.profile.index') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0 me-3">
                                                    <div class="avatar">
                                                        <img src="{{ auth()->user()->avatarLink }}" alt="Foto {{ auth()->user()->name }}" class="w-px-40 rounded-circle" />
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <span class="fw-semibold d-block">{{ auth()->user()->name }}</span>
                                                    <small class="text-muted">{{ Str::title(auth()->user()->role) }}</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit()">
                                            <i class="bx bx-power-off align-middle me-2"></i>
                                            <span class="align-top">Keluar</span>
                                            <form action="{{ route('logout') }}" method="post" id="logout-form">
                                                @csrf
                                            </form>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <!-- Content wrapper -->
                <div class="content-wrapper">

                    <!-- Content  -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        @yield('content')
                    </div>

                    @yield('modal')

                    <!-- Footer -->
                    <footer class="content-footer footer bg-footer-theme">
                        <div class="container-fluid d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
                            <div class="mb-2 mb-md-0">
                                © 2022, made with ❤️ by <a href="https://themeselection.com" target="_blank" class="footer-link fw-bolder">ThemeSelection</a>
                            </div>
                            <div>
                                {{-- More Footer --}}
                            </div>
                        </div>
                    </footer>
                </div>

            </div>
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>

    <!-- Core JS -->
    <script src="{{ asset('themes/admin/plugins/jquery/jquery.js') }}"></script>
    <script src="{{ asset('themes/admin/plugins/popper/popper.js') }}"></script>
    <script src="{{ asset('themes/admin/js/bootstrap.js') }}"></script>
    <script src="{{ asset('themes/admin/plugins/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('themes/admin/js/menu.js') }}"></script>

    <!-- Vendors JS -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="http://malsup.github.io/jquery.blockUI.js"></script>

    <!-- Main JS -->
    <script src="{{ asset('themes/admin/js/main.js') }}"></script>
    <script src="{{ asset('js/my.js') }}"></script>

    <!-- Page JS -->
    @stack('js')

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>

    <script>

    </script>
</body>
</html>
