<!DOCTYPE html>
<html class="no-js" lang="id">
<head>
    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ isset($title) ? $title . " | " : '' }}Ayoresik</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/assets/favicons/icon-72x72.png') }}">

    <!-- Font -->
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/font-awesome.css') }}">
    <script src="https://kit.fontawesome.com/740dcf2f7e.js" crossorigin="anonymous"></script>

    <!-- Main Styles -->
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/sal.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/green-audio-player.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/vendor/odometer-theme-default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/compro/css/app.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('themes/compro/css/my.css') }}">
    @stack('css')
</head>
<body class="sticky-header">
    <a href="#main-wrapper" id="backto-top" class="back-to-top" style="z-index: 101">
        <i class="fa-solid fa-angles-up"></i>
    </a>

    <div id="preloader"></div>

    {{-- <div class="my_switcher d-none d-lg-block">
        <ul>
            <li title="Light Mode">
                <a href="javascript:void(0)" class="setColor light" data-theme="light">
                    <i class="fas fa-sun"></i>
                </a>
            </li>
            <li title="Dark Mode">
                <a href="javascript:void(0)" class="setColor dark" data-theme="dark">
                    <i class="fas fa-moon"></i>
                </a>
            </li>
        </ul>
    </div> --}}

    <div id="main-wrapper" class="main-wrapper">

        <!-- Header -->
        <header class="header axil-header header-style-1">
            <div id="axil-sticky-placeholder"></div>
            <div class="axil-mainmenu">
                <div class="container">
                    <div class="header-navbar">
                        <div class="header-logo">
                            <a href="{{ route('compro.welcome') }}"><img class="light-version-logo" src="{{ asset('images/assets/logo.png') }}" style="width:200px" alt="Logo Ayoresik"></a>
                            <a href="{{ route('compro.welcome') }}"><img class="dark-version-logo" src="{{ asset('images/assets/logo.png') }}" style="width:200px" alt="Logo Ayoresik"></a>
                            <a href="{{ route('compro.welcome') }}"><img class="sticky-logo" src="{{ asset('images/assets/logo.png') }}" style="width:200px" alt="Logo Ayoresik"></a>
                        </div>
                        <div class="header-main-nav">
                            <!-- Start Mainmanu Nav -->
                            <nav class="mainmenu-nav" id="mobilemenu-popup">
                                <div class="d-block d-lg-none">
                                    <div class="mobile-nav-header">
                                        <div class="mobile-nav-logo">
                                            <a href="{{ route('compro.welcome') }}">
                                                <img class="light-mode" src="{{ asset('images/assets/logo.png') }}" alt="Logo Ayoresik">
                                                <img class="dark-mode" src="{{ asset('images/assets/logo.png') }}" alt="Logo Ayoresik">
                                            </a>
                                        </div>
                                        <button class="mobile-menu-close" data-bs-dismiss="offcanvas"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                                <ul class="mainmenu">
                                    <li><a href="{{ route('compro.welcome') }}" class="@if(Route::is('compro.welcome')) active @endif">Beranda</a></li>
                                    <li><a href="{{ route('compro.about') }}" class="@if(Route::is('compro.about')) active @endif">Tentang Kami</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="{{ route('compro.services') }}" class="@if(Route::is('compro.services')) active @endif">Layanan</a>
                                        <ul class="axil-submenu">
                                            <li><a href="{{ route('compro.services') }}#pick-up">Pick Up</a></li>
                                            <li><a href="{{ route('compro.services') }}#drop-off">Drop Off</a></li>
                                            <li><a href="{{ route('compro.services') }}#event">Event</a></li>
                                            <li><a href="{{ route('compro.services') }}#company">Company</a></li>
                                            <li><a href="{{ route('compro.services') }}#edukasi">Edukasi</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('compro.partner') }}" class="@if(Route::is('compro.partner')) active @endif">Mitra</a></li>
                                    <li><a href="{{ route('compro.blog') }}" class="@if(Route::is('compro.blog')) active @endif">Blog</a></li>
                                    <li><a href="{{ route('app.login') }}">Ke Aplikasi &nbsp &nbsp<i class="fas fa-mobile-alt me-2"></i></a></li>
                                </ul>
                            </nav>
                            <!-- End Mainmanu Nav -->
                        </div>
                        <div class="header-action">
                            <ul class="list-unstyled">
                                <li class="mobile-menu-btn sidemenu-btn d-lg-none d-block">
                                    <button class="btn-wrap" data-bs-toggle="offcanvas" data-bs-target="#mobilemenu-popup">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                </li>
                                {{-- <li class="my_switcher d-block d-lg-none">
                                    <ul>
                                        <li title="Light Mode">
                                            <a href="javascript:void(0)" class="setColor light" data-theme="light">
                                                <i class="fas fa-sun"></i>
                                            </a>
                                        </li>
                                        <li title="Dark Mode">
                                            <a href="javascript:void(0)" class="setColor dark" data-theme="dark">
                                                <i class="fas fa-moon"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Konten -->
        @yield('contents')

        <!-- Footer -->
        <footer class="footer-area" style="position: relative; z-index: 100;">
            <div class="container">
                <div class="footer-main">
                    <div class="row">
                        <div class="col-xl-6 col-lg-5" data-sal="slide-right" data-sal-duration="800" data-sal-delay="100">
                            <div class="footer-widget border-end">
                                <div class="footer-newsletter">
                                    <h2 class="title"><img src="{{ asset('images/assets/logo.png') }}" style="width:200px" alt="Logo Ayoresik"></h2>
                                    <p class="mb-3">PT.AYO RESIK INDONESIA<br>{{ $settings['address'] }}</p>
                                    <p class="mb-3">Telepon / WA:<br>{{ '0'.$settings['phone'] }}</p>
                                    <p class="mb-3">Email:<br>{{ $settings['email'] }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7" data-sal="slide-left" data-sal-duration="800" data-sal-delay="100">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="footer-widget">
                                        <h6 class="widget-title">Menu</h6>
                                        <div class="footer-menu-link">
                                            <ul class="list-unstyled">
                                                <li><a href="{{ route('compro.welcome') }}">Beranda</a></li>
                                                <li><a href="{{ route('compro.about') }}">Tentang Kami</a></li>
                                                <li><a href="{{ route('compro.services') }}">Layanan</a></li>
                                                <li><a href="{{ route('compro.partner') }}">Mitra</a></li>
                                                <li><a href="{{ route('compro.blog') }}">Blog</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="footer-widget">
                                        <h6 class="widget-title">Lainnya</h6>
                                        <div class="footer-menu-link">
                                            <ul class="list-unstyled">
                                                <li><a href="{{ route('compro.contents.index') }}">Konten</a></li>
                                                <li><a href="{{ route('compro.articles.index') }}">Artikel</a></li>
                                                <li><a href="{{ route('compro.faq') }}">Pertanyaan</a></li>
                                                <li><a href="{{ route('compro.contact') }}">Kontak Kami</a></li>
                                                @foreach (\App\Models\Page::pluck('title', 'slug') as $slug => $page)
                                                    <li><a href="{{ route('compro.page', ['page' => $slug]) }}">{{ $page }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom" data-sal="slide-up" data-sal-duration="500" data-sal-delay="100">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer-copyright">
                                <span class="copyright-text">© 2022. All rights reserved by <a href="https://ayoresik.com/">PT. AYO RESIK INDONESIA</a>.</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer-bottom-link">
                                <ul class="list-unstyled">
                                    <li>Terhubung Dengan Kami</li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Main Scripts -->
    <script src="{{ asset('themes/compro/js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/odometer.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/jquery-appear.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/sal.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/js.cookie.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/jquery.style.switcher.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/tilt.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/green-audio-player.min.js') }}"></script>
    <script src="{{ asset('themes/compro/js/vendor/jquery.nav.js') }}"></script>
    <script src="{{ asset('themes/compro/js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-element-bundle.min.js"></script>

    <!-- Custom JS -->
    <script src="{{ asset('js/my.js') }}"></script>
    @stack('js')
</body>
</html>
