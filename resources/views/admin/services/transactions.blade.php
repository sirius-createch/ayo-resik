@extends('layout.admin.main', ['title' => 'Transaksi'])

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
@endpush

@section('content')
    <x-admin-card header="Daftar Transaksi">
        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>No. Invoice</th>
                    <th>Partner</th>
                    <th>Customer</th>
                    <th>Tagihan</th>
                    <th>Biaya</th>
                    <th>Berat</th>
                    <th>Status</th>
                    <th width="100px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-show" title="Detail Transaksi">
        <div class="border-bottom pb-2 mb-2">
            <b>No. Invoice:</b> <br>
            <span id="show-code">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Waktu Transaksi:</b> <br>
            <span id="show-datetime">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Partner:</b> <br>
            <span id="show-partner">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Customer:</b> <br>
            <span id="show-customer">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Total Tagihan:</b> <br>
            <span id="show-total-bill">Rp0,-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Total Biaya Layanan:</b> <br>
            <span id="show-service-charge">Rp0,-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Total Berat:</b> <br>
            <span id="show-total-weight">0</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Hari Pengambilan:</b> <br>
            <span id="show-day-taken">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Status Saat Ini:</b> <br>
            <span id="show-current-status">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <div class="pb-2 mb-1">
                <b>Alamat Pengambilan:</b> <br>
                <span id="show-address">-</span>
            </div>
            <div id="show-map" class="w-100" style="height: 200px"></div>
        </div>
        <div>
            <b>Catatan:</b> <br>
            <span id="show-note">-</span>
        </div>

        @slot('footer')
            <div class="d-flex justify-content-start w-100">
                <button class="btn btn-secondary btn-product-list me-2" data-bs-target="#modal-product-list" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxl-dropbox align-middle me-2"></i>Daftar Sampah</button>
                <button class="btn btn-secondary btn-invoice-status-histories" data-bs-target="#modal-invoice-status-histories" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-history align-middle me-2"></i>Riwayat Status</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-product-list" title="Daftar Sampah">
        <div class="pb-2 mb-2 text-center">
            <b>No. Invoice</b> <br>
            <span id="product-list-no-invoice">-</span>
        </div>
        <div class="list-group" id="product-list">
            -
        </div>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-invoice-status-histories" title="Riwayat Status">
        <div class="pb-2 mb-2 text-center">
            <b>No. Invoice</b> <br>
            <span id="invoice-status-histories-no-invoice">-</span>
        </div>
        <div class="list-group" id="invoice-status-histories">
            -
        </div>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection


@push('js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        let invoice = {}
        let table = null
        let id = null

        $(document).ready(function () {
            const columns = [
                { data: 'code', name: 'invoices.code' },
                { data: 'partner', name: 'partner.name' },
                { data: 'customer', name: 'customer.name' },
                { data: 'total_bill', name: 'invoices.total_bill', className: "text-end" },
                { data: 'service_charge', name: 'invoices.service_charge', className: "text-end" },
                { data: 'total_weight', name: 'invoices.total_weight', className: "text-end" },
                { data: 'current_status', name: 'invoices.current_status' },
            ]

            table = datatablesInit("#table", "{{ route('admin.services.transactions.index') }}", columns, [1, 'desc'], datatablesTranslate('Transaksi'))
        })

        $(document).on('click', '.btn-show', function () {
            code = $(this).data('code')

            $("#show-code").html('-')
            $("#show-datetime").html('-')
            $("#show-partner").html('-')
            $("#show-customer").html('-')
            $("#show-total-bill").html('0')
            $("#show-service-charge").html('0')
            $("#show-total-weight").html('0')
            $("#show-day-taken").html('-')
            $("#show-current-status").html('-')
            $("#show-address").html('-')
            $("#show-note").html('-')

            ajaxGet("{{ route('admin.services.transactions.show', ['transaksi' => '-code-']) }}".replace('-code-', code), "#modal-show", function (result) {
                invoice = result

                let note = result.note ?? '-';
                if (note == '') note = '-';

                $("#show-code").html(result.code)
                $("#show-datetime").html(result.datetime)
                $("#show-partner").html(result.partner)
                $("#show-customer").html(result.customer)
                $("#show-total-bill").html(result.total_bill)
                $("#show-service-charge").html(result.service_charge)
                $("#show-total-weight").html(result.total_weight)
                $("#show-day-taken").html(result.day_taken)
                $("#show-current-status").html(result.current_status)
                $("#show-address").html(result.address)
                $("#show-note").html(note)

                setTimeout(function() {
                    map.setView([result.latitude, result.longitude], 17).invalidateSize();
                    marker.setLatLng([result.latitude, result.longitude])
                }, 100);
            })
        })

        $(document).on('click', '.btn-product-list', function () {
            $("#product-list-no-invoice").html(invoice.code)
            $("#product-list").html('')

            if (invoice.products.length > 0) {
                $.each(invoice.products, function (index, product) {
                    $("#product-list").append(`
                        <div class="list-group-item">
                            <div class="user-info">
                                <div class="h6 mb-1">` + product.name + `</div>
                                <div class="row">
                                    <div class="col-md-4 d-flex justify-content-between">
                                        <small>` + product.weight + `</small>
                                        <small>&times;</small>
                                    </div>
                                    <div class="col-md-4 text-right"><small>` + product.price + `</small></div>
                                    <div class="col-md-4 text-right fw-bold"><small>` + product.total + `</small></div>
                                </div>
                            </div>
                        </div>
                    `)
                })
            } else {
                $("#product-list").html(`
                    <div class="alert alert-primary mb-0" role="alert">
                        Tidak ada sampah yang dijual pada transaksi ini.
                    </div>
                `)
            }
        })

        $(document).on('click', '.btn-invoice-status-histories', function () {
            $("#invoice-status-histories-no-invoice").html(invoice.code)
            $("#invoice-status-histories").html('')

            if (invoice.statuses.length > 0) {
                $.each(invoice.statuses, function (index, status) {
                    $("#invoice-status-histories").append(`
                        <div class="list-group-item d-flex justify-content-between align-items-center">
                            <div>
                                <i class="bx bxs-circle me-2 align-middle text-` + status.color + `" style="font-size:.3em!important"></i>
                                <span class="badge rounded-pill bg-label-` + status.color + `"><i class="` + status.icon + ` me-1 align-middle"></i>` + status.name +`</span>
                            </div>
                            <div><small>` + status.time + `</small></div>
                        </div>
                    `)
                })
            } else {
                $("#invoice-status-histories").html(`
                    <div class="alert alert-primary mb-0" role="alert">
                        Tidak ada status yang terrekam pada transaksi ini.
                    </div>
                `)
            }
        })

        // Leaflet

        var map = L.map('show-map').setView([0, 0], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([0, 0]).addTo(map)

    </script>
@endpush
