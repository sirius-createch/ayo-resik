@extends('layout.admin.main', ['title' => 'Penarikan Saldo'])

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
@endpush

@section('content')
    <x-admin-card header="Daftar Penarikan Saldo">
        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>No. Permintaan</th>
                    <th>Partner</th>
                    <th>Customer</th>
                    <th>Nominal</th>
                    <th>Status</th>
                    <th width="100px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-show" title="Detail Penarikan Saldo">
        <div class="border-bottom pb-2 mb-2">
            <b>No. Permintaan:</b> <br>
            <span id="show-code">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Waktu Permintaan:</b> <br>
            <span id="show-datetime">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Partner:</b> <br>
            <span id="show-partner">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Customer:</b> <br>
            <span id="show-customer">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nominal:</b> <br>
            Rp<span id="show-amount">0</span>,-
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Status Saat Ini:</b> <br>
            <span id="show-current-status">-</span>
        </div>
        <div>
            <b>Catatan:</b> <br>
            <span id="show-note">-</span>
        </div>

        @slot('footer')
            <div class="d-flex justify-content-start w-100">
                <button class="btn btn-secondary btn-withdrawal-status-histories" data-bs-target="#modal-withdrawal-status-histories" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-history align-middle me-2"></i>Riwayat Status</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-withdrawal-status-histories" title="Riwayat Status">
        <div class="pb-2 mb-2 text-center">
            <b>No. Permintaan</b> <br>
            <span id="withdrawal-status-histories-no-withdrawal">-</span>
        </div>
        <div class="list-group" id="withdrawal-status-histories">
            -
        </div>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection


@push('js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        let withdrawal = {}
        let table = null
        let id = null

        $(document).ready(function () {
            const columns = [
                { data: 'code', name: 'withdrawals.code' },
                { data: 'partner', name: 'partner.name' },
                { data: 'customer', name: 'customer.name' },
                { data: 'amount', name: 'withdrawals.amount', className: "text-end" },
                { data: 'current_status', name: 'withdrawals.current_status' },
            ]

            table = datatablesInit("#table", "{{ route('admin.services.withdrawals.index') }}", columns, [1, 'desc'], datatablesTranslate('Penarikan saldo'))
        })

        $(document).on('click', '.btn-show', function () {
            code = $(this).data('code')

            $("#show-code").html('-')
            $("#show-datetime").html('-')
            $("#show-partner").html('-')
            $("#show-customer").html('-')
            $("#show-amount").html('0')
            $("#show-current-status").html('-')
            $("#show-note").html('-')

            ajaxGet("{{ route('admin.services.withdrawals.show', ['penarikan_saldo' => '-code-']) }}".replace('-code-', code), "#modal-show", function (result) {
                withdrawal = result

                let note = result.note ?? '-';
                if (note == '') note = '-';

                $("#show-code").html(result.code)
                $("#show-datetime").html(result.datetime)
                $("#show-partner").html(result.partner)
                $("#show-customer").html(result.customer)
                $("#show-amount").html(numberFormat(result.amount))
                $("#show-current-status").html(result.current_status)
                $("#show-note").html(note)

                setTimeout(function() {
                    map.setView([result.latitude, result.longitude], 10).invalidateSize();
                    marker.setLatLng([result.latitude, result.longitude])
                }, 100);
            })
        })

        $(document).on('click', '.btn-withdrawal-status-histories', function () {
            $("#withdrawal-status-histories-no-withdrawal").html(withdrawal.code)
            $("#withdrawal-status-histories").html('')

            if (withdrawal.statuses.length > 0) {
                $.each(withdrawal.statuses, function (index, status) {
                    $("#withdrawal-status-histories").append(`
                        <div class="list-group-item d-flex justify-content-between align-items-center">
                            <div>
                                <i class="bx bxs-circle me-2 align-middle text-` + status.color + `" style="font-size:.3em!important"></i>
                                <span class="badge rounded-pill bg-label-` + status.color + `"><i class="` + status.icon + ` me-1 align-middle"></i>` + status.name +`</span>
                            </div>
                            <div><small>` + status.time + `</small></div>
                        </div>
                    `)
                })
            } else {
                $("#withdrawal-status-histories").html(`
                    <div class="alert alert-primary mb-0" role="alert">
                        Tidak ada status yang terrekam pada permintaan ini.
                    </div>
                `)
            }
        })
    </script>
@endpush
