@extends('layout.admin.main', ['title' => 'Produk'])

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Daftar Produk">
        @slot('headerAction')
            <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal-create"><i class="bx bx-plus align-middle me-2"></i>Tambah Produk</button>
        @endslot

        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Harga Dasar</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-create" title="Tambah Produk">
        <x-admin-form id="form-create" method="post">
            <x-admin-form-input-file id="create_image" formId="form-create" name="image" label="Gambar" class="mb-3" accepted="image/*" />
            <x-admin-form-input id="create-name" name="name" label="Nama" class="mb-3" required />
            <x-admin-form-input id="create-base-price" name="base_price" label="Harga Dasar" class="mb-3" classInput="to-rupiah" prefix="Rp" suffix=",-" required />
        </x-admin-form>

        @slot('footer')
            <button id="create-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-show" title="Detail Produk">
        <div class="pb-2 mb-2 text-center">
            <a href="" data-fancybox style="cursor: zoom-in">
                <img src="" alt="Foto " id="show-image" class="w-25 rounded" style="object-fit: cover;">
            </a>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nama:</b> <br>
            <span id="show-name">-</span>
        </div>
        <div class="pb-2 mb-1">
            <b>Harga Dasar:</b> <br>
            Rp<span id="show-base-price">0</span>,-
        </div>

        @slot('footer')
            <button class="btn btn-warning btn-edit" data-bs-target="#modal-edit" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-edit align-middle me-2"></i>Ubah</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-edit" title="Ubah Produk">
        <x-admin-form id="form-edit" method="put">
            <x-admin-form-input-file id="edit_image" formId="form-edit" name="image" label="Gambar" class="mb-3" accepted="image/*" />
            <x-admin-form-input id="edit-name" name="name" label="Nama" class="mb-3" required />
            <x-admin-form-input id="edit-base-price" name="base_price" label="Harga Dasar" class="mb-3" classInput="to-rupiah" prefix="Rp" suffix=",-" required />
        </x-admin-form>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
                <button id="edit-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection


@push('js')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        let product = {}
        let table = null
        let id = null
        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            const columns = [
                { data: 'image', orderable: false, searchable: false, className: "text-center" },
                { data: 'name' },
                { data: 'base_price', className: "text-end" },
            ]

            table = datatablesInit("#table", "{{ route('admin.master.products.index') }}", columns, [2, 'asc'], datatablesTranslate('Produk'), function () {
                Fancybox.bind('[data-fancybox]', {})
            })
        })

        $(document).on('click', '.btn-show', function () {
            id = $(this).data('id')

            $("#show-image").attr('src', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-image").parent().attr('href', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-name").html('-')
            $("#show-base-price").html('-')

            ajaxGet("{{ route('admin.master.products.show', ['produk' => '-id-']) }}".replace('-id-', id), "#modal-show", function (result) {
                product = result

                $("#show-image").attr('src', result.image_link)
                $("#show-image").parent().attr('href', result.image_link)
                $("#show-name").html(result.name)
                $("#show-base-price").html(numberFormat(result.base_price))
                let images = null
                if(result.image){
                    images = [{name : result.image,size : result.image_size}]
                }
                dropzonePreview(dz_edit_image, images, '{{ asset("images/product") }}', '#form-edit', 'image')
            })
        })

        $(document).on('click', '.btn-edit', function () {
            $("#edit-name").val(product.name)
            $("#edit-base-price").val(numberFormat(product.base_price))
        })

        $(document).on('click', '.btn-delete', function () {
            // Delete
            let button = $(this)
            id = button.data('id')
            let name = button.data('name')

            DeleteConfirm.fire({
                title: "Yakin ingin menghapus produk " + name + "?",
            }).then((result) => {
                if (result.isConfirmed) {
                    loading()
                    const formData = new FormData();
                    formData.append('id',id)
                    formData.append('_method','delete')
                    formData.append('_token','{{ csrf_token() }}')
                    ajaxPost("{{ route('admin.master.products.destroy',['produk' => '-id-']) }}".replace('-id-', id),formData,null,function(){
                        table.ajax.reload()
                    })
                }
            })
        })

        $("#create-save").click(function() {
            // Save
            loading()
            const formData = new FormData(document.getElementById('form-create'));
            ajaxPost("{{ route('admin.master.products.store') }}",formData,'#modal-create',function(){
                table.ajax.reload()

                dropzonePreview(dz_create_image, null, '{{ asset("images/product") }}', '#form-create', 'image')

                $("#create-name").val('')
                $("#create-base-price").val('')
            })
        })
        $("#edit-save").click(function() {
            // Edit
            loading()
            const formData = new FormData(document.getElementById('form-edit'));
            ajaxPost("{{ route('admin.master.products.update',['produk' => '-id-']) }}".replace('-id-', id),formData,'#modal-edit',function(){
                table.ajax.reload()
            })
        })
    </script>
@endpush
