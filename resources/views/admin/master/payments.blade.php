@extends('layout.admin.main', ['title' => 'Metode Pembayaran'])

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
@endpush

@section('content')
    <x-admin-card header="Daftar Metode Pembayaran">
        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th width="20px">No.</th>
                    <th>Nama</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@push('js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            const columns = [
                { data: 'name' },
            ]

            datatablesInit("#table", "{{ route('admin.master.payments.index') }}", columns, [1, 'asc'], datatablesTranslate('Metode pembayaran'))
        })
    </script>
@endpush
