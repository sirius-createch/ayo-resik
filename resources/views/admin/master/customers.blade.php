@extends('layout.admin.main', ['title' => 'Customer'])

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Daftar Customer">
        <x-slot name="headerAction">
            <x-admin-button size="sm" icon="fa-solid fa-file-excel" face="success" link="{{ route('admin.master.customers.download') }}">Unduh Excel</x-admin-button>
        </x-slot>
        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>Avatar</th>
                    <th>Nama</th>
                    <th>Alamat Email</th>
                    <th>Nomor HP</th>
                    <th>Partner</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-show" title="Detail Customer">
        <div class="pb-2 mb-2 text-center">
            <a href="" data-fancybox style="cursor: zoom-in">
                <img src="" alt="Foto " id="show-avatar" class="w-25 rounded" style="object-fit: cover;">
            </a>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nama</b>: <br>
            <span id="show-name">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Alamat Email</b>: <br>
            <span id="show-email">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nomor HP</b>: <br>
            <span id="show-phone">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Partner</b>: <br>
            <span id="show-partner">-</span>
        </div>
        <div class="pb-2 mb-1">
            <b>Alamat</b>: <br>
            <span id="show-address">-</span>
        </div>
        <div id="show-map" class="w-100" style="height: 200px"></div>

        @slot('footer')
            <button class="btn btn-warning btn-edit-password" data-bs-target="#modal-edit-password" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-edit align-middle me-2"></i>Ubah Kata Sandi</button>
            <button class="btn btn-warning btn-edit" data-bs-target="#modal-edit" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-edit align-middle me-2"></i>Ubah Data</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-edit" title="Ubah Customer">
        <x-admin-form id="form-edit" method="put">
            <x-admin-form-input-file id="edit_avatar" formId="form-edit" name="avatar" label="Avatar" class="mb-3" accepted="image/*" />
            <x-admin-form-input id="edit-name" name="name" label="Nama" class="mb-3" required />
            <x-admin-form-input id="edit-email" name="email" label="Alamat Email" class="mb-3" type="email" required />
            <x-admin-form-input id="edit-phone" name="phone" label="Nomor HP" class="mb-3" prefix="+62" required />
            <x-admin-form-input id="edit-address" name="address" label="Alamat" required />
        </x-admin-form>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
                <button id="edit-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-edit-password" title="Ubah Kata Sandi">
        <x-admin-form id="form-edit-password" method="put">
            <x-admin-form-input id="edit-password" name="password" type="password" label="Kata Sandi Baru" required>
                <x-slot name="labelBadge">
                    <span class="cursor-pointer text-warning" id="generate-password"><i class="fa-solid fa-gear me-1"></i>Generate Kata Sandi</span>
                </x-slot>
            </x-admin-form-input>
        </x-admin-form>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
                <button id="edit-password-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        let customer = {}
        let table = null
        let id = null
        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            const columns = [
                { data: 'avatar', orderable: false, searchable: false, className: "text-center" },
                { data: 'name', name: 'users.name' },
                { data: 'email', name: 'users.email' },
                { data: 'phone', name: 'users.phone' },
                { data: 'partner', name: 'partner.name' },
            ]

            table = datatablesInit("#table", "{{ route('admin.master.customers.index') }}", columns, [2, 'asc'], datatablesTranslate('Customer'), function () {
                Fancybox.bind('[data-fancybox]', {})
            })
        })

        $(document).on('click', '.btn-show', function() {
            id = $(this).data('id')

            $("#show-avatar").attr('src', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-avatar").parent().attr('href', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-name").html('-')
            $("#show-email").html('-')
            $("#show-phone").html('-')
            $("#show-partner").html('-')
            $("#show-address").html('-')
            $("#edit-password").val('')

            ajaxGet("{{ route('admin.master.customers.show', ['customer' => '-id-']) }}".replace('-id-', id), "#modal-show", function (result) {
                customer = result

                $("#show-avatar").attr('src', result.avatar_link)
                $("#show-avatar").parent().attr('href', result.avatar_link)
                $("#show-name").html(result.name)
                $("#show-email").html(result.email)
                $("#show-phone").html(result.phone_converted)
                $("#show-partner").html(result.partner)
                $("#show-address").html(result.address)

                setTimeout(function() {
                    map.setView([result.latitude, result.longitude], 17).invalidateSize();
                    marker.setLatLng([result.latitude, result.longitude])
                }, 100);

                let images = null
                if(result.avatar){
                    images = [{name : result.avatar,size : result.avatar_size}]
                }
                dropzonePreview(dz_edit_avatar, images, '{{ asset("images/avatar") }}', '#form-edit', 'avatar')
            })
        })

        $(document).on('click', '.btn-edit', function () {
            $("#edit-name").val(customer.name)
            $("#edit-email").val(customer.email)
            $("#edit-phone").val(customer.phone)
            $("#edit-address").val(customer.address)
        })

        $(document).on('click', '.btn-delete', function () {
            // Delete
            let button = $(this)
            id = button.data('id')
            let name = button.data('name')

            DeleteConfirm.fire({
                title: "Yakin ingin menghapus customer " + name + "?",
            }).then((result) => {
                if (result.isConfirmed) {
                    loading()
                    const formData = new FormData();
                    formData.append('id',id)
                    formData.append('_method','delete')
                    formData.append('_token','{{ csrf_token() }}')
                    ajaxPost("{{ route('admin.master.customers.destroy',['customer' => '-id-']) }}".replace('-id-', id),formData,null,function(){
                        table.ajax.reload()
                    })
                }
            })
        })

        $("#generate-password").click(function (){
            loading()
            ajaxGet("{{ route('admin.master.customers.password.generate') }}", null, function (result) {
                $("#edit-password").val(result.password)
            })
        })

        $("#edit-save").click(function() {
            // Edit
            loading()
            const formData = new FormData(document.getElementById('form-edit'));
            ajaxPost("{{ route('admin.master.customers.update',['customer' => '-id-']) }}".replace('-id-', id),formData,'#modal-edit',function(){
                table.ajax.reload()
            })
        })

        $("#edit-password-save").click(function() {
            // Edit
            loading()
            const formData = new FormData(document.getElementById('form-edit-password'));
            ajaxPost("{{ route('admin.master.customers.password', ['customer' => '-id-']) }}".replace('-id-', id),formData,'#modal-edit-password')
        })

        // Leaflet

        var map = L.map('show-map').setView([0, -0], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([0, -0]).addTo(map)
    </script>
@endpush
