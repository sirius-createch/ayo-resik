@extends('layout.admin.main', ['title' => 'Partner'])

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Daftar Partner">
        @slot('headerAction')
            <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal-create"><i class="bx bx-plus align-middle me-2"></i>Tambah Partner</button>
        @endslot

        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>Avatar</th>
                    <th>Nama</th>
                    <th>Alamat Email</th>
                    <th>Nomor HP</th>
                    <th>Total Customer</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-create" title="Tambah Partner">
        <x-admin-form id="form-create" method="post">
            <x-admin-form-input-file id="create_avatar" formId="form-create" name="avatar" label="Avatar" class="mb-3" accepted="image/*" />
            <x-admin-form-input id="create-name" name="name" label="Nama" class="mb-3" required />
            <x-admin-form-input id="create-email" name="email" label="Alamat Email" class="mb-3" type="email" required />
            <x-admin-form-input id="create-phone" name="phone" label="Nomor HP" class="mb-3" prefix="+62" required />
            <x-admin-form-input id="create-address" name="address" label="Alamat" class="mb-3" required />
            <div>
                <x-admin-form-label required>Jadwal Pengambilan</x-admin-form-label>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-monday" name="schedule[monday]">Senin</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-tuesday" name="schedule[tuesday]">Selasa</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-wednesday" name="schedule[wednesday]">Rabu</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-thursday" name="schedule[thursday]">Kamis</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-friday" name="schedule[friday]">Jumat</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-saturday" name="schedule[saturday]">Sabtu</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="create-schedule-sunday" name="schedule[sunday]">Minggu</x-admin-form-checkbox>
            </div>
        </x-admin-form>

        @slot('footer')
            <button id="create-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-show" title="Detail Partner">
        <div class="pb-2 mb-2 text-center">
            <a href="" data-fancybox style="cursor: zoom-in">
                <img src="" alt="Foto " id="show-avatar" class="w-25 rounded" style="object-fit: cover;">
            </a>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nama:</b> <br>
            <span id="show-name">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Alamat Email:</b> <br>
            <span id="show-email">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Nomor HP:</b> <br>
            <span id="show-phone">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Total Customer:</b> <br>
            <span id="show-customer">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Jadwal Pengambilan:</b> <br>
            <span id="show-schedules">-</span>
        </div>
        <div class="pb-2 mb-1">
            <b>Alamat:</b> <br>
            <span id="show-address">-</span>
        </div>
        <div id="show-map" class="w-100" style="height: 200px"></div>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-secondary btn-customer-list" data-bs-target="#modal-customer-list" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-user-account align-middle me-2"></i>Daftar Customer</button>
                <button class="btn btn-warning btn-edit" data-bs-target="#modal-edit" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-edit align-middle me-2"></i>Ubah</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-customer-list" title="Daftar Customer">
        <div class="pb-2 mb-2 text-center">
            <b>Partner</b> <br>
            <span id="customer-list-partner-name">-</span>
        </div>
        <div class="list-group" id="customer-list">
            -
        </div>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
            </div>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-edit" title="Ubah Partner">
        <x-admin-form id="form-edit" method="put">
            <x-admin-form-input-file id="edit_avatar" formId="form-edit" name="avatar" label="Avatar" class="mb-3" accepted="image/*" />
            <x-admin-form-input id="edit-name" name="name" label="Nama" class="mb-3" required />
            <x-admin-form-input id="edit-email" name="email" label="Alamat Email" class="mb-3" type="email" required />
            <x-admin-form-input id="edit-phone" name="phone" label="Nomor HP" class="mb-3" prefix="+62" required />
            <x-admin-form-input id="edit-address" name="address" label="Alamat" class="mb-3" required />
            <div>
                <x-admin-form-label required>Jadwal Pengambilan</x-admin-form-label>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-monday" name="schedule[monday]">Senin</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-tuesday" name="schedule[tuesday]">Selasa</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-wednesday" name="schedule[wednesday]">Rabu</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-thursday" name="schedule[thursday]">Kamis</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-friday" name="schedule[friday]">Jumat</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-saturday" name="schedule[saturday]">Sabtu</x-admin-form-checkbox>
                <x-admin-form-checkbox class="mb-1" id="edit-schedule-sunday" name="schedule[sunday]">Minggu</x-admin-form-checkbox>
            </div>
        </x-admin-form>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
                <button id="edit-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        let partner = {}
        let table = null
        let id = null
        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            const columns = [
                { data: 'avatar', orderable: false, searchable: false, className: "text-center" },
                { data: 'name' },
                { data: 'email' },
                { data: 'phone' },
                { data: 'customers_count', searchable: false, className: "text-center" },
            ]

            table = datatablesInit("#table", "{{ route('admin.master.partners.index') }}", columns, [2, 'asc'], datatablesTranslate('Partner'), function () {
                Fancybox.bind('[data-fancybox]', {})
            })
        })

        $(document).on('click', '.btn-show', function () {
            id = $(this).data('id')

            $("#show-avatar").attr('src', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-avatar").parent().attr('href', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-name").html('-')
            $("#show-email").html('-')
            $("#show-phone").html('-')
            $("#show-customer").html('-')
            $("#show-schedules").html('-')
            $("#show-address").html('-')

            ajaxGet("{{ route('admin.master.partners.show', ['partner' => '-id-']) }}".replace('-id-', id), "#modal-show", function (result) {
                partner = result

                $("#show-avatar").attr('src', result.avatar_link)
                $("#show-avatar").parent().attr('href', result.avatar_link)
                $("#show-name").html(result.name)
                $("#show-email").html(result.email)
                $("#show-phone").html(result.phone_converted)
                $("#show-customer").html(result.total_customer)
                $("#show-schedules").html(result.text_schedule)
                $("#show-address").html(result.address)

                setTimeout(function() {
                    map.setView([result.latitude, result.longitude], 17).invalidateSize();
                    marker.setLatLng([result.latitude, result.longitude])
                }, 100);

                let images = null
                if(result.avatar){
                    images = [{name : result.avatar,size : result.avatar_size}]
                }
                dropzonePreview(dz_edit_avatar, images,'{{ asset("images/avatar") }}','#form-edit','avatar')
            })
        })

        $(document).on('click', '.btn-edit', function () {
            $("#edit-name").val(partner.name)
            $("#edit-email").val(partner.email)
            $("#edit-phone").val(partner.phone)
            $("#edit-address").val(partner.address)

            $("#edit-schedule-monday").prop('checked', partner.schedules?.monday ?? false)
            $("#edit-schedule-tuesday").prop('checked', partner.schedules?.tuesday ?? false)
            $("#edit-schedule-wednesday").prop('checked', partner.schedules?.wednesday ?? false)
            $("#edit-schedule-thursday").prop('checked', partner.schedules?.thursday ?? false)
            $("#edit-schedule-friday").prop('checked', partner.schedules?.friday ?? false)
            $("#edit-schedule-saturday").prop('checked', partner.schedules?.saturday ?? false)
            $("#edit-schedule-sunday").prop('checked', partner.schedules?.sunday ?? false)
        })

        $(document).on('click', '.btn-customer-list', function () {
            $("#customer-list-partner-name").html(partner.name)
            $("#customer-list").html('')

            if (partner.customers.length > 0) {
                $.each(partner.customers, function (index, customer) {
                    $("#customer-list").append(`
                        <div class="list-group-item">
                            <div class="user-info">
                                <h6 class="mb-1">` + customer.name + `</h6>
                                <small>` + customer.address + `</small>
                            </div>
                        </div>
                    `)
                })
            } else {
                $("#customer-list").html(`
                    <div class="alert alert-primary mb-0" role="alert">
                        Partner ini belum memiliki customer.
                    </div>
                `)
            }
        })

        $(document).on('click', '.btn-delete', function () {
            // Delete
            let button = $(this)
            id = button.data('id')
            let name = button.data('name')

            DeleteConfirm.fire({
                title: "Yakin ingin menghapus partner " + name + "?",
            }).then((result) => {
                if (result.isConfirmed) {
                    loading()
                    const formData = new FormData();
                    formData.append('id',id)
                    formData.append('_method','delete')
                    formData.append('_token','{{ csrf_token() }}')
                    ajaxPost("{{ route('admin.master.partners.destroy',['partner' => '-id-']) }}".replace('-id-', id),formData,null,function(){
                        table.ajax.reload()
                    })
                }
            })
        })

        $("#create-save").click(function() {
            // Save
            loading()
            const formData = new FormData(document.getElementById('form-create'));
            ajaxPost("{{ route('admin.master.partners.store') }}", formData, '#modal-create', function(){
                table.ajax.reload()

                dropzonePreview(dz_create_avatar, null, '{{ asset("images/avatar") }}', '#form-create', 'avatar')

                $("#create-name").val('')
                $("#create-email").val('')
                $("#create-phone").val('')
                $("#create-address").val('')

                $("#create-schedule-monday").prop('checked', false)
                $("#create-schedule-tuesday").prop('checked', false)
                $("#create-schedule-wednesday").prop('checked', false)
                $("#create-schedule-thursday").prop('checked', false)
                $("#create-schedule-friday").prop('checked', false)
                $("#create-schedule-saturday").prop('checked', false)
                $("#create-schedule-sunday").prop('checked', false)
            })
        })

        $("#edit-save").click(function() {
            // Edit
            loading()
            const formData = new FormData(document.getElementById('form-edit'));
            ajaxPost("{{ route('admin.master.partners.update',['partner' => '-id-']) }}".replace('-id-', id),formData,'#modal-edit',function(){
                table.ajax.reload()
            })
        })

        // Leaflet

        var map = L.map('show-map').setView([0, 0], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([0, 0]).addTo(map)
    </script>
@endpush
