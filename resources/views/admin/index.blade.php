@extends('layout.admin.main', ['title' => 'Dashboard'])

@section('content')
    <div class="d-flex flex-column justify-content-center h-100">
        <div class="row">
            <div class="col-md-4 mb-3">
                <x-admin-card>
                    <div class="d-flex display-5 align-items-center">
                        <div class="p-0 m-0">
                            <i class="bx bxs-user-account"></i>
                        </div>
                        <div class="border-start ps-3 ms-3">
                            {{ $customers }}<br>
                            <small class="h6">Total Customer</small>
                        </div>
                    </div>
                </x-admin-card>
            </div>
            <div class="col-md-4 mb-3">
                <x-admin-card>
                    <div class="d-flex display-5 align-items-center">
                        <div class="p-0 m-0">
                            <i class="bx bxs-user-badge"></i>
                        </div>
                        <div class="border-start ps-3 ms-3">
                            {{ $partners }}<br>
                            <small class="h6">Total Partner</small>
                        </div>
                    </div>
                </x-admin-card>
            </div>
            <div class="col-md-4 mb-3">
                <x-admin-card>
                    <div class="d-flex display-5 align-items-center">
                        <div class="p-0 m-0">
                            <i class="bx bxs-package"></i>
                        </div>
                        <div class="border-start ps-3 ms-3">
                            {{ $products }}<br>
                            <small class="h6">Total Jenis Produk</small>
                        </div>
                    </div>
                </x-admin-card>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mb-3">
                <x-admin-card>
                    <div class="d-flex display-5 align-items-center">
                        <div class="p-0 m-0">
                            <i class="bx bx-money"></i>
                        </div>
                        <div class="border-start ps-3 ms-3">
                            {{ Sirius::toRupiah($nominal) }}<br>
                            <small class="h6">Total Nominal Transaksi</small>
                        </div>
                    </div>
                </x-admin-card>
            </div>
            <div class="col-md-6 mb-3">
                <x-admin-card>
                    <div class="d-flex display-5 align-items-center">
                        <div class="p-0 m-0">
                            <i class="bx bx-dumbbell"></i>
                        </div>
                        <div class="border-start ps-3 ms-3">
                            {{ round($weight) }} kg<br>
                            <small class="h6">Total Berat Sampah</small>
                        </div>
                    </div>
                </x-admin-card>
            </div>
        </div>
    </div>
@endsection
