@extends('layout.admin.main', ['title' => 'Pengaturan'])

@section('content')
    <x-admin-card header="Pengaturan Company Profile">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Alamat
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-address"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item list-group-item-secondary d-flex justify-content-between align-items-center">
                Telepon / WA
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-phone"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Email
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-email"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item list-group-item-secondary d-flex justify-content-between align-items-center">
                Total Nominal Sampah
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-total-nominal"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Total Berat Sampah
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-total-weight"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item list-group-item-secondary d-flex justify-content-between align-items-center">
                Total Partner
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-total-partner"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Total Customer
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-total-customer"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
        </ul>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="edit-address" title="Ubah Alamat">
        <x-admin-form method="put" id="form-address" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="address">
            <x-admin-form-textarea id="form-edit-address" name="value" required>{{ $settings['address'] }}</x-admin-form-textarea>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-phone" title="Ubah Telepon / WA">
        <x-admin-form method="put" id="form-phone" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="phone">
            <x-admin-form-input id="form-edit-phone" name="value" value="0{{ $settings['phone'] }}" required/>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-email" title="Ubah Email">
        <x-admin-form method="put" id="form-email" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="email">
            <x-admin-form-input id="form-edit-email" name="value" value="{{ $settings['email'] }}" type="email" required/>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-total-nominal" title="Ubah Total Nominal Sampah">
        <x-admin-form method="put" id="form-total-nominal" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="nominal">
            <div class="btn-group w-100" role="group">
                <input type="radio" class="btn-check select-nominal" name="toggle" value="1" required id="nominal-database" @checked($settings['nominal-toggle'])>
                <label class="btn btn-outline-primary" for="nominal-database">Real-time Database</label>
                <input type="radio" class="btn-check select-nominal" name="toggle" value="0" required id="nominal-pengaturan" @checked(!$settings['nominal-toggle'])>
                <label class="btn btn-outline-primary" for="nominal-pengaturan">Atur Sendiri</label>
            </div>
            @if ($settings['nominal-toggle'])
                <x-admin-form-input id="form-edit-total-nominal" name="value" value="{{ number_format($settings['nominal-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" prefix="Rp" suffix=",-" style="display: none" required/>
            @else
                <x-admin-form-input id="form-edit-total-nominal" name="value" value="{{ number_format($settings['nominal-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" prefix="Rp" suffix=",-" required/>
            @endif
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-total-weight" title="Ubah Total Berat Sampah">
        <x-admin-form method="put" id="form-total-weight" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="weight">
            <div class="btn-group w-100" role="group">
                <input type="radio" class="btn-check select-weight" name="toggle" value="1" required id="weight-database" @checked($settings['weight-toggle'])>
                <label class="btn btn-outline-primary" for="weight-database">Real-time Database</label>
                <input type="radio" class="btn-check select-weight" name="toggle" value="0" required id="weight-pengaturan" @checked(!$settings['weight-toggle'])>
                <label class="btn btn-outline-primary" for="weight-pengaturan">Atur Sendiri</label>
            </div>
            @if ($settings['weight-toggle'])
                <x-admin-form-input id="form-edit-total-weight" name="value" value="{{ Str::remove(',0', number_format($settings['weight-value'], 1, ',', '.')) }}" class="mt-3" classInput="to-unit" suffix="kg" style="display: none" required/>
            @else
                <x-admin-form-input id="form-edit-total-weight" name="value" value="{{ Str::remove(',0', number_format($settings['weight-value'], 1, ',', '.')) }}" class="mt-3" classInput="to-unit" suffix="kg" required/>
            @endif
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-total-partner" title="Ubah Total Partner">
        <x-admin-form method="put" id="form-total-partner" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="partner">
            <div class="btn-group w-100" role="group">
                <input type="radio" class="btn-check select-partner" name="toggle" value="1" required id="partner-database" @checked($settings['partner-toggle'])>
                <label class="btn btn-outline-primary" for="partner-database">Real-time Database</label>
                <input type="radio" class="btn-check select-partner" name="toggle" value="0" required id="partner-pengaturan" @checked(!$settings['partner-toggle'])>
                <label class="btn btn-outline-primary" for="partner-pengaturan">Atur Sendiri</label>
            </div>
            @if ($settings['partner-toggle'])
                <x-admin-form-input id="form-edit-total-partner" name="value" value="{{ number_format($settings['partner-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" style="display: none" required/>
            @else
                <x-admin-form-input id="form-edit-total-partner" name="value" value="{{ number_format($settings['partner-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" required/>
            @endif
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-total-customer" title="Ubah Total Customer">
        <x-admin-form method="put" id="form-total-customer" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="customer">
            <div class="btn-group w-100" role="group">
                <input type="radio" class="btn-check select-customer" name="toggle" value="1" required id="customer-database" @checked($settings['customer-toggle'])>
                <label class="btn btn-outline-primary" for="customer-database">Real-time Database</label>
                <input type="radio" class="btn-check select-customer" name="toggle" value="0" required id="customer-pengaturan" @checked(!$settings['customer-toggle'])>
                <label class="btn btn-outline-primary" for="customer-pengaturan">Atur Sendiri</label>
            </div>
            @if ($settings['customer-toggle'])
                <x-admin-form-input id="form-edit-total-customer" name="value" value="{{ number_format($settings['customer-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" style="display: none" required/>
            @else
                <x-admin-form-input id="form-edit-total-customer" name="value" value="{{ number_format($settings['customer-value'], thousands_separator: '.') }}" class="mt-3" classInput="to-rupiah" required/>
            @endif
        </x-admin-form>
    </x-admin-modal>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $(".select-nominal").click(function() {
                const input = $("#form-edit-total-nominal")
                if ($("#nominal-database").is(":checked")) {
                    input.prop('required', false)
                    input.parent().parent().slideUp()
                }
                else {
                    input.prop('required', true)
                    input.parent().parent().slideDown()
                }
            })
            $(".select-weight").click(function() {
                const input = $("#form-edit-total-weight")
                if ($("#weight-database").is(":checked")) {
                    input.prop('required', false)
                    input.parent().parent().slideUp()
                }
                else {
                    input.prop('required', true)
                    input.parent().parent().slideDown()
                }
            })
            $(".select-partner").click(function() {
                const input = $("#form-edit-total-partner")
                if ($("#partner-database").is(":checked")) {
                    input.prop('required', false)
                    input.parent().slideUp()
                }
                else {
                    input.prop('required', true)
                    input.parent().slideDown()
                }
            })
            $(".select-customer").click(function() {
                const input = $("#form-edit-total-customer")
                if ($("#customer-database").is(":checked")) {
                    input.prop('required', false)
                    input.parent().slideUp()
                }
                else {
                    input.prop('required', true)
                    input.parent().slideDown()
                }
            })

            $('#form-address').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-address'));
                ajaxPost("{{ route('admin.compro.settings.update.general') }}", data, "#edit-address")
            })

            $('#form-phone').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-phone'));
                ajaxPost("{{ route('admin.compro.settings.update.general') }}", data, "#edit-phone")
            })

            $('#form-email').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-email'));
                ajaxPost("{{ route('admin.compro.settings.update.general') }}", data, "#edit-email")
            })

            $('#form-total-nominal').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-total-nominal'));
                ajaxPost("{{ route('admin.compro.settings.update.total') }}", data, "#edit-total-nominal")
            })

            $('#form-total-weight').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-total-weight'));
                ajaxPost("{{ route('admin.compro.settings.update.total') }}", data, "#edit-total-weight")
            })

            $('#form-total-partner').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-total-partner'));
                ajaxPost("{{ route('admin.compro.settings.update.total') }}", data, "#edit-total-partner")
            })

            $('#form-total-customer').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-total-customer'));
                ajaxPost("{{ route('admin.compro.settings.update.total') }}", data, "#edit-total-customer")
            })
        })
    </script>
@endpush
