@extends('layout.admin.main', ['title' => 'Slider'])

@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Daftar Slider">
        @slot('headerAction')
            <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal-create"><i class="bx bx-plus align-middle me-2"></i>Tambah Slider</button>
        @endslot
        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th width="20px">No.</th>
                    <th>Gambar</th>
                    <th>Judul</th>
                    <th>URL</th>
                    <th>Deskripsi</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="modal-create" title="Tambah Slider">
        <x-admin-form id="form-create" method="post">
            <x-admin-form-input-file id="create_image" formId="form-create" name="image" label="Gambar" class="mb-3" accepted="image/*" required />
            <x-admin-form-input id="create-title" name="title" label="Judul" class="mb-3" />
            <x-admin-form-input id="create-url" name="url" label="URL" class="mb-3" />
            <x-admin-form-textarea id="create-desc" name="desc" label="Deskripsi" class="mb-3" />
        </x-admin-form>

        @slot('footer')
            <button id="create-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-show" title="Detail Slider">
        <div class="pb-2 mb-2 text-center">
            <a href="" data-fancybox style="cursor: zoom-in">
                <img src="" alt="Foto " id="show-image" class="w-25 rounded" style="object-fit: cover;">
            </a>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Judul:</b> <br>
            <span id="show-title">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>URL:</b> <br>
            <span id="show-url">-</span>
        </div>
        <div class="border-bottom pb-2 mb-2">
            <b>Deskripsi:</b> <br>
            <span id="show-desc">-</span>
        </div>

        @slot('footer')
            <button class="btn btn-warning btn-edit" data-bs-target="#modal-edit" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bxs-edit align-middle me-2"></i>Ubah</button>
        @endslot
    </x-admin-modal>

    <x-admin-modal id="modal-edit" title="Ubah Slider">
        <x-admin-form id="form-edit" method="put">
            <x-admin-form-input-file id="edit_image" formId="form-edit" name="image" label="Gambar" class="mb-3" accepted="image/*" required />
            <x-admin-form-input id="edit-title" name="title" label="Judul" class="mb-3" />
            <x-admin-form-input id="edit-url" name="url" label="URL" class="mb-3" />
            <x-admin-form-textarea id="edit-desc" name="desc" label="Deskripsi" class="mb-3" />
        </x-admin-form>

        @slot('footer')
            <div class="d-flex justify-content-between w-100">
                <button class="btn btn-dark btn-customer-list" data-bs-target="#modal-show" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-arrow-back align-middle me-2"></i>Kembali</button>
                <button id="edit-save" class="btn btn-success"><i class="bx bxs-save align-middle me-2"></i>Simpan</button>
            </div>
        @endslot
    </x-admin-modal>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        let slider = null
        let table = null
        let id = null

        $(document).ready(function () {
            table = datatablesInit("#table", "{{ route('admin.compro.sliders.index') }}", [
                { data: 'image', orderable: false, searchable: false, className: "text-center" },
                {data: 'title'},
                {data: 'url'},
                {data: 'desc'},
            ], [2, 'asc'], datatablesTranslate('Slider'))
        })
        $(document).on('click', '.btn-show', function () {
            id = $(this).data('id')

            $("#show-image").attr('src', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-image").parent().attr('href', "https://ui-avatars.com/api/?name=" + "--")
            $("#show-title").html('-')
            $("#show-url").html('-')
            $("#show-desc").html('-')

            ajaxGet("{{ route('admin.compro.sliders.show','-id-') }}".replace('-id-', id), "#modal-show", function (result) {
                slider = result

                $("#show-image").attr('src', result.image_link)
                $("#show-image").parent().attr('href', result.image_link)
                $("#show-title").html(slider.title ?? '-')
                $("#show-url").html(slider.url ?? '#')
                $("#show-desc").html(slider.desc ?? '-')
                let images = null
                if(slider.image){
                    images = [{name : slider.image,size : slider.image_size}]
                }

                dropzonePreview(dz_edit_image, images, '{{ asset("images/slider") }}', '#form-edit', 'image')
            })
        })
        $(document).on('click', '.btn-edit', function () {
            $("#edit-title").val(slider.title)
            $("#edit-desc").val(slider.desc)
            $("#edit-url").val(slider.url)
        })
        $(document).on('click', '.btn-delete', function () {
            // Delete
            let button = $(this)
            id = button.data('id')
            let name = button.data('name')
            if (name == '') name = 'ini'

            DeleteConfirm.fire({
                title: "Yakin ingin menghapus slider " + name + "?",
            }).then((result) => {
                if (result.isConfirmed) {
                    loading()
                    const formData = new FormData();
                    formData.append('id',id)
                    formData.append('_method','delete')
                    formData.append('_token','{{ csrf_token() }}')
                    ajaxPost("{{ route('admin.compro.sliders.destroy',['slider' => '-id-']) }}".replace('-id-', id),formData,null,function(){
                        table.ajax.reload()
                    })
                }
            })
        })
        $("#create-save").click(function() {
            loading()
            const formData = new FormData(document.getElementById('form-create'));
            ajaxPost("{{ route('admin.compro.sliders.store') }}",formData,'#modal-create',function(){
                table.ajax.reload()

                dropzonePreview(dz_create_image, null, '{{ asset("images/slider") }}', '#form-create', 'image')
                $('#create-title').val('')
                $('#create-url').val('')
                $('#create-desc').val('')
            })
        })
        $("#edit-save").click(function() {
            loading()
            const formData = new FormData(document.getElementById('form-edit'));
            ajaxPost("{{ route('admin.compro.sliders.update','-id-') }}".replace('-id-', id),formData,'#modal-edit',function(){
                table.ajax.reload()
            })
        })
    </script>
@endpush
