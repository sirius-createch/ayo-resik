@extends('layout.admin.main', ['title' => 'Tambah Artikel'])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Tambah Artikel">
        @slot('headerAction')
            <x-admin-button size="sm" link="{{ route('admin.compro.articles.index') }}"><i class="fa-solid fa-arrow-left me-2"></i>Kembali</x-admin-button>
        @endslot

        <x-admin-form id="form-create" method="post" action="{{ route('admin.compro.articles.store')}}">
            <div class="row">
                <div class="col-md-8">
                    <x-admin-form-input id="title" name="title" placeholder="Judul" required class="mb-3" />
                    <x-admin-form-textarea id="content" name="content" tinymce></x-admin-form-textarea>
                </div>
                <div class="col-md-4">
                    <x-admin-form-input-file id="banner" formId="form-create" name="banner" label="Banner" accepted="image/*" class="mb-3" />
                    <div class="mb-3">
                        <x-admin-form-label required>Status Penerbitan</x-admin-form-label>
                        <div class="btn-group w-100" role="group">
                            <input type="radio" class="btn-check select-status" name="status" value="draft" required id="status-draft">
                            <label class="btn btn-outline-primary" for="status-draft">Draf</label>
                            <input type="radio" class="btn-check select-status" name="status" value="schedule" required id="status-schedule">
                            <label class="btn btn-outline-primary" for="status-schedule">Jadwalkan</label>
                            <input type="radio" class="btn-check select-status" name="status" value="publish" required id="publish" checked>
                            <label class="btn btn-outline-primary" for="publish">Terbitkan</label>
                        </div>
                    </div>
                    <x-admin-form-input type="datetime-local" id="published_at" name="" label="Waktu Terbit" class="mb-3" value="{{ date('Y-m-d H:i') }}" style="display: none" required /> <!-- name input ada di jquery untuk on/off -->
                    <x-admin-button type="submit" class="w-100 btn-save"><i class="fa-solid fa-save me-2"></i>Simpan Perubahan</x-admin-button>
                </div>
            </div>
        </x-admin-form>
    </x-admin-card>
@endsection

@push('js')
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/{{ config('app.tiny_mce_key') }}/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        Dropzone.autoDiscover = false;

        $(document).ready(function() {
            $(".select-status").click(function() {
                const input = $("#published_at")
                if ($("#status-schedule").is(":checked")) {
                    input.attr('name', 'published_at')
                    input.prop('required', true)
                    input.parent().slideDown()
                }
                else {
                    input.attr('name', '')
                    input.prop('required', false)
                    input.parent().slideUp()
                }
            })
        })
    </script>
@endpush
