@extends('layout.admin.main', ['title' => 'Ubah ' . $article->title])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Ubah Artikel">
        @slot('headerAction')
            <x-admin-button size="sm" link="{{ route('admin.compro.contents.index') }}"><i class="fa-solid fa-arrow-left me-2"></i>Kembali</x-admin-button>
        @endslot

        <x-admin-form id="form-edit" method="put" action="{{ route('admin.compro.articles.update',['artikel' => $article->slug]) }}">
            <div class="row">
                <div class="col-md-8">
                    <x-admin-form-input id="title" name="title" placeholder="Judul" required class="mb-3" value="{{ $article->title }}" />
                    <x-admin-form-textarea id="content" name="content" tinymce>{!! $article->content !!}</x-admin-form-textarea>
                </div>
                <div class="col-md-4">
                    <x-admin-form-input-file id="banner" formId="form-edit" name="banner" label="Banner" accepted="image/*" class="mb-3" value="{{ auth()->user()->banner }}" valueSize="{{ auth()->user()->banner_size }}" />
                    <div class="mb-3">
                        <x-admin-form-label required>Status Penerbitan</x-admin-form-label>
                        <div class="btn-group w-100" role="group">
                            <input type="radio" class="btn-check select-status" name="status" value="draft" required id="status-draft" @checked($article->status == 'draft')>
                            <label class="btn btn-outline-primary" for="status-draft">Draf</label>
                            <input type="radio" class="btn-check select-status" name="status" value="schedule" required id="status-schedule" @checked($article->status == 'scheduled')>
                            <label class="btn btn-outline-primary" for="status-schedule">Jadwalkan</label>
                            <input type="radio" class="btn-check select-status" name="status" value="publish" required id="status-publish" @checked($article->status == 'published')>
                            <label class="btn btn-outline-primary" for="status-publish">Terbitkan</label>
                        </div>
                    </div>
                    @if ($article->status == 'scheduled')
                        <x-admin-form-input type="datetime-local" id="published_at" name="" label="Waktu Terbit" class="mb-3" value="{{ date('Y-m-d H:i', strtotime($article->published_at ?? 'now')) }}"/> <!-- name input ada di jquery untuk on/off -->
                    @else
                        <x-admin-form-input type="datetime-local" id="published_at" name="" label="Waktu Terbit" class="mb-3" value="{{ date('Y-m-d H:i', strtotime($article->published_at ?? 'now')) }}" style="display: none"/> <!-- name input ada di jquery untuk on/off -->
                    @endif
                    <x-admin-button type="submit" class="w-100 btn-edit"><i class="fa-solid fa-save me-2 "></i>Simpan Perubahan</x-admin-button>
                </div>
            </div>
        </x-admin-form>
    </x-admin-card>
@endsection

@push('js')
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/{{ config('app.tiny_mce_key') }}/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        Dropzone.autoDiscover = false;

        $(document).ready(function() {
            $(".select-status").click(function() {
                const input = $("#published_at")
                if ($("#status-schedule").is(":checked")) {
                    input.attr('name', 'published_at')
                    input.prop('required', true)
                    input.parent().slideDown()
                }
                else {
                    input.attr('name', '')
                    input.prop('required', false)
                    input.parent().slideUp()
                }
            })

            @if (isset($article->banner))
                let images = [{name : "{{ $article->banner }}",size : "{{ $article->banner_size }}"}]
                dropzonePreview(dz_banner, images, '{{ asset("images/banner_article") }}', '#form-edit', 'banner')
            @endif
        })
    </script>
@endpush
