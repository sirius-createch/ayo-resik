@extends('layout.admin.main', ['title' => 'Konten'])

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />
@endpush

@section('content')
    <x-admin-card header="Daftar Konten">
        @slot('headerAction')
            <x-admin-button link="{{ route('admin.compro.contents.create') }}" size='sm'><i class="bx bx-plus align-middle me-2"></i>Tambah Konten</x-admin-button>
        @endslot

        <x-admin-table id="table">
            @slot('thead')
                <tr>
                    <th>No.</th>
                    <th>Banner</th>
                    <th>Judul</th>
                    <th>Tanggal Terbit</th>
                    <th>Penulis</th>
                    <th width="200px">Tindakan</th>
                </tr>
            @endslot
        </x-admin-table>
    </x-admin-card>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        let table = null

        $(document).ready(function () {
            @if (session('success'))
                Toast.fire({
                    icon: 'success',
                    title: "{{ session('success') }}",
                })
            @endif
            const columns = [
                { data: 'banner', orderable: false, searchable: false, className: "text-center" },
                { data: 'title' },
                { data: 'published_at' },
                { data: 'author' },
            ]

            table = datatablesInit("#table", "{{ route('admin.compro.contents.index') }}", columns, [2, 'asc'], datatablesTranslate('Konten'), function () {
                Fancybox.bind('[data-fancybox]', {})
            })
        })

        $(document).on('click', '.btn-delete', function () {
            // Delete
            let button = $(this)
            id = button.data('id')
            let name = button.data('name')

            DeleteConfirm.fire({
                title: "Yakin ingin menghapus konten " + name + "?",
            }).then((result) => {
                if (result.isConfirmed) {
                    const formData = new FormData();
                    formData.append('id', id)
                    formData.append('_method', 'delete')
                    formData.append('_token', '{{ csrf_token() }}')
                    ajaxPost("{{ route('admin.compro.contents.destroy', ['konten' => '-id-']) }}".replace('-id-', id), formData, null, function(){
                        table.ajax.reload()
                    })
                }
            })
        })
    </script>
@endpush
