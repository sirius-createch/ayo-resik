@extends('layout.admin.main', ['title' => 'Ubah ' . $content->title])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Ubah Konten">
        @slot('headerAction')
            <x-admin-button size="sm" link="{{ route('admin.compro.contents.index') }}"><i class="fa-solid fa-arrow-left me-2"></i>Kembali</x-admin-button>
        @endslot

        <x-admin-form id="form-edit" method="put" action="{{ route('admin.compro.contents.update',['konten'=>$content->slug]) }}">
            <div class="row">
                <div class="col-md-8">
                    <x-admin-form-input id="title" name="title" placeholder="Judul" required class="mb-3" value="{{ $content->title }}" />
                    <x-admin-form-textarea id="content" name="content" tinymce>{!! $content->content !!}</x-admin-form-textarea>
                </div>
                <div class="col-md-4">
                    <x-admin-form-input-file id="banner" formId="form-edit" name="banner" label="Banner" accepted="image/*" class="mb-3" value="{{ auth()->user()->banner }}" valueSize="{{ auth()->user()->banner_size }}" />
                    <div class="mb-3">
                        <x-admin-form-label required>Status Penerbitan</x-admin-form-label>
                        <div class="btn-group w-100" role="group">
                            <input type="radio" class="btn-check select-status" name="status" required value="draft" id="status-draft" @checked($content->status == 'draft')>
                            <label class="btn btn-outline-primary" for="status-draft">Draf</label>
                            <input type="radio" class="btn-check select-status" name="status" required value="schedule" id="status-schedule" @checked($content->status == 'scheduled')>
                            <label class="btn btn-outline-primary" for="status-schedule">Jadwalkan</label>
                            <input type="radio" class="btn-check select-status" name="status" required value="publish" id="status-publish" @checked($content->status == 'published')>
                            <label class="btn btn-outline-primary" for="status-publish">Terbitkan</label>
                        </div>
                    </div>
                    @if ($content->status == 'scheduled')
                        <x-admin-form-input type="datetime-local" id="published_at" name="" label="Waktu Terbit" class="mb-3" value="{{ date('Y-m-d H:i', strtotime($content->published_at ?? 'now')) }}"/> <!-- name input ada di jquery untuk on/off -->
                    @else
                        <x-admin-form-input type="datetime-local" id="published_at" name="" label="Waktu Terbit" class="mb-3" value="{{ date('Y-m-d H:i', strtotime($content->published_at ?? 'now')) }}" style="display: none"/> <!-- name input ada di jquery untuk on/off -->
                    @endif
                    <x-admin-button type="submit" class="w-100 btn-edit"><i class="fa-solid fa-save me-2"></i>Simpan Perubahan</x-admin-button>
                </div>
            </div>
        </x-admin-form>
    </x-admin-card>
@endsection

@push('js')
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/{{ config('app.tiny_mce_key') }}/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        Dropzone.autoDiscover = false;

        $(document).ready(function() {
            $(".select-status").click(function() {
                const input = $("#published_at")
                if ($("#status-schedule").is(":checked")) {
                    input.attr('name', 'published_at')
                    input.prop('required', true)
                    input.parent().slideDown()
                }
                else {
                    input.attr('name', '')
                    input.prop('required', false)
                    input.parent().slideUp()
                }
            })
            @if (isset($content->banner))
                let images = [{name : "{{ $content->banner }}",size : "{{ $content->banner_size }}"}]
                dropzonePreview(dz_banner, images, '{{ asset("images/banner_content") }}', '#form-edit', 'banner')
            @endif
        })
    </script>
@endpush
