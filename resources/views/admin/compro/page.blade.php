@extends('layout.admin.main', ['title' => 'Ubah Halaman ' . $page->title])

@section('content')
    <x-admin-card :header="'Ubah Halaman ' . $page->title">
        <x-admin-form id="form-edit" method="put" submit="Simpan Perubahan" submitIcon="fa-solid fa-save" submitWide>
            <x-admin-form-textarea id="content" name="content" required tinymce>{!! $page->content !!}</x-admin-form-textarea>
        </x-admin-form>
    </x-admin-card>
@endsection

@push('js')
    <script src="https://cdn.tiny.cloud/1/{{ config('app.tiny_mce_key') }}/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
@endpush
