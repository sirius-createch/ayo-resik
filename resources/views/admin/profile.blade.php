@extends('layout.admin.main', ['title' => 'Profil'])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endpush

@section('content')
    <x-admin-card header="Profil Admin">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Nama
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-name"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item list-group-item-secondary d-flex justify-content-between align-items-center">
                Alamat Email
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-email"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Kata Sandi
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-password"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
            <li class="list-group-item list-group-item-secondary d-flex justify-content-between align-items-center">
                Avatar
                <x-admin-button size="sm" data-bs-toggle="modal" data-bs-target="#edit-avatar"><i class="fa-solid fa-edit me-2"></i>Ubah</x-admin-button>
            </li>
        </ul>
    </x-admin-card>
@endsection

@section('modal')
    <x-admin-modal id="edit-name" title="Ubah Nama">
        <x-admin-form method="put" id="form-name" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="name">
            <x-admin-form-input id="form-edit-name" name="value" value="{{ auth()->user()->name }}" required/>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-email" title="Ubah Alamat Email">
        <x-admin-form method="put" id="form-email" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="email">
            <x-admin-form-input id="form-edit-email" name="value" value="{{ auth()->user()->email }}" type="email" required/>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-password" title="Ubah Kata Sandi">
        <x-admin-form method="put" id="form-password" submit="Simpan Perubahan" submitWide>
            <x-admin-form-input id="form-edit-current-password" name="current_password" type="password" label="Kata Sandi Sebelumnya" class="mb-3" required/>
            <x-admin-form-input id="form-edit-new-password" name="password" type="password" label="Kata Sandi Baru" class="mb-3" required/>
            <x-admin-form-input id="form-edit-new-password-confirmation" name="password_confirmation" type="password" label="Konfirmasi Kata Sandi Baru" class="mb-3" required/>
        </x-admin-form>
    </x-admin-modal>

    <x-admin-modal id="edit-avatar" title="Ubah Avatar">
        <x-admin-form method="put" id="form-avatar" submit="Simpan Perubahan" submitWide>
            <input type="hidden" name="name" value="avatar">
            <x-admin-form-input-file id="form_edit_avatar" name="value" value="{{ auth()->user()->avatar }}" valueSize="{{ auth()->user()->avatar_size }}" formId='form-avatar' accepted="image/*" required/>
        </x-admin-form>
    </x-admin-modal>
@endsection

@push('js')
    <script src="{{ asset('js/dropzone.min.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            $('#form-name').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-name'));
                ajaxPost("{{ route('admin.profile.update.general') }}", data, "#edit-name")
            })

            $('#form-email').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-email'));
                ajaxPost("{{ route('admin.profile.update.general') }}", data, "#edit-email")
            })

            $('#form-password').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-password'));
                ajaxPost("{{ route('admin.profile.update.password') }}", data, "#edit-password")
            })

            $('#form-avatar').submit(function (e) {
                e.preventDefault()
                loading()

                const data = new FormData(document.getElementById('form-avatar'));
                ajaxPost("{{ route('admin.profile.update.avatar') }}", data, "#edit-avatar")
            })
        })
    </script>
@endpush
