@extends('layout.admin.auth', ['title' => 'Masuk'])

@push('css')
    <link rel="stylesheet" href="{{ asset('themes/admin/css/pages/page-auth.css') }}" />
@endpush

@section('content')
    <h4 class="mb-2">Masuk</h4>
    <p class="mb-4">Silakan masukkan kredensial Anda.</p>

    <x-admin-form action="{{ route('login') }}" method="post" submit="Masuk" submitIcon="bx bxs-key" submitWide>
        <x-admin-form-input id="username" name="username" label="Alamat Email atau Nomor HP" class="mb-3" required />
        <x-admin-form-input id="password" name="password" type="password" label="Kata Sandi" required />
    </x-admin-form>
@endsection
