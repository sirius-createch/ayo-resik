@extends('layout.app.main', ['title' => 'Pengaturan', 'back' => route('app.partner.profile.index')])

@section('contents')
    <x-app-form id="form-edit" method="put" class="pt-2">
        <x-app-title>Atur Jadwal Ambil</x-app-title>

        <div class="m-3">
            <x-app-button id="" size="s" color="bg-blue-dark" class="w-100" data-menu="sheet-day-taken">
                <div class="d-flex justify-content-between align-items-center">
                    <div id="showed-days">{{ implode(', ', auth()->user()->schedules_converted) }}</div>
                    <div><i class="fa-solid fa-caret-down"></i></div>
                </div>
            </x-app-button>
            <input type="hidden" name="schedule[monday]" value="{{ auth()->user()->schedule->monday ? 1 : 0 }}">
            <input type="hidden" name="schedule[tuesday]" value="{{ auth()->user()->schedule->tuesday ? 1 : 0 }}">
            <input type="hidden" name="schedule[wednesday]" value="{{ auth()->user()->schedule->wednesday ? 1 : 0 }}">
            <input type="hidden" name="schedule[thursday]" value="{{ auth()->user()->schedule->thursday ? 1 : 0 }}">
            <input type="hidden" name="schedule[friday]" value="{{ auth()->user()->schedule->friday ? 1 : 0 }}">
            <input type="hidden" name="schedule[saturday]" value="{{ auth()->user()->schedule->saturday ? 1 : 0 }}">
            <input type="hidden" name="schedule[sunday]" value="{{ auth()->user()->schedule->sunday ? 1 : 0 }}">
        </div>

        <x-app-title>Atur Produk</x-app-title>

        <p class="m-3 text-center color-green-dark">Masukkan harga per kilogramnya.</p>

        <div class="m-3 pb-1">

            <div id="selected-products">
                @foreach (auth()->user()->products as $product)
                    <div class="card card-style mb-1" style="overflow: unset">
                        <div class="content fw-bold">
                            <button type="button" class="btn rounded-xs text-uppercase font-700 shadow-s bg-red-dark remove-product" style="position: absolute; left: -13px; top: 32%; padding: 1px 4px" data-id="{{ $product->id }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex justify-content-start align-items-center w-75">
                                    <img src="{{ $product->image_link }}" style="height: 35px; width: 35px; object-fit: cover;" class="mx-3" alt="Icon {{ $product->name }}">
                                    <p class="p-0">{{ $product->name }}</p>
                                </div>
                                <div class="">
                                    <input class="form-control to-rupiah text-end" value="{{ number_format($product->pivot->price, thousands_separator:'.') }}" name="products[{{ $product->id }}]">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="text-center mt-4">
                <x-app-button size="s" color="bg-blue-dark" icon="fa-solid fa-plus" data-menu="sheet-products">Tambah Jenis</x-app-button>
            </div>

        </div>

        <div class="text-center mt-4 mb-3 mx-3">
            <x-app-button size="l" icon="fa-solid fa-paper-plane" class="w-100" type="submit">Simpan Perubahan</x-app-button>
        </div>
    </x-app-form>
@endsection

@section('sheets')
    <x-app-sheet id="sheet-day-taken" title="Pilih Hari">
        <x-app-list-group class="mx-3">
            @foreach (['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'] as $day)
                <div class="mb-2 border-bottom">
                    <div class="form-check icon-check">
                        <input class="form-check-input" type="checkbox" value="{{ $day }}" id="check-{{ $day }}" @checked(auth()->user()->schedule->$day)>
                        <label class="form-check-label" for="check-{{ $day }}">{{ Sirius::longDay(date('w', strtotime($day))) }}</label>
                        <i class="icon-check-1 far fa-square color-gray-dark font-16"></i>
                        <i class="icon-check-2 far fa-check-square font-16 color-highlight"></i>
                    </div>
                </div>
            @endforeach
        </x-app-list-group>
        <div class="m-3">
            <x-app-button class="mb-4 w-100 close-menu" id="select-day"><i class="fa-solid fa-check me-2"></i>Pilih</x-app-button>
        </div>
    </x-app-sheet>


    <x-app-sheet id="sheet-products" title="Tambah Jenis Sampah">
        <div class="me-4 ms-3 mt-2">
            <x-app-form method="post" id="form-search-products">
                <x-app-form-input id="search-products" class="mb-2" name="search" placeholder="Cari ..." >
                    @slot('subLabel')
                        <i class="fa-solid fa-search"></i>
                    @endslot
                </x-app-form-input>
            </x-app-form>
            <div class="list-group list-custom-small close-menu mb-4" id="products">

            </div>
        </div>
    </x-app-sheet>
@endsection

@push('js')
    <script>
        const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

        let products = {}
        let selectedProducts = @json(auth()->user()->products->pluck('id')->toArray())

        function documentReady() {
            $("#select-day").click(function () {
                let selected = [];
                days.forEach(day => {
                    if ($("#check-" + day).is(":checked")) {
                        $('input[name="schedule[' + day + ']"]').eq(0).val(1)
                        selected.push($("#check-" + day).parent().find('label').eq(0).html())
                    }
                    else {
                        $('input[name="schedule[' + day + ']"]').eq(0).val(0)
                    }
                });
                $("#showed-days").html(selected.join(', '))
            })

            $("#search-products").change(function () {
                $("#products").html(`
                    <div>
                        <i class="fa-solid fa-spin fa-circle-notch"></i><span class="d-inline-block ms-2">Sedang mencari ...</span>
                    </div>
                `)

                const data = new FormData(document.getElementById('form-search-products'))
                data.append('selected', JSON.stringify(selectedProducts))

                ajaxPost("{{ route('app.partner.products.search') }}", data, null, function (result) {
                    products = result.data
                    $("#products").html(``)

                    if (Object.keys(products).length > 0) {
                        $.each(products, function (index, product) {
                            $("#products").append(`
                                <div class="mb-2 border-bottom d-flex justify-content-between align-items-center product" data-id="` + product.id + `">
                                    <div><img class="me-3 mt-2" src="` + product.image + `"><span>` + product.name + `</span></div>
                                </div>
                            `)
                        })
                    } else {
                        $("#products").html(`Produk yang dicari tidak ditemukan.`)
                    }
                }, function (errorResponse) {
                    $("#products").html(`<x-app-chip color='red' icon="fa-solid fa-xmark">` + errorResponse.message + `</x-app-chip>`)
                }, false)
            })

            $("#search-products").change()

            $("#form-search-products").submit(function(e) {
                e.preventDefault()
            })

            $(document).on('click', '.product', function () {
                const id = $(this).data('id')
                const product = products[id]
                selectedProducts.push(id)

                $("#selected-products").append(`
                    <div class="card card-style mb-1" style="overflow: unset">
                        <div class="content fw-bold">
                            <button type="button" class="btn rounded-xs text-uppercase font-700 shadow-s bg-red-dark remove-product" data-id="` + product.id + `" style="position: absolute; left: -13px; top: 32%; padding: 1px 4px">
                                <i class="fa-solid fa-minus"></i>
                            </button>

                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex justify-content-start align-items-center w-75">
                                    <img src="` + product.image + `" style="height: 35px; width: 35px; object-fit: cover;" class="mx-3" alt="Icon ` + product.name + `">
                                    <p class="p-0">` + product.name + `</p>
                                </div>
                                <div class="">
                                    <input class="form-control to-rupiah text-end" value="` + product.price + `" name="products[` + product.id + `]">
                                </div>
                            </div>
                        </div>
                    </div>
                `)

                $(this).remove()
            })

            $(document).on('click', '.remove-product', function () {
                const id = $(this).data('id')
                selectedProducts.splice( $.inArray(id, selectedProducts), 1 );

                $(this).parent().parent().remove()
                $("#search-products").change()
            })

            $("#form-edit").submit(function (e) {
                e.preventDefault();

                loadingWithText("Menyimpan perubahan ...");

                const data = new FormData(document.getElementById('form-edit'));

                ajaxPost("{{ route('app.partner.profile.edit.setting') }}", data, null, function (result) {
                    Swal.fire({
                        title: "Pengaturan disimpan!",
                        icon: 'success',
                    })
                }, function (errorResponse) {
                    Swal.fire({
                        title: "Gagal menyimpan!",
                        icon: 'error',
                        text: errorResponse.message
                    })
                }, false)
            })
        }
    </script>
@endpush
