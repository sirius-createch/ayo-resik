@extends('layout.app.main', [
    'title' => 'Dasbor',
    'headerSticky' => '<img src="'.asset('images/assets/logo.png').'" style="width:100px">',
    'headerTop' => '<img src="'.asset('images/assets/logo.png').'" style="width:150px">',
])

@section('contents')
    <div class="card card-style gradient-grey">
        <div class="content">
            <div class="d-flex justify-content-between align-items-center px-2">
                <div>
                    <p class="mb-n1 opacity-70 font-600">Selamat datang,</p>
                    <div class="h1 fw-bold">{{ auth()->user()->name }}</div>
                </div>
            </div>
            <hr class="my-2">
            <div class="d-flex justify-content-between align-items-center px-2 gap-2">
                <div>
                    <p class="mb-n1 opacity-70 font-600">Nominal Sampah</p>
                    <div class="h1 fw-bold gradient-grass color-white rounded-s p-2 my-2">{{ Sirius::toRupiah($nominal) }}</div>
                </div>
                <div>
                    <p class="mb-n1 opacity-70 font-600">Berat Sampah</p>
                    <div class="h1 fw-bold gradient-grass color-white rounded-s p-2 my-2">{{ Str::replace(',0', '', number_format($weight, 1, ',', '.')).' kg' }}</div>
                </div>
            </div>
        </div>
    </div>

    <x-app-title>
        @slot('actions')
            <x-app-button color="bg-blue-dark" size="s" link="{{ route('app.partner.orders.index') }}"><i class="fa-solid fa-arrow-right font-15"></i></x-app-button>
        @endslot
        Pesanan Terkini
    </x-app-title>

    <div class="my-3">
        @forelse ($invoices as $invoice)
            <a href="{{ route('app.partner.orders.show', ['invoice' => $invoice->code]) }}">
                <x-app-card class="mb-3">
                    <div class="d-flex justify-content-between align-items-center mb-0">
                        <p class="fw-bold font-15 mb-0">Pesanan #{{ $invoice->code }}</p>
                        <p class="fw-bold opacity-70">{{ Sirius::toLongDate($invoice->created_at) }}</p>
                    </div>
                    <p class="mb-0 fw-bold text-{{ App\Models\Invoice::mappingCurrentStatusColor($invoice->current_status) }}">{{ $invoice->current_status_converted }}</p>
                    <p class="mb-0">Total Jenis: {{ $invoice->products->count() }} Jenis Sampah</p>
                    <p class="mb-0">Pelanggan: {{ $invoice->customer->name ?? "Customer Tidak Diketahui" }}</p>
                    <p class="mb-0">Alamat: {{ $invoice->address }}</p>
                </x-app-card>
            </a>
        @empty
            <div class="mx-3">
                <x-app-chip color="blue" icon="fa-solid fa-check-double" class="w-100">Semua pesanan telah selesai diproses.</x-app-chip>
            </div>
        @endforelse
    </div>

    <x-app-title class="mt-4">
        @slot('actions')
            <x-app-button color="bg-blue-dark" size="s" link="{{ route('app.partner.withdrawals.index') }}"><i class="fa-solid fa-arrow-right font-15"></i></x-app-button>
        @endslot
        Tarikan Saldo Terkini
    </x-app-title>

    <div class="my-3">
         @forelse ($withdrawals as $withdrawal)
            <a href="{{ route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]) }}">
                <x-app-card class="mb-3">
                    <div class="d-flex justify-content-between align-items-center mb-0">
                        <p class="fw-bold font-15 mb-0">Pesanan #{{ $withdrawal->code }}</p>
                        <p class="fw-bold opacity-70">{{ Sirius::toLongDate($withdrawal->created_at) }}</p>
                    </div>
                    <p class="mb-0 fw-bold text-{{ App\Models\Withdrawal::mappingCurrentStatusColor($withdrawal->current_status) }}">{{ $withdrawal->current_status_converted }}</p>
                    <p class="mb-0">Total Tunai: {{ Sirius::toRupiah($withdrawal->amount) }}</p>
                    <p class="mb-0">Pelanggan: {{ $withdrawal->customer->name }}</p>
                </x-app-card>
            </a>
        @empty
            <div class="mx-3">
                <x-app-chip color="blue" icon="fa-solid fa-check-double" class="w-100">Semua permintaan penarikan telah selesai diproses.</x-app-chip>
            </div>
        @endforelse
    </div>

@endsection
