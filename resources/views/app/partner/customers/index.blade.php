@extends('layout.app.main', ['title' => 'Daftar Customer', 'back' => route('app.partner.profile.index')])

@section('contents')
    @php
        $orders = [
            'name' => 'Nama',
            'address' => 'Alamat',
        ];

        $data = ['id', 'name', 'avatar', 'address', 'isConfirmed']
    @endphp

    <x-app-listing id="listing" name="Customer" :orders="$orders" :route="route('app.partner.profile.customers.index')" :data="$data">
        @slot('skeleton')
            <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                <div class="mb-0 text-center">
                    <p class="m-1 py-3 w-25" style="margin: auto!important">skeleton</p>
                    <p class="m-1 w-100">skeleton</p>
                    <p class="m-1 w-100">skeleton</p>
                </div>
            </div>
        @endslot

        <a href="{{ route('app.partner.profile.customers.show', ['customer' => '[[id]]']) }}">
            <x-app-card class="mb-3">
                <div class="row mb-0">
                    <div class="col-3">
                        <p class="fw-bold"><img src="[[avatar]]" style="height: 50px; width: 50px; object-fit: cover;" class="mx-3" alt="Foto [[name]]"></p>
                    </div>
                    <div class="col">
                        <p class="fw-bold font-15 mb-0 text-start">[[name]]</p>
                        <p class="mb-0 text-start">[[address]]</p>
                        [[isConfirmed]]
                    </div>
                </div>
            </x-app-card>
        </a>
    </x-app-listing>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()
        }
    </script>
@endpush
