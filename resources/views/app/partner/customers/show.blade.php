@extends('layout.app.main', ['title' => 'Detail Customer', 'back' => route('app.partner.profile.customers.index'), 'footer' => $isConfirmed])

@section('contents')
    <div class="card card-style" data-card-height="250" style="height: 250px; background-image: url('{{ $customer->avatar_link }}')">
    </div>

    <x-app-card title="Informasi Umum">
        <div class="border-bottom pb-1 mb-1">
            <b>Nama:</b><br>
            <span>{{ $customer->name }}</span><br>
        </div>
        <div class="border-bottom pb-1 mb-1">
            <b>Alamat Email:</b><br>
            <span>{{ $customer->email }}</span><br>
        </div>
        <div class="border-bottom pb-1 mb-1">
            <b>Nomor Ponsel:</b><br>
            <span>{{ $customer->phone_converted }}</span><br>
        </div>
        <div class="border-bottom pb-1 mb-2">
            <b>Alamat:</b><br>
            <span>{{ $customer->address }}</span><br>
            <div id="show-map" class="my-2 w-100 rounded-s" style="height: 200px"></div>
        </div>
        <div class="border-bottom pb-1 mb-2">
            <b>Sudah Terhubung Sejak:</b><br>
            <span id="linked-date">{{ $customer->partner_confirm_at ? Sirius::toLongDateDayTime($customer->partner_confirm_at, separator: ' - ') : 'Belum dikonfirmasi!' }}</span><br>
        </div>
    </x-app-card>
@endsection

@section('footer')
    @if (!$isConfirmed)
        <div id="footer-bar" class="action-button">
            <div class="mx-3 mb-3 w-100">
                <x-app-button size="l" class="w-100" id="confirm"><i class="fa-solid fa-link me-2"></i>Konfirmasi Hubungan</x-app-button>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script>
        var map = L.map('show-map').setView([{{ $customer->latitude }}, {{ $customer->longitude }}], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([{{ $customer->latitude }}, {{ $customer->longitude }}]).addTo(map)

        @if (!$isConfirmed)
            function documentReady() {
                $("#confirm").click(function() {
                    DenyConfirm.fire({
                        title: "Hubungkan?",
                        text: "Setelah terhubung customer dapat mulai menyetorkan sampahnya!",
                        confirmButtonText: "<i class='fa-solid fa-check btn-icon-text me-2'></i>Hubungkan",
                        denyButtonText: "<i class='fa-solid fa-close btn-icon-text me-2'></i>Tolak Permintaan"
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menghubungkan dengan customer ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('confirm', '1')

                            ajaxPost("{{ route('app.partner.profile.customers.update', ['customer' => $customer->id]) }}", data, null, function (result) {
                                $("#footer-bar").remove()

                                $("#linked-date").html(result.data.now)

                                Swal.fire({
                                    title: "Penghubungan berhasil!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menghubungkan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                        if (result.isDenied) {
                            loadingWithText('Menolak hubungan dengan customer ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('confirm', '0')

                            ajaxPost("{{ route('app.partner.profile.customers.update', ['customer' => $customer->id]) }}", data, null, function (result) {
                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Hubungan ditolak!",
                                    icon: 'success'
                                });

                                setTimeout(() => {
                                    window.location.href = "{{ route('app.partner.profile.customers.index') }}"
                                }, 1000);
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menolak hubungan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })
            }
        @endif
    </script>
@endpush
