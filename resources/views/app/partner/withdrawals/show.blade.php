@extends('layout.app.main', ['title' => 'Detail Tarikan', 'back' => route('app.partner.withdrawals.index'), 'footer' => !in_array($withdrawal->current_status, ['seen', 'confirmed', 'not_received'])])

@section('contents')
    <div class="timeline-body mt-0">
        <div class="timeline-deco"></div>

        @if (in_array($withdrawal->current_status, ['seen', 'confirmed', 'not_received']))
            <div id="current-status-template" class="d-none">
                <div class="timeline-item mt-4">
                    <i class="[[icon]] bg-[[color]] color-white shadow-l timeline-icon"></i>
                    <div class="timeline-item-content rounded-s">
                        <h5 class="font-400 pt-1 pb-1">
                            [[name]]<br>
                            <span class="opacity-30">[[datetime]]</span>
                        </h5>
                    </div>
                </div>
            </div>
            <div id="container-new-status">

            </div>
        @endif

        @foreach ($statuses as $status)
            <div class="timeline-item mt-4">
                <i class="{{ $status['icon'] }} bg-{{ $status['color'] }} color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        {{ $status['name'] }}<br>
                        <span class="opacity-30">{{ $status['datetime'] }}</span>
                    </h5>
                </div>
            </div>
        @endforeach

    </div>

    <x-app-title>Permintaan #{{ $withdrawal->code }}</x-app-title>

    <x-app-card class="my-3">
        <div class="d-flex justify-content-between align-items-center mb-0">
            <p class="fw-bold font-15 mb-0">{{ $withdrawal->customer->name }}</p>
            <p class="fw-bold opacity-70">{{ Sirius::toLongDate($withdrawal->created_at) }}</p>
        </div>
        <p class="mb-0">Alamat: {{ $withdrawal->customer->address }}</p>
    </x-app-card>

    <x-app-card class="my-3 text-center">
        <div class="mb-2">Nominal Ditarik:</div>
        <div class="fw-bold font-30 mb-2">{{ Sirius::toRupiah($withdrawal->amount) }}</div>
    </x-app-card>

@endsection

@section('footer')
    @if (in_array($withdrawal->current_status, ['seen', 'confirmed', 'not_received']))
        <div id="footer-bar" class="action-button">
            <div class="mx-3 mb-3 w-100">
                {{-- Seen --}}
                <div id="footer-seen" class="row @if ($withdrawal->current_status != 'seen') d-none @endif">
                    <div class="col-6">
                        <x-app-button size="l" color="bg-red-dark" class="w-100" id="deny"><i class="fa-solid fa-xmark me-2"></i>Tolak</x-app-button>
                    </div>
                    <div class="col-6">
                        <x-app-button size="l" color="bg-highlight" class="w-100" id="confirm"><i class="fa-solid fa-check me-2"></i>Terima</x-app-button>
                    </div>
                </div>
                {{-- Verified --}}
                <div id="footer-confirmed-not" class="@if (!in_array($withdrawal->current_status, ['confirmed', 'not_received'])) d-none @endif">
                    <x-app-button size="l" color="bg-highlight" class="w-100" id="done"><i class="fa-solid fa-check-double me-2"></i>Sudah Diberikan</x-app-button>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script>
        @if (in_array($withdrawal->current_status, ['seen', 'confirmed', 'not_received']))
            function documentReady() {
                $("#deny").click(function() {
                    DeleteConfirm.fire({
                        title: "Tolak permintaan?",
                        text: "Yakin ingin menolak permintaan?",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menolak permintaan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'denied')

                            ajaxPost("{{ route('app.partner.withdrawals.update', ['withdrawal' => $withdrawal->code]) }}", data, null, function (result) {
                                const status = result.data.status
                                var currentStatus = $("#current-status-template").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                                var containerNewStatus = $("#container-new-status").html()
                                $("#container-new-status").html(currentStatus)
                                $("#container-new-status").append(containerNewStatus)

                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Permintaan ditolak!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menolak permintaan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })

                $("#confirm").click(function() {
                    Confirm.fire({
                        title: "Terima permintaan?",
                        text: "Yakin ingin menerima permintaan?",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menerima permintaan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'confirmed')

                            ajaxPost("{{ route('app.partner.withdrawals.update', ['withdrawal' => $withdrawal->code]) }}", data, null, function (result) {
                                const status = result.data.status
                                var currentStatus = $("#current-status-template").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                                var containerNewStatus = $("#container-new-status").html()
                                $("#container-new-status").html(currentStatus)
                                $("#container-new-status").append(containerNewStatus)

                                $("#footer-seen").addClass('d-none')
                                $("#footer-confirmed-not").removeClass('d-none')

                                Swal.fire({
                                    title: "Permintaan diterima!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menerima permintaan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })

                $("#done").click(function() {
                    Confirm.fire({
                        title: "Uang sudah diberikan?",
                        text: "Pastikan uang sudah yang diminta sudah sesuai dengan nominal yang ditarik.",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menyelesaikan permintaan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'sent')

                            ajaxPost("{{ route('app.partner.withdrawals.update', ['withdrawal' => $withdrawal->code]) }}", data, null, function (result) {
                                const status = result.data.status
                                var currentStatus = $("#current-status-template").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                                var containerNewStatus = $("#container-new-status").html()
                                $("#container-new-status").html(currentStatus)
                                $("#container-new-status").append(containerNewStatus)

                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Customer diberitahu!",
                                    text: "Setelah customer mengonfirmasi bahwa uang telah diterima, permintaan akan selesai!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menyelesaikan permintaan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })
            }
        @endif
    </script>
@endpush
