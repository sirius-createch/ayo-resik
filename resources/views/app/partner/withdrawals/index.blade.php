@extends('layout.app.main', ['title' => 'Tarik Tunai'])

@section('contents')
    @php
        $orders = [
            'code' => 'Kode',
            'created_at' => 'Tanggal',
            'current_status' => 'Status',
            'amount' => 'Nominal Tarik'
        ];

        $data = ['code', 'date', 'statusColor', 'status', 'amount', 'customer', 'address'];
    @endphp

    <x-app-listing id="listing" name="Riwayat penarikan" :orders="$orders" :route="route('app.partner.withdrawals.index')" :data="$data" sort="desc">
        @slot('skeleton')
            <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="m-1 w-75">skeleton</p>
                    <p class="m-1 w-25">skeleton</p>
                </div>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
            </div>
        @endslot

        <a href="{{ route('app.partner.withdrawals.show', ['withdrawal' => '[[code]]']) }}">
            <x-app-card class="mb-3">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="fw-bold font-15 mb-0">Permintaan #[[code]]</p>
                    <p class="fw-bold opacity-70">[[date]]</p>
                </div>
                <p class="mb-0 fw-bold text-[[statusColor]]">[[status]]</p>
                <p class="mb-0">Nominal Tarik: [[amount]]</p>
                <p class="mb-0">Pelanggan: [[customer]]</p>
                <p class="mb-0">Alamat: [[address]]</p>
            </x-app-card>
        </a>
    </x-app-listing>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()
        }
    </script>
@endpush
