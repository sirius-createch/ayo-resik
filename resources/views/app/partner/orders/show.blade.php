@extends('layout.app.main', ['title' => 'Detail Pesanan', 'back' => route('app.partner.orders.index'), 'footer' => !in_array($invoice->current_status, ['seen', 'confirmed', 'verified'])])

@section('contents')
    <div class="timeline-body mt-0">
        <div class="timeline-deco"></div>

        @if (in_array($invoice->current_status, ['seen', 'confirmed', 'verified']))
            <div id="current-status-template" class="d-none">
                <div class="timeline-item mt-4">
                    <i class="[[icon]] bg-[[color]] color-white shadow-l timeline-icon"></i>
                    <div class="timeline-item-content rounded-s">
                        <h5 class="font-400 pt-1 pb-1">
                            [[name]]<br>
                            <span class="opacity-30">[[datetime]]</span>
                        </h5>
                    </div>
                </div>
            </div>
            <div id="container-new-status">

            </div>
        @endif

        @foreach ($statuses as $status)
            <div class="timeline-item mt-4">
                <i class="{{ $status['icon'] }} bg-{{ $status['color'] }} color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        {{ $status['name'] }}<br>
                        <span class="opacity-30">{{ $status['datetime'] }}</span>
                    </h5>
                </div>
            </div>
        @endforeach

    </div>

    <x-app-title>Pesanan #{{ $invoice->code }}</x-app-title>

    <x-app-card class="my-3">
        <div class="d-flex justify-content-between align-items-center mb-0">
            <p class="fw-bold font-15 mb-0">{{ $invoice->customer->name }}</p>
            <p class="fw-bold opacity-70">{{ Sirius::toLongDate($invoice->created_at) }}</p>
        </div>
        <p class="mb-0">Alamat: {{ $invoice->customer->address }}</p>
        <p class="mb-0">Metode Bayar: {{ $invoice->payment_method->name }}</p>
        <p class="mb-0">Jadwal Ambil: <span id="detail-day-taken">{{ $invoice->day_taken_converted }}</span></p>
        <p class="mb-0">Catatan: {{ ($invoice->note && $invoice->note != '') ? $invoice->note : '-' }}</p>
    </x-app-card>

    <x-app-card class="my-3 pb-2">
        <p class="fw-bold font-15 mb-0">Jenis Item</p>
        @foreach ($invoice->products as $product)
            <div class="border-top mt-2 pt-2">
                <div class="d-flex justify-content-start align-items-center">
                    <img src="{{ $product->image_link }}" style="height: 50px; width: 50px; object-fit: cover;" class="mx-3" alt="Icon {{ $product->name }}">
                    <div class="w-100">
                        <p class="p-0 mb-0 fw-bold">{{ $product->name }}</p>
                        <div class="row mb-0">
                            <div class="col-4">{{ Sirius::toRupiah($product->pivot->price) }}</div>
                            <div class="col-3 text-end" id="weight-{{ $product->id }}">{{ Str::replace(',0', '', number_format($product->pivot->weight_real ?? $product->pivot->weight, 1, ',', '.')) . ' kg' }}</div>
                            <div class="col-5 text-end" id="price-{{ $product->id }}">{{ Sirius::toRupiah($product->pivot->price * ($product->pivot->weight_real ?? $product->pivot->weight)) }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <p class="fw-bold font-15 mb-2 mt-4">Pembayaran</p>

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>Total Harga</div>
            <div class="fw-bold" id="total-price">{{ Sirius::toRupiah($invoice->total_bill + $invoice->service_charge) }}</div>
        </div>

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>Biaya Layanan</div>
            <div class="fw-bold" id="service-charge">{{ Sirius::toRupiah($invoice->service_charge) }}</div>
        </div>

        <hr class="my-2">

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>Total Pembayaran</div>
            <div class="fw-bold" id="estimate-price">{{ Sirius::toRupiah($invoice->total_bill) }}</div>
        </div>

    </x-app-card>

    <x-app-card class="my-3">
        <p class="mb-0 fw-bold">Alamat Pengambilan:</p>
        <p class="mb-0">{{ $invoice->address }}</p>
        <div id="map" class="my-2 w-100 rounded-s" style="height: 200px"></div>
    </x-app-card>
@endsection

@section('sheets')
    @if (in_array($invoice->current_status, ['seen', 'confirmed', 'verified']))
        <x-app-sheet id="sheet-confirm" title="Pilih Hari Pengambilan">
            <div class="mx-3">
                <div class="input-style has-borders no-icon w-100">
                    <select id="day-taken" name="day-taken">
                        @foreach (auth()->user()->schedules as $schedule)
                            <option value="{{ $schedule }}">{{ Sirius::longDay(date('w', strtotime($schedule))) }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa-solid fa-chevron-down"></i></span>
                </div>
                <x-app-button size="l" color="bg-highlight" class="w-100 mt-3 close-menu" id="confirm"><i class="fa-solid fa-check me-2"></i>Konfirmasi</x-app-button>
            </div>
        </x-app-sheet>
        <x-app-sheet id="sheet-verify" title="Verifikasi Berat Sampah">
            <div class="mx-3">
                @foreach ($invoice->products as $product)
                <div class="mb-2 pb-2 border-bottom d-flex justify-content-between align-items-center">
                    <div><img class="me-3" style="height: 50px; width: 50px; object-fit: cover;" src="{{ $product->image_link }}"><span class="fw-bold">{{ $product->name }}</span></div>
                    <div>
                        <div class="d-flex justify-content-start align-items-center gap-2">
                            <div class="stepper rounded-s float-start">
                                <a href="#" class="stepper-sub bg-highlight rounded-start" data-id="{{ $product->id }}"><i class="fa-solid fa-minus"></i></a>
                                <input value="{{ str_replace(',0', '', number_format($product->pivot->weight, 1, ',', '.')) }}" name="product[{{ $product->id }}]" data-id="{{ $product->id }}" id="product-{{ $product->id }}" class="to-unit">
                                <a href="#" class="stepper-add bg-highlight rounded-end" data-id="{{ $product->id }}"><i class="fa-solid fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <x-app-button size="l" color="bg-highlight" class="w-100 mt-2 mb-5 close-menu" id="verify"><i class="fa-solid fa-check me-2"></i>Verifikasi</x-app-button>
            </div>
        </x-app-sheet>
    @endif
@endsection

@section('footer')
    @if (in_array($invoice->current_status, ['seen', 'confirmed', 'verified']))
        <div id="footer-bar" class="action-button">
            <div class="mx-3 mb-3 w-100">
                {{-- Seen --}}
                <div id="footer-seen" class="row @if ($invoice->current_status != 'seen') d-none @endif">
                    <div class="col-6">
                        <x-app-button size="l" color="bg-red-dark" class="w-100" id="deny"><i class="fa-solid fa-x me-2"></i>Tolak</x-app-button>
                    </div>
                    <div class="col-6">
                        <x-app-button size="l" color="bg-highlight" class="w-100" data-menu="sheet-confirm"><i class="fa-solid fa-check me-2"></i>Terima</x-app-button>
                    </div>
                </div>
                {{-- Confirmed --}}
                <div id="footer-confirmed" class="@if ($invoice->current_status != 'confirmed') d-none @endif">
                    <x-app-button size="l" color="bg-magenta-dark" class="w-100" data-menu="sheet-verify"><i class="fa-solid fa-clipboard-check me-2"></i>Verifikasi</x-app-button>
                </div>
                {{-- Verified --}}
                <div id="footer-verified" class="@if ($invoice->current_status != 'verified') d-none @endif">
                    <x-app-button size="l" color="bg-highlight-dark" class="w-100" id="done"><i class="fa-solid fa-check-double me-2"></i>Selesai</x-app-button>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script>
        var map = L.map('map').setView([{{ $invoice->latitude }}, {{ $invoice->longitude }}], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([{{ $invoice->latitude }}, {{ $invoice->longitude }}]).addTo(map)

        @if (in_array($invoice->current_status, ['seen', 'confirmed', 'verified']))
            function documentReady() {
                $("#deny").click(function() {
                    DeleteConfirm.fire({
                        title: "Tolak pesanan?",
                        text: "Yakin ingin menolak pesanan?",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menolak pesanan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'denied')

                            ajaxPost("{{ route('app.partner.orders.update', ['invoice' => $invoice->code]) }}", data, null, function (result) {
                                const status = result.data.status
                                var currentStatus = $("#current-status-template").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                                var containerNewStatus = $("#container-new-status").html()
                                $("#container-new-status").html(currentStatus)
                                $("#container-new-status").append(containerNewStatus)

                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Pesanan ditolak!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menolak pesanan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })

                $("#confirm").click(function() {
                    loadingWithText('Mengonfirmasi pesanan ...')

                    const data = new FormData()
                    data.append('_method', 'put')
                    data.append('_token', "{{ csrf_token() }}")
                    data.append('status', 'confirmed')
                    data.append('day_taken', $("#day-taken").val())

                    ajaxPost("{{ route('app.partner.orders.update', ['invoice' => $invoice->code]) }}", data, null, function (result) {
                        const status = result.data.status
                        const additional = result.data.additional

                        var currentStatus = $("#current-status-template").html()
                        currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                        currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                        currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                        currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                        var containerNewStatus = $("#container-new-status").html()
                        $("#container-new-status").html(currentStatus)
                        $("#container-new-status").append(containerNewStatus)

                        $("#detail-day-taken").html(additional.dayTaken)
                        $("#footer-seen").addClass('d-none')
                        $("#footer-confirmed").removeClass('d-none')

                        Swal.fire({
                            title: "Pesanan dikonfirmasi!",
                            icon: 'success'
                        });
                    }, function (error) {
                        Swal.fire({
                            title: "Gagal mengonfirmasi pesanan!",
                            icon: 'error',
                            text: error.message
                        })
                    }, false)
                })

                $("#verify").click(function() {
                    loadingWithText('Memverifikasi pesanan ...')

                    const data = new FormData()
                    data.append('_method', 'put')
                    data.append('_token', "{{ csrf_token() }}")
                    data.append('status', 'verified')
                    @foreach ($invoice->products as $product)
                        data.append('products[{{ $product->id }}]', $("#product-{{ $product->id }}").val())
                    @endforeach

                    ajaxPost("{{ route('app.partner.orders.update', ['invoice' => $invoice->code]) }}", data, null, function (result) {
                        const status = result.data.status
                        const additional = result.data.additional

                        var currentStatus = $("#current-status-template").html()
                        currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                        currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                        currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                        currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                        var containerNewStatus = $("#container-new-status").html()
                        $("#container-new-status").html(currentStatus)
                        $("#container-new-status").append(containerNewStatus)

                        $.each(additional.weight, function(id, weight) {
                            $("#weight-" + id).html(weight)
                        })

                        $.each(additional.price, function(id, price) {
                            $("#price-" + id).html(price)
                        })

                        $("#total-price").html(additional.total_price)
                        $("#service-charge").html(additional.service_charge)
                        $("#estimate-price").html(additional.total_bill)

                        $("#footer-confirmed").addClass('d-none')
                        $("#footer-verified").removeClass('d-none')

                        Swal.fire({
                            title: "Pesanan diverifikasi!",
                            icon: 'success'
                        });
                    }, function (error) {
                        Swal.fire({
                            title: "Gagal memverifikasi pesanan!",
                            icon: 'error',
                            text: error.message
                        })
                    }, false)
                })

                $("#done").click(function() {
                    Confirm.fire({
                        title: "Selesaikan pesanan?",
                        text: "Yakin ingin menyelesaikan pesanan?",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Menyelesaikan pesanan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'done')

                            ajaxPost("{{ route('app.partner.orders.update', ['invoice' => $invoice->code]) }}", data, null, function (result) {
                                const status = result.data.status
                                var currentStatus = $("#current-status-template").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)

                                var containerNewStatus = $("#container-new-status").html()
                                $("#container-new-status").html(currentStatus)
                                $("#container-new-status").append(containerNewStatus)

                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Pesanan telah selesai!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal menyelesaikan pesanan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })

                $(document).on('click', '.stepper-add', function () {
                    let input = $(this).parent().find('input').eq(0)
                    let id = $(this).data('id')
                    changeWeight(input, 1, id)
                })

                $(document).on('click', '.stepper-sub', function () {
                    let input = $(this).parent().find('input').eq(0)
                    let id = $(this).data('id')
                    changeWeight(input, -1, id)
                })
            }

            function changeWeight(input, nominal, id) {
                let value = parseFloat(input.val().replaceAll('.', '').replaceAll(',', '.')) + nominal
                if (value < 0) value = 0
                input.val(ToUnit.format(value)).trigger('change')
            }
        @endif
    </script>
@endpush
