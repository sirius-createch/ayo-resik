@extends('layout.app.main', ['title' => 'Laporan'])

@section('contents')
    <x-app-card>
        <div id="reports">
            @php
                $tabs = [
                    [
                        'title' => 'Laporan Nominal',
                        'target' => '#nominal-report'
                    ],
                    [
                        'title' => 'Laporan Berat',
                        'target' => '#weight-report'
                    ],
                ]
            @endphp

            <x-app-tab-group-controls :tabs=$tabs />

            <x-app-tab-group-item parent="#reports" id="nominal-report" class="show">

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="nominal-period">
                        <option value="daily">Harian</option>
                        <option value="monthly">Bulanan</option>
                        <option value="yearly">Tahunan</option>
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="nominal-daily">
                        @foreach ($days as $key => $day)
                            <option value="{{ $key }}">{{ $day }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="nominal-monthly" class="d-none">
                        @foreach ($months as $key => $month)
                            <option value="{{ $key }}">{{ $month }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="nominal-yearly" class="d-none">
                        @foreach ($years as $year)
                            <option value="{{ $year }}">{{ $year }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="card card-style gradient-highlight mx-0 mb-3">
                    <div class="content">
                        <div class="d-flex justify-content-between align-items-center px-2">
                            <div>
                                <p class="mb-n1 color-white opacity-70 font-600">Total Biaya Layanan:</p>
                                <div class="color-white h1 fw-bold" id="total-service-charge">Rp0,-</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-style gradient-highlight mx-0">
                    <div class="content">
                        <div class="d-flex justify-content-between align-items-center px-2">
                            <div>
                                <p class="mb-n1 color-white opacity-70 font-600">Total Nominal Sampah:</p>
                                <div class="color-white h1 fw-bold" id="total-nominal">Rp0,-</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="container-nominal-chart">
                    <canvas id="nominal-chart" class="w-100" style="height:200px"></canvas>
                </div>
            </x-app-tab-group-item>

            <x-app-tab-group-item parent="#reports" id="weight-report">

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="weight-period">
                        <option value="daily">Harian</option>
                        <option value="monthly">Bulanan</option>
                        <option value="yearly">Tahunan</option>
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="weight-daily">
                        @foreach ($days as $key => $day)
                            <option value="{{ $key }}">{{ $day }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="weight-monthly" class="d-none">
                        @foreach ($months as $key => $month)
                            <option value="{{ $key }}">{{ $month }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="input-style has-borders no-icon" style="margin-bottom: 1rem!important">
                    <select id="weight-yearly" class="d-none">
                        @foreach ($years as $year)
                            <option value="{{ $year }}">{{ $year }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa fa-chevron-down"></i></span>
                </div>

                <div class="card card-style gradient-highlight mx-0">
                    <div class="content">
                        <div class="d-flex justify-content-between align-items-center px-2">
                            <div>
                                <p class="mb-n1 color-white opacity-70 font-600">Total Berat Sampah:</p>
                                <div class="color-white h1 fw-bold" id="total-weight">100 kg</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="container-weight-chart">
                    <canvas id="weight-chart" class="w-100" style="height:500px"></canvas>
                </div>
            </x-app-tab-group-item>
        </div>
    </x-app-card>
@endsection

@push('js')
    <script>
        function documentReady() {
            $("#nominal-period").change(function() {
                if ($(this).val() == 'yearly') {
                    $("#nominal-daily").addClass('d-none')
                    $("#nominal-monthly").addClass('d-none')
                    $("#nominal-yearly").removeClass('d-none')

                    $("#nominal-yearly").trigger('change')
                } else if (($(this).val() == 'monthly')) {
                    $("#nominal-daily").addClass('d-none')
                    $("#nominal-monthly").removeClass('d-none')
                    $("#nominal-yearly").addClass('d-none')

                    $("#nominal-monthly").trigger('change')
                } else {
                    $("#nominal-daily").removeClass('d-none')
                    $("#nominal-monthly").addClass('d-none')
                    $("#nominal-yearly").addClass('d-none')

                    $("#nominal-daily").trigger('change')
                }
            })

            $("#weight-period").change(function() {
                if ($(this).val() == 'yearly') {
                    $("#weight-daily").addClass('d-none')
                    $("#weight-monthly").addClass('d-none')
                    $("#weight-yearly").removeClass('d-none')

                    $("#weight-yearly").trigger('change')
                } else if (($(this).val() == 'monthly')) {
                    $("#weight-daily").addClass('d-none')
                    $("#weight-monthly").removeClass('d-none')
                    $("#weight-yearly").addClass('d-none')

                    $("#weight-monthly").trigger('change')
                } else {
                    $("#weight-daily").removeClass('d-none')
                    $("#weight-monthly").addClass('d-none')
                    $("#weight-yearly").addClass('d-none')

                    $("#weight-daily").trigger('change')
                }
            })

            $("#nominal-daily, #nominal-monthly, #nominal-yearly").change(function() {
                loadingWithText('Menghitung laporan ...')

                var period = $("#nominal-period").val()
                var value = $("#nominal-" + period).val()

                const data = new FormData()
                data.append('_method', 'post')
                data.append('_token', '{{ csrf_token() }}')
                data.append('period', period)
                data.append('value', value)

                ajaxPost("{{ route('app.partner.reports.nominal') }}", data, null, function (result) {
                    nominal = result.data
                    $("#total-nominal").html((ToRupiah.format(nominal.overview ?? 0)).replaceAll('\u00A0', '') + ',-')
                    $("#total-service-charge").html((ToRupiah.format(nominal.service_charge)).replaceAll('\u00A0', '') + ',-')
                    $('#container-nominal-chart').html(`<canvas id="nominal-chart" class="w-100" style="height:200px"></canvas>`)
                    var nominalChartCtx = document.getElementById('nominal-chart').getContext('2d');
                    var nominalChart = new Chart(nominalChartCtx, {
                        type: (nominal.datasets.length <= 1 ? 'bar' : 'line'),
                        data: {
                            labels: nominal.labels,
                            datasets: [{
                                label: "Total",
                                data: nominal.datasets,
                                backgroundColor: '#8CC152FF',
                                backgroundColor: '#8CC152AA',
                                borderWidth: 1,
                                pointRadius: 4,
                                pointRotation: 3,
                                tension: .3,
                                fill: true
                            }]
                        },
                        options: ChartOptionToRupiah
                    });

                    Swal.close()
                }, function (errorResponse) {
                    Swal.fire({
                        title: "Gagal menghitung nominal!",
                        text: errorResponse.message,
                        icon: 'error',
                    })
                }, false)
            })

            $("#weight-daily, #weight-monthly, #weight-yearly").change(function() {
                loadingWithText('Menghitung laporan ...')

                var period = $("#weight-period").val()
                var value = $("#weight-" + period).val()

                const data = new FormData()
                data.append('_method', 'post')
                data.append('_token', '{{ csrf_token() }}')
                data.append('period', period)
                data.append('value', value)

                ajaxPost("{{ route('app.partner.reports.weights') }}", data, null, function (result) {
                    weight = result.data
                    $("#total-weight").html(ToUnit.format(weight.overview ?? 0) + ' kg')

                    $('#container-weight-chart').html(`<canvas id="weight-chart" class="w-100" style="height:200px"></canvas>`)
                    var weightChartCtx = document.getElementById('weight-chart').getContext('2d');
                    var weightChart = new Chart(weightChartCtx, {
                        type: (weight.datasets.length <= 1 ? 'bar' : 'line'),
                        data: {
                            labels: weight.labels,
                            datasets: [{
                                label: "Total",
                                data: weight.datasets,
                                backgroundColor: '#8CC152FF',
                                backgroundColor: '#8CC152AA',
                                borderWidth: 1,
                                pointRadius: 4,
                                pointRotation: 3,
                                tension: .3,
                                fill: true
                            }]
                        },
                        options: ChartOptionToKg
                    });

                    Swal.close()
                }, function (errorResponse) {
                    Swal.fire({
                        title: "Gagal menghitung berat!",
                        text: errorResponse.message,
                        icon: 'error',
                    })
                }, false)
            })

            $("#nominal-daily").trigger('change')
            $("#weight-daily").trigger('change')
        }
    </script>
@endpush
