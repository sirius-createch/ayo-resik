@extends('layout.app.main', ['title' => 'Tutorial', 'back' => route('app.customer.home')])

@section('contents')
    <x-app-card title="Pilih Sampah yang Akan Anda Uangkan" banner="{{ asset('images/assets/tutorial/pick.png') }}">
        Masuk ke Menu Setor , klik tombol + Tambah Jenis – Pilih Jenis Sampah nya – Masukkan angka ESTIMASI berat (kilogram). Jika ada tambahan jenis sampah tinggal +Tambah Jenis. Jika ada jenis sampah yang di hapus tinggal klik tanda minus (-) warna merah.
Kemudian Pilih Metode Pembayaran (Masuk Saldo atau Tunai) kemudian Ajukan Pengambilan.
    </x-app-card>

    <x-app-card title="Tunggu Konfirmasi Dari Partner" banner="{{ asset('images/assets/tutorial/konfirm.png') }}">
        Pada sudut kanan atas aplikasi terdapat tombol untuk melihat notifikasi / pemberitahuan proses setiap transaksi yang dilakukan.
    </x-app-card>

    <x-app-card title="Cara Berganti Partner" banner="{{ asset('images/assets/tutorial/done.png') }}">
        Masuk ke Menu Partner - Pilih Partner Pengganti – Hubungkan – Menunggu Konfirmasi terhubung dari partner baru. Sebelum berganti Partner ika masih ada Saldo di Partner sebelumnya dan Anda sudah terhubung dengan partner baru , maka saldo anda di partner sebelumnya akan hilang. Jadi Pastikan Saldo Anda diangka Nol / sudah diuangkan sebelum berpindah partner.
    </x-app-card>
    
    <x-app-card title="Hubungkan dengan partner Pilihan Anda" banner="{{ asset('images/assets/tutorial/partner.png') }}">
        Masuk ke Menu Partner - Pilih Partner terdekat / yang anda kenal – Hubungkan – Menunggu Konfirmasi Partner . Jika sudah di konfirmasi / terhubung dengan partner maka transaksi  sudah bisa dilakukan
    </x-app-card>
    
    <!--<x-app-card title="Yeay, Sampahmu Jadi Uang" banner="{{ asset('images/assets/tutorial/done.png') }}">-->
    <!--    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis, eveniet. Maiores, repudiandae autem libero, consequatur, dolorum dolores voluptatum voluptates corrupti iure repellat odit vel recusandae! Magnam officiis ipsam iste ipsum.-->
    <!--</x-app-card>-->
@endsection