@extends('layout.app.main', ['title' => 'Partner', 'back' => route('app.customer.home')])

@section('contents')
    <div class="card card-style @if ($isConnected) gradient-highlight @else bg-red-dark @endif" id="partner">
        <div class="content">
            <div id="partner-status">
                @if ($partner)
                    @if ($isConnected)
                        <p class="mb-n1 color-white font-600">Anda sudah <span class="badge bg-success">terhubung</span> dengan partner:</p>
                    @else
                        <p class="mb-n1 color-white font-600">Masih <span class="badge bg-warning">menunggu konfirmasi</span> dengan partner:</p>
                    @endif
                @endif
            </div>
            <div class="my-2 color-white h2 fw-bold" id="partner-name">{{ $partner->name ?? 'Anda tidak terhubung dengan Partner manapun!' }}</div>
            <p class="mb-n1 color-white font-600">Alamat: <span id="partner-address">{{ $partner->address ?? '-' }}</span></p>
            <p class="mb-n1 color-white font-600">Jadwal Ambil: <span id="partner-schedules">{{ $schedules ?? '-' }}</span></p>
        </div>
    </div>

    <x-app-title>Daftar Partner</x-app-title>

    @php
        $orders = [
            'distance' => 'Jarak',
            'p.name' => 'Nama',
            'p.address' => 'Alamat'
        ];

        $data = ['id', 'name', 'distance', 'address', 'phone', 'schedules', 'products']
    @endphp

    <x-app-listing id="listing" name="Partner" :orders="$orders" :route="route('app.customer.partner.nearby')" :data="$data">
        @slot('skeleton')
            <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="m-1 w-75">skeleton</p>
                    <p class="m-1 w-25">skeleton</p>
                </div>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1 p-2">skeleton</p>
            </div>
        @endslot

        <x-app-card class="mb-3">
            <div class="d-flex justify-content-between align-items-center mb-0">
                <p class="fw-bold font-15 mb-0">[[name]]</p>
                <p class="fw-bold opacity-70">[[distance]] km</p>
            </div>
            <hr class="my-2 opacity-20">
            <p class="mb-0"><b>Alamat</b>: [[address]]</p>
            <p class="mb-0"><b>Telepon</b>: [[phone]]</p>
            <p class="mb-0"><b>Jadwal Ambil</b>: [[schedules]]</p>
            <p class="mb-0"><b>Jenis Diterima</b>: [[products]]</p>
            <x-app-button size="s" class="btn-full w-100 mt-2 btn-connect" data-id="[[id]]" data-name="[[name]]"><i class="fa-solid fa-link me-2"></i>Hubungkan</x-app-button>
        </x-app-card>
    </x-app-listing>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()

            $(document).on('click', '.btn-connect', function () {
                let id = $(this).data('id')
                let name = $(this).data('name')

                @if (auth()->user()->partner)
                    const text = "Mengganti partner akan membuat saldo Anda menjadi NOL!<br><br>Yakin ingin terhubung dengan partner<br> " + name + "?"
                @else
                    const text = "Ingin terhubung dengan partner<br>" + name + "?"
                @endif

                @if (!auth()->user()->partner){{ 'Confirm' }}@else{{ 'DeleteConfirm' }}@endif.fire({
                    title: "Perhatian!",
                    icon: "warning",
                    html: text
                }).then((result) => {
                    if (result.isConfirmed) {
                        loadingWithText("Mengirim permintaan ganti partner ...")

                        let data = new FormData()
                        data.append('_method', 'post')
                        data.append('_token', "{{ csrf_token() }}")
                        data.append('partner_id', id)

                        ajaxPost("{{ route('app.customer.partner.change') }}", data, null, function (result) {
                            let partner = result.data

                            $("#partner").removeClass(`bg-highlight`);
                            $("#partner").addClass(`bg-red-dark`);

                            $("#partner-status").html(`
                                <p class="mb-n1 color-white font-600">Masih <span class="badge bg-warning">menunggu konfirmasi</span> dengan partner:</p>
                            `)

                            $("#partner-name").html(partner.name)
                            $("#partner-address").html(partner.address)
                            $("#partner-schedules").html(partner.schedules)

                            Swal.fire({
                                title: "Permintaan diajukan!",
                                icon: 'success',
                                text: "Harap tunggu " + partner.name + " mengonfirmasi permintaan Anda."
                            })

                            listingReload()
                        }, function (error) {
                            Swal.fire({
                                title: "Pengajuan gagal!",
                                icon: 'error',
                                text: error.message
                            })
                        }, false)
                    }
                })
            })

            @if (session('success'))
                Swal.fire({
                    title: "{{ session('success') }}",
                    icon: "success",
                    text: "Sekarang Anda dapat mulai mencari partner agar dapat menikmati seluruh fitur pada aplikasi ini!"
                })
            @endif
        }
    </script>
@endpush
