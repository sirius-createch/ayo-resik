@extends('layout.app.main', ['title' => 'Tarik Tunai', 'back' => route('app.customer.home')])

@section('contents')
    <x-app-card style="border: 2px solid #27AE60 !important">
        <p class="pb-0 mb-0 opacity-80 text-dark fw-bold">Saldo Anda</p>
        <div class="fw-bold font-25 text-dark mb-0">{{ Sirius::toRupiah(auth()->user()->balance) }}</div>
    </x-app-card>

    <x-app-form method="post" id="form-create">
        <div class="bg-white py-3 px-4">
            <div class="d-flex justify-content-between align-items-end">
                <p class="pb-0 mb-0 font-15 opacity-80">Nominal</p>
                <x-app-button color="bg-dark-dark" size="s" icon="fa-solid fa-clock-rotate-left" link="{{ route('app.customer.withdrawals.index') }}">Riwayat</x-app-button>
            </div>
            <div class="d-flex justify-content-between align-items-end gap-1 my-2">
                <div class="h1 fw-bold font-35 text-dark mb-0">Rp</div>
                <div class="input-style no-borders no-icon w-100">
                    <input name="nominal" class="form-control border-0 font-35 fw-bold to-rupiah" value="0">
                </div>
                <div class="h1 fw-bold font-35 text-dark mb-0">,-</div>
            </div>
        </div>
        <div class="bg-white py-4 my-3 px-4">
            <p class="pb-0 mb-0 font-15 opacity-80">Pilih Nominal</p>
            <div class="row mb-0">
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 50000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp50" class="@if(auth()->user()->balance < 50000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp50.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="50000" id="Rp50" @disabled(auth()->user()->balance < 50000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 100000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp100" class="@if(auth()->user()->balance < 100000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp100.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="100000" id="Rp100" @disabled(auth()->user()->balance < 100000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 150000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp150" class="@if(auth()->user()->balance < 150000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp150.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="150000" id="Rp150" @disabled(auth()->user()->balance < 150000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 200000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp200" class="@if(auth()->user()->balance < 200000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp200.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="200000" id="Rp200" @disabled(auth()->user()->balance < 200000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 250000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp250" class="@if(auth()->user()->balance < 250000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp250.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="250000" id="Rp250" @disabled(auth()->user()->balance < 250000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 300000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp300" class="@if(auth()->user()->balance < 300000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp300.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="300000" id="Rp300" @disabled(auth()->user()->balance < 300000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 350000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp350" class="@if(auth()->user()->balance < 350000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp350.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="350000" id="Rp350" @disabled(auth()->user()->balance < 350000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="col-6 fw-bold my-2">
                    <div class="d-flex justify-content-between align-items-content border rounded rounded-5 px-2 py-1" @if(auth()->user()->balance < 400000) style="background-color: var(--bs-gray-300)" @endif>
                        <div class="form-check icon-check mb-0 me-0">
                            <label for="Rp400" class="@if(auth()->user()->balance < 400000) text-gray @else text-dark @endif font-20 pb-0 mb-0">Rp400.000</label>
                            <input class="form-check-input" type="radio" name="selected_nominal" value="400000" id="Rp400" @disabled(auth()->user()->balance < 400000)>
                            <i class="icon-check-1 far fa-circle color-gray-dark font-16"></i>
                            <i class="icon-check-2 far fa-check-circle font-16 color-green-dark"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-3 px-4">
            <x-app-button type="submit" icon='fa-solid fa-credit-card' class="w-100">Tarik</x-app-button>
        </div>
    </x-app-form>
@endsection

@push('js')
    <script>
        function documentReady() {
            $("input[name='selected_nominal']").change(function() {
                $("input[name='nominal']").val(ToRupiah.format($(this).val()).replace('Rp\u00A0', ''))
            })

            $("input[name='nominal']").keyup(function() {
                $("input:radio").prop("checked", false);
            })

            $("#form-create").submit(function(e) {
                e.preventDefault()
                loadingWithText('Mengirim pengajuan ...')

                const data = new FormData(document.getElementById('form-create'));

                ajaxPost("{{ route('app.customer.withdrawals.store') }}", data, null, function (result) {
                    Swal.fire({
                        title: "Permintaan diajukan!",
                        icon: "success",
                    })

                    let withdrawal = result.data
                    setTimeout(() => {
                        window.location.href = "{{ route('app.customer.withdrawals.show', ['withdrawal' => '-code-']) }}".replace('-code-', withdrawal)
                    }, 1000);
                }, function (error) {
                    Swal.fire({
                        title: "Pengajuan gagal!",
                        icon: 'error',
                        text: error.message
                    })
                }, false)
            })
        }
    </script>
@endpush
