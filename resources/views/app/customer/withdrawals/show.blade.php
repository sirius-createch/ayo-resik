@extends('layout.app.main', ['title' => 'Detail Tarikan', 'back' => route('app.customer.withdrawals.index')])

@section('contents')
    <div class="timeline-body mt-0">
        <div class="timeline-deco"></div>

        @if ($withdrawal->current_status == 'sent' || $withdrawal->current_status == 'requested' || $withdrawal->current_status == 'seen')
            <div class="timeline-item mt-4 d-none" id="current_status">
                <i class="[[icon]] bg-[[color]] color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        [[name]]<br>
                        <span class="opacity-30">[[datetime]]</span>
                    </h5>
                </div>
            </div>
        @endif

        @foreach ($statuses as $status)
            <div class="timeline-item mt-4">
                <i class="{{ $status['icon'] }} bg-{{ $status['color'] }} color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        {{ $status['name'] }}<br>
                        <span class="opacity-30">{{ $status['datetime'] }}</span>
                    </h5>
                </div>
            </div>
        @endforeach

    </div>

    <x-app-title>Permintaan #{{ $withdrawal->code }}</x-app-title>

    <x-app-card class="my-3">
        <div class="d-flex justify-content-between align-items-center mb-0">
            <p class="fw-bold font-15 mb-0">{{ $withdrawal->partner->name }}</p>
            <p class="fw-bold opacity-70">{{ Sirius::toLongDate($withdrawal->created_at) }}</p>
        </div>
        <p class="mb-0">Alamat: {{ $withdrawal->partner->address }}</p>
    </x-app-card>

    <x-app-card class="my-3 text-center">
        <div class="mb-2">Nominal Ditarik:</div>
        <div class="fw-bold font-30 mb-2">{{ Sirius::toRupiah($withdrawal->amount) }}</div>
    </x-app-card>

@endsection

@section('footer')
    @if ($withdrawal->current_status == 'sent' || $withdrawal->current_status == 'requested' || $withdrawal->current_status == 'seen')
        <div id="footer-bar" class="action-button">
            <div class="mx-3 mb-3 w-100">
                @if ($withdrawal->current_status == 'sent')
                    <x-app-button size="l" color="bg-highlight" class="w-100" id="confirm"><i class="fa-solid fa-check-double me-2"></i>Konfirmasi Diterima</x-app-button>
                @else
                    <x-app-button size="l" color="bg-red-dark" class="w-100" id="cancel"><i class="fa-solid fa-xmark me-2"></i>Batalkan Pengajuan</x-app-button>
                @endif
            </div>
        </div>
    @endif
@endsection

@push('js')
    @if ($withdrawal->current_status == 'sent' || $withdrawal->current_status == 'requested' || $withdrawal->current_status == 'seen')
        <script>
            function documentReady() {
                @if ($withdrawal->current_status == 'sent')
                    $("#confirm").click(function() {
                        DenyConfirm.fire({
                            title: "Sudah diterima?",
                            text: "Sudahkah Anda menerima uang tarikan dari {{ auth()->user()->partner->name }}?",
                            confirmButtonText: "<i class='fa-solid fa-check btn-icon-text me-2'></i>Sudah",
                            denyButtonText: "<i class='fa-solid fa-close btn-icon-text me-2'></i>Belum"
                        }).then((result) => {
                            if (result.isConfirmed || result.isDenied) {
                                loadingWithText('Mengubah status tarikan ...')

                                const data = new FormData()
                                data.append('_method', 'put')
                                data.append('_token', "{{ csrf_token() }}")

                                if (result.isConfirmed) {
                                    data.append('status', 'done')
                                }
                                if (result.isDenied) {
                                    data.append('status', 'not_received')
                                }

                                ajaxPost("{{ route('app.customer.withdrawals.update', ['withdrawal' => $withdrawal->code]) }}", data, null, function (result) {
                                    const status = result.data
                                    var currentStatus = $("#current_status").html()
                                    currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                    currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                    currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                    currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)
                                    $("#current_status").html(currentStatus)
                                    $("#current_status").removeClass('d-none')
                                    $("#footer-bar").remove()

                                    if (status.color == 'success') {
                                        Swal.fire({
                                            title: "Transaksi selesai!",
                                            icon: 'success'
                                        });
                                    }
                                    else {
                                        Swal.fire({
                                            title: "Parter dikabarkan!",
                                            icon: 'success',
                                            text: "Kami telah memberi tahu {{ auth()->user()->partner->name }} bahwa Anda belum menerima uang tarikan Anda!",
                                        });
                                    }
                                }, function (error) {
                                    Swal.fire({
                                        title: "Gagal mengubah status!",
                                        icon: 'error',
                                        text: error.message
                                    })
                                }, false)
                            }
                        })
                    })
                @else
                    $("#cancel").click(function() {
                        DeleteConfirm.fire({
                            title: "Batalkan pengajuan?",
                            text: "Yakin ingin membatalkan pengajuan?",
                        }).then((result) => {
                            if (result.isConfirmed) {
                                loadingWithText('Membatalkan pengajuan ...')

                                const data = new FormData()
                                data.append('_method', 'put')
                                data.append('_token', "{{ csrf_token() }}")
                                data.append('status', 'canceled')

                                ajaxPost("{{ route('app.customer.withdrawals.update', ['withdrawal' => $withdrawal->code]) }}", data, null, function (result) {
                                    const status = result.data
                                    var currentStatus = $("#current_status").html()
                                    currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                    currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                    currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                    currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)
                                    $("#current_status").html(currentStatus)
                                    $("#current_status").removeClass('d-none')
                                    $("#footer-bar").remove()

                                    Swal.fire({
                                        title: "Pengajuan dibatalkan!",
                                        icon: 'success'
                                    });
                                }, function (error) {
                                    Swal.fire({
                                        title: "Gagal membatalkan pengajuan!",
                                        icon: 'error',
                                        text: error.message
                                    })
                                }, false)
                            }
                        })
                    })
                @endif
            }
        </script>
    @endif
@endpush
