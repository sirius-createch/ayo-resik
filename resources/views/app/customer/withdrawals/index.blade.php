@extends('layout.app.main', ['title' => 'Riwayat Tarikan', 'back' => route('app.customer.withdrawals.create')])

@section('contents')
    @php
        $orders = [
            'code' => 'Kode',
            'amount' => 'Nominal',
            'created_at' => 'Tanggal',
            'current_status' => 'Status'
        ];

        $data = ['code', 'amount', 'status', 'statusColor', 'date']
    @endphp

    <x-app-listing id="listing" name="Riwayat penarikan" :orders="$orders" :route="route('app.customer.withdrawals.index')" :data="$data" sort="desc">
        @slot('skeleton')
            <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="m-1 w-75">skeleton</p>
                    <p class="m-1 w-25">skeleton</p>
                </div>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1 p-2">skeleton</p>
            </div>
        @endslot

        <a href="{{ route('app.customer.withdrawals.show', ['withdrawal' => '[[code]]']) }}">
            <x-app-card class="mb-3">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="fw-bold font-15 mb-0">Permintaan #[[code]]</p>
                    <p class="fw-bold opacity-70">[[date]]</p>
                </div>
                <p class="mb-0 fw-bold text-[[statusColor]]">[[status]]</p>
                <p class="mb-0 text-center">Total Tunai</p>
                <p class="fw-bold text-center font-20">[[amount]]</p>
            </x-app-card>
        </a>
    </x-app-listing>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()
        }
    </script>
@endpush
