@extends('layout.app.main', [
    'title' => 'Dasbor',
    'headerSticky' => '<img src="'.asset('images/assets/logo.png').'" style="width:100px">',
    'headerTop' => '<img src="'.asset('images/assets/logo.png').'" style="width:150px">',
])

@section('contents')
    <div class="card mb-0" style="background: transparent">
        <div class="content mt-0">
            <div class="d-flex align-items-base gap-3 px-2">
                <div>
                    <a href="{{ route('app.customer.profile.index') }}" class="text-white">
                        <img src="{{ auth()->user()->avatar_link }}" alt="Avatar {{ auth()->user()->name }}" class="rounded-3" style="object-fit: cover; width: 50px; height: 50px;">
                    </a>
                </div>
                <div>
                    <p class="mb-n1 opacity-70 font-600">Selamat Datang,</p>
                    <div class="h1 fw-bold">
                        {{ auth()->user()->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-style bg-phone">
        <div class="content">
            <div class="d-flex justify-content-between align-items-center px-2">
                <div>
                    <p class="mb-n1 color-white opacity-70 font-600">Saldo</p>
                    <div class="color-white h1 fw-bold">
                        {{ Sirius::toRupiah(auth()->user()->balance) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (!auth()->user()->partner)
        <div class="card card-style gradient-blue color-white">
            <div class="content">
                Anda tidak terhubung dengan partner manapun! Silakan masuk ke menu <b>PARTNER</b> untuk melakukan permintaan hubungan dengan partner dan nikmati semua fitur kami!
            </div>
        </div>

        <div class="card card-style gradient-red color-white">
            <div class="content">
                Jika Anda pernah melakukan permintaan hubungan dengan partner, artinya permintaan Anda ditolak oleh partner yang Anda pilih, harap pilih partner yang lain!
            </div>
        </div>
    @elseif (!auth()->user()->partner_confirm_at)
        <div class="card card-style gradient-yellow color-white">
            <div class="content">
                Permintaan hubungan partner telah dilakukan! Setelah partner menerima permintaan Anda, semua fitur aplikasi kami akan terbuka!
            </div>
        </div>
    @endif

    <div class="mx-4">
        <div class="row">
            @if (auth()->user()->partner && auth()->user()->partner_confirm_at)
                <div class="col-4">
                    <a href="{{ route('app.customer.orders.create') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                        <img src="{{ asset('images/assets/menus/dump.png') }}" style="width: 100%" alt="Icon Setor Sampah">
                        <span class="font-12">Setor</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="{{ route('app.customer.withdrawals.create') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                        <img src="{{ asset('images/assets/menus/withdrawal.png') }}" style="width: 100%" alt="Icon Tarik Tunai">
                        <span class="font-12">Tarik Tunai</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="{{ route('app.customer.orders.index') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                        <img src="{{ asset('images/assets/menus/orders.png') }}" style="width: 100%" alt="Icon Pesanan">
                        <span class="font-12">Pesanan</span>
                    </a>
                </div>
            @endif
            <div class="col-4">
                <a href="{{ route('app.customer.partner.index') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                    <img src="{{ asset('images/assets/menus/partner.png') }}" style="width: 100%" alt="Icon Partner">
                    <span class="font-12">Partner</span>
                </a>
            </div>
            @if (auth()->user()->partner && auth()->user()->partner_confirm_at)
                <div class="col-4">
                    <a href="{{ route('app.customer.products.index') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                        <img src="{{ asset('images/assets/menus/products.png') }}" style="width: 100%" alt="Icon Jenis Sampah">
                        <span class="font-12">Jenis</span>
                    </a>
                </div>
            @endif
            <div class="col-4">
                <a href="{{ route('app.customer.tutorial') }}" class="btn btn-full w-100 mb-3 rounded-xs text-uppercase font-700 shadow-s text-dark opacity-90 p-1">
                    <img src="{{ asset('images/assets/menus/tutorial.png') }}" style="width: 100%" alt="Icon Manual">
                    <span class="font-10">Tutorial</span>
                </a>
            </div>
        </div>
    </div>

    <div class="pb-4">
        <h4 class="mx-4 mb-2 opacity-80">Konten untuk Anda</h4>

        @if (count($contents) > 0)
            <div class="splide single-slider slider-arrows" id="contents">
                <div class="splide__track">
                    <div class="splide__list">
                        @foreach ($contents as $content)
                            <div class="splide__slide">
                                <a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}">
                                    <div data-card-height="300" class="card mx-3 rounded-m shadow-l"  style="background-image: url({{ $content->banner_link }});">
                                        <div class="card-bottom text-center mb-2">
                                            <h2 class="color-white text-uppercase font-900 mb-0">{{ $content->title }}</h2>
                                            <p class="under-heading color-white">{{ Str::limit(strip_tags($content->content), 30) }}</p>
                                        </div>
                                        <div class="card-overlay"></div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="mx-3">
                <x-app-chip color="red" icon="fa-solid fa-x" class="w-100">Belum ada konten yang tersedia.</x-app-chip>
            </div>
        @endif

    </div>

    <div class="pb-4">
        <h4 class="mx-4 mb-2 opacity-80">Artikel</h4>

        @if (count($articles) > 0)
            <div class="splide single-slider slider-arrows" id="articles">
                <div class="splide__track">
                    <div class="splide__list">
                        @foreach ($articles as $article)
                            <div class="splide__slide">
                                <a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}">
                                    <div data-card-height="300" class="card mx-3 rounded-m shadow-l"  style="background-image: url({{ $article->banner_link }});">
                                        <div class="card-bottom text-center mb-2">
                                            <h2 class="color-white text-uppercase font-900 mb-0">{{ $article->title }}</h2>
                                            <p class="under-heading color-white">{{ Str::limit(strip_tags($article->content), 30) }}</p>
                                        </div>
                                        <div class="card-overlay"></div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="mx-3">
                <x-app-chip color="red" icon="fa-solid fa-x" class="w-100">Belum ada konten yang tersedia.</x-app-chip>
            </div>
        @endif

    </div>
@endsection
