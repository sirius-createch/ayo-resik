@extends('layout.app.auth', ['title' => "Konfirmasi Alamat"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-5">Konfirmasi Alamat</h3>

    <div class="mx-5 mb-n3">
        <x-app-form action="{{ route('app.customer.address.update') }}" method="put" submit="Konfirmasi Alamat" submitWide>
            <x-app-form-input id="address" name="address" label="Alamat Lengkap" icon="fa-solid fa-map-location-dot" placeholder="Masukkan alamat lengkap Anda" class="mb-3" required/>
            <x-app-button id="get-current-position" size="s" color="bg-blue-dark" class="w-100"><i class="fa-solid fa-location-dot me-2"></i>Ambil Lokasi Saat Ini</x-app-button>
            <div id="map" class="my-2 w-100 rounded-s" style="height: 200px"></div>
            <input type="hidden" id="latitude" name="latitude" value="">
            <input type="hidden" id="longitude" name="longitude" value="">
        </x-app-form>
    </div>
@endsection

@push('js')
    <script>
        var latitude = -7.2754438
        var longitude = 112.6426431

        var map = L.map('map').setView([latitude, longitude], 4);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([latitude, longitude]).addTo(map)

        function onMapClick(e) {
            console.log(e);
            latitude = e.latlng.lat
            longitude = e.latlng.lng

            map.setView([latitude, longitude], 17).invalidateSize()
            marker.setLatLng([latitude, longitude])

            $("#latitude").val(latitude)
            $("#longitude").val(longitude)
        }

        map.on('click', onMapClick);

        function documentReady() {
            $("#get-current-position").click(function() {
                if ("geolocation" in navigator) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        latitude = position.coords.latitude
                        longitude = position.coords.longitude

                        map.setView([latitude, longitude], 17).invalidateSize()
                        marker.setLatLng([latitude, longitude])

                        $("#latitude").val(latitude)
                        $("#longitude").val(longitude)
                    });
                } else {
                    alert("Harap nyalakan dan perbolehkan halaman ini mengakses GPS Anda!");
                }
            })
        }
    </script>
@endpush
