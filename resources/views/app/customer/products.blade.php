@extends('layout.app.main', ['title' => 'Jenis Sampah', 'back' => route('app.customer.home')])

@section('contents')
    <x-app-card title="Daftar Sampah" subtitle="Yang Diterima {{ auth()->user()->partner->name }}">
        <div class="list-group list-custom-small list-menu ms-0 me-1">
            @foreach (auth()->user()->partner->products as $product)
                <span>
                    <img src="{{ $product->image_link }}" class="rounded-s">
                    <span class="fw-bold text-dark" style="font-size:1.1em">{{ $product->name }}</span>
                </span>
            @endforeach
        </div>
    </x-app-card>

    <div class="mx-4">
        <x-app-button class="w-100" size="l" link="{{ route('app.customer.orders.create') }}"><i class="fa-solid fa-trash-alt me-2 fw-normal"></i>Setor Sampah</x-app-button>
    </div>
@endsection
