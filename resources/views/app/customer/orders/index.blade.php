@extends('layout.app.main', ['title' => 'Pesanan', 'back' => route('app.customer.home')])

@section('contents')
    @php
        $orders = [
            'code' => 'Kode',
            'created_at' => 'Tanggal',
            'total_bill' => 'Pendapatan',
            'total_weight' => 'Berat',
            'current_status' => 'Status'
        ];

        $data = ['code', 'products', 'billText', 'bill', 'weight', 'day', 'status', 'statusColor', 'date']
    @endphp

    <x-app-listing id="listing" name="Pesanan" :orders="$orders" :route="route('app.customer.orders.index')" :data="$data" sort="desc">
        @slot('skeleton')
            <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="m-1 w-75">skeleton</p>
                    <p class="m-1 w-25">skeleton</p>
                </div>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
                <p class="m-1">skeleton</p>
            </div>
        @endslot

        <a href="{{ route('app.customer.orders.show', ['invoice' => '[[code]]']) }}">
            <x-app-card class="mb-3">
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="fw-bold font-15 mb-0">Pesanan #[[code]]</p>
                    <p class="fw-bold opacity-70">[[date]]</p>
                </div>
                <p class="mb-0 fw-bold text-[[statusColor]]">[[status]]</p>
                <p class="mb-0">Total Jenis: [[products]] Jenis Sampah</p>
                <p class="mb-0">[[billText]]: [[bill]]</p>
                <p class="mb-0">Berat Sampah: [[weight]]</p>
                <p class="mb-0">Jadwal Ambil: [[day]]</p>
            </x-app-card>
        </a>
    </x-app-listing>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()
        }
    </script>
@endpush
