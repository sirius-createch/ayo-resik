@extends('layout.app.main', ['title' => 'Detail Pesanan', 'back' => route('app.customer.orders.index')])

@section('contents')
    <div class="timeline-body mt-0">
        <div class="timeline-deco"></div>

        @if ($invoice->current_status == 'requested' || $invoice->current_status == 'seen')
            <div class="timeline-item mt-4 d-none" id="current_status">
                <i class="[[icon]] bg-[[color]] color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        [[name]]<br>
                        <span class="opacity-30">[[datetime]]</span>
                    </h5>
                </div>
            </div>
        @endif

        @foreach ($statuses as $status)
            <div class="timeline-item mt-4">
                <i class="{{ $status['icon'] }} bg-{{ $status['color'] }} color-white shadow-l timeline-icon"></i>
                <div class="timeline-item-content rounded-s">
                    <h5 class="font-400 pt-1 pb-1">
                        {{ $status['name'] }}<br>
                        <span class="opacity-30">{{ $status['datetime'] }}</span>
                    </h5>
                </div>
            </div>
        @endforeach

    </div>

    <x-app-title>Pesanan #{{ $invoice->code }}</x-app-title>

    <x-app-card class="my-3">
        <div class="d-flex justify-content-between align-items-center mb-0">
            <p class="fw-bold font-15 mb-0">{{ $invoice->partner->name }}</p>
            <p class="fw-bold opacity-70">{{ Sirius::toLongDate($invoice->created_at) }}</p>
        </div>
        <p class="mb-0">Alamat: {{ $invoice->partner->address }}</p>
        <p class="mb-0">Total Berat: {{ Str::replace(',0', '', number_format($invoice->total_weight, 1, ',', '.')) . ' kg' }}</p>
        <p class="mb-0">Metode Bayar: {{ $invoice->payment_method->name }}</p>
        <p class="mb-0">Jadwal Ambil: {{ $invoice->day_taken_converted }}</p>
        <p class="mb-0">Catatan: {{ ($invoice->note && $invoice->note != '') ? $invoice->note : '-' }}</p>
    </x-app-card>

    <x-app-card class="my-3 pb-2">
        <p class="fw-bold font-15 mb-0">Jenis Item</p>
        @foreach ($invoice->products as $product)
            <div class="border-top mt-2 pt-2">
                <div class="d-flex justify-content-start align-items-center">
                    <img src="{{ $product->image_link }}" style="height: 50px; width: 50px; object-fit: cover;" class="mx-3" alt="Icon {{ $product->name }}">
                    <div class="w-100">
                        <p class="p-0 mb-0 fw-bold">{{ $product->name }}</p>
                        <div class="row mb-0">
                            <div class="col-4">{{ Sirius::toRupiah($product->pivot->price) }}</div>
                            <div class="col-3 text-end">{{ Str::replace(',0', '', number_format($product->pivot->weight_real ?? $product->pivot->weight, 1, ',', '.')) . ' kg' }}</div>
                            <div class="col-5 text-end">{{ Sirius::toRupiah($product->pivot->price * ($product->pivot->weight_real ?? $product->pivot->weight)) }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <p class="fw-bold font-15 mb-2 mt-4">Pendapatan</p>

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>Total Harga</div>
            <div class="fw-bold" id="total-price">{{ Sirius::toRupiah($invoice->total_bill + $invoice->service_charge) }}</div>
        </div>

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>Biaya Layanan</div>
            <div class="fw-bold" id="service-charge">{{ Sirius::toRupiah($invoice->service_charge) }}</div>
        </div>

        <hr class="my-2">

        <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
            <div><i class="fa-solid fa-coins me-2 text-warning"></i>{{ (in_array($invoice->current_status, ['verified', 'done']) ? 'Total Pendapatan' : 'Estimasi Pendapatan') }}</div>
            <div class="fw-bold" id="estimate-price">{{ Sirius::toRupiah($invoice->total_bill) }}</div>
        </div>
    </x-app-card>

    <x-app-card class="my-3">
        <p class="mb-0 fw-bold">Alamat Pengambilan:</p>
        <p class="mb-0">{{ $invoice->address }}</p>
        <div id="map" class="my-2 w-100 rounded-s" style="height: 200px"></div>
    </x-app-card>
@endsection

@section('footer')
    @if ($invoice->current_status == 'requested' || $invoice->current_status == 'seen')
        <div id="footer-bar" class="action-button">
            <div class="mx-3 mb-3 w-100">
                <x-app-button size="l" color="bg-red-dark" class="w-100" id="cancel"><i class="fa-solid fa-x me-2"></i>Batalkan Pesanan</x-app-button>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script>
        var map = L.map('map').setView([{{ $invoice->latitude }}, {{ $invoice->longitude }}], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([{{ $invoice->latitude }}, {{ $invoice->longitude }}]).addTo(map)

        @if ($invoice->current_status == 'requested' || $invoice->current_status == 'seen')
            function documentReady() {
                $("#cancel").click(function() {
                    DeleteConfirm.fire({
                        title: "Batalkan pesanan?",
                        text: "Yakin ingin membatalkan pesanan?",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            loadingWithText('Membatalkan pesanan ...')

                            const data = new FormData()
                            data.append('_method', 'put')
                            data.append('_token', "{{ csrf_token() }}")
                            data.append('status', 'canceled')

                            ajaxPost("{{ route('app.customer.orders.update', ['invoice' => $invoice->code]) }}", data, null, function (result) {
                                const status = result.data
                                var currentStatus = $("#current_status").html()
                                currentStatus = currentStatus.replaceAll('[[icon]]', status.icon).replaceAll('%5B%5Bicon%5D%5D', status.icon)
                                currentStatus = currentStatus.replaceAll('[[color]]', status.color).replaceAll('%5B%5Bcolor%5D%5D', status.color)
                                currentStatus = currentStatus.replaceAll('[[name]]', status.name).replaceAll('%5B%5Bname%5D%5D', status.name)
                                currentStatus = currentStatus.replaceAll('[[datetime]]', status.datetime).replaceAll('%5B%5Bdatetime%5D%5D', status.datetime)
                                $("#current_status").html(currentStatus)
                                $("#current_status").removeClass('d-none')
                                $("#footer-bar").remove()

                                Swal.fire({
                                    title: "Pesanan dibatalkan!",
                                    icon: 'success'
                                });
                            }, function (error) {
                                Swal.fire({
                                    title: "Gagal membatalkan pesanan!",
                                    icon: 'error',
                                    text: error.message
                                })
                            }, false)
                        }
                    })
                })
            }
        @endif
    </script>
@endpush
