@extends('layout.app.main', ['title' => 'Setor Sampah', 'back' => route('app.customer.home')])

@section('contents')
    <div class="accordion" id="accordion">
    </div>

    <x-app-form>
        <x-app-title>Jenis Daur Ulang</x-app-title>

        <div class="my-3">
            <div class="d-flex">
                <x-app-button type="button" color="btn-white" data-bs-toggle="collapse" data-bs-target="#collapse-product" class="mx-3 mb-3 flex-fill text-start">
                    <span class="fa fa-box font-10 me-3"></span>
                    Pilih jenis daur ulang
                    <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </x-app-button>
            </div>

            <x-app-card id="collapse-product" class="mb-0 collapse" data-bs-parent="#accordion">
                    <div id="selected-products">

                    </div>

                    <p class="mt-3 text-center bg-light text-dark fw-bold">Masukkan ESTIMASI berat (kilogram)</p>

                    <x-app-button color="bg-phone" data-menu="sheet-products" class="w-100"><i class="fa-solid fa-plus me-2"></i>Tambah Jenis Daur Ulang</x-app-button>
            </x-app-card>
        </div>

        <x-app-title>Perkiraan Pendapatan</x-app-title>

        <div class="my-3">
            <div class="d-flex">
                <x-app-button type="button" color="btn-white" data-bs-toggle="collapse" data-bs-target="#collapse-pricing" class="mx-3 mb-3 flex-fill text-start">
                    <span class="fa fa-money-bill font-10 me-3"></span>
                    Lihat perkiraan pendapatan
                    <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </x-app-button>
            </div>

            <x-app-card id="collapse-pricing" class="mb-0 collapse" data-bs-parent="#accordion">
                <div id="overview-selected-products">

                </div>

                <hr class="my-2">

                <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
                    <div><i class="fa-solid fa-coins me-2 text-warning"></i>Total Harga</div>
                    <div class="fw-bold" id="total-price">Rp0,-</div>
                </div>

                <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
                    <div><i class="fa-solid fa-coins me-2 text-warning"></i>Biaya Layanan</div>
                    <div class="fw-bold" id="service-charge">Rp0,-</div>
                </div>

                <hr class="my-2">

                <div class="d-flex justify-content-between align-items-center fw-bold" style="font-size: 1.1em">
                    <div><i class="fa-solid fa-coins me-2 text-warning"></i>Estimasi Pendapatan</div>
                    <div class="fw-bold" id="estimate-price">Rp0,-</div>
                </div>
            </x-app-card>
        </div>

        <x-app-title>Lokasi Pengambilan</x-app-title>

        <div class="my-3">
            <div class="d-flex">
                <x-app-button type="button" color="btn-white" data-bs-toggle="collapse" data-bs-target="#collapse-location" class="mx-3 mb-3 flex-fill text-start">
                    <span class="fa fa-location-dot font-10 me-3"></span>
                    Pilih lokasi pengambilan
                    <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </x-app-button>
            </div>

            <x-app-card id="collapse-location" class="mb-0 collapse" data-bs-parent="#accordion">
                <x-app-form-input id="address" name="address" label="Alamat Lengkap" value="{{ auth()->user()->address }}" required class="mb-2" />
                <x-app-button id="get-current-position" size="s" color="bg-blue-dark" class="w-100"><i class="fa-solid fa-location-crosshairs me-2"></i>Ambil Lokasi Saat Ini</x-app-button>
                <div id="map" class="mt-2 w-100 rounded-s" style="height: 200px"></div>
                <input type="hidden" id="latitude" name="latitude" value="{{ auth()->user()->latitude }}">
                <input type="hidden" id="longitude" name="longitude" value="{{ auth()->user()->longitude }}">
            </x-app-card>
        </div>

        <x-app-title>Metode Pembayaran</x-app-title>

        <div class="my-3">
            <div class="d-flex">
                <x-app-button type="button" color="btn-white" data-bs-toggle="collapse" data-bs-target="#collapse-payment" class="mx-3 mb-3 flex-fill text-start">
                    <span class="fa fa-wallet font-10 me-3"></span>
                    Pilih metode pembayaran
                    <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </x-app-button>
            </div>

            <x-app-card id="collapse-payment" class="mb-0 collapse" data-bs-parent="#accordion">
                <div class="mb-3">
                    <div class="input-style has-borders no-icon w-100 input-style-active">
                        <label for="payment-method" class="fw-bold" style="font-size: 1.1em">Pilih *</label>
                        <select id="payment-method" name="payment-method">
                            @foreach ($payments as $id => $payment)
                                <option value="{{ $id }}">{{ $payment }}</option>
                            @endforeach
                        </select>
                        <span><i class="fa-solid fa-chevron-down"></i></span>
                    </div>
                </div>
                <x-app-form-input id="note" name="note" label="Catatan" />
            </x-app-card>
        </div>

        <div class="text-center my-3">
            <x-app-button size="l" icon="fa-solid fa-paper-plane" id="btn-send">Ajukan Pengambilan</x-app-button>
        </div>

    </x-app-form>
@endsection

@section('sheets')
    <x-app-sheet id="sheet-products" title="Tambah Jenis Sampah">
        <div class="me-4 ms-3 mt-2">
            <x-app-form method="post" id="form-search-products">
                <x-app-form-input id="search-products" class="mb-2" name="search" placeholder="Cari ..." >
                    @slot('subLabel')
                        <i class="fa-solid fa-search"></i>
                    @endslot
                </x-app-form-input>
            </x-app-form>
            <div class="list-group list-custom-small close-menu mb-4" id="products">

            </div>
        </div>
    </x-app-sheet>
@endsection

@push('js')
    <script>
        const serviceChargePercentage = {{ config('app.service_rate') }};
        let products = {}
        let selectedProducts = {}
        var latitude = {{ auth()->user()->latitude }}
        var longitude = {{ auth()->user()->longitude }}

        var map = L.map('map').setView([latitude, longitude], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([latitude, longitude]).addTo(map)

        function onMapClick(e) {
            console.log(e);
            latitude = e.latlng.lat
            longitude = e.latlng.lng

            map.setView([latitude, longitude], 17).invalidateSize()
            marker.setLatLng([latitude, longitude])

            $("#latitude").val(latitude)
            $("#longitude").val(longitude)
        }

        map.on('click', onMapClick);

        function documentReady() {
            $("#get-current-position").click(function() {
                if ("geolocation" in navigator) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        latitude = position.coords.latitude
                        longitude = position.coords.longitude

                        map.setView([latitude, longitude], 17).invalidateSize()
                        marker.setLatLng([latitude, longitude])

                        $("#latitude").val(latitude)
                        $("#longitude").val(longitude)
                    });
                } else {
                    alert("Harap nyalakan dan perbolehkan halaman ini mengakses GPS Anda!");
                }
            })

            $("#search-products").change(function () {
                $("#products").html(`
                    <div>
                        <i class="fa-solid fa-spin fa-circle-notch"></i><span class="d-inline-block ms-2">Sedang mencari ...</span>
                    </div>
                `)

                const data = new FormData(document.getElementById('form-search-products'))
                data.append('selected', JSON.stringify(selectedProducts))

                ajaxPost("{{ route('app.customer.products.search') }}", data, null, function (result) {
                    products = result.data
                    $("#products").html(``)

                    if (Object.keys(products).length > 0) {
                        $.each(products, function (index, product) {
                            $("#products").append(`
                                <div class="mb-2 border-bottom d-flex justify-content-between align-items-center product" data-id="` + product.id + `">
                                    <div class="fw-bold text-dark" style="font-size:1.1em"><img class="me-3 mt-2" src="` + product.image + `"><span>` + product.name + `</span></div>
                                    <div class="fw-bold text-dark" style="font-size:1.1em">` + ToRupiah.format(product.price).replace('\u00A0', '') + `,-</div>
                                </div>
                            `)
                        })
                    } else {
                        $("#products").html(`Produk yang dicari tidak ditemukan.`)
                    }
                }, function (errorResponse) {
                    $("#products").html(`<x-app-chip color='red' icon="fa-solid fa-x">` + errorResponse.message + `</x-app-chip>`)
                }, false)
            })

            $("#search-products").change()

            $("#form-search-products").submit(function(e) {
                e.preventDefault()
            })

            $(document).on('click', '.product', function () {
                const id = $(this).data('id')
                const product = products[id]
                selectedProducts[id] = product

                $("#selected-products").append(`
                    <div class="border rounded mb-1" style="overflow: unset;">
                        <div class="fw-bold">

                            <div class="d-flex justify-content-between align-items-center p-2">
                                <button type="button" class="btn rounded-circle text-uppercase font-700 shadow-s bg-red-dark remove-product me-2" data-id="` + product.id + `" style="padding: 1px; margin-left: -20px;">
                                    <i class="fa-solid fa-times fa-fw"></i>
                                </button>

                                <div class="d-flex justify-content-start align-items-center w-75">
                                    <img src="` + product.image + `" style="height: 35px; width: 35px; object-fit: cover;" class="me-2" alt="Icon ` + product.name + `">
                                    <p class="p-0">` + product.name + `</p>
                                </div>

                                <div class="d-flex justify-content-start align-items-center gap-2">
                                    <div class="stepper rounded-s float-start">
                                        <a href="javascript:;" class="stepper-sub bg-highlight rounded-start" data-id="` + product.id + `"><i class="fa-solid fa-minus"></i></a>
                                        <input value="0" name="product[` + product.id + `]" data-id="` + product.id + `" class="to-unit selected-product">
                                        <a href="javascript:;" class="stepper-add bg-highlight rounded-end" data-id="` + product.id + `"><i class="fa-solid fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `)

                $("#overview-selected-products").append(`
                    <div class="d-flex justify-content-between align-items-center" id="overview-product-` + product.id + `">
                        <div>` + product.name + `: <span id="weight-product-` + product.id + `">0</span> kg</div>
                        <div class="fw-bold" id="price-product-` + product.id + `">Rp0,-</div>
                    </div>
                `)

                $(this).remove()
            })

            $(document).on('click', '.remove-product', function () {
                const id = $(this).data('id')
                console.log(id);
                delete selectedProducts[id]

                calcTotal()
                $("#overview-product-" + id).remove()
                $(this).parent().parent().parent().remove()
                $("#search-products").change()
            })

            $(document).on('click', '.stepper-add', function () {
                let input = $(this).parent().find('input').eq(0)
                let id = $(this).data('id')
                changeWeight(input, 1, id)
            })

            $(document).on('click', '.stepper-sub', function () {
                let input = $(this).parent().find('input').eq(0)
                let id = $(this).data('id')
                changeWeight(input, -1, id)
            })

            function changeWeight(input, nominal, id) {
                let value = parseFloat(input.val().replaceAll('.', '').replaceAll(',', '.')) + nominal
                input.val(ToUnit.format(value)).trigger('change')
            }

            $(document).on('change', '.selected-product', function () {
                let id = $(this).data('id')
                let value = $(this).val()
                let valueOrigin = parseFloat(value.replaceAll('.', '').replaceAll(',', '.'))
                if (valueOrigin < 0) {
                    value = 0
                    valueOrigin = 0
                    $(this).val(0)
                }
                selectedProducts[id].weight = valueOrigin
                $("#weight-product-" + id).html(value)
                $("#price-product-" + id).html(ToRupiah.format(selectedProducts[id].price * valueOrigin).replace('\u00A0', '') + ',-')
                calcTotal()
            })

            function calcTotal() {
                let price = 0;
                $.each(selectedProducts, function (index, product) {
                    price += (product.price * product.weight)
                })
                $("#total-price").html(ToRupiah.format(price).replace('\u00A0', '') + ',-')
                const serviceCharge = price * serviceChargePercentage
                $("#service-charge").html(ToRupiah.format(serviceCharge).replace('\u00A0', '') + ',-')
                const total = price - serviceCharge;
                $("#estimate-price").html(ToRupiah.format(total).replace('\u00A0', '') + ',-')
            }

            $("#btn-send").click(function () {
                var isPassed = true
                if (Object.keys(selectedProducts).length <= 0) {
                    Swal.fire({
                        title: "Harap pilih sampahnya!",
                        icon: 'error',
                    })
                    isPassed = false
                }

                $.each(selectedProducts, function (index, product) {
                    if (product.weight == 0) {
                        Swal.fire({
                            title: "Data belum lengkap!",
                            icon: 'error',
                            text: "Masih ada sampah yang beratnya belum diatur, harap periksa kembali!"
                        })
                        isPassed = false
                        return false
                    }
                })

                if (isPassed) {
                    loadingWithText("Mengirim pengajuan ...")

                    const data = new FormData()
                    data.append('_method', 'post')
                    data.append('_token', '{{ csrf_token() }}')
                    data.append('products', JSON.stringify(selectedProducts))
                    data.append('address', $("#address").val())
                    data.append('latitude', $("#latitude").val())
                    data.append('longitude', $("#longitude").val())
                    data.append('payment', $("#payment-method").val())
                    data.append('note', $("#note").val())

                    ajaxPost("{{ route('app.customer.orders.store') }}", data, null, function (result) {
                        Swal.fire({
                            title: "Permintaan diajukan!",
                            icon: "success",
                        })

                        let invoice = result.data
                        setTimeout(() => {
                            window.location.href = "{{ route('app.customer.orders.show', ['invoice' => '-code-']) }}".replace('-code-', invoice)
                        }, 1000);
                    }, function (error) {
                        Swal.fire({
                            title: "Pengajuan gagal!",
                            icon: 'error',
                            text: error.message ?? "Terjadi masalah pada sistem, harap hubungi penyedia layanan Anda!"
                        })
                    }, false)
                }
            })
        }
    </script>
@endpush
