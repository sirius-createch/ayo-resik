@extends('layout.app.main', ['title' => 'Pemberitahuan', 'back' => true, 'notification' => false])

@section('contents')
    @php
        $orders = [
            'created_at' => 'Waktu Masuk',
            'title' => 'Judul',
            'detail' => 'Deskripsi',
        ];

        $data = ['icon', 'title', 'detail', 'link', 'color'];
    @endphp

    <x-app-card>
        @if ($unread != '0')
            <x-app-chip color="red" icon="fa-solid fa-exclamation-triangle" class="w-100 mb-0">Ada {{ $unread }} pemberitahuan belum dibaca</x-app-chip>
        @endif
        <x-app-listing id="listing" name="Pemberitahuan" :orders="$orders" :route="route('app.notification.index')" :data="$data" sort="desc" listGroup>
            @slot('skeleton')
                <div class="card rounded-m" style="padding: 20px 15px 20px 15px!important">
                    <p class="m-1">skeleton</p>
                    <p class="m-1">skeleton</p>
                </div>
            @endslot

            <x-app-list-group-item link="[[link]]" icon="[[icon]]" iconColor="[[color]]" title="[[title]]" subtitle="[[detail]]" />
        </x-app-listing>
    </x-app-card>
@endsection

@push('js')
    <script>
        function documentReady() {
            listingReady()
        }
    </script>
@endpush
