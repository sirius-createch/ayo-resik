@php
    $data['title'] = "Profil";
    if (auth()->user()->role == 'customer') $data['back'] = route('app.customer.home');
@endphp
@extends('layout.app.main', $data)

@section('contents')
    <x-app-card>
        <div class="fw-bold">{{ auth()->user()->name }}</div>
        <div>{{ auth()->user()->phone }}</div>
        <div>{{ auth()->user()->email }}</div>
        <div>{{ auth()->user()->address }}</div>
        @if (auth()->user()->role == 'customer')
            <div class="border-top mt-2 pt-2 fw-bold">Saldo: {{ Sirius::toRupiah(auth()->user()->balance) }}</div>
        @endif
    </x-app-card>

    @if (auth()->user()->role == 'partner')
        <x-app-card>
            <div class="border-bottom pb-1 mb-1">
                <b>Jadwal Ambil:</b><br>
                <span>{{ implode(', ', auth()->user()->schedules_converted) }}</span><br>
            </div>
            <div>
                <b>Sampah Diterima:</b><br>
                <span>{{ auth()->user()->products->pluck('name')->implode(', ') }}</span><br>
            </div>
        </x-app-card>
    @endif

    <div class="m-3">
        <x-app-button link="{{ auth()->user()->role == 'customer' ? route('app.customer.profile.edit') : route('app.partner.profile.edit.information') }}" class="w-100 mb-2 text-start" color="btn-white"><i class="fa-solid color-phone fa-fw fa-edit me-3"></i>Ubah Informasi Umum</x-app-button>
        <x-app-button link="{{ auth()->user()->role == 'customer' ? route('app.customer.profile.password') : route('app.partner.profile.edit.password') }}" class="w-100 mb-2 text-start" color="btn-white"><i class="fa-solid color-phone fa-fw fa-edit me-3"></i>Ubah Kata Sandi</x-app-button>
        @if (auth()->user()->role == 'partner')
            <x-app-button link="{{ route('app.partner.profile.edit.setting') }}" class="w-100 mb-2 text-start" color="btn-white"><i class="fa-solid color-phone fa-fw fa-edit me-3"></i>Ubah Pengaturan</x-app-button>
            <x-app-button link="{{ route('app.partner.profile.customers.index') }}" class="w-100 mb-2 text-start" color="btn-white"><i class="fa-solid color-phone fa-fw fa-users me-3"></i>Daftar Customer</x-app-button>
        @endif
        <x-app-button class="w-100 text-start" color="btn-white" data-menu="sheet-logout-confirmation"><div class="text-danger"><i class="fa-solid fa-fw fa-right-from-bracket me-3"></i>Keluar</div></x-app-button>
    </div>
@endsection

@push('js')
    <script>
        var map = L.map('show-map').setView([{{ auth()->user()->latitude }}, {{ auth()->user()->longitude }}], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([{{ auth()->user()->latitude }}, {{ auth()->user()->longitude }}]).addTo(map)
    </script>
@endpush
