@extends('layout.app.main', ['title' => 'Kata Sandi', 'back' => route('app.'. auth()->user()->role .'.profile.index')])

@section('contents')
    <x-app-card title="Ubah Kata Sandi">
        <x-app-form id="form-edit" method="put" submit="Simpan Perubahah" submitWide class="pt-2">
            <x-app-form-input id="edit-current-password" name="current_password" type="password" label="Kata Sandi Saat Ini" required class="mb-3" />
            <x-app-form-input id="edit-password" name="password" type="password" label="Kata Sandi Baru" required class="mb-3" />
            <x-app-form-input id="edit-password-confirmation" name="password_confirmation" type="password" label="Konfirmasi Kata Sandi Baru" required class="mb-3" />
        </x-app-form>
    </x-app-card>
@endsection

@push('js')
    <script>
        function documentReady() {
            $("#form-edit").submit(function (e) {
                e.preventDefault();
                loadingWithText("Memperbarui profil ...")

                let data = new FormData(document.getElementById('form-edit'))
                ajaxPost("{{ auth()->user()->role == 'customer' ? route('app.customer.profile.password') : route('app.partner.profile.edit.password') }}", data, null, function (result) {
                    Swal.fire({
                        title: 'Kata sandi diperbarui!',
                        icon: 'success',
                    })

                    $("#edit-current-password, #edit-password, #edit-password-confirmation").val('')
                }, function (error) {
                    Swal.fire({
                        title: "Gagal memperbarui kata sandi!",
                        icon: 'error',
                        text: error.message
                    })
                }, false)
            })
        }
    </script>
@endpush
