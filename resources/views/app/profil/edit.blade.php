@extends('layout.app.main', ['title' => 'Informasi Umum', 'back' => route('app.'. auth()->user()->role .'.profile.index')])

@section('contents')
    <x-app-card title="Ubah Informasi Umum">
        <x-app-form id="form-edit" method="put" submit="Simpan Perubahah" submitWide class="pt-2">
            <x-app-form-input-file id="edit_avatar" formId="form-edit" name="avatar" class="mb-3" label="Avatar" accepted="image/*" value="{{ auth()->user()->avatar }}" valueSize="{{ auth()->user()->avatar_size }}" />
            <x-app-form-input id="edit-name" name="name" label="Nama" value="{{ auth()->user()->name }}" required class="mb-3" />
            <x-app-form-input id="edit-email" name="email" label="Alamat Email" type="email" value="{{ auth()->user()->email }}" required class="mb-3" />
            <x-app-form-input id="edit-phone" name="phone" label="Nomor HP" value="0{{ auth()->user()->phone }}" required class="mb-3" />
            <x-app-form-input id="edit-address" name="address" label="Alamat Lengkap" value="{{ auth()->user()->address }}" required class="mb-2" />
            <x-app-button id="get-current-position" size="s" color="bg-blue-dark" class="w-100"><i class="fa-solid fa-location-dot me-2"></i>Ambil Lokasi Saat Ini</x-app-button>
            <div id="edit-map" class="my-2 w-100 rounded-s" style="height: 200px"></div>
            <input type="hidden" id="edit-latitude" name="latitude" value="{{ auth()->user()->latitude }}">
            <input type="hidden" id="edit-longitude" name="longitude" value="{{ auth()->user()->longitude }}">
        </x-app-form>
    </x-app-card>
@endsection

@push('js')
    <script>
        var latitude = {{ auth()->user()->latitude }}
        var longitude = {{ auth()->user()->longitude }}

        var map = L.map('edit-map').setView([latitude, longitude], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(map);

        var marker = L.marker([latitude, longitude]).addTo(map)

        function onMapClick(e) {
            console.log(e);
            latitude = e.latlng.lat
            longitude = e.latlng.lng

            map.setView([latitude, longitude], 17).invalidateSize()
            marker.setLatLng([latitude, longitude])

            $("#edit-latitude").val(latitude)
            $("#edit-longitude").val(longitude)
        }

        map.on('click', onMapClick);

        function documentReady() {
            $("#get-current-position").click(function() {
                if ("geolocation" in navigator) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        latitude = position.coords.latitude
                        longitude = position.coords.longitude

                        map.setView([latitude, longitude], 17).invalidateSize()
                        marker.setLatLng([latitude, longitude])

                        $("#edit-latitude").val(latitude)
                        $("#edit-longitude").val(longitude)
                    });
                } else {
                    alert("Harap nyalakan dan perbolehkan halaman ini mengakses GPS Anda!");
                }
            })

            $("#form-edit").submit(function (e) {
                e.preventDefault();
                loadingWithText("Memperbarui profil ...")

                let data = new FormData(document.getElementById('form-edit'))
                ajaxPost("{{ auth()->user()->role == 'customer' ? route('app.customer.profile.update') : route('app.partner.profile.edit.information') }}", data, null, function (result) {
                    Swal.fire({
                        title: 'Profil diperbarui!',
                        icon: 'success',
                    })
                }, function (error) {
                    Swal.fire({
                        title: "Gagal memberbarui profil!",
                        icon: 'error',
                        text: error.message
                    })
                }, false)
            })
        }
    </script>
@endpush
