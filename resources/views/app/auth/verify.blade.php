@extends('layout.app.auth', ['title' => "Verifikasi"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-3">Masukkan Kode OTP</h3>
    <div class="text-center mt-3 opacity-80 mb-5">Verifikasi bahwa ini benar Anda.</div>

    <div class="mx-5 mb-n3">
        <x-app-form action="{{ route('login') }}" method="post" submit="Verifikasi" submitIcon="fa-solid fa-lock-open" submitWide>
            <div class="row">
                <div class="col-3 text-center">
                    <div class="input-style has-borders Validate-field mb-4">
                        <input class="form-control fw-bold font-25 text-center" id="otp1" name="otp1" type="number" style="padding: 0!important" required />
                    </div>
                </div>
                <div class="col-3 text-center">
                    <div class="input-style has-borders Validate-field mb-4">
                        <input class="form-control fw-bold font-25 text-center" id="otp2" name="otp2" type="number" style="padding: 0!important" required />
                    </div>
                </div>
                <div class="col-3 text-center">
                    <div class="input-style has-borders Validate-field mb-4">
                        <input class="form-control fw-bold font-25 text-center" id="otp3" name="otp3" type="number" style="padding: 0!important" required />
                    </div>
                </div>
                <div class="col-3 text-center">
                    <div class="input-style has-borders Validate-field mb-4">
                        <input class="form-control fw-bold font-25 text-center" id="otp4" name="otp4" type="number" style="padding: 0!important" required />
                    </div>
                </div>
            </div>
        </x-app-form>
    </div>

    <div class="text-center mt-5 opacity-80">Kode OTP tidak diterima? <a href="{{ route('app.register') }}">Kirim Ulang</a>.</div>
@endsection

<script>
</script>
