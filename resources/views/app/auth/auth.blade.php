<script>
    window.localStorage.removeItem('loggedInUser');
    window.location.replace("{{ route('login') }}");
</script>
