@extends('layout.app.auth', ['title' => "Lupa Kata Sandi"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-3">Lupa Kata Sandi</h3>
    <div class="text-center mt-3 opacity-80 mb-5">Masukkan alamat email terdaftar Anda untuk melakukan permintaan pengaturan ulang kata sandi.</div>

    <div class="mx-5 mb-n3">
        <x-app-form id="form" action="{{ route('app.forgot-password') }}" method="post" submit="Kirim Permintaan" submitIcon="fa-solid fa-paper-plane" submitWide>
            <x-app-form-input id="email" name="email" label="Alamat Email" type="email" icon="fa-solid fa-envelope" placeholder="Masukkan alamat email Anda" class="mb-3" required/>
        </x-app-form>
    </div>

    <div class="text-center mt-5 opacity-80">Saya sudah ingat! <a href="{{ route('app.login') }}" class="color-green-dark">Masuk</a>.</div>
@endsection

@push('js')
    <script>
        function documentReady() {
            $("#form").submit(function (e) {
                e.preventDefault();
                loadingWithText("Mengirim email ...")

                let data = new FormData(document.getElementById('form'))
                ajaxPost("{{ route('app.forgot-password') }}", data, null, function (result) {
                    Swal.fire({
                        title: 'Email sudah terkirim!',
                        icon: 'success',
                        text: "Silakan buka akun email Anda, dan ikuti pentunjuk di email tersebut untuk mengatur ulang kata sandi akun Anda!",
                    })

                    $("#email").val('')
                }, function (error) {
                    Swal.fire({
                        title: "Gagal mengirim email!",
                        icon: 'error',
                        text: error.message
                    })
                }, false)
            })
        }
    </script>
@endpush
