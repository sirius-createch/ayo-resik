@extends('layout.app.auth', ['title' => "Masuk"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-3">Masuk</h3>
    <div class="text-center mt-3 opacity-80 mb-5">Masukkan nomor HP dan kata sandi Anda untuk masuk.</div>

    <div class="mx-5 mb-n3">
        @if (session('success'))
            <x-app-chip icon="fa-solid fa-check" color="green" class="mb-4">{{ session('success') }}</x-app-chip>
        @endif
        <x-app-form action="{{ route('login') }}" method="post" submit="Masuk" submitIcon="fa-solid fa-key" submitWide>
            <x-app-form-input id="username" name="username" label="Nomor HP" icon="fa-solid fa-phone" placeholder="Masukkan nomor telepon Anda" class="mb-3" required/>
            <x-app-form-input id="password" name="password" label="Kata Sandi" icon="fa-solid fa-lock" placeholder="Masukkan kata sandi Anda" type="password" required/>
        </x-app-form>
    </div>

    <div class="text-center mt-5 opacity-80">Belum punya akun? <a href="{{ route('app.register') }}" class="color-green-dark">Daftar</a>.</div>
    <div class="text-center mt-1 opacity-80">Lupa kata sandi? <a href="{{ route('app.forgot-password') }}" class="color-green-dark">Atur ulang kata sandi Anda</a>!</div>
@endsection
