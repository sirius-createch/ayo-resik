@extends('layout.app.auth', ['title' => "Daftar"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-5">Daftar</h3>

    <div class="mx-5 mb-n3">
        <x-app-form action="{{ route('register') }}" method="post" submit="Daftar" submitWide>
            <x-app-form-input id="name" name="name" label="Nama" icon="fa-solid fa-user" placeholder="Masukkan nama Anda" class="mb-3" required/>
            <x-app-form-input id="email" name="email" label="Alamat Email" icon="fa-solid fa-at" type="email" placeholder="Masukkan alamat email Anda" class="mb-3"/>
            <x-app-form-input id="phone" name="phone" label="Nomor HP" icon="fa-solid fa-phone" placeholder="Masukkan nomor HP Anda" class="mb-3" required/>
            <x-app-form-input id="password" name="password" label="Kata Sandi" icon="fa-solid fa-lock" placeholder="Masukkan kata sandi Anda" class="mb-3" type="password" required/>
            <x-app-form-input id="password_confirmation" name="password_confirmation" label="Konfirmasi Kata Sandi" icon="fa-solid fa-lock" placeholder="Ketikkan ulang kata sandi Anda" type="password" required/>
        </x-app-form>
    </div>

    <div class="text-center mt-5 opacity-80">Sudah punya akun? <a href="{{ route('app.login') }}" class="color-green-dark">Masuk</a>.</div>
@endsection
