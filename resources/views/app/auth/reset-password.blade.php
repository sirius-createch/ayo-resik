@extends('layout.app.auth', ['title' => "Atur Ulang Kata Sandi"])

@section('contents')
    <h3 class="text-center mt-3 opacity-80 mb-3">Atur Ulang Kata Sandi</h3>
    <div class="text-center mt-3 opacity-80 mb-5">Masukkan kata sandi baru Anda untuk masuk.</div>

    <div class="mx-5 mb-n3">
        <x-app-form action="{{ route('app.reset-password') }}" method="put" submit="Atur Ulang Kata Sandi" submitIcon="fa-solid fa-key" submitWide>
            <x-app-form-input id="email" name="email" type="hidden" value="{{ $email }}" />
            <x-app-form-input id="token" name="token" type="hidden" value="{{ $token }}" />
            <x-app-form-input id="password" name="password" label="Kata Sandi" icon="fa-solid fa-lock" placeholder="Masukkan kata sandi Anda" type="password" class="mb-3" required/>
            <x-app-form-input id="password" name="password_confirmation" label="Konfirmasi Kata Sandi" icon="fa-solid fa-lock" placeholder="Ketik ulang kata sandi Anda" type="password" required/>
        </x-app-form>
    </div>
@endsection
