@extends('layout.compro.main')

@section('contents')
    {{-- Breadcrumb --}}
    <div class="breadcrum-area">
        <div class="container">
            <div class="breadcrumb">
                <ul class="list-unstyled">
                    <li><a href="{{ route('compro.welcome') }}">Beranda</a></li>
                    <li class="active">Konten</li>
                </ul>
                <h1 class="title h2">Konten</h1>
            </div>
        </div>
    </div>

    {{-- Pagination --}}
    <section class="section-padding-equal">
        <div class="container">
            <div class="row row-40">
                <div class="col-lg-8">
                    <div class="row row-cols-1 row-cols-sm-2">
                        @forelse ($contents['data'] as $content)
                            <div class="col">
                                <div class="project-grid">
                                    <div class="thumbnail">
                                        <a href="{{ route('compro.contents.show', ['konten' => $content['slug']]) }}" class="w-100">
                                            <img src="{{ $content['banner'] }}" alt="Banner {{ $content['title'] }}">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="{{ route('compro.contents.show', ['konten' => $content['slug']]) }}">{{ $content['title'] }}</a></h4>
                                        <span class="subtitle">{{ Sirius::toLongDateDay($content['published_at']) }}</span>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="text-white">
                                Konten yang dicari tidak tersedia.
                            </div>
                        @endforelse
                    </div>
                    <div class="pagination justify-content-center mt--20">
                        <ul>
                            @foreach ($contents['links'] as $link)
                                @if ($link['url'])
                                    <li><a class="page-numbers @if($link['active']) current @endif" href="{{ $link['url'] }}">{{ Str::contains($link['label'], 'Lanjut') ? "Lanjut >" : (Str::contains($link['label'], 'Kembali') ? "< Kembali" : $link['label']) }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="axil-sidebar">
                        <div class="widget widget-search">
                            <h4 class="widget-title">Cari Konten</h4>
                            <form class="blog-search">
                                <input type="search" style="padding-right: 3.5rem" name="search" placeholder="Cari ..." value="{{ request()->search }}">
                                <button class="search-button"><i class="fal fa-search"></i></button>
                            </form>
                        </div>
                        <div class="widget widget-recent-post">
                            <h4 class="widget-title">Konten Terkini</h4>
                            <div class="post-list-wrap">
                                @forelse ($recents as $content)
                                    <div class="single-post">
                                        <div class="post-thumbnail">
                                            <a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}">
                                            <img src="{{ $content->banner_link }}" style="height: 50px; object-fit: cover;" alt="Banner {{ $content->title }}"></a>
                                        </div>
                                        <div class="post-content">
                                            <h6 class="title"><a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}">{{ $content->title }}</a></h6>
                                            <ul class="blog-meta list-unstyled">
                                                <li>{{ Sirius::toLongDateDay($content->published_at) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @empty
                                    <div class="single-post">
                                        <div class="post-content">
                                            Tidak ada konten.
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
