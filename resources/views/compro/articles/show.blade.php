@extends('layout.compro.main')

@section('contents')
    {{-- Breadcrumb --}}
    <div class="breadcrum-area">
        <div class="container">
            <div class="breadcrumb">
                <ul class="list-unstyled">
                    <li><a href="{{ route('compro.welcome') }}">Beranda</a></li>
                    <li><a href="{{ route('compro.articles.index') }}">Artikel</a></li>
                    <li class="active">{{ $article->title }}</li>
                </ul>
                <h1 class="title h2">{{ $article->title }}</h1>
                <small>{{ Sirius::toLongDateDay($article->published_at) }}</small>
            </div>
        </div>
    </div>

    {{-- Isi --}}
    <section class="section-padding-equal">
        <div class="container">
            <div class="row row-40">
                <div class="col-lg-8">
                    <div class="post-thumbnail mb-5">
                        <img src="{{ $article->banner_link }}" alt="Banner {{ $article->title }}" class="w-100" style="height: 350px; object-fit: cover; border-radius: 2rem">
                    </div>
                    <p>
                        {!! $article->content !!}
                    </p>
                </div>
                <div class="col-lg-4">
                    <div class="axil-sidebar">
                        <div class="widget widget-search">
                            <h4 class="widget-title">Cari Artikel</h4>
                            <form action="{{ route('compro.articles.index') }}" class="blog-search">
                                <input type="search" style="padding-right: 3.5rem" name="search" placeholder="Cari ..." value="{{ request()->search }}">
                                <button class="search-button"><i class="fal fa-search"></i></button>
                            </form>
                        </div>
                        <div class="widget widget-recent-post">
                            <h4 class="widget-title">Artikel Terkini</h4>
                            <div class="post-list-wrap">
                                @forelse ($recents as $article)
                                    <div class="single-post">
                                        <div class="post-thumbnail">
                                            <a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}">
                                            <img src="{{ $article->banner_link }}" style="height: 50px; object-fit: cover;" alt="Banner {{ $article->title }}"></a>
                                        </div>
                                        <div class="post-content">
                                            <h6 class="title"><a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}">{{ $article->title }}</a></h6>
                                            <ul class="blog-meta list-unstyled">
                                                <li>{{ Sirius::toLongDateDay($article->published_at) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @empty
                                    <div class="single-post">
                                        <div class="post-content">
                                            Tidak ada artikel.
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
