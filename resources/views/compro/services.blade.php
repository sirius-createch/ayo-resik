@extends('layout.compro.main')

@section('contents')
    <div class="breadcrum-area breadcrumb-banner">
        <div class="container">
            <div class="section-heading heading-left sal-animate" data-sal="slide-right" data-sal-duration="1000" data-sal-delay="300">
                <h2 class="title">Misi Kami Menyediakan Akses Daur Ulang Bagi Semua Orang</h2>
                <div>
                    <a href="#pick-up" class="axil-btn btn-fill-white justify mb-3 me-3"><i class="fa-solid fa-box me-3"></i>Pick Up<i class="fa-solid fa-chevron-right ms-5"></i></a>
                    <a href="#drop-off" class="axil-btn btn-fill-white justify mb-3 me-3"><i class="fa-solid fa-truck me-3"></i>Drop Off<i class="fa-solid fa-chevron-right ms-5"></i></a>
                </div>
                <div>
                    <a href="#event" class="axil-btn btn-fill-white justify mb-3 me-3"><i class="fa-solid fa-calendar-day me-3"></i>Event<i class="fa-solid fa-chevron-right ms-5"></i></a>
                    <a href="#company" class="axil-btn btn-fill-white justify mb-3 me-3"><i class="fa-solid fa-building me-3"></i>Company<i class="fa-solid fa-chevron-right ms-5"></i></a>
                </div>
                <div>
                    <a href="#edukasi" class="axil-btn btn-fill-white justify mb-3 me-3"><i class="fa-solid fa-book me-3"></i>Edukasi<i class="fa-solid fa-chevron-right ms-5"></i></a>
                </div>
            </div>
            <div class="banner-thumbnail sal-animate" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                <img class="paralax-image" src="https://picsum.photos/800/700" alt="Illustration" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
            </div>
        </div>
    </div>

    <div id="pick-up">
        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="case-study-featured">
                            <div class="section-heading heading-left">
                                <h2 class="title mb--40"><i class="fa-solid fa-box me-4"></i>Pick Up</h2>
                                <p style="line-height: 2.5em">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sunt nostrum nobis quasi. Vitae omnis, accusamus facilis corporis optio dignissimos, deleniti ad voluptas et amet consequuntur veniam, nulla repellendus eaque aperiam!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-start">
                            <img src="https://picsum.photos/750/300" alt="Ilustrasi">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="why-choose-us">
                            <div class="section-heading heading-left">
                                <h3 class="title mb--40">Cara Pakai</h3>
                                <div class="accordion" id="accordion-pick-up">
                                    <div class="accordion-item">
                                        <h4 class="accordion-header pick-up-step" id="header-pick-up-1" data-thumbnail-target="#pick-up-thumbnail-1">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#child-pick-up-1" aria-expanded="true" aria-controls="child-pick-up-1">
                                                <i class="fa-solid fa-1 fa-fw"></i>Pilih Jenis Sampah
                                            </button>
                                        </h4>
                                        <div id="child-pick-up-1" class="accordion-collapse collapse show" aria-labelledby="header-pick-up-1" data-bs-parent="#accordion-pick-up">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum adipisci eveniet provident reiciendis magnam aliquam ipsam laborum quisquam, aspernatur, voluptates doloribus tempora assumenda cum vitae facere ad voluptas odio atque!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header pick-up-step" id="header-pick-up-2" data-thumbnail-target="#pick-up-thumbnail-2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-pick-up-2" aria-expanded="false" aria-controls="child-pick-up-2">
                                                <i class="fa-solid fa-2 fa-fw"></i>Unggah Foto Sampah
                                            </button>
                                        </h4>
                                        <div id="child-pick-up-2" class="accordion-collapse collapse" aria-labelledby="header-pick-up-2" data-bs-parent="#accordion-pick-up">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis velit illum ratione laborum, dolores minima dolorum nesciunt! Iste sunt, cumque error, mollitia iure quos, atque delectus nesciunt odit unde dolorum?
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header pick-up-step" id="header-pick-up-3" data-thumbnail-target="#pick-up-thumbnail-3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-pick-up-3" aria-expanded="false" aria-controls="child-pick-up-3">
                                                <i class="fa-solid fa-3 fa-fw"></i>Masukkan Informasi Pengantaran
                                            </button>
                                        </h4>
                                        <div id="child-pick-up-3" class="accordion-collapse collapse" aria-labelledby="header-pick-up-3" data-bs-parent="#accordion-pick-up">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam numquam amet nesciunt maxime at cum facere. Illo voluptas accusamus, et mollitia expedita architecto, qui non fuga dolorem id reiciendis eveniet.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header pick-up-step" id="header-pick-up-4" data-thumbnail-target="#pick-up-thumbnail-4">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-pick-up-4" aria-expanded="false" aria-controls="child-pick-up-4">
                                                <i class="fa-solid fa-4 fa-fw"></i>Pilih Metode Pembayaran
                                            </button>
                                        </h4>
                                        <div id="child-pick-up-4" class="accordion-collapse collapse" aria-labelledby="header-pick-up-4" data-bs-parent="#accordion-pick-up">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa magni officiis praesentium esse hic quia optio voluptas consequatur, harum, excepturi distinctio nulla accusantium dolorum. Quos quo dolorum rem sequi labore!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header pick-up-step" id="header-pick-up-5" data-thumbnail-target="#pick-up-thumbnail-5">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-pick-up-5" aria-expanded="false" aria-controls="child-pick-up-5">
                                                <i class="fa-solid fa-5 fa-fw"></i>Pengantaran dan Pembayaran
                                            </button>
                                        </h4>
                                        <div id="child-pick-up-5" class="accordion-collapse collapse" aria-labelledby="header-pick-up-5" data-bs-parent="#accordion-pick-up">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa magni officiis praesentium esse hic quia optio voluptas consequatur, harum, excepturi distinctio nulla accusantium dolorum. Quos quo dolorum rem sequi labore!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-center">
                            <img src="https://picsum.photos/501/600" alt="Ilustrasi" id="pick-up-thumbnail-1" class="pick-up-thumbnails">
                            <img src="https://picsum.photos/502/600" alt="Ilustrasi" id="pick-up-thumbnail-2" class="pick-up-thumbnails" style="display: none">
                            <img src="https://picsum.photos/503/600" alt="Ilustrasi" id="pick-up-thumbnail-3" class="pick-up-thumbnails" style="display: none">
                            <img src="https://picsum.photos/504/600" alt="Ilustrasi" id="pick-up-thumbnail-4" class="pick-up-thumbnails" style="display: none">
                            <img src="https://picsum.photos/505/600" alt="Ilustrasi" id="pick-up-thumbnail-5" class="pick-up-thumbnails" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding bg-color-light pb--70">
            <div class="container">

                <div class="process-work content-reverse sal-animate" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                    <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                        <img src="https://picsum.photos/750/300" alt="Thumbnail">
                    </div>
                    <div class="content ps-5">
                        <h4 class="title">Bergabunglah <br> dengan Gerakan <br> Kami</h4>
                        <span class="subtitle">#LetsMove</span>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div id="drop-off">
        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="case-study-featured">
                            <div class="section-heading heading-left">
                                <h2 class="title mb--40"><i class="fa-solid fa-truck me-4"></i>Drop Off</h2>
                                <p style="line-height: 2.5em">Lorem ipsum dolor sit, amet consectetur adipisicing elit. In nulla consequatur nihil commodi a, mollitia perferendis rem hic eveniet temporibus modi. Eligendi praesentium officia sed assumenda? Tempore culpa iste ex?</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-start">
                            <img src="https://picsum.photos/750/301" alt="Ilustrasi">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="why-choose-us">
                            <div class="section-heading heading-left">
                                <h3 class="title mb--40">Cara Pakai</h3>
                                <div class="accordion" id="accordion-drop-off">
                                    <div class="accordion-item">
                                        <h4 class="accordion-header drop-off-step" id="header-drop-off-1" data-thumbnail-target="#drop-off-thumbnail-1">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#child-drop-off-1" aria-expanded="true" aria-controls="child-drop-off-1">
                                                <i class="fa-solid fa-1 fa-fw"></i>Pilih Drop Off
                                            </button>
                                        </h4>
                                        <div id="child-drop-off-1" class="accordion-collapse collapse show" aria-labelledby="header-drop-off-1" data-bs-parent="#accordion-drop-off">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati ipsa sunt fugit rem placeat? Sit quos natus quo a. Deserunt, eum beatae expedita qui pariatur praesentium eligendi reprehenderit natus! Aliquam.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header drop-off-step" id="header-drop-off-2" data-thumbnail-target="#drop-off-thumbnail-2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-drop-off-2" aria-expanded="false" aria-controls="child-drop-off-2">
                                                <i class="fa-solid fa-2 fa-fw"></i>Unggah Foto Sampah
                                            </button>
                                        </h4>
                                        <div id="child-drop-off-2" class="accordion-collapse collapse" aria-labelledby="header-drop-off-2" data-bs-parent="#accordion-drop-off">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum provident recusandae quibusdam expedita? Quidem aliquam recusandae placeat doloremque nostrum, maiores cupiditate consequatur error ut repellendus similique expedita aliquid sequi saepe.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header drop-off-step" id="header-drop-off-3" data-thumbnail-target="#drop-off-thumbnail-3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-drop-off-3" aria-expanded="false" aria-controls="child-drop-off-3">
                                                <i class="fa-solid fa-3 fa-fw"></i>Masukkan Informasi Pengantaran
                                            </button>
                                        </h4>
                                        <div id="child-drop-off-3" class="accordion-collapse collapse" aria-labelledby="header-drop-off-3" data-bs-parent="#accordion-drop-off">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem architecto quos aut assumenda quisquam corporis quae reiciendis. Reiciendis, quibusdam perspiciatis labore veniam quam sunt vel, fugiat quos unde vitae quia.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header drop-off-step" id="header-drop-off-4" data-thumbnail-target="#drop-off-thumbnail-4">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-drop-off-4" aria-expanded="false" aria-controls="child-drop-off-4">
                                                <i class="fa-solid fa-4 fa-fw"></i>Pilih Metode Pembayaran
                                            </button>
                                        </h4>
                                        <div id="child-drop-off-4" class="accordion-collapse collapse" aria-labelledby="header-drop-off-4" data-bs-parent="#accordion-drop-off">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates commodi nulla animi porro pariatur eos libero doloribus temporibus, soluta ullam expedita itaque neque ea eum incidunt. Nulla asperiores aperiam excepturi.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header drop-off-step" id="header-drop-off-5" data-thumbnail-target="#drop-off-thumbnail-5">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-drop-off-5" aria-expanded="false" aria-controls="child-drop-off-5">
                                                <i class="fa-solid fa-5 fa-fw"></i>Pengantaran dan Pembayaran
                                            </button>
                                        </h4>
                                        <div id="child-drop-off-5" class="accordion-collapse collapse" aria-labelledby="header-drop-off-5" data-bs-parent="#accordion-drop-off">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab beatae, illo officia dolor nisi debitis facere accusantium similique, placeat nulla vitae eos nemo enim tenetur, vel quasi quaerat ipsa non!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-center">
                            <img src="https://picsum.photos/501/601" alt="Ilustrasi" id="drop-off-thumbnail-1" class="drop-off-thumbnails">
                            <img src="https://picsum.photos/502/602" alt="Ilustrasi" id="drop-off-thumbnail-2" class="drop-off-thumbnails" style="display: none">
                            <img src="https://picsum.photos/503/603" alt="Ilustrasi" id="drop-off-thumbnail-3" class="drop-off-thumbnails" style="display: none">
                            <img src="https://picsum.photos/504/604" alt="Ilustrasi" id="drop-off-thumbnail-4" class="drop-off-thumbnails" style="display: none">
                            <img src="https://picsum.photos/505/605" alt="Ilustrasi" id="drop-off-thumbnail-5" class="drop-off-thumbnails" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding bg-color-light pb--70">
            <div class="container">

                <div class="process-work content-reverse sal-animate" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                    <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                        <img src="https://picsum.photos/750/300" alt="Thumbnail">
                    </div>
                    <div class="content ps-5">
                        <h4 class="title">Bergabunglah <br> dengan Gerakan <br> Kami</h4>
                        <span class="subtitle">#LetsMove</span>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div id="event">
        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="case-study-featured">
                            <div class="section-heading heading-left">
                                <h2 class="title mb--40"><i class="fa-solid fa-calendar-day me-4"></i>Event</h2>
                                <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim aliquid quis temporibus eveniet id nihil sapiente itaque voluptatibus distinctio quam, officiis vero ducimus repellendus animi veniam rem excepturi, ut at?</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-start">
                            <img src="https://picsum.photos/751/301" alt="Ilustrasi">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="why-choose-us">
                            <div class="section-heading heading-left">
                                <h3 class="title mb--40">Cara Pakai</h3>
                                <div class="accordion" id="accordion-event">
                                    <div class="accordion-item">
                                        <h4 class="accordion-header event-step" id="header-event-1" data-thumbnail-target="#event-thumbnail-1">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#child-event-1" aria-expanded="true" aria-controls="child-event-1">
                                                <i class="fa-solid fa-1 fa-fw"></i>Isi Data Event
                                            </button>
                                        </h4>
                                        <div id="child-event-1" class="accordion-collapse collapse show" aria-labelledby="header-event-1" data-bs-parent="#accordion-event">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus consequatur odit suscipit eligendi ratione nihil praesentium nemo delectus dolore tempora doloremque adipisci nisi, deleniti ipsam accusantium quos. Maxime, libero perferendis.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header event-step" id="header-event-2" data-thumbnail-target="#event-thumbnail-2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-event-2" aria-expanded="false" aria-controls="child-event-2">
                                                <i class="fa-solid fa-2 fa-fw"></i>Pilih Layanan
                                            </button>
                                        </h4>
                                        <div id="child-event-2" class="accordion-collapse collapse" aria-labelledby="header-event-2" data-bs-parent="#accordion-event">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate repellat, excepturi iste voluptatibus, itaque, quisquam tempore magni minima vitae quidem ab! Doloremque natus sit libero beatae. Doloribus voluptate reprehenderit dolorem?
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header event-step" id="header-event-3" data-thumbnail-target="#event-thumbnail-3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-event-3" aria-expanded="false" aria-controls="child-event-3">
                                                <i class="fa-solid fa-3 fa-fw"></i>Pilih Jenis Sampah
                                            </button>
                                        </h4>
                                        <div id="child-event-3" class="accordion-collapse collapse" aria-labelledby="header-event-3" data-bs-parent="#accordion-event">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde ad ratione corrupti rem molestiae, cupiditate repellat fugit quae beatae ex esse veritatis impedit consectetur! Debitis quam sunt vel culpa ex!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-center">
                            <img src="https://picsum.photos/506/600" alt="Ilustrasi" id="event-thumbnail-1" class="event-thumbnails">
                            <img src="https://picsum.photos/507/600" alt="Ilustrasi" id="event-thumbnail-2" class="event-thumbnails" style="display: none">
                            <img src="https://picsum.photos/508/600" alt="Ilustrasi" id="event-thumbnail-3" class="event-thumbnails" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding bg-color-light pb--70">
            <div class="container">

                <div class="process-work content-reverse sal-animate" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                    <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                        <img src="https://picsum.photos/750/300" alt="Thumbnail">
                    </div>
                    <div class="content ps-5">
                        <h4 class="title">Bergabunglah <br> dengan Gerakan <br> Kami</h4>
                        <span class="subtitle">#LetsMove</span>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div id="company">
        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="case-study-featured">
                            <div class="section-heading heading-left">
                                <h2 class="title mb--40"><i class="fa-solid fa-calendar-day me-4"></i>Company</h2>
                                <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque quidem totam odit libero doloribus inventore aperiam possimus repudiandae eum molestias? Minima accusantium incidunt vitae doloribus sed culpa ipsum eos porro?</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-start">
                            <img src="https://picsum.photos/751/302" alt="Ilustrasi">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="why-choose-us">
                            <div class="section-heading heading-left">
                                <h3 class="title mb--40">Cara Pakai</h3>
                                <div class="accordion" id="accordion-company">
                                    <div class="accordion-item">
                                        <h4 class="accordion-header company-step" id="header-company-1" data-thumbnail-target="#company-thumbnail-1">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#child-company-1" aria-expanded="true" aria-controls="child-company-1">
                                                <i class="fa-solid fa-1 fa-fw"></i>Isi Data Usaha/Perusahaan
                                            </button>
                                        </h4>
                                        <div id="child-company-1" class="accordion-collapse collapse show" aria-labelledby="header-company-1" data-bs-parent="#accordion-company">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique eum ut nostrum expedita culpa aspernatur corrupti in, voluptate labore quaerat et maiores, eaque vitae beatae maxime vel commodi assumenda nam!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header company-step" id="header-company-2" data-thumbnail-target="#company-thumbnail-2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-company-2" aria-expanded="false" aria-controls="child-company-2">
                                                <i class="fa-solid fa-2 fa-fw"></i>Pilih Layanan
                                            </button>
                                        </h4>
                                        <div id="child-company-2" class="accordion-collapse collapse" aria-labelledby="header-company-2" data-bs-parent="#accordion-company">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto repellendus eius fugit deleniti. Tempora voluptates aliquid enim temporibus quas, rem accusantium rerum, eveniet eos eius unde inventore sint neque asperiores.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header company-step" id="header-company-3" data-thumbnail-target="#company-thumbnail-3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-company-3" aria-expanded="false" aria-controls="child-company-3">
                                                <i class="fa-solid fa-3 fa-fw"></i>Pilih Lama Berlangganan
                                            </button>
                                        </h4>
                                        <div id="child-company-3" class="accordion-collapse collapse" aria-labelledby="header-company-3" data-bs-parent="#accordion-company">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Amet optio alias nam eaque, delectus culpa doloremque deserunt minima assumenda incidunt ducimus molestias vitae totam aliquid ea obcaecati ad voluptate consequatur.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-center">
                            <img src="https://picsum.photos/506/606" alt="Ilustrasi" id="company-thumbnail-1" class="company-thumbnails">
                            <img src="https://picsum.photos/507/606" alt="Ilustrasi" id="company-thumbnail-2" class="company-thumbnails" style="display: none">
                            <img src="https://picsum.photos/508/606" alt="Ilustrasi" id="company-thumbnail-3" class="company-thumbnails" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding bg-color-light pb--70">
            <div class="container">

                <div class="process-work content-reverse sal-animate" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                    <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                        <img src="https://picsum.photos/750/300" alt="Thumbnail">
                    </div>
                    <div class="content ps-5">
                        <h4 class="title">Bergabunglah <br> dengan Gerakan <br> Kami</h4>
                        <span class="subtitle">#LetsMove</span>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div id="edukasi">
        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="case-study-featured">
                            <div class="section-heading heading-left">
                                <h2 class="title mb--40"><i class="fa-solid fa-book me-4"></i>Edukasi</h2>
                                <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio labore repellendus a accusamus sunt hic cum fugiat illo asperiores? Quas labore accusantium mollitia neque laudantium! Autem, itaque. Officiis, unde minima.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-start">
                            <img src="https://picsum.photos/752/302" alt="Ilustrasi">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding case-study-featured-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="why-choose-us">
                            <div class="section-heading heading-left">
                                <h3 class="title mb--40">Cara Pakai</h3>
                                <div class="accordion" id="accordion-edukasi">
                                    <div class="accordion-item">
                                        <h4 class="accordion-header edukasi-step" id="header-edukasi-1" data-thumbnail-target="#edukasi-thumbnail-1">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#child-edukasi-1" aria-expanded="true" aria-controls="child-edukasi-1">
                                                <i class="fa-solid fa-1 fa-fw"></i>Pilih Edukasi
                                            </button>
                                        </h4>
                                        <div id="child-edukasi-1" class="accordion-collapse collapse show" aria-labelledby="header-edukasi-1" data-bs-parent="#accordion-edukasi">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quas explicabo magnam similique cum quibusdam at delectus temporibus ullam inventore eos, dicta, eius provident nisi perferendis nobis accusantium, quidem ad? Nihil.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header edukasi-step" id="header-edukasi-2" data-thumbnail-target="#edukasi-thumbnail-2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-edukasi-2" aria-expanded="false" aria-controls="child-edukasi-2">
                                                <i class="fa-solid fa-2 fa-fw"></i>Pilih Layanan
                                            </button>
                                        </h4>
                                        <div id="child-edukasi-2" class="accordion-collapse collapse" aria-labelledby="header-edukasi-2" data-bs-parent="#accordion-edukasi">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facere illum vero beatae impedit aliquam minus tempora, asperiores consectetur quo vel eaque, odit saepe dignissimos, laudantium veritatis mollitia ipsa neque facilis?
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h4 class="accordion-header edukasi-step" id="header-edukasi-3" data-thumbnail-target="#edukasi-thumbnail-3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#child-edukasi-3" aria-expanded="false" aria-controls="child-edukasi-3">
                                                <i class="fa-solid fa-3 fa-fw"></i>Pilih Jenis Sampah
                                            </button>
                                        </h4>
                                        <div id="child-edukasi-3" class="accordion-collapse collapse" aria-labelledby="header-edukasi-3" data-bs-parent="#accordion-edukasi">
                                            <div class="accordion-body" style="line-height: 2em">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea vero debitis perspiciatis nulla at nihil accusamus libero, eaque ad fugit fugiat hic unde labore eos est voluptatibus dicta. Explicabo, voluptatibus.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="case-study-featured-thumb text-center">
                            <img src="https://picsum.photos/501/606" alt="Ilustrasi" id="edukasi-thumbnail-1" class="edukasi-thumbnails">
                            <img src="https://picsum.photos/502/606" alt="Ilustrasi" id="edukasi-thumbnail-2" class="edukasi-thumbnails" style="display: none">
                            <img src="https://picsum.photos/503/606" alt="Ilustrasi" id="edukasi-thumbnail-3" class="edukasi-thumbnails" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-padding bg-color-light pb--70">
            <div class="container">

                <div class="process-work content-reverse sal-animate" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                    <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                        <img src="https://picsum.photos/750/300" alt="Thumbnail">
                    </div>
                    <div class="content ps-5">
                        <h4 class="title">Bergabunglah <br> dengan Gerakan <br> Kami</h4>
                        <span class="subtitle">#LetsMove</span>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <section class="section section-padding mb-5" style="padding-block: 70px!important">
        <div class="container">
            <div class="section-heading">
                <h2 class="title mb--50">Jenis Daur Ulang</h2>
            </div>
            <swiper-container slides-per-view="5" speed="500" loop="true" navigation="true">
                @foreach (App\Models\Product::all() as $product)
                    <swiper-slide class="text-center">
                        <img src="{{ $product->image_link }}" class="w-100">
                    </swiper-slide>
                @endforeach
            </swiper-container>
        </div>
    </section>
@endsection

@push('js')
    <script>
        $(document).on('click', '.pick-up-step', function () {
            const target = $(this).data('thumbnail-target')
            $('.pick-up-thumbnails').hide()
            $(target).fadeIn()
        })

        $(document).on('click', '.drop-off-step', function () {
            const target = $(this).data('thumbnail-target')
            $('.drop-off-thumbnails').hide()
            $(target).fadeIn()
        })

        $(document).on('click', '.event-step', function () {
            const target = $(this).data('thumbnail-target')
            $('.event-thumbnails').hide()
            $(target).fadeIn()
        })

        $(document).on('click', '.company-step', function () {
            const target = $(this).data('thumbnail-target')
            $('.company-thumbnails').hide()
            $(target).fadeIn()
        })

        $(document).on('click', '.edukasi-step', function () {
            const target = $(this).data('thumbnail-target')
            $('.edukasi-thumbnails').hide()
            $(target).fadeIn()
        })
    </script>
@endpush
