@extends('layout.compro.main')

@section('contents')
    {{-- Tentang Kami --}}
    <section class="section-padding bg-color-light mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="why-choose-us">
                        <div class="section-heading heading-left">
                            <span class="subtitle">Pertanyaan</span>
                            <h2 class="title">FAQ</h2>
                        </div>
                        <div class="accordion" id="choose-accordion">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="far fa-question"></i> Apakah Aplikasi Ayoresik itu?
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Adalah aplikasi yang dibuat untuk membantu masyarakat agar mendapatkan manfaat pengelolaan sampah daur ulang yang dikelola oleh para penggiat lingkungan
                                        yang berperan sebagai Partner ayoresik.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="far fa-question"></i>Mengapa Harus Menggunakan Ayoresik?
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Selain benefit sampah daur ulang yang bisa diuangkan, dengan menggunakan aplikasi ayoresik kita ikut bertanggung jawab terhadap lingkungan yang lebih baik. 
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fal fa-question"></i>Dimana Aplikasi Ayoresik Bisa Diunduh?
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Aplikasi AYORESIK bisa diunduh pada link yang tertera pada alamat website
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingFour">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <i class="fal fa-question"></i>Apakah Partner Ayoresik itu?
                                    </button>
                                </h2>
                                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Partner ayoresik adalah kelompok / individu penggiat lingkungan seperti Bank Sampah, Pengepul, Pelapak, TPS 3R, ataupun komunitas pengumpul sampah.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingFive">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <i class="fal fa-question"></i>Sampah Apa Saja yang Bisa Diterima di Ayoresik?
                                    </button>
                                </h2>
                                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Mulai dari botol plastic, kertas, karton dan beberapa jenis sampah daur ulang yang lain dimana, untuk detail jenis sampahnya dapat dilihat pada aplikasi ayoresik.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSix">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <i class="fal fa-question"></i>Dimana Saja Area Cakupan Ayoresik?
                                    </button>
                                </h2>
                                <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Saat ini, aplikasi ayoresik mencakup area Sidoarjo Jawa Timur & sekitarnya.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSeven">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        <i class="fal fa-question"></i>Apakah Saya Dapat Membatalkan Pesanan Sampah?
                                    </button>
                                </h2>
                                <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        Bisa! tidak hanya membatalkan, waktu penjemputanpun dapat disesuaikan bahkan berat timbanganpun dapat diperbaharui, sebelum proses pembayaran terjadi.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingEight">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                        <i class="fal fa-question"></i>Berapa Berat Minimal Untuk Melakukan Pemesanan Pengambilan Sampah?
                                    </button>
                                </h2>
                                <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#choose-accordion">
                                    <div class="accordion-body">
                                        berat minimum adalah 5 Kg untuk masing-masing jenis sampah Daur Ulang.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
