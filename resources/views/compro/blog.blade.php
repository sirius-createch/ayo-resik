@extends('layout.compro.main')

@section('contents')
    <div class="breadcrum-area">
        <div class="container">
            <div class="breadcrumb">
                <h2 class="title">Blog</h2>
            </div>
        </div>
    </div>

    <section class="section section-padding bg-color-light mb-5" style="padding-block: 70px!important">
        <div class="container">
            <div class="section-heading mb-0">
                <h3 class="title mb-0">Artikel</h3>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            @forelse (App\Models\Article::where('published_at', '<>', null)->limit(12)->latest()->get() as $article)
                <div class="col-md-4">
                    <div class="blog-grid">
                        <div class="post-thumbnail">
                            <a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}" class="w-100">
                                <img src="{{ $article->banner_link }}" alt="Banner {{ $article->title }}" class="w-100" style="height: 250px; object-fit: cover; border-radius: 2rem">
                            </a>
                        </div>
                        <div class="mb-3">
                            <h4 class="title mb-0"><a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}">{{ $article->title }}</a></h4>
                            <small>{{ Sirius::toLongDateDay($article->published_at) }}</small>
                        </div>
                        <p>{{ Str::limit(strip_tags($article->content), 250) }}</p>
                    </div>
                </div>
            @empty
                <div class="text-white">
                    Artikel belum tersedia.
                </div>
            @endforelse
        </div>
        <div class="text-center"><a href="{{ route('compro.articles.index') }}" class="axil-btn btn-fill-primary btn-fill-success my-5 w-100">Lebih Banyak Artikel</a></div>
    </div>

    <section class="section section-padding bg-color-light mb-5" style="padding-block: 70px!important">
        <div class="container">
            <div class="section-heading mb-0">
                <h3 class="title mb-0">Konten</h3>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            @forelse (App\Models\Content::where('published_at', '<>', null)->limit(12)->latest()->get() as $content)
                <div class="col-md-4">
                    <div class="blog-grid">
                        <div class="post-thumbnail">
                            <a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}" class="w-100">
                                <img src="{{ $content->banner_link }}" alt="Banner {{ $content->title }}" class="w-100" style="height: 250px; object-fit: cover; border-radius: 2rem">
                            </a>
                        </div>
                        <div class="mb-3">
                            <h4 class="title mb-0"><a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}">{{ $content->title }}</a></h4>
                            <small>{{ Sirius::toLongDateDay($content->published_at) }}</small>
                        </div>
                        <p>{{ Str::limit(strip_tags($content->content), 250) }}</p>
                    </div>
                </div>
            @empty
                <div class="text-white">
                    Konten belum tersedia.
                </div>
            @endforelse
        </div>
        <div class="text-center"><a href="{{ route('compro.contents.index') }}" class="axil-btn btn-fill-primary btn-fill-success my-5 w-100">Lebih Banyak Konten</a></div>
    </div>
@endsection
