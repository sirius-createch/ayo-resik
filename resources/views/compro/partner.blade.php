@extends('layout.compro.main')

@section('contents')
    <div class="breadcrum-area breadcrumb-banner">
        <div class="container">
            <div class="section-heading heading-left" data-sal="slide-right" data-sal-duration="1000" data-sal-delay="300">
                <h2 class="title">Mitra Ayoresik</h2>
                <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt facilis ducimus non reprehenderit. Quisquam natus laborum itaque, porro sit temporibus dolorum recusandae molestiae amet culpa voluptatibus et quidem quis nostrum!</p>
            </div>
            <div class="banner-thumbnail" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400">
                <img class="paralax-image" src="https://picsum.photos/800/600" alt="Illustration" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
            </div>
        </div>
    </div>

    <section class="section section-padding-equal">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-4 col-lg-5 offset-xl-1" data-sal="slide-up" data-sal-duration="800" data-sal-delay="100">
                    <div class="contact-form-box">
                        <img src="https://picsum.photos/400/700" alt="Ilustrasi">
                    </div>
                </div>
                <div class="col-lg-7" data-sal="slide-up" data-sal-duration="800">
                    <div class="about-us">
                        <div class="section-heading heading-left mb-0">
                            <h2 class="title mb--40">Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit Temporibus Debitis Omnis Eaque Quaerat Magnam Earum Quia Nulla</h2>
                            <p style="line-height: 2.5em!important">Nulla et velit gravida, facilisis quam a, molestie ante. Mauris placerat suscipit dui, eget maximus tellus blandit a. Praesent non tellus sed ligula commodo blandit in et mauris. Quisque efficitur ipsum ut dolor molestie pellentesque.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding bg-color-light mb-5" style="padding-block: 70px!important">
        <div class="container">
            <div class="section-heading mb-0">
                <h3 class="title mb-0">Tawaran Program</h3>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h3><span class="border border-success rounded-circle me-3 px-2"><i class="fa-solid fa-1 fa-2xs fa-fw text-success"></i></span>Pengepul</h3>
                            <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae officia, delectus sit deleniti ut esse ullam, blanditiis ipsum veritatis, veniam nobis neque voluptatem harum illum quo dolorem possimus molestias aliquid.</p>
                            <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/500/350" alt="Ilustrasi" class="rounded">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/501/350" alt="Ilustrasi" class="rounded">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h3><span class="border border-success rounded-circle me-3 px-2"><i class="fa-solid fa-2 fa-2xs fa-fw text-success"></i></span>Pemilik Gudang Sortir</h3>
                            <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex dolorum quasi voluptas veritatis maiores provident harum corporis eos laudantium. Quis repellendus excepturi cupiditate maiores soluta, explicabo quasi corrupti molestias rerum.</p>
                            <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h3><span class="border border-success rounded-circle me-3 px-2"><i class="fa-solid fa-3 fa-2xs fa-fw text-success"></i></span>Agen Ayoresik</h3>
                            <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet, rerum. Doloribus error a consequatur. Commodi, nisi, rerum autem dolores officiis quam molestias laboriosam possimus architecto odit distinctio, praesentium recusandae animi.</p>
                            <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/500/351" alt="Ilustrasi" class="rounded">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/501/351" alt="Ilustrasi" class="rounded">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h3><span class="border border-success rounded-circle me-3 px-2"><i class="fa-solid fa-4 fa-2xs fa-fw text-success"></i></span>Bank Sampah</h3>
                            <p style="line-height: 2.5em">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic, ut omnis. Reiciendis laborum explicabo voluptate sapiente deserunt tempore ducimus, beatae, quia omnis sit obcaecati laudantium quidem? Cupiditate consequuntur eos aliquam.</p>
                            <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h3><span class="border border-success rounded-circle me-3 px-2"><i class="fa-solid fa-5 fa-2xs fa-fw text-success"></i></span>Gudang Akhir</h3>
                            <p style="line-height: 2.5em">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore quaerat natus quo delectus iure eius rem blanditiis atque, adipisci, nemo rerum laudantium cumque, iste enim iusto provident dignissimos tempora quia.</p>
                            <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/502/350" alt="Ilustrasi" class="rounded">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding bg-color-light pb--70 mb-5">
        <div class="container">

            <div class="process-work content-reverse" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="100">
                <div class="thumbnail paralax-image" style="will-change: transform; transform: perspective(1000px) rotateX(0deg) rotateY(0deg);">
                    <img src="https://picsum.photos/750/300" alt="Thumbnail">
                </div>
                <div class="content ps-5">
                    <h4 class="title">Bergabunglah <br> Menjadi Mitra <br> Ayoresik</h4>
                    <a href="#" class="axil-btn btn-fill-primary btn-fill-success">Gabung Sekarang</a>
                </div>
            </div>

        </div>
    </section>
@endsection
