@extends('layout.compro.main')

@section('contents')
    <section class="section-padding privacy-policy-area">
        <div class="container">
            <div class="section-heading heading-center">
                <h2 class="title">{{ $page->title }}</h2>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <p>
                        {!! $page->content !!}
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
