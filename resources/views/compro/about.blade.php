@extends('layout.compro.main')

@section('contents')
    <section class="banner banner-style-2 mb-5" style="background-image: url(https://picsum.photos/1400/720)">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="banner-content">
                        <h2 class="title">Saatnya Melakukan Perubahan Bersama Kami</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding case-study-featured-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-6">
                    <div class="case-study-featured-thumb text-start">
                        <img src="https://picsum.photos/750/510" alt="Ilustrasi">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <p style="line-height: 2.5em">Ayoresik adalah layanan digital untuk menunjang partisipasi aktif masyarakat dalam mengelola sampah secara bertanggung jawab. Memberi "kehidupan kedua" bagi sampah melalui proses daur ulang yang baik akan berdampak positif mengurangi volume sampah secara signifikan. Saatnya melakukan perubahan bersama kami demi lingkungan yang lebih baik.</p>
                            <p style="line-height: 2.5em">Aplikasi kami adalah alat bantu masyarakat untuk mendapatkan nilai ekonomis dari sampah daur ulang yang sebelumnya langsung dibuang. Dengan menggunakan aplikasi Ayoresik kita semua turut berperan mengurangi volume sampah demi lingkungan yang lebuh baik.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding-equal">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12" data-sal="slide-up" data-sal-duration="800">
                    <div class="about-us">
                        <div class="section-heading heading-left mb-0">
                            <h2 class="subtitle">Solusi Kami</h2>
                            <h3 class="title mb--40">1. Edukasi</h3>
                            <p>Edukasi di sekolah dan lingkungan Masyarakat untuk meningkatkan kesadaran tentang pentingnya daur ulang sampah dari sumbernya.</p>
                            <p>Melakukan kampanye edukasi untuk mengenalkan tentang cara memilah sampah dan manfaat daur ulang.</p>
                            <p>Mendorong program 3R (Reduce, Reuse, Recycle di semua sektor).</p>
                            <p>Melibatkan komunitas daLam program daur ulang, seperti bank sampah dan Gerakan kebersihan lingkungan.</p>
                            <p>Penggunaan aplikasi ayoresik untuk mengatur jadwal pengumpulan sampah dan memberi informasi kepada Masyarakat.</p>
                            <p>Memberikan reward kepada semua mitra atau partner yang turut berpartisipasi dalam Gerakan menyelamatkan lingkungan.</p>
                            <h3 class="title mb--40">2. Reward Partisipasi</h3>
                            <p>Setelah Masyarakat teredukasi terkait pemilahan sampah organic dan anorganik, berlanjut pada partisipasi aktif dan konsisten. Agar aktifitas ini berjalan konsistensi, maka Ayoresik akan memberikan Reward dari Partisipasi Masyarakat ini.</p>
                            <h3 class="title mb--40">3. Layanan</h3>
                            <p>Setelah Masyarakat teredukasi terkait pemilahan sampah organic dan anorganik, berlanjut pada partisipasi aktif dan konsisten. Agar aktifitas ini berjalan konsistensi, maka Ayoresik akan memberikan Reward dari Partisipasi Masyarakat ini.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding spalsh-why-choose mb-5">
        <div class="container">
            <div class="section-heading heading-left">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h2 class="title">Hadir Memberi Dampak</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">

                <div class="col-12 col-md-6" data-sal="slide-up" data-sal-duration="800">
                    <div class="support-box online-docuentation splash-hover-control active">
                        <a href="javascript:;">
                            <div class="inner">
                                <div class="content">
                                    <div class="heading">
                                        <h3 class="title mb-0">Lingkungan</h3>
                                        <div class="icon d-flex align-items-center justify-content-center text-success">
                                            <i class="fa-solid fa-seedling fa-3x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6" data-sal="slide-up" data-sal-duration="800">
                    <div class="support-box online-docuentation splash-hover-control active">
                        <a href="javascript:;">
                            <div class="inner">
                                <div class="content">
                                    <div class="heading">
                                        <h3 class="title mb-0">Ekonomi</h3>
                                        <div class="icon d-flex align-items-center justify-content-center text-success">
                                            <i class="fa-solid fa-money-bill-wave fa-3x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6" data-sal="slide-up" data-sal-duration="800">
                    <div class="support-box online-docuentation splash-hover-control active">
                        <a href="javascript:;">
                            <div class="inner">
                                <div class="content">
                                    <div class="heading">
                                        <h3 class="title mb-0">Sosial</h3>
                                        <div class="icon d-flex align-items-center justify-content-center">
                                            <i class="fa-solid fa-people-group fa-3x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6" data-sal="slide-up" data-sal-duration="800">
                    <div class="support-box online-docuentation splash-hover-control active">
                        <a href="javascript:;">
                            <div class="inner">
                                <div class="content">
                                    <div class="heading">
                                        <h3 class="title mb-0">Kesehatan</h3>
                                        <div class="icon d-flex align-items-center justify-content-center text-danger">
                                            <i class="fa-solid fa-heart-pulse fa-3x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
