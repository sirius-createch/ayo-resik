@extends('layout.compro.main')

@section('contents')
    <section class="section section-padding">
        <div class="container">
            <div class="section-heading heading-center mb-0">
                <span class="subtitle">Kontak Kami</span>
                <h2 class="title">Punya pertanyaan lebih lanjut?<br>Hubungi kami!</h2>
            </div>
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="contact-info mb--100 mb_md--30 mt_md--0 mt--150">
                        <h4 class="title">Alamat</h4>
                        <p>{{ $settings['address'] }}</p>
                        <h4 class="phone-number"><a target="_blank" href="https://www.google.co.id/maps/place/{{ $settings['address'] }}">Cek Google Map</a></h4>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 offset-xl-1">
                    <div class="contact-info mb--100 mb_md--30 mt_md--0 mt--150">
                        <h4 class="title">Telepon</h4>
                        <p>Our customer care is open from Mon-Fri, 10:00 am to 6:00 pm</p>
                        <h4 class="phone-number"><a target="_blank" href="https://wa.me/62{{ $settings['phone'] }}">{{ '0'.$settings['phone'] }}</a></h4>
                    </div>
                    <div class="contact-info mb--30">
                        <h4 class="title">Email</h4>
                        <p>Our support team will get back to in 48-h during standard business hours.</p>
                        <h4 class="phone-number"><a target="_blank" href="mailto:{{ $settings['email'] }}">{{ $settings['email'] }}</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
