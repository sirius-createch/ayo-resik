@extends('layout.compro.main')

@section('contents')
    @if ($sliders->count())
        <div class="single-blog single-blog-gallery header-slider">
            <div class="single-blog-content blog-grid blog-thumb-slide">
                <div class="post-thumbnail pb-5 mx-0">
                    <div class="slick-slider slick-arrow-nav slick-dot-nav" data-slick='{"infinite": true, "autoplay": true, "arrows": true, "dots": true, "slidesToShow": 1}'>
                        @foreach ($sliders as $slider)
                            <div class="slick-slide" style="background: url('{{ $slider->image_link }}');">
                                <a href="{{ $slider->url ?? 'javascript:;' }}" class="w-100 position-relative container">
                                    @if ($slider->title || $slider->desc)
                                        <div class="slider-item">
                                            <div class="w-100 d-flex flex-column gap-4">
                                                @if ($slider->title)
                                                    <h3 class="title text-white mb-0">{{ $slider->title }}</h3>
                                                @endif
                                                @if ($slider->desc)
                                                    <p class="text-white">{{ $slider->desc }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <section class="case-study-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" data-sal="slide-right" data-sal-duration="1000">
                    <div class="case-study-featured-thumb thumb-1">
                        <img class="paralax-image" src="{{ asset('themes/compro/images/case-study-2.png') }}" alt="Ilustrasi">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 offset-xl-1" data-sal="slide-left" data-sal-duration="1000" data-sal-delay="200">
                    <div class="case-study-featured">
                        <div class="section-heading heading-left">
                            <h2 class="title">Tentang Aplikasi</h2>
                            <p style="line-height: 2.5em">Aplikasi kami membantu masyarakat untuk mendapatkan nilai ekonomis dari sampah daur ulang yang sebelumya langsung dibuang. Dengan menggunakan aplikasi
                            Ayoresik - kita semua juga turut berperan mengurangi volume sampah demi lingkungan yang lebih baik</p>
                            <a href="{{ route('app.login') }}" class="axil-btn btn-fill-primary btn-large">Ke Aplikasi &nbsp &nbsp <i class="fas fa-mobile-alt me-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <section class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4" data-sal="slide-up" data-sal-duration="800" data-sal-delay="100">
                    <div class="services-grid service-style-2">
                        <div class="content">
                            <h5 class="title"> <a href="service-design.html">For Companies</a></h5>
                            <p>Simply drag and drop photos and videos into your workspace to automatically add them to your Collab Cloud library.</p>
                            <a href="service-design.html" class="more-btn">Find out more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" data-sal="slide-up" data-sal-duration="800" data-sal-delay="200">
                    <div class="services-grid service-style-2">
                        <div class="content">
                            <h5 class="title"> <a href="service-development.html">Development</a></h5>
                            <p>Simply drag and drop photos and videos into your workspace to automatically add them to your Collab Cloud library.</p>
                            <a href="service-development.html" class="more-btn">Find out more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" data-sal="slide-up" data-sal-duration="800" data-sal-delay="300">
                    <div class="services-grid service-style-2">
                        <div class="content">
                            <h5 class="title"> <a href="service-content-strategy.html">Content strategy</a></h5>
                            <p>Simply drag and drop photos and videos into your workspace to automatically add them to your Collab Cloud library.</p>
                            <a href="service-content-strategy.html" class="more-btn">Find out more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="section section-padding-equal">
        <div class="container">
            <div class="section-heading">
                <h2 class="title">Mitra Bisnis</h2>
            </div>
            <swiper-container slides-per-view="5" speed="500" loop="true" navigation="true">
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="https://picsum.photos/200/200" class="w-100 rounded">
                    </div>
                </swiper-slide>
            </swiper-container>

            <div class="section-heading mb-0">
                <span class="subtitle">Kami Turut Mendukung SDG</span>
            </div>
            <swiper-container slides-per-view="6" speed="500" loop="true" navigation="true">
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/gender-equality.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/decent-work-and-economic-growth.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/reduced-inequalities.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/responsible-consumption-and-production.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/climate-action.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
                <swiper-slide class="text-center">
                    <div class="p-5">
                        <img src="{{ asset('images/sdg/life-below-water.png') }}" class="w-100 rounded">
                    </div>
                </swiper-slide>
            </swiper-container>
        </div>
    </section>

    <section class="section section-padding-equal pt--200 pt_md--80 pt_sm--60">
        <div class="container">
            <div class="section-heading heading-left">
                <h2 data-sal="slide-right" data-sal-duration="1000" class="title">Konten</h2>
            </div>
            <div class="axil-isotope-wrapper">
                <div class="row isotope-list" style="position: relative; height: 1504.69px;">
                    @forelse ($contents as $content)
                        <div class="col-xl-3 col-md-6 project" data-sal="slide-right" data-sal-duration="1000">
                            <div class="project-grid">
                                <div class="thumbnail">
                                    <a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}" class="w-100">
                                        <img src="{{ $content->banner_link }}" alt="Banner {{ $content->title }}" style="width: 100%; aspect-ratio: 1 / 1; object-fit: cover;">
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="title"><a href="{{ route('compro.contents.show', ['konten' => $content->slug]) }}">{{ $content->title }}</a></h5>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-xl-4 col-md-6 project">
                            Konten belum tersedia.
                        </div>
                    @endforelse
                </div>
                <div class="more-project-btn">
                    <a href="{{ route('compro.contents.index') }}" class="axil-btn btn-fill-primary">Lihat Konten Lainnya</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-padding-equal pt--200 pt_md--80 pt_sm--60">
        <div class="container">
            <div class="section-heading heading-left">
                <h2 data-sal="slide-right" data-sal-duration="1000" class="title">Artikel</h2>
            </div>
            <div class="axil-isotope-wrapper">
                <div class="row isotope-list" style="position: relative; height: 1504.69px;">
                    @forelse ($articles as $article)
                        <div class="col-xl-3 col-md-6 project" data-sal="slide-right" data-sal-duration="1000">
                            <div class="project-grid">
                                <div class="thumbnail">
                                    <a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}" class="w-100">
                                        <img src="{{ $article->banner_link }}" alt="Banner {{ $article->title }}" style="width: 100%; aspect-ratio: 1 / 1; object-fit: cover;">
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="title"><a href="{{ route('compro.articles.show', ['artikel' => $article->slug]) }}">{{ $content->title }}</a></h5>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-xl-4 col-md-6 project">
                            Artikel belum tersedia.
                        </div>
                    @endforelse
                </div>
                <div class="more-project-btn">
                    <a href="{{ route('compro.articles.index') }}" class="axil-btn btn-fill-primary">Lihat Artikel Lainnya</a>
                </div>
            </div>
        </div>
    </section>
@endsection
