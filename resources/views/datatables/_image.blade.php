<a href="{{ $model->image_link }}" data-fancybox style="cursor: zoom-in">
    <img src="{{ $model->image_link }}" alt="Gambar {{ $model->name }}" class="rounded pull-up" style="object-fit: cover; width:25px; height: 25px;">
</a>
