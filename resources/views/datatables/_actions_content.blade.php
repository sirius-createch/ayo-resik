<x-admin-button link="{{ route('admin.compro.contents.edit', ['konten' => $model->slug]) }}" face="warning" type="button" class="btn btn-sm"><i class='bx bx-edit align-middle me-1'></i>Ubah</x-admin-button>
<button type="button" class="btn btn-sm btn-danger btn-delete" data-id="{{ $model->id }}" data-name="{{ $model->title }}"><i class='bx bxs-trash align-middle me-1'></i>Hapus</button>
