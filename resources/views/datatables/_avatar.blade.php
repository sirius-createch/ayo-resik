<a href="{{ $model->avatar_link }}" data-fancybox style="cursor: zoom-in">
    <img src="{{ $model->avatar_link }}" alt="Avatar {{ $model->name }}" class="rounded pull-up" style="object-fit: cover; width:25px; height: 25px;">
</a>
