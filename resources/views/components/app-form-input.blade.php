<div {!! $attributes !!}>

    <div class="input-style has-borders @if($icon) has-icon @endif validate-field input-style-active">
        @if($icon) <i class="{{ $icon }}"></i> @endif

        <input
            class="form-control @error($name) is-invalid @enderror"
            type="{{ $type ?? 'text' }}"
            id="{{ $id }}"
            name="{{ $name }}"
            placeholder="{{ $placeholder ?? '' }}"
            value="{{ $value ?? old($name) }}"
            @isset($required) required @endisset
            @isset($readonly) readonly @endisset
            @isset($disabled) disabled @endisset
            @isset($autofocus) autofocus @endisset
        />

        @isset($label) <label for="{{ $id }}" class="fw-bold" style="font-size: 1.1em">{{ $label }} @isset($required)*@endisset</label> @endisset
        @isset($subLabel) <em>{!! $subLabel !!}</em> @endisset
    </div>

    @error($name)
        <div class="invalid-feedback d-block">
            {{ $message }}
        </div>
    @enderror

</div>
