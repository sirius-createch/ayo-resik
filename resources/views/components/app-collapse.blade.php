<div {!! $attributes->merge(['class' => "list-group list-custom-small list-icon-0 $color ps-3 pe-4"]) !!}>
    <a data-bs-toggle="collapse" class="no-effect collapsed" href="#{{ $id }}" aria-expanded="false">
        <span class="font-14 color-white">{{ $title }}</span>
        <i class="fa fa-angle-down color-white"></i>
    </a>
</div>
<div class="px-3 py-2 collapse" id="{{ $id }}">
    {!! $slot !!}
</div>
