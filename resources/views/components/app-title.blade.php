<div {!! $attributes->merge(['class' => '']) !!}>
    <h2 class="d-flex justify-content-between align-items-center app-subtitle border-bottom">
        <div>
            {!! $slot !!}
        </div>
        <div>
            {!! $actions !!}
        </div>
    </h2>
</div>
