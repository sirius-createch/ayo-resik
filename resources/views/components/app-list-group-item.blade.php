<a href="{{ $link }}" {!! $attributes !!}>
    @isset($icon) <i class="font-14 {{ $icon }} rounded-xl shadow-xl {{ $iconColor }}"></i> @endisset
    <span>{{ $title }}</span>
    @isset($subtitle) <strong>{{ $subtitle }}</strong> @endisset
    @isset($action) {!! $action !!} @else <i class="fa fa-angle-right"></i> @endisset
</a>
