<div id="{{ $id }}" class="menu menu-box-bottom rounded-m" data-menu-height="{{ $height ?? 365 }}" data-menu-effect="menu-over">
    <div class="menu-title">
        @isset($subtitle) <p class="color-green-dark">{{ $subtitle }}</p> @endisset
        <h1 class="font-24">{{ $title }}</h1>
        <a href="#" class="close-menu"><i class="fa fa-times-circle"></i></a>
    </div>

    {!! $slot !!}
</div>
