@isset ($link)
    <a href="{{ $link }}" {{ $attributes->merge(['class' => "btn btn-$size rounded-xs text-uppercase font-700 shadow-s $color"]) }}>
        @isset ($icon)
            <i class="{{ $icon }} me-1"></i>
        @endisset
        {{ $slot }}
    </a>
@else
    <button type="{{ $type ?? 'button' }}" {{ $attributes->merge(['class' => "btn btn-$size rounded-xs text-uppercase font-700 shadow-s $color"]) }} {{ $attributes->merge(['onclick' => '']) }}>
        @isset ($icon)
            <i class="{{ $icon }} me-1 align-middle"></i>
        @endisset
        {{ $slot }}
    </button>
@endisset
