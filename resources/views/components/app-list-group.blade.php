<div {!! $attributes->merge(['class' => "list-group list-custom-$size"]) !!}>
    {!! $slot !!}
</div>
