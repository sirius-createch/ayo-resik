<div class="my-3">
    <div class="@if (!$listGroup) mx-3 @endif">
        <div class="row compact mb-3">
            <div class="col-2 mb-1">
                <div class="input-style has-borders no-icon mb-4">
                    <select id="{{ $id }}-limit">
                        @foreach ($limits as $limit)
                            <option value="{{ $limit }}">{{ $limit }}</option>
                        @endforeach
                    </select>
                    <span><i class="fa-solid fa-chevron-down"></i></span>
                </div>
            </div>
            <div class="col-10 mb-1">
                <div class="d-flex justify-content-berween align-items-center gap-1">
                    <div class="input-style has-borders no-icon mb-4 w-100">
                        <select id="{{ $id }}-order">
                            @foreach ($orders as $key => $order)
                                <option value="{{ $key }}">{{ $order }}</option>
                            @endforeach
                        </select>
                        <em class="me-4">Urut Berdasarkan</em>
                        <span><i class="fa-solid fa-chevron-down"></i></span>
                    </div>
                    <div>
                        @php
                            $sortIcon = ($sort == 'asc') ? 'fa-arrow-down-a-z' : 'fa-arrow-up-a-z';
                        @endphp
                        <x-app-button id="btn-{{ $id }}-sort" color="border-gray color-gray bg-white" class="rounded-s"><i class="fa-solid {{ $sortIcon }} font-20"></i></x-app-button>
                        <input type="hidden" id="{{ $id }}-sort" value="{{ $sort }}">
                    </div>
                </div>

            </div>
            <div class="col-12">
                <x-app-form-input id="{{ $id }}-search" name="" placeholder="Cari ..." >
                    @slot('subLabel')
                        <i class="fa-solid fa-search"></i>
                    @endslot
                </x-app-form-input>
            </div>
        </div>
    </div>

    @if ($listGroup)
        <x-app-list-group size="{{ $listGroupSize }}" id="{{ $id }}-container">

        </x-app-list-group>
    @else
        <div id="{{ $id }}-container">

        </div>
    @endif

    <nav>
        <ul class="pagination justify-content-center @if ($listGroup) mt-4 @endif" id="{{ $id }}-nav">

        </ul>
    </nav>

    <div class="d-none" id="{{ $id }}-skeleton">
        <div class="loading-skeleton @if (!$listGroup) mx-3 @endif">
            @isset ($skeleton) {!! $skeleton !!} @endisset
        </div>
    </div>

    <div class="d-none" id="{{ $id }}-template">
        {!! $slot !!}
    </div>
</div>

@push('js')
    <script>
        function {{ $id }}Ready() {
            {{ $id }}Reload(1);

            $('#{{ $id }}-limit, #{{ $id }}-order, #{{ $id }}-sort, #{{ $id }}-search').change(function() {
                {{ $id }}Reload(1);
            })

            $("#btn-{{ $id }}-sort").click(function () {
                let input = $("#{{ $id }}-sort")
                if (input.val() == 'asc') {
                    input.val('desc').trigger('change')
                    $(this).html(`<i class="fa-solid fa-arrow-up-a-z font-20"></i>`)
                }
                else {
                    input.val('asc').trigger('change')
                    $(this).html(`<i class="fa-solid fa-arrow-down-a-z font-20"></i>`)
                }
            })

            $(document).on('click', '.{{ $id }}-page', function() {
                if ($(this).data('page') !== '') {
                    {{ $id }}Reload($(this).data('page'));
                }
            })
        }

        function {{ $id }}Reload(page) {

            let skeleton = $("#{{ $id }}-skeleton").html();
            let template = $("#{{ $id }}-template").html();

            $('#{{ $id }}-container').html(``);

            for (let index = 0; index < parseInt($("#{{ $id }}-limit").val()); index++) {
                $('#{{ $id }}-container').append(skeleton);
            }

            $('#{{ $id }}-nav').html(``);

            let data = new FormData();
            data.append('_method', 'post')
            data.append('_token', '{{ csrf_token() }}')
            data.append('search', $("#{{ $id }}-search").val())
            data.append('order', $("#{{ $id }}-order").val())
            data.append('sort', $("#{{ $id }}-sort").val())
            data.append('limit', $("#{{ $id }}-limit").val())
            data.append('page', page)

            ajaxPost("{{ $route }}", data, null, function (result) {
                $('#{{ $id }}-container').html(``);

                try {
                    result = result.data ?? null;

                    if (result != null && result.data.length > 0) {
                        $.each(result.data, function(index, data) {
                            let convertedTemplate = template;
                            @foreach ($data as $datum)
                                convertedTemplate = convertedTemplate.replaceAll('[[{{ $datum }}]]', data.{{ $datum }}).replaceAll('%5B%5B{{ $datum }}%5D%5D', data.{{ $datum }})
                            @endforeach
                            $('#{{ $id }}-container').append(convertedTemplate);
                        })

                        let pagination = {
                            'prev_page_url': '<i class="fa-solid fa-angle-left me-2"></i>Kembali',
                            'current_page': '',
                            'next_page_url' : 'Lanjut<i class="fa-solid fa-angle-right ms-2"></i>'
                        }

                        $.each(pagination, function (index, label) {
                            let active = '{{ $id }}-page';
                            let color = 'color-black';
                            let page = result[index]

                            if (page == null) active = 'opacity-20';

                            if (!$.isNumeric(page) && page != null) {
                                page = parseInt(page.replace(result.path + '?page=', ''))
                            }

                            if (label == '') {
                                label = page
                                color = 'bg-highlight';
                                active = 'active';
                            }

                            $('#{{ $id }}-nav').append(`
                                <li class="page-item ` + active + `" data-page="` + page + `"><div class="page-link rounded-xs ` + color + ` shadow-l border-0">` + label + `</div></li>
                            `);
                        })
                    }
                    else {
                        $('#{{ $id }}-container').html(`
                            <div class="@if (!$listGroup) mx-3 @endif">
                                <x-app-chip icon="fa-solid fa-times" color="red" class="w-100">{{ $name }} yang Anda cari tidak ditemukan.</x-app-chip>
                            </div>
                        `);
                    }
                } catch (e) {
                    console.log(e);
                }
            }, null, false)
        }
    </script>
@endpush
