<form
    {!! $attributes !!}
    action="{{ $action }}"
    method="{{ $method != 'get' ? 'post' : 'get' }}"
    @isset($sendFile) enctype="multipart/form-data" @endisset
    >

    @if ($method != 'get')
        @method($method)
        @csrf
    @endif

    {!! $slot !!}

    @isset($submit)
        <x-app-button type="submit" color="{{ $submitColor ?? 'bg-highlight' }}" size="{{ $submitSize ?? 'm' }}" icon="{{ $submitIcon ?? 'fa-solid fa-paper-plane' }}" class="mt-3 {{ $submitWide ? 'btn-full w-100' : '' }}" >{{ $submit }}</x-app-button>
    @endisset
</form>
