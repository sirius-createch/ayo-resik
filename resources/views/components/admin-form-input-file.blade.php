<div {{ $attributes }}>
    @if ($label)
        <x-admin-form-label for="{{ $id ?? '' }}" required="{{ $required ?? 0 }}">
            {{ $label }}
            @if ($labelBadge)
                @slot('badge')
                    {!! $labelBadge !!}
                @endslot
            @endif
        </x-admin-form-label>
    @endif

    <div class="dropzone" id="{{ $id }}"></div>
</div>

@push('js')
    <script>
        let dz_{{ $id }} = null
            $(document).ready(function () {
                var accepted = "{{ $accepted }}"
                if (accepted == "") accepted = null
                dz_{{ $id }} = dropzoneInit('#{{ $id }}', '#{{ $formId }}', "{{ route('uploader.dropzone') }}", "{{ csrf_token() }}", "{{ $name }}", {{ $totalFileMax }}, accepted, dropzoneTranslate("{{ $label }}"))

                @isset ($value)
                    @if ($value !== '')
                        var images = [{name : "{!! $value !!}", size : "{{ $valueSize }}"}]
                        dropzonePreview(dz_{{ $id }}, images, '{{ asset("images/avatar") }}', '#{{ $formId }}', '{{ $name }}')
                    @endif
                @endisset
            })
    </script>
@endpush
