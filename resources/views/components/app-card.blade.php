<div {!! $attributes->merge(['class' => 'card card-style']) !!}>
    @if ($banner)
        <div class="card mb-0 bg-6" data-card-height="150" style="height: 150px; background-image: url('{{ $banner }}')"></div>
    @endif
    <div class="content @if ($banner) mt-3 @endif">
        @isset($subtitle) <p class="mb-n1 font-600">{{ $subtitle }}</p> @endisset
        @isset($title) <h1>{{ $title }}</h1> @endisset
        {!! $slot !!}
    </div>
</div>
