<div class="tab-controls tabs-small tabs-rounded" data-highlight="bg-green-dark">
    @foreach ($tabs as $tab)
        @if ($loop->first)
            <a href="#" class="no-effect bg-green-dark no-click" data-active="" data-bs-toggle="collapse" data-bs-target="{{ $tab['target'] }}">{!! $tab['title'] !!}</a>
        @else
            <a href="#" class="no-effect" data-bs-toggle="collapse" data-bs-target="{{ $tab['target'] }}">{!! $tab['title'] !!}</a>
        @endif
    @endforeach
</div>

<div class="clearfix mb-3"></div>
