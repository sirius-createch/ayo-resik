<div data-bs-parent="{{ $parent }}" id="{{ $id }}" {!! $attributes->merge(['class' => "collapse"]) !!}>
    {!! $slot !!}
</div>
