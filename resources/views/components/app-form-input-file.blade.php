<div {{ $attributes }}>
    <div class="input-style has-borders validate-field input-style-active">
        @isset($label) <label for="{{ $id }}" class="color-highlight">{{ $label }} @isset($required)*@endisset</label> @endisset
        <div class="dropzone" id="{{ $id }}"></div>
    </div>
</div>

@push('js')
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            let dz_{{ $id }} = null
            $(document).ready(function () {
                var accepted = "{{ $accepted }}"
                if (accepted == "") accepted = null
                dz_{{ $id }} = dropzoneInit('#{{ $id }}', '#{{ $formId }}', "{{ route('uploader.dropzone') }}", "{{ csrf_token() }}", "{{ $name }}", {{ $totalFileMax }}, accepted, dropzoneTranslate("{{ $label }}"))

                @isset ($value)
                    @if ($value !== '')
                        var images = [{name : "{!! $value !!}", size : "{{ $valueSize }}"}]
                        dropzonePreview(dz_{{ $id }}, images, '{{ asset("images/avatar") }}', '#{{ $formId }}', '{{ $name }}')
                    @endif
                @endisset
            })

        });
    </script>
@endpush
