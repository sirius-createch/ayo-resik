<div {{ $attributes->merge(['class' => 'form-check']) }}>
    <input class="form-check-input" type="checkbox" value="{{ $value ?? '' }}" id="{{ $id }}" name="{{ $name ?? '' }}" @isset($checked) checked @endisset @isset($checked) required @endisset />
    <label class="form-check-label" for="{{ $id }}">
        {!! $slot !!}
    </label>
</div>
