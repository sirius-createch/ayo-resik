<div {!! $attributes->merge(['class' => "chip chip-$size bg-$color-dark"]) !!}>
    @isset($icon) <i class="{{ $icon }} color-white bg-{{ $color }}-light"></i> @endisset
    <span class="color-white">{!! $slot !!}</span>
</div>
