<?php

return [
    'name' => 'Ayoresik',
    'manifest' => [
        'name' => env('APP_NAME', 'Ayoresik'),
        'short_name' => 'Ayoresik',
        'start_url' => '/app/',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation' => 'any',
        'status_bar' => 'black',
        'icons' => [
            '72x72' => [
                'path' => '/images/assets/favicons/icon-72x72.png',
                'purpose' => 'any',
            ],
            '96x96' => [
                'path' => '/images/assets/favicons/icon-96x96.png',
                'purpose' => 'any',
            ],
            '128x128' => [
                'path' => '/images/assets/favicons/icon-128x128.png',
                'purpose' => 'any',
            ],
            '144x144' => [
                'path' => '/images/assets/favicons/icon-144x144.png',
                'purpose' => 'any',
            ],
            '152x152' => [
                'path' => '/images/assets/favicons/icon-152x152.png',
                'purpose' => 'any',
            ],
            '192x192' => [
                'path' => '/images/assets/favicons/icon-192x192.png',
                'purpose' => 'any',
            ],
            '384x384' => [
                'path' => '/images/assets/favicons/icon-384x384.png',
                'purpose' => 'any',
            ],
            '512x512' => [
                'path' => '/images/assets/favicons/icon-512x512.png',
                'purpose' => 'any',
            ],
        ],
        'splash' => [
            '640x1136' => '/images/assets/splashes/splash-640x1136.png',
            '750x1334' => '/images/assets/splashes/splash-750x1334.png',
            '828x1792' => '/images/assets/splashes/splash-828x1792.png',
            '1125x2436' => '/images/assets/splashes/splash-1125x2436.png',
            '1242x2208' => '/images/assets/splashes/splash-1242x2208.png',
            '1242x2688' => '/images/assets/splashes/splash-1242x2688.png',
            '1536x2048' => '/images/assets/splashes/splash-1536x2048.png',
            '1668x2224' => '/images/assets/splashes/splash-1668x2224.png',
            '1668x2388' => '/images/assets/splashes/splash-1668x2388.png',
            '2048x2732' => '/images/assets/splashes/splash-2048x2732.png',
        ],
        'shortcuts' => [
            [
                'name' => 'Homepage Customer',
                'description' => 'Beranda Customer',
                'url' => '/app/customer',
                'icons' => [
                    'src' => '/images/assets/favicons/icon-72x72.png',
                    'purpose' => 'any',
                ],
            ],
            [
                'name' => 'Homepage Partner',
                'description' => 'Beranda Partner',
                'url' => '/app/partner',
            ],
        ],
        'custom' => [],
    ],
];
