var staticCacheName = "ayoresik" + new Date().getDate();
var filesToCache = [
    '/app/offline',
    '/themes/app/css/custom.css',
    '/themes/app/css/style.css',
    '/themes/app/js/custom.js',
    '/themes/app/fonts/css/all.min.css',
    '/images/assets/offline.png',
    '/images/assets/favicons/icon-72x72.png',
    '/images/assets/favicons/icon-96x96.png',
    '/images/assets/favicons/icon-128x128.png',
    '/images/assets/favicons/icon-144x144.png',
    '/images/assets/favicons/icon-152x152.png',
    '/images/assets/favicons/icon-192x192.png',
    '/images/assets/favicons/icon-384x384.png',
    '/images/assets/favicons/icon-512x512.png',
];

var appLogging = true;

// Cache on install
self.addEventListener("install", event => {
    event.waitUntil(
		caches.open(staticCacheName)
		.then(function(cache) {
			return cache.addAll(filesToCache);
		}).catch(function(error) {
			if(appLogging){console.log('Service Worker Cache: Error Check REQUIRED_FILES array in serviceworker.js - files are missing or path to files is incorrectly written -  ' + error);}
		})
		.then(function() {
            if(appLogging){console.log('Service Worker: Cache is OK');}
			return self.skipWaiting();
		})
	);
	if(appLogging){console.log('Service Worker: Installed');}
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("ayoresik-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Fetch page either from network or cache (network priority)
self.addEventListener('fetch', (event) => {
    // Check if this is a navigation request
    if (event.request.mode === 'navigate') {
        // Open the cache
        event.respondWith(caches.open(staticCacheName).then((cache) => {
            // console.log('cache', cache);
            // Go to the network first
            return fetch(event.request).then((fetchedResponse) => {
                // console.log('fetchedResponse', fetchedResponse);
                if (event.request.method == 'GET') {
                    cache.put(event.request, fetchedResponse.clone());
                }
                return fetchedResponse;
            }).catch(() => {
                // If the network is unavailable
                return cache.match(event.request).then(function(response) {
                    if (response !== undefined) {
                        // Show cache page
                        // console.log('response', response);
                        return response;
                    } else {
                        // Or show offline page
                        return caches.match("/app/offline").then(function(responseOffline) {
                            // console.log('responseOffline', responseOffline);
                            return responseOffline;
                        });
                    }
                });
            });
        }));
    } else {
        return;
    }
});

importScripts("https://www.gstatic.com/firebasejs/9.9.2/firebase-app-compat.js")
importScripts("https://www.gstatic.com/firebasejs/9.9.2/firebase-messaging-compat.js")

const firebaseConfig = {
    apiKey: "AIzaSyCPApubQR96XiGyCWbLWta4A9FJ3gLZuHE",
    authDomain: "ayo-resik.firebaseapp.com",
    projectId: "ayo-resik",
    storageBucket: "ayo-resik.appspot.com",
    messagingSenderId: "272571463716",
    appId: "1:272571463716:web:f6161827c1dd7a02a180b1",
    measurementId: "G-41E0PLQWWF"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// show arbitrary notification from payload data (use firebase 'data' notification instead of 'message' notification)
const messaging = firebase.messaging();
messaging.onBackgroundMessage(function(payload) {
    return self.registration.showNotification(payload.data.title, JSON.parse(payload.data.options));
});

self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    var urlToBeOpened = null;
    if(typeof event.notification.data.url == 'string') urlToBeOpened = event.notification.data.url;

    if(event.notification.data.actionUrl) {
        if(typeof event.notification.data.actionUrl[event.action] == 'string') {
            urlToBeOpened = event.notification.data.actionUrl[event.action];
        }
    }

    if(urlToBeOpened) {
        event.waitUntil(
            clients.openWindow(urlToBeOpened)
        );
    }
})
