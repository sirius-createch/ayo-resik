<?php

use App\Http\Controllers\Api\CustomerApiController;
use App\Http\Controllers\Api\InvoiceApiController;
use App\Http\Controllers\Api\PartnerApiController;
use App\Http\Controllers\Api\ProfileApiController;
use App\Http\Controllers\Api\ReportApiController;
use App\Http\Controllers\Api\WithdrawalApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::get('profile', [ProfileApiController::class, 'index'])->name('profile.index');

// Route::post('customer/nearby-partner', [CustomerApiController::class, 'nearby_partner'])->name('customer.nearby_partner');
// Route::get('customer/list-product', [CustomerApiController::class, 'product'])->name('customer.product');
// Route::post('customer/change-partner', [CustomerApiController::class, 'change_partner'])->name('customer.change_partner');

// Route::get('partner/list-change-partner', [PartnerApiController::class, 'change_partner_index'])->name('partner.change_partner_index');
// Route::put('partner/{customer_id}/confirm-change-partner', [PartnerApiController::class, 'confirm_change_partner'])->name('partner.confirm_change_partner');

// Route::put('profile/{id}/update', [ProfileApiController::class, 'update'])->name('profile.update');
// Route::post('profile/customers', [ProfileApiController::class, 'customers'])->name('profile.customers');
// Route::post('profile/notification', [ProfileApiController::class, 'notification'])->name('profile.notification');

// Route::put('withdrawal/{id}/confirm', [WithdrawalApiController::class, 'confirm'])->name('profile.confirm');
// Route::apiResource('withdrawal', WithdrawalApiController::class)->names('withdrawal');
// Route::apiResource('invoice', InvoiceApiController::class)->names('invoice');

// Route::post('report/expense', [ReportApiController::class, 'expense'])->name('report.expense');
// Route::post('report/weight', [ReportApiController::class, 'weight'])->name('report.weight');
