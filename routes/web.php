<?php

use App\Http\Controllers\AppNotificationController;
use App\Http\Controllers\AppProfileController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Compro\AboutController;
use App\Http\Controllers\Compro\ArticleController as ComproArticleController;
use App\Http\Controllers\Compro\ContentController as ComproContentController;
use App\Http\Controllers\Compro\PageController as ComproPageController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\Customer\InvoiceController as CustomerInvoiceController;
use App\Http\Controllers\Customer\PartnerController as CustomerPartnerController;
use App\Http\Controllers\Customer\ProductController as CustomerProductController;
use App\Http\Controllers\Customer\WithdrawalController as CustomerWithdrawalController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Partner\CustomerController as PartnerCustomerController;
use App\Http\Controllers\Partner\InvoiceController as PartnerInvoiceController;
use App\Http\Controllers\Partner\ProductController as PartnerProductController;
use App\Http\Controllers\Partner\ReportController as PartnerReportController;
use App\Http\Controllers\Partner\WithdrawalController as PartnerWithdrawalController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UploaderController;
use App\Http\Controllers\WithdrawalController;
use App\Http\Controllers\SliderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Override Default Auth
Route::get('/login', fn () => redirect(route('app.login')))->name('login');
Route::get('/masuk', fn () => redirect(route('app.login')));
Route::get('/register', fn () => redirect(route('app.register')))->name('register');
Route::get('/daftar', fn () => redirect(route('app.register')));

Route::get('/atur-ulang-kata-sandi/{token}', [AuthController::class, 'resetPasswordView'])->name('password.reset'); //Helper route for fortify
// Admin
Route::group(['prefix' => '/admin', 'as' => 'admin.'], function () {
    Route::get('/masuk', [AuthController::class, 'adminLoginView'])->name('login');

    Route::group(['middleware' => ['auth', 'is_admin']], function () {
        Route::get('/', [DashboardController::class, 'admin'])->name('home');

        Route::group(['prefix' => '/profil', 'as' => 'profile.'], function () {
            Route::get('/', [ProfileController::class, 'index'])->name('index');
            Route::group(['prefix' => '/update', 'as' => 'update.'], function () {
                Route::put('/general', [ProfileController::class, 'updateGeneral'])->name('general');
                Route::put('/password', [ProfileController::class, 'updatePassword'])->name('password');
                Route::put('/avatar', [ProfileController::class, 'updateAvatar'])->name('avatar');
            });
        });

        Route::group(['prefix' => '/master', 'as' => 'master.'], function () {
            Route::get('/customer/search', [CustomerController::class, 'search'])->name('customers.search');
            Route::get('/partner/search', [PartnerController::class, 'search'])->name('partners.search');

            Route::get('customer/download', [CustomerController::class, 'download'])->name('customers.download');
            Route::apiResource('customer', CustomerController::class)->names('customers');
            Route::get('customer/password/generate', [CustomerController::class, 'generatePassword'])->name('customers.password.generate');
            Route::put('customer/password/{customer}', [CustomerController::class, 'updatePassword'])->name('customers.password');

            Route::apiResource('partner', PartnerController::class)->names('partners');
            Route::apiResource('produk', ProductController::class)->names('products');
            Route::apiResource('metode-pembayaran', PaymentMethodController::class)->names('payments');
        });

        Route::group(['prefix' => '/layanan', 'as' => 'services.'], function () {
            Route::apiResource('transaksi', InvoiceController::class)->names('transactions')->only(['index', 'show']);
            Route::apiResource('penarikan-saldo', WithdrawalController::class)->names('withdrawals')->only(['index', 'show']);
        });

        Route::group(['prefix' => '/laporan', 'as' => 'reports.'], function () {
            Route::any('/pengeluaran', [ReportController::class, 'expense'])->name('expenses');
            Route::any('/produk', [ReportController::class, 'product'])->name('products');
        });

        Route::group(['prefix' => '/company-profile', 'as' => 'compro.'], function () {
            Route::group(['prefix' => '/pengaturan', 'as' => 'settings.'], function () {
                Route::get('/', [SettingController::class, 'index'])->name('index');
                Route::group(['prefix' => '/update', 'as' => 'update.'], function () {
                    Route::put('/general', [SettingController::class, 'updateGeneral'])->name('general');
                    Route::put('/total', [SettingController::class, 'updateTotal'])->name('total');
                });
            });

            Route::apiResource('slider', SliderController::class)->names('sliders');
            Route::resource('/konten', ContentController::class)->names('contents')->except('show');
            Route::resource('/artikel', ArticleController::class)->names('articles')->except('show');

            Route::group(['prefix' => '/halaman', 'as' => 'pages.'], function () {
                Route::get('/{page}', [PageController::class, 'edit'])->name('edit');
                Route::put('/{page}', [PageController::class, 'update'])->name('update');
            });
        });
    });
});

// App
Route::group(['prefix' => '/app', 'as' => 'app.'], function () {
    Route::get('/', [DashboardController::class, 'app'])->name('home');
    Route::get('/masuk', [AuthController::class, 'appLoginView'])->name('login');
    Route::get('/daftar', [AuthController::class, 'appRegisterView'])->name('register');
    Route::get('/verifikasi', [AuthController::class, 'appVerifyView'])->name('verify');
    Route::get('/lupa-kata-sandi', [AuthController::class, 'forgotPasswordView'])->name('forgot-password');
    Route::post('/lupa-kata-sandi', [AuthController::class, 'sendRequestPasswordReset'])->name('forgot-password');
    Route::get('/atur-ulang-kata-sandi/{token}', [AuthController::class, 'resetPasswordView'])->name('reset-password');
    Route::put('/atur-ulang-kata-sandi', [AuthController::class, 'resetPassword'])->name('reset-password');

    // Route::get('/reset-password/{token}', function (string $token) {
    //     return view('auth.reset-password', ['token' => $token]);
    // })->middleware('guest')->name('password.reset');

    Route::post('/simpan-device-token', [AuthController::class, 'storeDeviceToken'])->name('store-device-token');
    Route::post('/masuk-otomatis', [AuthController::class, 'sessionLogin'])->name('auto-login');
    Route::view('/offline', 'laravelpwa::offline')->name('offline');

    Route::group(['prefix' => '/pemberitahuan', 'as' => 'notification.'], function () {
        Route::match(['get', 'post'], '/', [AppNotificationController::class, 'index'])->name('index');
        Route::get('/ke/{notification}', [AppNotificationController::class, 'goto'])->name('goto');
    });

    Route::group(['prefix' => '/customer', 'as' => 'customer.', 'middleware' => ['auth', 'is_customer']], function () {
        Route::group(['prefix' => '/konfirmasi-alamat', 'as' => 'address.'], function () {
            Route::get('/', [AppProfileController::class, 'confirmAddress'])->name('confirm');
            Route::put('/', [AppProfileController::class, 'updateAddress'])->name('update');
        });

        Route::group(['middleware' => 'must_fill_address'], function () {
            Route::get('/', [DashboardController::class, 'customer'])->name('home');

            Route::group(['prefix' => '/profil', 'as' => 'profile.'], function () {
                Route::get('/', [AppProfileController::class, 'index'])->name('index');
                Route::get('/ubah', [AppProfileController::class, 'edit'])->name('edit');
                Route::get('/ubah/kata-sandi', [AppProfileController::class, 'editPassword'])->name('password');
                Route::put('/ubah', [AppProfileController::class, 'update'])->name('update');
                Route::put('/ubah/kata-sandi', [AppProfileController::class, 'updatePassword'])->name('password');
            });

            Route::group(['prefix' => '/partner', 'as' => 'partner.'], function () {
                Route::get('/', [CustomerPartnerController::class, 'index'])->name('index');
                Route::post('/nearby', [CustomerPartnerController::class, 'nearby'])->name('nearby');
                Route::post('/change', [CustomerPartnerController::class, 'change'])->name('change');
            });

            Route::group(['middleware' => 'must_connected_with_partner'], function () {
                Route::group(['prefix' => '/produk', 'as' => 'products.'], function () {
                    Route::get('/', [CustomerProductController::class, 'index'])->name('index');
                    Route::post('/search', [CustomerProductController::class, 'search'])->name('search');
                });

                Route::group(['prefix' => '/setor-sampah', 'as' => 'orders.'], function () {
                    Route::get('/', [CustomerInvoiceController::class, 'create'])->name('create');
                    Route::post('/', [CustomerInvoiceController::class, 'store'])->name('store');
                });

                Route::group(['prefix' => '/pesanan', 'as' => 'orders.'], function () {
                    Route::match(['get', 'post'], '/', [CustomerInvoiceController::class, 'index'])->name('index');
                    Route::get('/{invoice}', [CustomerInvoiceController::class, 'show'])->name('show');
                    Route::put('/{invoice}', [CustomerInvoiceController::class, 'update'])->name('update');
                });

                Route::group(['prefix' => '/tarik-tunai', 'as' => 'withdrawals.'], function () {
                    Route::get('/', [CustomerWithdrawalController::class, 'create'])->name('create');
                    Route::post('/', [CustomerWithdrawalController::class, 'store'])->name('store');
                    Route::match(['get', 'post'], '/riwayat', [CustomerWithdrawalController::class, 'index'])->name('index');
                    Route::get('/riwayat/{withdrawal}', [CustomerWithdrawalController::class, 'show'])->name('show');
                    Route::put('/riwayat/{withdrawal}', [CustomerWithdrawalController::class, 'update'])->name('update');
                });
            });

            Route::view('/tutorial', 'app.customer.tutorial')->name('tutorial');
        });
    });

    Route::group(['prefix' => '/partner', 'as' => 'partner.', 'middleware' => ['auth', 'is_partner']], function () {
        Route::get('/', [DashboardController::class, 'partner'])->name('home');
        Route::post('/products/search', PartnerProductController::class)->name('products.search');

        Route::group(['prefix' => '/pesanan', 'as' => 'orders.'], function () {
            Route::match(['get', 'post'], '/', [PartnerInvoiceController::class, 'index'])->name('index');
            Route::get('/{invoice}', [PartnerInvoiceController::class, 'show'])->name('show');
            Route::put('/{invoice}', [PartnerInvoiceController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => '/penarikan', 'as' => 'withdrawals.'], function () {
            Route::match(['get', 'post'], '/', [PartnerWithdrawalController::class, 'index'])->name('index');
            Route::get('/{withdrawal}', [PartnerWithdrawalController::class, 'show'])->name('show');
            Route::put('/{withdrawal}', [PartnerWithdrawalController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => '/laporan', 'as' => 'reports.'], function () {
            Route::get('/', [PartnerReportController::class, 'index'])->name('index');
            Route::post('/nominal', [PartnerReportController::class, 'nominal'])->name('nominal');
            Route::post('/berat', [PartnerReportController::class, 'weights'])->name('weights');
        });

        Route::group(['prefix' => '/profil', 'as' => 'profile.'], function () {
            Route::get('/', [AppProfileController::class, 'index'])->name('index');

            Route::group(['prefix' => '/ubah', 'as' => 'edit.'], function () {
                Route::get('/informasi', [AppProfileController::class, 'edit'])->name('information');
                Route::get('/pengaturan', [AppProfileController::class, 'setting'])->name('setting');
                Route::get('/kata-sandi', [AppProfileController::class, 'editPassword'])->name('password');
                Route::put('/informasi', [AppProfileController::class, 'update'])->name('information');
                Route::put('/pengaturan', [AppProfileController::class, 'set'])->name('setting');
                Route::put('/kata-sandi', [AppProfileController::class, 'updatePassword'])->name('password');
            });

            Route::group(['prefix' => '/customer', 'as' => 'customers.'], function () {
                Route::match(['get', 'post'], '/', [PartnerCustomerController::class, 'index'])->name('index');
                Route::get('/{customer}', [PartnerCustomerController::class, 'show'])->name('show');
                Route::put('/{customer}', [PartnerCustomerController::class, 'update'])->name('update');
            });
        });
    });
});

// Compro

Route::group(['prefix' => '/', 'as' => 'compro.'], function () {
    Route::get('/', [DashboardController::class, 'compro'])->name('welcome');

    Route::view('/tentang-kami', 'compro.about')->name('about');
    Route::view('/layanan', 'compro.services')->name('services');
    Route::view('/mitra', 'compro.partner')->name('partner');
    Route::view('/blog', 'compro.blog')->name('blog');

    Route::view('/pertanyaan', 'compro.faq')->name('faq');
    Route::view('/kontak-kami', 'compro.contact')->name('contact');
    Route::resource('/konten', ComproContentController::class)->names('contents')->only(['index', 'show']);
    Route::resource('/artikel', ComproArticleController::class)->names('articles')->only(['index', 'show']);
    Route::get('/{page}', ComproPageController::class)->name('page');
});

// Uploader
Route::group(['prefix' => '/uploader', 'as' => 'uploader.'], function () {
    Route::post('/dropzone', [UploaderController::class, 'dropzone'])->name('dropzone');
    Route::post('/tinymce', [UploaderController::class, 'tinymce'])->name('tinymce');
});
