<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_product', function (Blueprint $table) {
            $table->foreignId('invoice_id')->constrained();
            $table->foreignId('product_id')->constrained();
            $table->primary(['invoice_id', 'product_id']);
            $table->index(['invoice_id', 'product_id']);
            $table->unsignedDouble('weight')->comment('in kilograms');
            $table->unsignedDouble('weight_real')->nullable()->comment('in kilograms');
            $table->unsignedInteger('price')->comment('per kilograms');
            $table->string('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_product');
    }
};
