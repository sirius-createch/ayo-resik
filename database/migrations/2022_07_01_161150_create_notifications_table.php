<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('detail');
            $table->string('link');
            $table->string('icon')->default('fa-solid fa-bell');
            $table->unsignedBigInteger('broadcasted_to');
            $table->timestamp('broadcasted_at')->nullable();
            $table->timestamp('readed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('broadcasted_to')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
};
