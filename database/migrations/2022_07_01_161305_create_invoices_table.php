<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('code', 25);
            $table->unsignedBigInteger('partner_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedDecimal('total_bill');
            $table->unsignedDouble('total_weight')->comment('in kilograms');
            $table->enum('day_taken', ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'])->nullable();
            $table->text('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->foreignId('payment_method_id')->constrained();
            $table->enum('current_status', ['requested', 'seen', 'denied', 'confirmed', 'verified', 'done', 'canceled']);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('partner_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
