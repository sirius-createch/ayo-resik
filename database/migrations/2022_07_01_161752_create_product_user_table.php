<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_user', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained();
            $table->unsignedBigInteger('partner_id');
            $table->primary(['product_id', 'partner_id']);
            $table->index(['product_id', 'partner_id']);
            $table->unsignedBigInteger('price')->comment('per kilograms');
            $table->timestamps();

            $table->foreign('partner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_user');
    }
};
