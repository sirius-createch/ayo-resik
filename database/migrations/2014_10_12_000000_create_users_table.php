<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('phone', 20)->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->unsignedInteger('avatar_size')->nullable();
            $table->enum('role', ['admin', 'partner', 'customer']);
            $table->unsignedBigInteger('partner_id')->nullable();
            $table->unsignedBigInteger('balance')->default(0);
            $table->text('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->rememberToken();
            $table->string('device_token')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamp('partner_confirm_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('partner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
