<?php

namespace Database\Seeders;

use App\Models\Schedule;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = User::whereRole('partner')->pluck('id')->toArray();
        $schedules = [];
        foreach ($partners as $id) {
            $schedules[] = [
                'partner_id' => $id,
                'sunday' => mt_rand(0, 1),
                'monday' => mt_rand(0, 1),
                'tuesday' => mt_rand(0, 1),
                'wednesday' => mt_rand(0, 1),
                'thursday' => mt_rand(0, 1),
                'friday' => mt_rand(0, 1),
                'saturday' => mt_rand(0, 1),
            ];
        }
        Schedule::insert($schedules);
        DB::table('schedules')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
