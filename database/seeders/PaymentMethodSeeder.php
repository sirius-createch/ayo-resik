<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            ['name' => 'Tunai'],
            ['name' => 'Masuk Saldo'],
        ];

        PaymentMethod::insert($params);
        DB::table('payment_methods')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
