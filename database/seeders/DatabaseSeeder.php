<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Content;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Notification;
use App\Models\Page;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\Schedule;
use App\Models\Setting;
use App\Models\User;
use App\Models\Withdrawal;
use App\Models\WithdrawalStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        PaymentMethod::truncate();
        DB::table('product_user')->truncate();
        Product::truncate();
        Schedule::truncate();
        User::truncate();
        DB::table('invoice_product')->truncate();
        Invoice::truncate();
        InvoiceStatus::truncate();
        Withdrawal::truncate();
        WithdrawalStatus::truncate();
        Notification::truncate();
        Article::truncate();
        Content::truncate();
        Page::truncate();
        Setting::truncate();
        Schema::enableForeignKeyConstraints();
        $this->call([
            PaymentMethodSeeder::class,
            ProductSeeder::class,
            UserSeeder::class,
            ProductUserSeeder::class,
            ScheduleSeeder::class,
            InvoiceSeeder::class,
            InvoiceProductSeeder::class,
            InvoiceStatusSeeder::class,
            UpdateCustomerBalanceSeeder::class,
            WithdrawalSeeder::class,
            WithdrawalStatusSeeder::class,
            UpdatePartnerBalanceSeeder::class,
            NotificationSeeder::class,
            ArticleSeeder::class,
            ContentSeeder::class,
            PageSeeder::class,
            SettingSeeder::class,
        ]);
    }
}
