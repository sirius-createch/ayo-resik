<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProductUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::pluck('base_price', 'id');
        $partners = User::whereRole('partner')->get();
        foreach ($partners as $partner) {
            $params = [];
            foreach ($products as $id => $price) {
                $isIn = mt_rand(0, 1);
                if ($isIn) {
                    $params[$id] = ['price' => mt_rand($price, $price * 10)];
                }
            }
            if (count($params) > 0) {
                $partner->products()->sync($params);
            }
        }
    }
}
