<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name' => 'address',
            'value' => 'Universitas Surabaya, Kampus UBAYA Tenggilis, Kalirungkut, Rungkut, Surabaya.',
        ]);

        Setting::create([
            'name' => 'phone',
            'value' => '82XXXXXXXXX',
        ]);

        Setting::create([
            'name' => 'email',
            'value' => 'customer-service@ayoresik.com',
        ]);

        Setting::create([
            'name' => 'nominal-toggle',
            'value' => 0,
        ]);

        Setting::create([
            'name' => 'nominal-value',
            'value' => 100000000,
        ]);

        Setting::create([
            'name' => 'weight-toggle',
            'value' => 0,
        ]);

        Setting::create([
            'name' => 'weight-value',
            'value' => 100,
        ]);

        Setting::create([
            'name' => 'partner-toggle',
            'value' => 0,
        ]);

        Setting::create([
            'name' => 'partner-value',
            'value' => 20,
        ]);

        Setting::create([
            'name' => 'customer-toggle',
            'value' => 0,
        ]);

        Setting::create([
            'name' => 'customer-value',
            'value' => 1000,
        ]);
    }
}
