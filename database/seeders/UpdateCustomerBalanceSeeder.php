<?php

namespace Database\Seeders;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Database\Seeder;

class UpdateCustomerBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $invoices = Invoice::where('payment_method_id', 2)->get();

        $customers = [];
        $partners = [];
        foreach ($invoices as $invoice) {
            $customers[$invoice->customer_id] = ($customers[$invoice->customer_id] ?? 0) + $invoice->total_bill;
            $partners[$invoice->partner_id] = ($partners[$invoice->partner_id] ?? 0) + $invoice->total_bill;
        }

        foreach ($customers as $id => $balance) {
            User::find($id)->update(['balance' => $balance]);
        }

        foreach ($partners as $id => $balance) {
            User::find($id)->update(['balance' => $balance]);
        }
    }
}
