<?php

namespace Database\Seeders;

use App\Models\Invoice;
use App\Models\InvoiceStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $invoices = Invoice::all();

        $params = [];
        foreach ($invoices as $invoice) {
            $statuses = match ($invoice->current_status) {
                'requested' => ['requested'],
                'seen' => ['requested', 'seen'],
                'denied' => ['requested', 'seen', 'denied'],
                'confirmed' => ['requested', 'seen', 'confirmed'],
                'verified' => ['requested', 'seen', 'confirmed', 'verified'],
                'done' => ['requested', 'seen', 'confirmed', 'verified', 'done'],
                'canceled' => ['requested', 'canceled'],
            };

            foreach ($statuses as $status) {
                $params[] = [
                    'invoice_id' => $invoice->id,
                    'name' => $status,
                ];
            }
        }

        InvoiceStatus::insert($params);
        DB::table('invoice_statuses')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
