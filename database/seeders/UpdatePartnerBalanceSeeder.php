<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdatePartnerBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sums = User::select(['partner_id', DB::raw('SUM(balance) as balance')])->groupBy('partner_id')->whereNotNull('partner_id')->pluck('balance', 'partner_id');

        foreach ($sums as $id => $balance) {
            User::findOrFail($id)->update(['balance' => $balance]);
        }
    }
}
