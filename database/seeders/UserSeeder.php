<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'phone' => 123456789,
            'name' => 'Admin',
            'email' => 'root@example.com',
            'password' => Hash::make('password'),
            'role' => 'admin',
            'balance' => 0,
            'address' => 'Jalan Kaliwaru 1',
            'latitude' => '-7.2458',
            'longitude' => '112.7378',
            'verified_at' => now(),
        ]);

        User::create([
            'phone' => 0,
            'name' => 'Admin 2',
            'email' => 'admin@ayoresik.com',
            'password' => Hash::make('12345'),
            'role' => 'admin',
            'balance' => 0,
            'address' => 'Jalan Kaliwaru 1',
            'latitude' => '-7.2458',
            'longitude' => '112.7378',
            'verified_at' => now(),
        ]);

        User::create([
            'phone' => 85163529901,
            'name' => 'Code: Sirius',
            'email' => 'partner@example.com',
            'password' => Hash::make('password'),
            'role' => 'partner',
            'balance' => 0,
            'address' => 'UBAYA Tenggilis',
            'latitude' => '-7.3202339',
            'longitude' => '112.7650341',
            'verified_at' => now(),
        ]);

        User::create([
            'phone' => 82330495179,
            'name' => 'Fathul Husnan',
            'email' => 'customer@example.com',
            'password' => Hash::make('password'),
            'role' => 'customer',
            'balance' => 1000000,
            'address' => 'Jl. Tenggilis Mejoyo Selatan I No.20',
            'latitude' => '-7.3231711',
            'longitude' => '112.7582181',
            'partner_id' => 3,
            'verified_at' => now(),
            'partner_confirm_at' => now(),
        ]);

        User::factory(mt_rand(5, 50))->partner()->create();

        $partners = User::whereRole('partner')->pluck('id')->toArray();

        User::factory(mt_rand(50, 300))->customer($partners)->create();
    }
}
