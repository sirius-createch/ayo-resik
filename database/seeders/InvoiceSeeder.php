<?php

namespace Database\Seeders;

use App\Models\Invoice;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::factory(mt_rand(50, 500))->create();
        $invoices = Invoice::all();
        foreach ($invoices as $key => $invoice) {
            $invoice->preventAccessor = true;
            $invoice->update(['code' => 'INV'.date('Ymd').Str::padLeft($key + 1, 3, '0')]);
        }
    }
}
