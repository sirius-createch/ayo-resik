<?php

namespace Database\Seeders;

use App\Helpers\Sirius;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WithdrawalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = User::where('role', 'customer')->where('balance', '>', 0)->get();
        $statuses = ['requested', 'seen', 'denied', 'confirmed', 'sent', 'not_received', 'done', 'canceled'];
        $params = [];

        $key = 1;
        foreach ($customers as $customer) {
            $balance = $customer->balance;
            $total_transaction = mt_rand(0, 5);
            if ($total_transaction > 0) {
                for ($i = 0; $i < $total_transaction; $i++) {
                    if ($balance < 1000) {
                        break;
                    }
                    $amount = mt_rand(1000, $balance);
                    $status = $statuses[array_rand($statuses)];
                    $params[] = [
                        'code' => 'WIT'.date('Ymd').Str::padLeft($key, 3, '0'),
                        'partner_id' => $customer->partner_id,
                        'customer_id' => $customer->id,
                        'amount' => $amount,
                        'current_status' => $status,
                        'note' => mt_rand(0, 1) ? Sirius::randomLoremIpsumSentance() : null,
                    ];
                    $key++;

                    if ($status != 'canceled' && $status != 'denied') {
                        $balance -= $amount;
                    }
                }
                $customer->update(['balance' => $balance]);
            }
        }

        Withdrawal::insert($params);
        DB::table('withdrawals')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
