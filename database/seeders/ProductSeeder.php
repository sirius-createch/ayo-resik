<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            ['name' => 'Besi', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Kardus', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Kertas', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Botol Kaca', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Gelas Plastik', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Botol Plastik', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Kaleng', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Alumunium', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Kayu', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Drum', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Buku', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Majalah', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Koran', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Tembaga', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Kuningan', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Stainless Steel', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Dus Minuman Kemasan', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Puing', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Botol Plastik Daur Ulang', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
            ['name' => 'Alat Elektronik', 'base_price' => mt_rand(1, 100) * 1000 / mt_rand(1, 100)],
        ];

        Product::insert($params);
        DB::table('products')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
