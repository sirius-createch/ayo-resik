<?php

namespace Database\Seeders;

use App\Models\Withdrawal;
use App\Models\WithdrawalStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WithdrawalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $withdrawals = Withdrawal::all();

        $params = [];
        foreach ($withdrawals as $withdrawal) {
            $statuses = match ($withdrawal->current_status) {
                'requested' => ['requested'],
                'seen' => ['requested', 'seen'],
                'denied' => ['requested', 'seen', 'denied'],
                'confirmed' => ['requested', 'seen', 'confirmed'],
                'sent' => ['requested', 'seen', 'confirmed', 'sent'],
                'not_received' => ['requested', 'seen', 'confirmed', 'sent', 'not_received'],
                'done' => ['requested', 'seen', 'confirmed', 'sent', 'done'],
                'canceled' => ['requested', 'canceled'],
            };

            foreach ($statuses as $status) {
                $params[] = [
                    'withdrawal_id' => $withdrawal->id,
                    'name' => $status,
                ];
            }
        }

        WithdrawalStatus::insert($params);
        DB::table('withdrawal_statuses')->update(['created_at' => now(), 'updated_at' => now()]);
    }
}
