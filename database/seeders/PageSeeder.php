<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'slug' => Str::slug('Kebijakan Privasi'),
            'title' => Str::title('Kebijakan Privasi'),
            'content' => fake()->paragraphs(10, true),
        ]);

        Page::create([
            'slug' => Str::slug('Ketentuan Penggunaan'),
            'title' => Str::title('Ketentuan Penggunaan'),
            'content' => fake()->paragraphs(10, true),
        ]);
    }
}
