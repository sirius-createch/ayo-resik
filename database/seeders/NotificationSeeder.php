<?php

namespace Database\Seeders;

use App\Helpers\Sirius;
use App\Models\Invoice;
use App\Models\Notification;
use App\Models\Withdrawal;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $params = [];

        $invoices = Invoice::all();
        foreach ($invoices as $invoice) {
            foreach ($invoice->statuses as $status) {
                if ($status->name != 'seen') {
                    $title = match ($status->name) {
                        'denied' => 'Permintaan pengambilan sampah ditolak!',
                        'confirmed' => 'Permintaan pengambilan sampah telah dikonfirmasi!',
                        'verified' => 'Sampah Anda telah diverifikasi!',
                        'done' => 'Pengambilan sampah telah selesai!',
                        default => '',
                    };

                    $params[] = match ($status->name) {
                        'requested' => [
                            'title' => 'Ada permintaan pengambilan sampah!',
                            'detail' => 'Estimasi Tagihan: '.Sirius::toRupiah($invoice->total_bill),
                            'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                            'link' => route('app.partner.orders.show', ['invoice' => $invoice->code]),
                            'broadcasted_to' => $invoice->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        'canceled' => [
                            'title' => 'Permintaan pengambilan sampah dibatalkan!',
                            'detail' => 'Nomor Transaksi: '.$invoice->code,
                            'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                            'link' => route('app.partner.orders.show', ['invoice' => $invoice->code]),
                            'broadcasted_to' => $invoice->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        default => [
                            'title' => $title,
                            'detail' => 'Nomor Transaksi: '.$invoice->code,
                            'icon' => Invoice::mappingCurrentStatusIcon($invoice->current_status, 'fontawesome'),
                            'link' => route('app.customer.orders.show', ['invoice' => $invoice->code]),
                            'broadcasted_to' => $invoice->customer_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                    };
                }
            }
        }

        $withdrawals = Withdrawal::all();
        foreach ($withdrawals as $withdrawal) {
            foreach ($withdrawal->statuses as $status) {
                if ($status->name != 'seen') {
                    $title = match ($status->name) {
                        'denied' => 'Permintaan penarikan saldo ditolak!',
                        'confirmed' => 'Permintaan penarikan saldo telah dikonfirmasi!',
                        'sent' => 'Uang Anda telah dikirim!',
                        default => '',
                    };

                    $params[] = match ($status->name) {
                        'requested' => [
                            'title' => 'Ada permintaan penarikan saldo!',
                            'detail' => 'Nominal: '.Sirius::toRupiah($withdrawal->amount),
                            'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                            'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                            'broadcasted_to' => $withdrawal->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        'canceled' => [
                            'title' => 'Permintaan penarikan saldo dibatalkan!',
                            'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                            'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                            'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                            'broadcasted_to' => $withdrawal->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        'not_received' => [
                            'title' => 'Customer belum menerima uangnya!',
                            'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                            'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                            'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                            'broadcasted_to' => $withdrawal->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        'done' => [
                            'title' => 'Uang telah diterima oleh customer Anda!',
                            'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                            'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                            'link' => route('app.partner.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                            'broadcasted_to' => $withdrawal->partner_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                        default => [
                            'title' => $title,
                            'detail' => 'Nomor Transaksi: '.$withdrawal->code,
                            'icon' => Withdrawal::mappingCurrentStatusIcon($withdrawal->current_status, 'fontawesome'),
                            'link' => route('app.customer.withdrawals.show', ['withdrawal' => $withdrawal->code]),
                            'broadcasted_to' => $withdrawal->customer_id,
                            'readed_at' => mt_rand(0, 1) ? now() : null,
                        ],
                    };
                }
            }
        }

        Notification::insert($params);
        DB::table('notifications')->update(['broadcasted_at' => now(), 'created_at' => now(), 'updated_at' => now()]);
    }
}
