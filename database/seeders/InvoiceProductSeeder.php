<?php

namespace Database\Seeders;

use App\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $invoices = Invoice::all();

        foreach ($invoices as $invoice) {
            $products = $invoice->partner->products;
            $total_bill = $total_weight = 0;
            $params = [];
            foreach ($products as $product) {
                $price = $weight = 0;
                $isIn = mt_rand(0, 1);
                if ($isIn) {
                    $price = $product->pivot->price;
                    $weight = mt_rand(1, 10) / mt_rand(1, 10);
                    $weight_real = mt_rand(1, 10) / mt_rand(1, 10);
                    $params[$product->id] = [
                        'price' => $price,
                        'weight' => $weight,
                        'weight_real' => in_array($invoice->current_status, ['verified', 'done']) ? $weight_real : null,
                    ];

                    if (in_array($invoice->current_status, ['verified', 'done'])) {
                        $price *= $weight_real;
                    } else {
                        $price *= $weight;
                    }
                }
                $total_bill += $price;
                $total_weight += $weight;
            }
            if (count($params) > 0) {
                $invoice->products()->sync($params);
            }
            $service_charge = config('app.service_rate') * $total_bill;
            $invoice->update(['total_bill' => $total_bill - $service_charge, 'total_weight' => $total_weight,'service_charge' => $service_charge]);
        }
    }
}
