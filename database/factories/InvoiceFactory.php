<?php

namespace Database\Factories;

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Invoice>
 */
class InvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $customer = User::with(['partner', 'partner.schedule'])->whereRole('customer')->inRandomOrder()->first();
        $schedule = [];
        if ($customer->partner->schedule->monday) {
            $schedule[] = 'monday';
        }
        if ($customer->partner->schedule->tuesday) {
            $schedule[] = 'tuesday';
        }
        if ($customer->partner->schedule->wednesday) {
            $schedule[] = 'wednesday';
        }
        if ($customer->partner->schedule->thursday) {
            $schedule[] = 'thursday';
        }
        if ($customer->partner->schedule->friday) {
            $schedule[] = 'friday';
        }
        if ($customer->partner->schedule->saturday) {
            $schedule[] = 'saturday';
        }
        if ($customer->partner->schedule->sunday) {
            $schedule[] = 'sunday';
        }

        $status = $this->faker->randomElement(['requested', 'seen', 'denied', 'confirmed', 'verified', 'done', 'canceled']);
        $payment = $this->faker->randomElement(PaymentMethod::pluck('id'));

        return [
            'code' => now(),
            'partner_id' => $customer->partner_id,
            'customer_id' => $customer->id,
            'total_bill' => 0,
            'total_weight' => 0,
            'day_taken' => $this->faker->randomElement($schedule),
            'address' => $customer->address,
            'latitude' => $customer->latitude,
            'longitude' => $customer->longitude,
            'payment_method_id' => $payment,
            'current_status' => $status,
            'note' => mt_rand(0, 1) ? $this->faker->sentence : null,
            'service_charge' => 0
        ];
    }
}
