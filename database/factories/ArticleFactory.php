<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->unique->sentence(4);

        return [
            'slug' => Str::slug($title),
            'title' => Str::title($title),
            'content' => $this->faker->paragraphs(10, true),
            'author_id' => 1,
            'published_at' => mt_rand(0, 1) ? now() : null,
        ];
    }
}
