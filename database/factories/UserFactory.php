<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'phone' => Str::replace(' ', '', Str::replaceFirst('0', '', Str::replace('(+62)', '0', $this->faker->unique()->phoneNumber))),
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role' => $this->faker->randomElement(['partner', 'customer']),
            'balance' => $this->faker->numberBetween(0, 100000),
            'address' => $this->faker->address,
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'remember_token' => Str::random(10),
            'verified_at' => now(),
            'partner_confirm_at' => null,
        ];
    }

    /**
     * Indicate that the user is partner.
     *
     * @return static
     */
    public function partner()
    {
        return $this->state(function (array $attributes) {
            return [
                'role' => 'partner',
            ];
        });
    }

    /**
     * Indicate that the user is customer.
     *
     * @param  array  $partnerIds list of partner's ids
     * @return static
     */
    public function customer(array $partnerIds)
    {
        return $this->state(function (array $attributes) use ($partnerIds) {
            return [
                'role' => 'customer',
                'partner_id' => $this->faker->randomElement($partnerIds),
                'partner_confirm_at' => now(),
            ];
        });
    }
}
